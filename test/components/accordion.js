import rn from 'react-native';
import React from 'react';
import { shallow } from 'enzyme';
import Accordion from 'components/Accordion';

test('it renders closed', () => {
  const title = React.createElement(rn.Text, 'Testing');
  const expandedStyle = { backgroundColor: 'red' };
  const collapsedStyle = { backgroundColor: 'blue' };
  const content = React.createElement(rn.Text, 'Content');
  const tree = shallow(<Accordion
    titleContent={title}
    expandedStyle={expandedStyle}
    collapsedStyle={collapsedStyle}
    content={content}
  />);
  expect(tree).toMatchSnapshot();
});

test('it renders expanded', () => {
  const title = React.createElement(rn.Text, 'Testing');
  const expandedStyle = { backgroundColor: 'red' };
  const collapsedStyle = { backgroundColor: 'blue' };
  const content = React.createElement(rn.Text, 'Content');
  const tree = shallow(<Accordion
    titleContent={title}
    expandedStyle={expandedStyle}
    collapsedStyle={collapsedStyle}
    content={content}
  />);
  tree.find('TouchableOpacity').simulate('press');
  expect(tree).toMatchSnapshot();
});

test('it calls props.iconClicked on press', () => {
  const spy = jest.fn();
  const tree = shallow(<Accordion iconClicked={spy} />);
  tree.find('TouchableOpacity').simulate('press');
  expect(spy).toHaveBeenCalled();
});
