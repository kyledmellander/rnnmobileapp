import thunk from 'redux-thunk';
import configureStore from 'redux-mock-store';

const mockStore = configureStore([thunk]);
export default mockStore();

export { mockStore as makeConfiguredStore };
