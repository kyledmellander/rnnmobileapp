import analytics, * as analyticFns from 'lib/analytics';
import { AsyncStorage } from 'react-native';
import uuid from 'react-native-uuid';
import analyticsClient from 'src/config/analytics';
import { anonymousIdKey, userKey } from 'src/config/storageConsts';
import { reportError } from 'src/config/exceptions';

jest.mock('react-native', () => ({
  AsyncStorage: {
    getItem: jest.fn(() => Promise.resolve()),
    setItem: jest.fn(() => Promise.resolve())
  }
}));
jest.mock('react-native-uuid', () => ({
  v4: jest.fn(),
}));

describe('analytics.js', async () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });
  describe('getAnonymousId', async () => {
    it('should create a uuid if one doesn\'t exist', async () => {
      const mockUuid = 'foo';
      uuid.v4.mockReturnValueOnce(mockUuid);
      const anonymousId = await analyticFns.getAnonymousId();
      expect(AsyncStorage.getItem).toHaveBeenCalledWith(anonymousIdKey);
      expect(AsyncStorage.setItem).toHaveBeenCalledWith(anonymousIdKey, mockUuid);
      expect(anonymousId).toEqual(mockUuid);
    });
    it('should create a uuid if one does exist', async () => {
      const mockUuid = 'foo';
      AsyncStorage.getItem.mockReturnValueOnce(mockUuid);
      const anonymousId = await analyticFns.getAnonymousId();
      expect(AsyncStorage.setItem).toHaveBeenCalledTimes(0);
      expect(anonymousId).toEqual(mockUuid);
    });
  });
  describe('getUserId', () => {
    it('should return falsey if one doesn\'t exist', async () => {
      const userId = await analyticFns.getUserId();
      expect(AsyncStorage.getItem).toHaveBeenCalledWith(userKey);
      expect(userId).not.toBeTruthy();
    });
    it('should return the id if one does exist', async () => {
      const mockUser = '{"id": 1}';
      AsyncStorage.getItem.mockReturnValueOnce(mockUser);
      const userId = await analyticFns.getUserId();
      expect(AsyncStorage.getItem).toHaveBeenCalledWith(userKey);
      expect(userId).toBe(1);
    });
  });
  describe('addUserIdsToEvent', () => {
    it('should add in userId and anonId', async () => {
      const anonymousId = 'anonId';
      const userId = 'userId';
      const getAnonIdSpy = jest
        .spyOn(analyticFns, 'getAnonymousId')
        .mockImplementation(() => anonymousId);
      const getUserIdSpy = jest.spyOn(analyticFns, 'getUserId').mockImplementation(() => userId);
      const mockEvent = { foo: 'bar' };
      const eventWithIds = await analyticFns.addUserIdsToEvent(mockEvent);
      expect(eventWithIds).toEqual({ ...mockEvent, anonymousId, userId });
      getAnonIdSpy.mockRestore();
      getUserIdSpy.mockRestore();
    });
  });
  describe('analyticsMethod', async () => {
    it('should call the analytic client with event after adding in addUserIdsToEvent', async () => {
      const method = 'track';
      const event = { event: 'Some Event' };
      const mockAddUserIdsToEvent = { userId: 1, anonymousId: 'a' };
      const addUserIdsToEventSpy = jest
        .spyOn(analyticFns, 'addUserIdsToEvent')
        .mockImplementation(() => mockAddUserIdsToEvent);
      await analyticFns.analyticsMethod(method, event);
      expect(addUserIdsToEventSpy).toHaveBeenCalledWith(event);
      expect(analyticsClient[method]).toHaveBeenCalledWith(mockAddUserIdsToEvent);
      addUserIdsToEventSpy.mockRestore();
    });
    it('should call the analytic client with event after adding in addUserIdsToEvent', async () => {
      const method = 'track';
      const event = { event: 'Some Event' };
      const errorText = 'Some Error Text';
      const addUserIdsToEventSpy = jest
        .spyOn(analyticFns, 'addUserIdsToEvent')
        .mockImplementation(() => { throw errorText; });
      await analyticFns.analyticsMethod(method, event);
      expect(addUserIdsToEventSpy).toHaveBeenCalledWith(event);
      expect(analyticsClient[method]).toHaveBeenCalledTimes(0);
      expect(reportError).toHaveBeenCalled();
      addUserIdsToEventSpy.mockRestore();
    });
  });
  describe('main export', async () => {
    it('should let you call identify, track, and page', async () => {
      const methods = ['identify', 'track', 'page'];
      const analyticsMethodSpy = jest
        .spyOn(analyticFns, 'analyticsMethod')
        .mockImplementation(() => {});
      methods.forEach((method) => {
        const mockEvent = { event: 'Some Event' };
        analytics[method](mockEvent);
      });
      expect(analyticsMethodSpy).toHaveBeenCalledTimes(methods.length);
      analyticsMethodSpy.mockRestore();
    });
  });
});
