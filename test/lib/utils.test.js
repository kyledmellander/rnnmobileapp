import utils from 'lib/utils';

describe('utils.js', () => {
  describe('formatMoney', () => {
    it('should format money correctly', () => {
      expect(utils.formatMoney('1.2')).toBe('1.20');
      expect(utils.formatMoney('1')).toBe('1.00');
      expect(utils.formatMoney('1000')).toBe('1,000.00');
      expect(utils.formatMoney('1000.2')).toBe('1,000.20');
      expect(utils.formatMoney('1000.200')).toBe('1,000.20');
      expect(utils.formatMoney('1000000.200')).toBe('1,000,000.20');
    });
  });
});
