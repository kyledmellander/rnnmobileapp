import React from 'react';
import { shallow } from 'enzyme';
import Contact from 'screens/contact';
import { makeConfiguredStore } from '../mocks/store';

jest.mock('lib/utils', () => ({
  getBasketWord: () => 'crate',
}));

test('it renders', () => {
  const store = makeConfiguredStore({ contactUs: {} });
  const tree = shallow(<Contact store={store} />);
  expect(tree).toMatchSnapshot();
});
