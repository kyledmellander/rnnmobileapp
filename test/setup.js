/* eslint-disable import/no-extraneous-dependencies */
import { Response, Headers, Request } from 'whatwg-fetch';
import fetchMock from 'jest-fetch-mock';

global.Response = Response;
global.Headers = Headers;
global.Request = Request;
global.fetch = fetchMock;


jest.mock('src/config/feathers', () => {
  const stubs = {
    create: jest.fn(() => Promise.resolve()),
    update: jest.fn(() => Promise.resolve()),
    find: jest.fn(() => Promise.resolve()),
    remove: jest.fn(() => Promise.resolve()),
    get: jest.fn(() => Promise.resolve())
  };
  return {
    service: jest.fn(() => stubs),
    stubs,
    clearSession: jest.fn(),
    getSessionToken: jest.fn(),
    authenticate: jest.fn(),
    refreshTokens: jest.fn(() => Promise.resolve())
  };
});

jest.mock('src/config/settings', () => ({
  stripeKey: '',
}));

jest.mock('PushNotificationIOS', () => ({
  super: jest.fn(),
  addEventListener: jest.fn(),
  removeEventListener: jest.fn(),
  openURL: jest.fn(),
  canOpenURL: jest.fn(),
  getInitialURL: jest.fn(),
  getInitialNotification: jest.fn(() => Promise.resolve()),
}));

jest.mock('react-native-adjust');

jest.mock('NativeEventEmitter');

jest.mock('src/config/exceptions', () => ({
  reportError: jest.fn(),
}));

jest.mock('src/config/analytics', () => ({
  track: jest.fn(),
  identify: jest.fn(),
  page: jest.fn(),
}));
