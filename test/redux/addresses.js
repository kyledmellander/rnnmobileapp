import reducer, { actions, types, initialState } from 'appRedux/addresses';
import mockFeathers from 'src/config/feathers';
import store from '../mocks/store';

const { stubs, service: serviceStub } = mockFeathers;

describe('addresses.js', () => {
  beforeEach(() => {
    store.clearActions();
  });

  afterAll(() => {
    serviceStub.mockReset();
  });

  afterEach(() => {
    serviceStub.mockClear();
    Object.values(stubs).map(o => o.mockReset());
  });

  it('getAddresses calls service and dispatches result', async () => {
    const results = [{ id: 1 }, { id: 2 }];
    const stub = stubs.find;
    stub.mockResolvedValueOnce(results);
    const action = actions.getAddresses();

    await action(store.dispatch);

    expect(serviceStub).toHaveBeenCalledWith('addresses');
    expect(stub).toHaveBeenCalled();
    expect(store.getActions()).toMatchSnapshot();
  });

  it('getAddresses dispatches error', async () => {
    const findStub = stubs.find;
    findStub.mockRejectedValue({});
    const action = actions.getAddresses();

    await action(store.dispatch);

    expect(serviceStub).toHaveBeenCalledWith('addresses');
    expect(store.getActions()).toMatchSnapshot();
  });

  it('addAddress calls service and dispatches results', async () => {
    const address = { id: 3 };
    const results = [{ id: 1 }, { id: 2 }, { id: 3 }];
    const findStub = stubs.find;
    const createStub = stubs.create;
    createStub.mockResolvedValueOnce({});
    findStub.mockResolvedValueOnce(results);
    const action = actions.addAddress(address);

    await action(store.dispatch);

    expect(serviceStub).toHaveBeenCalledWith('addresses');
    expect(findStub).toHaveBeenCalled();
    expect(createStub).toHaveBeenCalledWith(address);
    expect(store.getActions()).toMatchSnapshot();
  });

  it('addAddress dispatches error', async () => {
    const address = { id: 3 };
    const createStub = stubs.create;
    createStub.mockRejectedValue({});
    const action = actions.addAddress(address);

    await action(store.dispatch);

    expect(serviceStub).toHaveBeenCalledWith('addresses');
    expect(createStub).toHaveBeenCalledWith(address);
    expect(store.getActions()).toMatchSnapshot();
  });

  it('updateAddress calls service and dispatches results', async () => {
    const address = { id: 3, state: 'WA' };
    const results = [{ id: 1 }, { id: 2 }, { id: 3, state: 'WA' }];
    const findStub = stubs.find;
    const updateStub = stubs.update;
    updateStub.mockResolvedValueOnce({});
    findStub.mockResolvedValueOnce(results);
    const action = actions.updateAddress(address);

    await action(store.dispatch);

    expect(serviceStub).toHaveBeenCalledWith('addresses');
    expect(findStub).toHaveBeenCalled();
    expect(updateStub).toHaveBeenCalledWith(address.id, address);
    expect(store.getActions()).toMatchSnapshot();
  });

  it('updateAddress dispatches error', async () => {
    const address = { id: 3, state: 'WA' };
    const updateStub = stubs.update;
    updateStub.mockRejectedValueOnce({});
    const action = actions.updateAddress(address);

    await action(store.dispatch);

    expect(serviceStub).toHaveBeenCalledWith('addresses');
    expect(store.getActions()).toMatchSnapshot();
  });

  it('removeAddress calls service and dispatches results', async () => {
    const address = { id: 3 };
    const results = [{ id: 1 }, { id: 2 }];
    const findStub = stubs.find;
    const removeStub = stubs.remove;
    removeStub.mockResolvedValueOnce({});
    findStub.mockResolvedValueOnce(results);
    const action = actions.removeAddress(address);

    await action(store.dispatch);

    expect(serviceStub).toHaveBeenCalledWith('addresses');
    expect(findStub).toHaveBeenCalled();
    expect(removeStub).toHaveBeenCalledWith(address.id);
    expect(store.getActions()).toMatchSnapshot();
  });

  it('removeAddress dispatches error', async () => {
    const address = { id: 3, state: 'WA' };
    const removeStub = stubs.remove;
    removeStub.mockRejectedValueOnce({});
    const action = actions.removeAddress(address);

    await action(store.dispatch);

    expect(serviceStub).toHaveBeenCalledWith('addresses');
    expect(store.getActions()).toMatchSnapshot();
  });

  it('setDefaultAddress calls service and dispatches results', async () => {
    const address = { id: 3, state: 'WA', default: true };
    const results = [{ id: 1 }, { id: 2 }, { id: 3, state: 'WA', default: true }];
    const findStub = stubs.find;
    const updateStub = stubs.update;
    updateStub.mockResolvedValueOnce({});
    findStub.mockResolvedValueOnce(results);
    const action = actions.setDefaultAddress(address);

    await action(store.dispatch);

    expect(serviceStub).toHaveBeenCalledWith('addresses');
    expect(findStub).toHaveBeenCalled();
    expect(updateStub).toHaveBeenCalledWith(address.id, address);
    expect(store.getActions()).toMatchSnapshot();
  });

  it('setDefaultAddress dispatches error', async () => {
    const address = { id: 3, state: 'WA' };
    const updateStub = stubs.update;
    updateStub.mockRejectedValueOnce({});
    const action = actions.setDefaultAddress(address);

    await action(store.dispatch);

    expect(serviceStub).toHaveBeenCalledWith('addresses');
    expect(store.getActions()).toMatchSnapshot();
  });

  it('clearAddressStatus dispatches', async () => {
    const action = actions.clearAddressStatus();

    await action(store.dispatch);

    expect(store.getActions()).toMatchSnapshot();
  });

  it('reducer updates state on getAddresses', () => {
    const action = {
      type: types.getAddresses,
      addresses: [{ id: 1 }, { id: 2 }],
    };
    expect(reducer(initialState, action)).toMatchSnapshot();
  });

  it('reducer updates state on addAddress', () => {
    const action = {
      type: types.addAddress,
      addresses: [{ id: 1 }, { id: 2 }, { id: 3 }],
    };
    expect(reducer(initialState, action)).toMatchSnapshot();
  });

  it('reducer updates state on updateAddress', () => {
    const action = {
      type: types.updateAddress,
      addresses: [{ id: 1 }, { id: 2 }, { id: 3 }],
    };
    expect(reducer(initialState, action)).toMatchSnapshot();
  });

  it('reducer updates state on setDefaultAddress', () => {
    const action = {
      type: types.setDefaultAddress,
      addresses: [{ id: 1 }, { id: 2 }, { id: 3 }],
    };
    expect(reducer(initialState, action)).toMatchSnapshot();
  });

  it('reducer updates state on removeAddress', () => {
    const action = {
      type: types.removeAddress,
      addresses: [{ id: 1 }, { id: 2 }],
    };
    expect(reducer(initialState, action)).toMatchSnapshot();
  });

  it('reducer updates state on clearAddressStatus', () => {
    const action = {
      type: types.clearAddressStatus,
      addresses: [{ id: 1 }, { id: 2 }],
    };
    const state = {
      ...initialState,
      addedAddress: true,
      updateAddress: true,
      udpatedDefault: true,
      removedAddress: true,
    };
    expect(reducer(state, action)).toMatchSnapshot();
  });
});
