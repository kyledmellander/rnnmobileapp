import reducer, { actions, types, initialState, keys } from 'appRedux/products';
import mockFeathers from 'src/config/feathers';
import store from '../mocks/store';

const { stubs, service: serviceStub } = mockFeathers;

describe('products.js', () => {
  beforeEach(() => {
    store.clearActions();
  });

  afterAll(() => {
    serviceStub.mockReset();
  });

  afterEach(() => {
    serviceStub.mockClear();
    Object.values(stubs).map(o => o.mockReset());
  });

  test('searchProducts calls service and dispatches', async () => {
    const page = 2;
    const term = 'Test';
    const key = 'testkey';
    const findStub = stubs.find;
    const expectedResults = [{ id: 1 }, { id: 2 }, { id: 3 }];
    findStub.mockResolvedValueOnce(expectedResults);
    const searchProductsAction = actions.searchProducts(page, term, key);
    const expectedCallArgs = {
      page: {
        size: 10,
        number: page,
      },
      filter: {
        testkey: term,
      },
    };

    await searchProductsAction(store.dispatch);

    const callArgs = findStub.mock.calls[0][0];
    expect(callArgs).toEqual({ query: expectedCallArgs });
    expect(serviceStub).toHaveBeenCalledWith('products');
    expect(store.getActions()).toMatchSnapshot();
  });

  test('getProducts calls service and dispatches', async () => {
    const page = 2;
    const findStub = stubs.find;
    const expectedResults = [{ id: 1 }, { id: 2 }, { id: 3 }];
    findStub.mockResolvedValueOnce(expectedResults);
    const getProductsAction = actions.getProducts(page);
    const expectedCallArgs = {
      page: {
        size: 10,
        number: page,
      },
    };

    await getProductsAction(store.dispatch);

    const callArgs = findStub.mock.calls[0][0];
    expect(callArgs).toEqual({ query: expectedCallArgs });
    expect(serviceStub).toHaveBeenCalledWith('products');
    expect(store.getActions()).toMatchSnapshot();
  });

  test('getUnratedProducts calls service and dispatches', async () => {
    const page = 2;
    const findStub = stubs.find;
    const expectedResults = [{ id: 1 }, { id: 2 }, { id: 3 }];
    findStub.mockResolvedValueOnce(expectedResults);
    const getUnratedProductsAction = actions.getUnratedProducts(page);
    const expectedCallArgs = {
      page: {
        size: 10,
        number: page,
      },
      filter: {
        rated: false,
      },
    };

    await getUnratedProductsAction(store.dispatch);

    const callArgs = findStub.mock.calls[0][0];
    expect(callArgs).toEqual({ query: expectedCallArgs });
    expect(serviceStub).toHaveBeenCalledWith('products');
    expect(store.getActions()).toMatchSnapshot();
  });

  test('updateUnratedProducts dispatches', () => {
    const product = { id: 1 };

    actions.updateUnratedProducts(product)(store.dispatch);

    expect(store.getActions()).toMatchSnapshot();
  });

  test('reducer populates nameMatches on action', () => {
    const action = {
      type: types.searchProducts,
      key: keys.name,
      page: 0,
      matches: [
        { id: 1, meta: { totalResults: 12 } },
        { id: 2 },
        { id: 3 },
        { id: 4 },
        { id: 5 },
        { id: 6 },
        { id: 7 },
        { id: 8 },
        { id: 9 },
        { id: 10 },
      ],
    };
    expect(reducer(initialState, action)).toMatchSnapshot();
  });

  test('reducer concats nameMatches on action', () => {
    const action = {
      type: types.searchProducts,
      key: keys.name,
      page: 0,
      matches: [
        { id: 1, meta: { totalResults: 12 } },
        { id: 2 },
        { id: 3 },
        { id: 4 },
        { id: 5 },
        { id: 6 },
        { id: 7 },
        { id: 8 },
        { id: 9 },
        { id: 10 },
      ],
    };
    const state = {
      ...initialState,
      nameMatches: {
        beers: [{ id: 0 }, { id: 12 }],
      },
    };
    expect(reducer(state, action)).toMatchSnapshot();
  });

  test('reducer populates breweryMatches on action', () => {
    const action = {
      type: types.searchProducts,
      key: keys.brewery,
      page: 0,
      matches: [
        { id: 1, meta: { totalResults: 12 } },
        { id: 2 },
        { id: 3 },
        { id: 4 },
        { id: 5 },
        { id: 6 },
        { id: 7 },
        { id: 8 },
        { id: 9 },
        { id: 10 },
      ],
    };
    expect(reducer(initialState, action)).toMatchSnapshot();
  });

  test('reducer concats breweryMatches on action', () => {
    const action = {
      type: types.searchProducts,
      key: keys.brewery,
      page: 0,
      matches: [
        { id: 1, meta: { totalResults: 12 } },
        { id: 2 },
        { id: 3 },
        { id: 4 },
        { id: 5 },
        { id: 6 },
        { id: 7 },
        { id: 8 },
        { id: 9 },
        { id: 10 },
      ],
    };
    const state = {
      ...initialState,
      breweryMatches: {
        beers: [{ id: 0 }, { id: 12 }],
      },
    };
    expect(reducer(state, action)).toMatchSnapshot();
  });

  test('reducer populates styleMatches on action', () => {
    const action = {
      type: types.searchProducts,
      key: keys.style,
      page: 0,
      matches: [
        { id: 1, meta: { totalResults: 12 } },
        { id: 2 },
        { id: 3 },
        { id: 4 },
        { id: 5 },
        { id: 6 },
        { id: 7 },
        { id: 8 },
        { id: 9 },
        { id: 10 },
      ],
    };
    expect(reducer(initialState, action)).toMatchSnapshot();
  });

  test('reducer concats styleMatches on action', () => {
    const action = {
      type: types.searchProducts,
      key: keys.style,
      page: 0,
      matches: [
        { id: 1, meta: { totalResults: 12 } },
        { id: 2 },
        { id: 3 },
        { id: 4 },
        { id: 5 },
        { id: 6 },
        { id: 7 },
        { id: 8 },
        { id: 9 },
        { id: 10 },
      ],
    };
    const state = {
      ...initialState,
      styleMatches: {
        beers: [{ id: 0 }, { id: 12 }],
      },
    };
    expect(reducer(state, action)).toMatchSnapshot();
  });

  test('reducer populates products on action', () => {
    const action = {
      type: types.getProducts,
      page: 0,
      products: [
        { id: 1, meta: { totalProducts: 12, totalRatings: 3 } },
        { id: 2 },
        { id: 3 },
        { id: 4 },
        { id: 5 },
        { id: 6 },
        { id: 7 },
        { id: 8 },
        { id: 9 },
        { id: 10 },
      ],
    };
    expect(reducer(initialState, action)).toMatchSnapshot();
  });

  test('reducer concats products on action', () => {
    const action = {
      type: types.getProducts,
      page: 0,
      products: [
        { id: 1, meta: { totalProducts: 12, totalRatings: 3 } },
        { id: 2 },
        { id: 3 },
        { id: 4 },
        { id: 5 },
        { id: 6 },
        { id: 7 },
        { id: 8 },
        { id: 9 },
        { id: 10 },
      ],
    };
    const state = {
      ...initialState,
      products: {
        beers: [{ id: 1 }, { id: 12 }],
      },
    };
    expect(reducer(state, action)).toMatchSnapshot();
  });

  test('reducer populates unratedProducts on action', () => {
    const action = {
      type: types.getUnratedProducts,
      page: 0,
      products: [
        { id: 1, meta: { totalProducts: 12, totalRatings: 3 } },
        { id: 2 },
        { id: 3 },
        { id: 4 },
        { id: 5 },
        { id: 6 },
        { id: 7 },
        { id: 8 },
        { id: 9 },
        { id: 10 },
      ],
    };
    expect(reducer(initialState, action)).toMatchSnapshot();
  });

  test('reducer concats unratedProducts on action', () => {
    const action = {
      type: types.getUnratedProducts,
      page: 0,
      products: [
        { id: 1, meta: { totalProducts: 12, totalRatings: 3 } },
        { id: 2 },
        { id: 3 },
        { id: 4 },
        { id: 5 },
        { id: 6 },
        { id: 7 },
        { id: 8 },
        { id: 9 },
        { id: 10 },
      ],
    };
    const state = {
      ...initialState,
      unratedProducts: {
        beers: [{ id: 1 }, { id: 12 }],
      },
    };
    expect(reducer(state, action)).toMatchSnapshot();
  });

  test('reducer updates unratedProducts on action', () => {
    const action = {
      type: types.updateUnratedProducts,
      product: {
        id: 1,
        meta: { totalProducts: 12, totalRatings: 3 },
        rating: { score: 3 },
      },
    };
    const state = {
      ...initialState,
      unratedProducts: {
        beers: [{ id: 1 }, { id: 12 }],
        total: 2,
      },
    };
    expect(reducer(state, action)).toMatchSnapshot();
  });
});
