import { actions } from 'appRedux/ratings';
import mockFeathers from 'src/config/feathers';
import store from '../mocks/store';

const { stubs, service: serviceStub } = mockFeathers;

describe('ratings.js', () => {
  beforeEach(() => {
    store.clearActions();
  });

  afterAll(() => {
    serviceStub.mockReset();
  });

  afterEach(() => {
    serviceStub.mockClear();
    Object.values(stubs).map(o => o.mockReset());
  });

  it('rateBeer creates rating if beer has none', async () => {
    const beer = {
      id: 1,
      rating: {},
    };
    const score = 5;
    const expectedCallArgs = {
      productId: beer.id,
      score,
    };
    const rateBeerAction = actions.rateBeer(beer, score);

    await rateBeerAction(store.dispatch);

    const callArgs = stubs.create.mock.calls[0][0];
    expect(serviceStub).toHaveBeenCalledWith('ratings');
    expect(callArgs).toEqual(expectedCallArgs);
    expect(store.getActions()).toMatchSnapshot();
  });

  it('rateBeer updates rating if beer has one', async () => {
    const beer = {
      id: 1,
      rating: {
        id: 2,
        score: 3,
      },
    };
    const newRating = {
      id: 2,
      score: 5,
    };
    const expectedCallArgs = [beer.rating.id, { score: newRating }];
    const rateBeerAction = actions.rateBeer(beer, newRating);

    await rateBeerAction(store.dispatch);

    const callArgs = stubs.update.mock.calls[0];
    expect(serviceStub).toHaveBeenCalledWith('ratings');
    expect(callArgs).toEqual(expectedCallArgs);
    expect(store.getActions()).toMatchSnapshot();
  });
});
