import reducer, { initialState } from 'appRedux/appStoreRating/appStoreRatingReducer';
import types from 'appRedux/appStoreRating/types';

const getPrevState = override => ({ ...initialState, ...override });

describe('addressesForm.js', () => {
  describe('reducer', () => {
    it('set loading indicator when fetching app store rating', () => {
      const action = {
        type: types.fetchingAppStoreRating
      };
      expect(reducer(initialState, action)).toMatchSnapshot();
    });
    it('clear loading indicator and show rating on success', () => {
      const action = {
        type: types.fetchingAppStoreRatingSuccess,
        payload: { appStoreRating: { showRatingModal: true } },
      };
      const prevState = getPrevState({ fetchingAppStoreRating: true });
      expect(reducer(prevState, action)).toMatchSnapshot();
    });
    it('clear loading indicator but dont show rating modal on success', () => {
      const action = {
        type: types.fetchingAppStoreRatingSuccess,
        payload: { appStoreRating: { showRatingModal: false } },
      };
      const prevState = getPrevState({ fetchingAppStoreRating: true });
      expect(reducer(prevState, action)).toMatchSnapshot();
    });
    it('clear loading indicator but dont show rating modal on success', () => {
      const action = {
        type: types.fetchingAppStoreRatingFailure,
        payload: { appStoreRating: { showRatingModal: true } },
      };
      const prevState = getPrevState({ fetchingAppStoreRating: false });
      expect(reducer(prevState, action)).toMatchSnapshot();
    });
    it('set loading indicator when patching app store rating', () => {
      const action = {
        type: types.patchingAppStoreRating
      };
      expect(reducer(initialState, action)).toMatchSnapshot();
    });
    it('clear loading indicator on patching success', () => {
      const action = {
        type: types.patchingAppStoreRatingSuccess,
        payload: { appStoreRating: { showRatingModal: false } },
      };
      const prevState = getPrevState({ patchingAppStoreRating: true });
      expect(reducer(prevState, action)).toMatchSnapshot();
    });
    it('clear loading indicator on patching failure', () => {
      const action = {
        type: types.patchingAppStoreRatingFailure,
        payload: { appStoreRating: { showRatingModal: false } },
      };
      const prevState = getPrevState({ patchingAppStoreRating: true });
      expect(reducer(prevState, action)).toMatchSnapshot();
    });
    it('use initial state if undefined', () => {
      const action = {
        type: 'gibberish type',
        payload: { appStoreRating: { showRatingModal: false } },
      };
      expect(reducer(undefined, action)).toMatchSnapshot();
    });
    it('return previous state on undefined type', () => {
      const action = {
        type: 'gibberish type',
        payload: { appStoreRating: { showRatingModal: false } },
      };
      const prevState = getPrevState({ patchingAppStoreRating: true });
      expect(reducer(prevState, action)).toMatchSnapshot();
    });
  });
});
