import mockFeathers from 'src/config/feathers';
// eslint-disable-next-line import/order
import store from '../mocks/store';

jest.mock('src/config/pushNotifications', () => ({
  notificationsEnabled: jest.fn(() => Promise.resolve()),
  setNotificationsEnabled: jest.fn(() => Promise.resolve()),
}));
// eslint-disable-next-line import/first
import pushNotifications from 'src/config/pushNotifications';
// eslint-disable-next-line import/first
import reducer, { actions, types } from 'appRedux/notifications';


const enabledStub = pushNotifications.notificationsEnabled;
const setSpy = pushNotifications.setNotificationsEnabled;
const { stubs, service: serviceStub } = mockFeathers;

describe('notifications.js', () => {
  beforeEach(() => {
    store.clearActions();
  });

  afterAll(() => {
    serviceStub.mockReset();
  });

  afterEach(() => {
    serviceStub.mockClear();
    Object.values(stubs).map(o => o.mockReset());
  });

  it('getNotificationSettings calls service and dispatches result', async () => {
    const findStub = stubs.find;
    findStub.mockResolvedValueOnce(true).mockResolvedValueOnce({ appNotifications: true });
    enabledStub.mockReturnValueOnce(true);
    const getNotificationSettingsAction = actions.getNotificationSettings();

    await getNotificationSettingsAction(store.dispatch);

    expect(enabledStub).toHaveBeenCalled();
    expect(serviceStub).toHaveBeenCalledWith('subscriptions');
    expect(findStub).toHaveBeenCalled();
    expect(store.getActions()).toMatchSnapshot();
  });

  it('toggleNotifications sets notificationsEnabled to true if currently false', async () => {
    enabledStub.mockResolvedValueOnce(false);
    const toggleNotificationsAction = actions.toggleNotifications();

    await toggleNotificationsAction(store.dispatch);

    expect(setSpy).toHaveBeenCalledWith(true);
    expect(store.getActions()).toMatchSnapshot();
  });

  it('toggleNotifications sets notificationsEnabled to false if currently true', async () => {
    enabledStub.mockResolvedValueOnce(true);
    const toggleNotificationsAction = actions.toggleNotifications();

    await toggleNotificationsAction(store.dispatch);

    expect(setSpy).toHaveBeenCalledWith(false);
    expect(store.getActions()).toMatchSnapshot();
  });

  it('toggleNotifications dispatches error', async () => {
    enabledStub.mockRejectedValue(new Error('testing'));
    const toggleNotificationsAction = actions.toggleNotifications();

    await toggleNotificationsAction(store.dispatch);

    expect(store.getActions()).toMatchSnapshot();
  });

  it('reducer updates state based on action', () => {
    const action = {
      type: types.toggleNotifications,
      notificationsEnabled: false,
    };

    expect(reducer({}, action)).toMatchSnapshot();
  });
});
