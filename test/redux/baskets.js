import reducer, { actions, types, initialState } from 'appRedux/baskets';
import mockFeathers from 'src/config/feathers';
import store from '../mocks/store';

const { stubs, service: serviceStub } = mockFeathers;

describe('baskets.js', () => {
  beforeEach(() => {
    store.clearActions();
  });

  afterAll(() => {
    serviceStub.mockReset();
  });

  afterEach(() => {
    serviceStub.mockClear();
    Object.values(stubs).map(o => o.mockReset());
  });

  test('getOpenBasket calls service and dispatches result on open basket found', async () => {
    const results = [{ id: 1 }, { id: 2 }, { id: 3 }];
    const findStub = stubs.find;
    const getStub = stubs.get;
    const basket = { id: 4 };
    getStub.mockResolvedValueOnce(basket);
    findStub.mockResolvedValueOnce(results);
    const action = actions.getOpenBasket();

    await action(store.dispatch);

    expect(serviceStub).toHaveBeenCalledWith('baskets');
    expect(serviceStub).toHaveBeenLastCalledWith('shippingCycles');
    expect(findStub).toHaveBeenCalledWith({ query: { open: true } });
    expect(getStub).toHaveBeenCalledWith(results[0].id);
    expect(findStub).toHaveBeenCalledWith({ query: { basketId: basket.id } });
    expect(store.getActions()).toMatchSnapshot();
  });

  test('getOpenBasket calls service and dispatches result on no open basket found', async () => {
    const results = [];
    const findStub = stubs.find;
    findStub.mockResolvedValueOnce(results).mockResolvedValueOnce([{ id: 1 }]);
    const action = actions.getOpenBasket();

    await action(store.dispatch);

    expect(serviceStub).toHaveBeenCalledWith('baskets');
    expect(findStub).toHaveBeenCalledWith({ query: { open: true } });
    expect(store.getActions()).toMatchSnapshot();
  });

  test('getOpenBasket dispatches error', async () => {
    const findStub = stubs.find;
    findStub.mockRejectedValueOnce();
    const action = actions.getOpenBasket();

    try {
      await action(store.dispatch);
    } catch (e) {
      expect(e).toBeTruthy();
    }

    expect(serviceStub).toHaveBeenCalledWith('baskets');
    expect(store.getActions()).toMatchSnapshot();
  });

  test('updateBasket calls service and dispatches result on open basket found', async () => {
    const basket = { id: 1 };
    const markedFor = 'testing';
    const shippingCycle = { id: 2 };
    const updateStub = stubs.update;
    updateStub.mockResolvedValueOnce({ id: 1, markedFor: 'testing', shippingCycle });
    const action = actions.updateBasket(basket, markedFor, shippingCycle);

    await action(store.dispatch);

    expect(serviceStub).toHaveBeenCalledWith('baskets');
    expect(updateStub).toHaveBeenCalledWith(basket.id, { shippingCycle, markedFor });
    expect(store.getActions()).toMatchSnapshot();
  });

  test('updateBasket dispatches error', async () => {
    const basket = { id: 1 };
    const markedFor = 'testing';
    const shippingCycle = { id: 2 };
    const updateStub = stubs.update;
    updateStub.mockRejectedValueOnce();
    const action = actions.updateBasket(basket, markedFor, shippingCycle);

    try {
      await action(store.dispatch);
    } catch (e) {
      expect(e).toBeTruthy();
    }

    expect(serviceStub).toHaveBeenCalledWith('baskets');
    expect(store.getActions()).toMatchSnapshot();
  });

  test('getClosedBaskets calls service and dispatches result', async () => {
    const results = [{ id: 1 }, { id: 2 }, { id: 3 }];
    const findStub = stubs.find;
    findStub.mockResolvedValueOnce(results);
    const action = actions.getClosedBaskets();

    await action(store.dispatch);

    expect(serviceStub).toHaveBeenCalledWith('baskets');
    expect(findStub).toHaveBeenCalledWith({ query: { open: false } });
    expect(store.getActions()).toMatchSnapshot();
  });

  test('getClosedBaskets dispatches error', async () => {
    const findStub = stubs.find;
    findStub.mockRejectedValueOnce();
    const action = actions.getClosedBaskets();

    try {
      await action(store.dispatch);
    } catch (e) {
      expect(e).toBeTruthy();
    }

    expect(serviceStub).toHaveBeenCalledWith('baskets');
    expect(store.getActions()).toMatchSnapshot();
  });

  test('populateBasket calls service and dispatches result', async () => {
    const result = { id: 1, markedFor: 'shipping' };
    const closedBasket = { id: 1 };
    const getStub = stubs.get;
    getStub.mockResolvedValueOnce(result);
    const action = actions.populateBasket(closedBasket);

    await action(store.dispatch);

    expect(serviceStub).toHaveBeenCalledWith('baskets');
    expect(getStub).toHaveBeenCalledWith(closedBasket.id);
    expect(store.getActions()).toMatchSnapshot();
  });

  test('populateBasket dispatches error', async () => {
    const getStub = stubs.get;
    getStub.mockRejectedValueOnce();
    const action = actions.populateBasket();

    try {
      await action(store.dispatch);
    } catch (e) {
      expect(e).toBeTruthy();
    }

    expect(serviceStub).toHaveBeenCalledWith('baskets');
    expect(store.getActions()).toMatchSnapshot();
  });

  test('loadMoreBaskets calls service and dispatches result', async () => {
    const results = [{ id: 4 }, { id: 5 }, { id: 6 }];
    const page = 1;
    const findStub = stubs.find;
    findStub.mockResolvedValueOnce(results);
    const action = actions.loadMoreBaskets(page);

    await action(store.dispatch);

    expect(serviceStub).toHaveBeenCalledWith('baskets');
    expect(findStub).toHaveBeenCalledWith({
      query: { open: false, page: { number: page, size: 5 } },
    });
    expect(store.getActions()).toMatchSnapshot();
  });

  test('loadMoreBaskets dispatches error', async () => {
    const findStub = stubs.find;
    findStub.mockRejectedValueOnce();
    const action = actions.loadMoreBaskets(1);

    try {
      await action(store.dispatch);
    } catch (e) {
      expect(e).toBeTruthy();
    }

    expect(serviceStub).toHaveBeenCalledWith('baskets');
    expect(store.getActions()).toMatchSnapshot();
  });

  test('reducer updates state on updateBasket', () => {
    const action = {
      type: types.updateBasket,
      basket: {
        id: 1,
        shippingCycle: {
          shippingDate: '2018-01-18 8:00:00 GMT',
          closingDate: '2018-01-17 8:00:00 GMT',
          lastDayToAddBeer: '2018-01-16 8:00:00 GMT',
          pickupsReady: '2018-01-19 8:00:00 GMT',
          closingDateForPickups: '2018-01-17 8:00:00 GMT',
          lastDayToAddBeerPickups: '2018-01-16 8:00:00 GMT',
        },
      },
    };

    expect(reducer(initialState, action)).toMatchSnapshot();
  });

  test('reducer updates state on getClosedBaskets', () => {
    const action = {
      type: types.getClosedBaskets,
      closedBaskets: [
        { id: 1, shippedAt: '2017-3-1 8:00:00 GMT' },
        { id: 2, shippedAt: '2017-3-1 8:00:00 GMT' },
      ],
    };

    expect(reducer(initialState, action)).toMatchSnapshot();
  });

  test('reducer updates state on populateBasket', () => {
    const action = {
      type: types.populateBasket,
      populatedBasket: { id: 1, markedFor: 'testing', shippedAt: '2017-3-1 8:00:00 GMT' },
    };
    const state = {
      ...initialState,
      closedBaskets: [{ id: 1 }, { id: 2 }],
    };
    expect(reducer(state, action)).toMatchSnapshot();
  });

  test('reducer updates state on loadMoreBaskets, length less than pageSize', () => {
    const action = {
      type: types.loadMoreBaskets,
      additionalBaskets: [{ id: 3, markedFor: 'testing', shippedAt: '2017-3-1 8:00:00 GMT' }],
    };
    const state = {
      ...initialState,
      basketPage: 1,
      closedBaskets: [
        { id: 1, shippedAt: '2017-3-1 8:00:00 GMT' },
        { id: 2, shippedAt: '2017-3-1 8:00:00 GMT' },
      ],
    };
    expect(reducer(state, action)).toMatchSnapshot();
  });

  test('reducer updates state on loadMoreBaskets, length more than pageSize', () => {
    const action = {
      type: types.loadMoreBaskets,
      additionalBaskets: [
        { id: 3, markedFor: 'testing', shippedAt: '2017-3-2 8:00:00 GMT' },
        { id: 4, markedFor: 'testing', shippedAt: '2017-3-3 8:00:00 GMT' },
        { id: 5, markedFor: 'testing', shippedAt: '2017-3-4 8:00:00 GMT' },
        { id: 6, markedFor: 'testing', shippedAt: '2017-3-5 8:00:00 GMT' },
        { id: 7, markedFor: 'testing', shippedAt: '2017-3-6 8:00:00 GMT' },
      ],
    };
    const state = {
      ...initialState,
      basketPage: 1,
      closedBaskets: [
        { id: 1, shippedAt: '2017-3-1 8:00:00 GMT' },
        { id: 2, shippedAt: '2017-3-1 8:00:00 GMT' },
      ],
    };
    expect(reducer(state, action)).toMatchSnapshot();
  });

  test('reducer updates state on getOpenBasket', () => {
    const action = {
      type: types.getOpenBasket,
      basket: {
        id: 1,
        markedFor: 'testing',
        shippingCycle: {
          shippingDate: '2018-01-18 8:00:00 GMT',
          closingDate: '2018-01-17 8:00:00 GMT',
          lastDayToAddBeer: '2018-01-16 8:00:00 GMT',
          pickupsReady: '2018-01-19 8:00:00 GMT',
          closingDateForPickups: '2018-01-17 8:00:00 GMT',
          lastDayToAddBeerPickups: '2018-01-16 8:00:00 GMT',
        },
      },
      openShippingCycles: [
        {
          id: 1,
          shippingDate: '2018-01-18 8:00:00 GMT',
          closingDate: '2018-01-17 8:00:00 GMT',
          lastDayToAddBeer: '2018-01-16 8:00:00 GMT',
          pickupsReady: '2018-01-19 8:00:00 GMT',
          closingDateForPickups: '2018-01-17 8:00:00 GMT',
          lastDayToAddBeerPickups: '2018-01-16 8:00:00 GMT',
        },
        {
          id: 2,
          shippingDate: '2018-01-23 8:00:00 GMT',
          closingDate: '2018-01-22 8:00:00 GMT',
          lastDayToAddBeer: '2018-01-21 8:00:00 GMT',
          pickupsReady: '2018-01-24 8:00:00 GMT',
          closingDateForPickups: '2018-01-24 8:00:00 GMT',
          lastDayToAddBeerPickups: '2018-01-21 8:00:00 GMT',
        },
      ],
    };

    expect(reducer(initialState, action)).toMatchSnapshot();
  });
});
