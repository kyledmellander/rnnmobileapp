import types from 'appRedux/user/types';
import actions from 'appRedux/user/userActions';
import reducer from 'appRedux/user/userReducer';
import mockFeathers from 'src/config/feathers';
import store from '../mocks/store';

const {
  stubs, service: serviceStub, authenticate, getSessionToken, clearSession
} = mockFeathers;

describe('user.js', () => {
  beforeEach(() => {
    store.clearActions();
  });

  afterAll(() => {
    serviceStub.mockReset();
  });

  afterEach(() => {
    serviceStub.mockClear();
    Object.values(stubs).map(o => o.mockReset());
  });

  it('login authenticates and dispatches on valid credentials', async () => {
    const email = 'test@test.com';
    const password = 'testing123';
    authenticate.mockReturnValueOnce('thisisatoken');
    const loginAction = actions.login(email, password);

    await loginAction(store.dispatch);
    expect(authenticate.mock.calls[0]).toEqual([
      { email, password, strategy: 'local' },
    ]);
    expect(store.getActions()).toMatchSnapshot();
    authenticate.mockReset();
  });

  it('login dispatches error in invalid credentials', async () => {
    const email = 'test@test.com';
    const password = 'testing123';

    authenticate.mockReturnValueOnce(null);
    const loginAction = actions.login(email, password);

    await loginAction(store.dispatch);
    expect(authenticate.mock.calls[0]).toEqual([
      { email, password, strategy: 'local' },
    ]);
    expect(store.getActions()).toMatchSnapshot();
    authenticate.mockReset();
  });

  it('restoreSession gets sessionToken and dispatches', async () => {
    getSessionToken.mockReturnValueOnce('thisisatoken');
    const restoreSessionAction = actions.restoreSession();

    await restoreSessionAction(store.dispatch);
    expect(getSessionToken).toHaveBeenCalled();
    expect(store.getActions()).toMatchSnapshot();
    getSessionToken.mockReset();
  });

  it('signOut clears sesion and dispatches', async () => {
    clearSession.mockReturnValueOnce('thisisatoken');
    const signOutAction = actions.signOut();

    await signOutAction(store.dispatch);
    expect(clearSession).toHaveBeenCalled();
    expect(store.getActions()).toMatchSnapshot();
    clearSession.mockReset();
  });

  it('createUser calls service and dispatches', async () => {
    const email = 'test@test.com';
    const password = 'testing123';
    const createStub = stubs.create;
    createStub.mockResolvedValueOnce({ userToken: 'thisisatoken', email });
    const createUserAction = actions.createUser(email, password);

    await createUserAction(store.dispatch);

    const callArgs = createStub.mock.calls[0][0];
    expect(callArgs.email).toEqual(email);
    expect(callArgs.password).toEqual(password);
    expect(store.getActions()).toMatchSnapshot();
  });

  it('createUser dispatches error', async () => {
    const email = 'test@test.com';
    const password = 'testing123';
    const createStub = stubs.create;
    createStub.mockRejectedValueOnce({});
    const createUserAction = actions.createUser(email, password);

    await createUserAction(store.dispatch);

    const callArgs = createStub.mock.calls[0][0];
    expect(callArgs.email).toEqual(email);
    expect(callArgs.password).toEqual(password);
    expect(store.getActions()).toMatchSnapshot();
  });

  it('reducer updates state on login', () => {
    const action = {
      type: types.login,
      email: 'testing@test.com',
      token: 'itsatoken',
    };
    expect(reducer({ actionCount: 0 }, action)).toMatchSnapshot();
  });

  it('reducer updates state on logout', () => {
    const action = {
      type: types.logout,
    };
    expect(reducer({ actionCount: 0 }, action)).toMatchSnapshot();
  });

  it('reducer updates state on createUser', () => {
    const action = {
      type: types.createUser,
      email: 'testing@test.com',
    };
    expect(reducer({ actionCount: 0 }, action)).toMatchSnapshot();
  });

  it('reducer updates state on createUserError', () => {
    const action = {
      type: types.createUserError,
      email: 'testing@test.com',
    };
    expect(reducer({ actionCount: 0 }, action)).toMatchSnapshot();
  });
});
