import { actions } from 'appRedux/appStoreRating';
import { types as modalTypes } from 'appRedux/modals';
import types from 'appRedux/appStoreRating/types';
import mockFeathers from 'src/config/feathers';
import store from 'app/config/store';

jest.mock('app/config/store', () => ({ getState: jest.fn() }));


const { stubs, service: serviceStub } = mockFeathers;
const getReduxState = showRatingPrompt => ({
  user: { user: { id: '123', showRatingPrompt } },
  appStoreRating: { appStoreRatingId: '123' },
});


describe('appStoreRating.js', () => {
  beforeAll(() => {
    jest.mock('app/config/store', () => ({ getState: jest.fn() }));
  });

  afterAll(() => {
    serviceStub.mockReset();
  });

  afterEach(() => {
    serviceStub.mockClear();
    Object.values(stubs).map(o => o.mockReset());
  });

  describe('actions', async () => {
    it('fetches store rating if user.showRatingPrompt, shows rating modal if response is true', async () => {
      const action = actions.fetchAppStoreRating();
      const expected = { showRatingModal: true };

      const find = jest.fn();
      find.mockReturnValue(Promise.resolve(expected));
      serviceStub.mockReturnValue({ find });
      store.getState.mockReturnValue(getReduxState(true));

      const dispatch = jest.fn();
      await action(dispatch);

      expect(dispatch).toHaveBeenCalledWith({
        payload: { appStoreRating: expected },
        type: types.fetchingAppStoreRatingSuccess
      });
      expect(dispatch).toHaveBeenCalledWith({ type: types.fetchingAppStoreRating });
      expect(dispatch).toHaveBeenCalledWith({ type: modalTypes.showStoreRating });
    });

    it('does not call appStoreRating service if redux.user => user.showRatingPrompt = false', async () => {
      const stub = stubs.find;
      const action = actions.fetchAppStoreRating();
      store.getState.mockReturnValue(getReduxState(false));

      const dispatch = jest.fn();
      await action(dispatch);

      expect(serviceStub).not.toHaveBeenCalledWith('appStoreRatings');
      expect(stub).not.toHaveBeenCalled();
    });

    it('calls patch appStoreRating service', async () => {
      const action = actions.patchAppStoreRating();
      store.getState.mockReturnValue(getReduxState(true));
      const expected = { showRatingModal: false };

      const updateStub = jest.fn();
      updateStub.mockReturnValue(Promise.resolve(expected));
      serviceStub.mockReturnValue({ update: updateStub });
      store.getState.mockReturnValue(getReduxState(true));

      const dispatch = jest.fn();
      await action(dispatch);

      expect(serviceStub).toHaveBeenCalledWith('appStoreRatings');
      expect(dispatch).toHaveBeenCalledWith({ type: types.patchingAppStoreRating });
      expect(dispatch).toHaveBeenCalledWith({
        payload: { appStoreRating: expected },
        type: types.patchingAppStoreRatingSuccess,
      });
    });
  });
});
