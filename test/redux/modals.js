import reducer, { actions, initialState } from 'appRedux/modals';
import store from '../mocks/store';

describe('modals.js', () => {
  beforeEach(() => {
    store.clearActions();
  });

  test('1.0 show store rating sets visibleModal = storeRatingModal', async () => {
    const showStoreAction = actions.showStoreRating;

    expect(reducer(initialState, showStoreAction())).toMatchSnapshot();
  });

  test('1.1 show store rating sets visibleModal = storeRatingModal', async () => {
    const hideModalAction = actions.hideModal;

    expect(reducer(initialState, hideModalAction())).toMatchSnapshot();
  });

  test('1.2 do nothing if given bad url', async () => {
    const defaultAction = {
      type: 'gibberish'
    };

    expect(reducer(initialState, defaultAction)).toEqual(initialState);
  });
});
