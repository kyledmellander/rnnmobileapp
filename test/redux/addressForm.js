import reducer, { actions, types, initialState, parsePhone } from 'appRedux/addressForm';
import mockFeathers from 'src/config/feathers';
import store from '../mocks/store';

const { stubs, service: serviceStub } = mockFeathers;

describe('addressesForm.js', () => {
  describe('parsePhone', () => {
    it('should just return numbers if length is leq 3 or gt 10', () => {
      const threeNums = '555';
      const elevenNums = '55555555555';
      expect(parsePhone(threeNums)).toEqual(threeNums);
      expect(parsePhone(elevenNums)).toEqual(elevenNums);
    });
    it('should put parens around the area code', () => {
      expect(parsePhone('5555')).toEqual('(555) 5');
      expect(parsePhone('55555')).toEqual('(555) 55');
    });
    it('should add a hyphen for full numbers', () => {
      expect(parsePhone('5555555555')).toEqual('(555) 555-5555');
    });
  });
  beforeEach(() => {
    store.clearActions();
  });

  afterAll(() => {
    serviceStub.mockReset();
  });

  afterEach(() => {
    serviceStub.mockClear();
    Object.values(stubs).map(o => o.mockReset());
  });

  describe('actions', async () => {
    it('should dispatch resetStore when resetStore() is called', async () => {
      const action = actions.resetStore();
      await action(store.dispatch);
      expect(store.getActions()).toMatchSnapshot();
    });
    it('should dispatch setupFromAddress when setupFromAddress() is called', async () => {
      const action = actions.setupFromAddress();
      await action(store.dispatch);
      expect(store.getActions()).toMatchSnapshot();
    });
    it('should call service zipCodes and dispatches result', async () => {
      const zip = '98101';
      const results = { city: 'foo', stateCode: 'BA' };
      const stub = stubs.get;
      stub.mockResolvedValueOnce(results);
      const action = actions.getCityFromZip(zip);

      await action(store.dispatch);

      expect(serviceStub).toHaveBeenCalledWith('zipCodes');
      expect(stub).toHaveBeenCalledWith(zip);
      expect(store.getActions()).toMatchSnapshot();
    });
    it('should dispatch setAddressType when setAddressType() is called', async () => {
      const mockVal = 'commercial';
      const action = actions.setAddressType(mockVal);
      await action(store.dispatch);
      expect(store.getActions()).toMatchSnapshot();
    });
    it('should dispatch setName when setName() is called', async () => {
      const mockVal = 'bill';
      const action = actions.setName(mockVal);
      await action(store.dispatch);
      expect(store.getActions()).toMatchSnapshot();
    });
    it('should dispatch setPhone when setPhone() is called', async () => {
      const mockVal = '4252007013';
      const action = actions.setPhone(mockVal);
      await action(store.dispatch);
      expect(store.getActions()).toMatchSnapshot();
    });
    it('should dispatch setLine1 when setLine1() is called', async () => {
      const mockVal = '55555 NE 55th St';
      const action = actions.setLine1(mockVal);
      await action(store.dispatch);
      expect(store.getActions()).toMatchSnapshot();
    });
    it('should dispatch setLine2 when setLine2() is called', async () => {
      const mockVal = 'Appt. 55';
      const action = actions.setLine2(mockVal);
      await action(store.dispatch);
      expect(store.getActions()).toMatchSnapshot();
    });
    it('should dispatch setCity when setCity() is called', async () => {
      const mockVal = 'Seattle';
      const action = actions.setCity(mockVal);
      await action(store.dispatch);
      expect(store.getActions()).toMatchSnapshot();
    });
    describe('calling setZip', async () => {
      it('should dispatch setZip AND call getCityFromZip if zip length is eq 5', async () => {
        const getCityFromZipSpy = jest.spyOn(actions, 'getCityFromZip');
        getCityFromZipSpy.mockReturnValueOnce(() => (Promise.resolve()));
        const mockVal = '98101';
        const action = actions.setZip(mockVal);
        await action(store.dispatch);
        expect(store.getActions()).toMatchSnapshot();
        expect(getCityFromZipSpy).toHaveBeenCalledWith(mockVal);
        getCityFromZipSpy.mockReset();
        getCityFromZipSpy.mockRestore();
      });
      it('should dispatch setZip AND call getCityFromZip if zip length is lt 5', async () => {
        const mockVal = '9810';
        const action = actions.setZip(mockVal);
        await action(store.dispatch);
        expect(store.getActions()).toMatchSnapshot();
      });
    });
  });

  describe('reducer', () => {
    it('should updates state on resestStore', () => {
      const action = {
        type: types.resetStore
      };
      expect(reducer(initialState, action)).toMatchSnapshot();
    });
    it('should updates state on setIsLoadingCity', () => {
      const action = {
        type: types.setIsLoadingCity,
        isLoadingCity: true
      };
      expect(reducer(initialState, action)).toMatchSnapshot();
    });
    it('should updates state on setupFromAddress', () => {
      const action = {
        type: types.setupFromAddress,
        address: {
          name: 'name',
          phone: '5555555555',
          line1: 'line1',
          line2: 'line2',
          city: 'city',
          zip: 'zip',
          state: 'state',
          id: 'id',
        }
      };
      expect(reducer(initialState, action)).toMatchSnapshot();
    });
    it('should updates state on setAddressType', () => {
      const action = {
        type: types.setAddressType,
        name: 'commercial'
      };
      expect(reducer(initialState, action)).toMatchSnapshot();
    });
    it('should updates state on setName', () => {
      const action = {
        type: types.setName,
        name: 'foo'
      };
      expect(reducer(initialState, action)).toMatchSnapshot();
    });
    it('should updates state on setPhone', () => {
      const action = {
        type: types.setPhone,
        phone: '5555555555'
      };
      expect(reducer(initialState, action)).toMatchSnapshot();
    });
    it('should updates state on setLine1', () => {
      const action = {
        type: types.setLine1,
        line1: 'line1'
      };
      expect(reducer(initialState, action)).toMatchSnapshot();
    });
    it('should updates state on setLine2', () => {
      const action = {
        type: types.setLine2,
        line2: 'line2'
      };
      expect(reducer(initialState, action)).toMatchSnapshot();
    });
    it('should updates state on setCity', () => {
      const action = {
        type: types.setCity,
        city: 'city'
      };
      expect(reducer(initialState, action)).toMatchSnapshot();
    });
    it('should updates state on setZip', () => {
      const action = {
        type: types.setZip,
        zip: '98101'
      };
      expect(reducer(initialState, action)).toMatchSnapshot();
    });
    it('should updates state on setState', () => {
      const action = {
        type: types.setState,
        state: 'BA'
      };
      expect(reducer(initialState, action)).toMatchSnapshot();
    });
  });
});
