import reducer, { actions, types } from 'appRedux/offers';
import mockFeathers from 'src/config/feathers';
import store from '../mocks/store';

const { stubs, service: serviceStub } = mockFeathers;

jest.mock('app/config/store', () => ({ getState: () => ({ user: { user: { id: 0 } } }) }));

describe('offers.js', () => {
  beforeEach(() => {
    store.clearActions();
  });

  afterAll(() => {
    serviceStub.mockReset();
  });

  afterEach(() => {
    serviceStub.mockClear();
    Object.values(stubs).map(o => o.mockReset());
  });

  it('getOffers calls service and dispatches results', async () => {
    const findStub = stubs.find;
    const mockResults = [
      {
        id: 1,
        title: 'test 1',
      },
      {
        id: 2,
        title: 'test 2',
      },
    ];
    findStub.mockResolvedValueOnce(mockResults);
    const getOffersAction = actions.getOffers();

    await getOffersAction(store.dispatch);

    expect(serviceStub).toHaveBeenCalledWith('offers');
    expect(findStub).toHaveBeenCalled();
    expect(store.getActions()).toMatchSnapshot();
  });

  it('getOffers dispatches error action on api error if title present', async () => {
    const findStub = stubs.find;
    // eslint-disable-next-line prefer-promise-reject-errors
    findStub.mockReturnValueOnce(Promise.reject({
      errors: [{ title: 'test' }],
    }));
    const getOffersAction = actions.getOffers();

    try {
      await getOffersAction(store.dispatch);
    } catch (e) {
      expect(e).toBeTruthy();
    }

    expect(serviceStub).toHaveBeenCalledWith('offers');
    expect(findStub).toHaveBeenCalled();
    expect(store.getActions()).toMatchSnapshot();
  });

  it('reducer updates state based on action', () => {
    const action = {
      type: types.getOffers,
      offers: [
        {
          id: 1,
          title: 'testing',
        },
        {
          id: 2,
          title: 'testing 2',
        },
      ],
    };
    expect(reducer({}, action)).toMatchSnapshot();
  });
});
