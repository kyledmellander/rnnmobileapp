import reducer, { actions, types } from 'appRedux/zipCode';
import app from 'src/config/feathers';
import store from '../mocks/store';

beforeEach(() => {
  store.clearActions();
});

test('getZipCode calls service and dispatches results', async () => {
  const zipCode = '12345';
  const serviceStub = app.service;
  const getStub = jest.fn();
  const expectedResults = {
    stateCode: 'Test',
    city: 'TestVille',
    zipCodeCovered: false,
  };
  getStub.mockReturnValueOnce(Promise.resolve(expectedResults));
  serviceStub.mockReturnValueOnce({ get: getStub });
  const getZipCodeAction = actions.getZipCode(zipCode);

  await getZipCodeAction(store.dispatch);

  const callArgs = getStub.mock.calls[0];
  expect(serviceStub).toHaveBeenCalledWith('zipCodes');
  expect(callArgs).toEqual([zipCode]);
  expect(store.getActions()).toMatchSnapshot();
});

test('reducer updates state based on action', () => {
  const action = {
    type: types.getZipCode,
    stateCode: 'Test',
    city: 'San AnTestio',
    zipCodeCovered: true,
  };

  const state = reducer({}, action);

  expect(state).toMatchSnapshot();
});

test('reducer updates error state based on action', () => {
  const action = {
    type: types.zipCodeError,
    value: 'there was an error',
  };

  const state = reducer({}, action);

  expect(state).toMatchSnapshot();
});
