import reducer, { actions, types, initialState } from 'appRedux/paymentMethods';
import mockFeathers from 'src/config/feathers';
import store from '../mocks/store';

const infiniteNoop = () => infiniteNoop;

const { stubs, service: serviceStub } = mockFeathers;

jest.mock('src/config/stripe', () => ({
  createToken: jest.fn(),
}));

jest.mock('src/config/settings', () => ({
  stripeKey: '',
}));
// eslint-disable-next-line
import stripe from 'src/config/stripe';

describe('paymentMethods.js', () => {
  beforeEach(() => {
    store.clearActions();
  });

  afterAll(() => {
    serviceStub.mockReset();
  });

  afterEach(() => {
    serviceStub.mockClear();
    Object.values(stubs).map(o => o.mockReset());
    jest.restoreAllMocks(); // reset spies
  });

  it('getInitData calls service and dispatches result', async () => {
    const results = [{ id: 1 }, { id: 2 }];
    const stub = stubs.find;
    stub.mockResolvedValueOnce(results);
    const action = actions.getInitData();

    await action(store.dispatch);

    expect(serviceStub).toHaveBeenCalledWith('paymentMethods');
    expect(store.getActions()).toMatchSnapshot();
  });

  it('getInitData dispatches error', async () => {
    const findStub = stubs.find;
    findStub.mockRejectedValueOnce({});
    const action = actions.getInitData();

    await action(store.dispatch);

    expect(serviceStub).toHaveBeenCalledWith('paymentMethods');
    expect(store.getActions()).toMatchSnapshot();
  });

  it('addPaymentMethod calls service and dispatches result', async () => {
    const stub = stubs.create;
    const token = 12345;
    const createTokenStub = stripe.createToken;
    createTokenStub.mockResolvedValueOnce({ id: token });
    stub.mockResolvedValueOnce();
    const paymentMethod = {
      number: 1,
      month: 12,
      year: 2017,
      cvv: 123,
    };
    const spyGetInitData = jest.spyOn(actions, 'getInitData').mockImplementation(infiniteNoop);
    const action = actions.addPaymentMethod(paymentMethod);

    await action(store.dispatch);

    const stripeCallArgs = createTokenStub.mock.calls[0];
    const callArgs = stub.mock.calls[0];
    expect(spyGetInitData).toHaveBeenCalled();
    expect(callArgs).toEqual([{ cardToken: token }]);
    /* eslint-disable */
    expect(stripeCallArgs).toEqual([{
      number: paymentMethod.number,
      exp_month: paymentMethod.month,
      exp_year: paymentMethod.year,
      cvc: paymentMethod.cvv
    }]);
    /* eslint-enable */
    expect(serviceStub).toHaveBeenCalledWith('paymentMethods');
    expect(store.getActions()).toMatchSnapshot();
    createTokenStub.mockClear();
  });

  it('addPaymentMethod dispatches error', async () => {
    const createTokenStub = stripe.createToken;
    const stub = stubs.create;
    stub.mockRejectedValueOnce({});
    const paymentMethod = {
      number: 1,
      month: 12,
      year: 2017,
      cvv: 123,
    };
    const action = actions.addPaymentMethod(paymentMethod);

    await action(store.dispatch);

    expect(serviceStub).toHaveBeenCalledWith('paymentMethods');
    expect(store.getActions()).toMatchSnapshot();
    createTokenStub.mockClear();
  });

  it('removePaymentMethod calls service and updates result', async () => {
    const removeStub = stubs.remove;
    const paymentMethodId = 2;
    const results = [{ id: 1 }];
    const findStub = stubs.find;
    removeStub.mockResolvedValueOnce();
    findStub.mockResolvedValueOnce(results);
    const action = actions.removePaymentMethod({ id: paymentMethodId });

    await action(store.dispatch);

    expect(serviceStub).toHaveBeenCalledWith('paymentMethods');
    expect(removeStub).toHaveBeenCalledWith(paymentMethodId);
    expect(findStub).toHaveBeenCalled();
    expect(store.getActions()).toMatchSnapshot();
  });

  it('removePaymentMethod dispatches error', async () => {
    const removeStub = stubs.remove;
    removeStub.mockRejectedValueOnce({});
    const action = actions.removePaymentMethod({ id: 1 });

    await action(store.dispatch);

    expect(serviceStub).toHaveBeenCalledWith('paymentMethods');
    expect(store.getActions()).toMatchSnapshot();
  });

  it('updatePaymentMethod calls service and updates result', async () => {
    const updateStub = stubs.update;
    const paymentMethodId = 2;
    const results = [{ id: 1 }];
    const findStub = stubs.find;
    updateStub.mockResolvedValueOnce();
    findStub.mockResolvedValueOnce(results);
    const action = actions.updatePaymentMethod({ id: paymentMethodId });

    await action(store.dispatch);

    expect(serviceStub).toHaveBeenCalledWith('paymentMethods');
    expect(updateStub).toHaveBeenCalledWith(paymentMethodId, {});
    expect(findStub).toHaveBeenCalled();
    expect(store.getActions()).toMatchSnapshot();
  });

  it('updatePaymentMethod dispatches error', async () => {
    const updatePaymentMethodStub = stubs.update;
    updatePaymentMethodStub.mockRejectedValueOnce({});
    const action = actions.updatePaymentMethod({ id: 1 });

    await action(store.dispatch);

    expect(serviceStub).toHaveBeenCalledWith('paymentMethods');
    expect(store.getActions()).toMatchSnapshot();
  });

  it('reducer updates state on updatePaymentMethod', () => {
    const action = {
      type: types.updatePaymentMethod,
      paymentMethods: [{ id: 1 }, { id: 2 }],
      cardMessage: 'foo',
    };
    expect(reducer(initialState, action)).toMatchSnapshot();
  });

  it('reducer updates state on removePaymentMethod', () => {
    const action = {
      type: types.removePaymentMethod,
      paymentMethods: [{ id: 1 }],
      cardMessage: 'bar',
    };
    expect(reducer(initialState, action)).toMatchSnapshot();
  });

  it('reducer updates state on getInitData', () => {
    const action = {
      type: types.gotInitData,
      paymentMethods: [{ id: 1 }, { id: 3 }],
    };
    expect(reducer(initialState, action)).toMatchSnapshot();
  });

  it('reducer updates state on addPaymentMethod', () => {
    const action = {
      type: types.addPaymentMethod,
      cardMessage: 'baz'
    };
    expect(reducer(initialState, action)).toMatchSnapshot();
  });

  it('reducer updates state on clearPaymentMethodStatus', () => {
    const action = {
      type: types.clearPaymentMethodStatus,
    };
    expect(reducer(initialState, action)).toMatchSnapshot();
  });
});
