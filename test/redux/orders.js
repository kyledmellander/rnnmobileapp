import reducer, {
  actions,
  types,
  initialState
} from 'appRedux/orders';
import mockFeathers from 'src/config/feathers';
import store from '../mocks/store';

const { stubs, service: serviceStub } = mockFeathers;

describe('orders.js', () => {
  beforeEach(() => {
    store.clearActions();
  });

  afterAll(() => {
    serviceStub.mockReset();
  });

  afterEach(() => {
    serviceStub.mockClear();
    Object.values(stubs).map(o => o.mockReset());
  });

  it('createOrder calls service and dispatches result', async () => {
    const order = { id: 1 };
    const createStub = stubs.create;
    createStub.mockResolvedValueOnce(order);
    const createOrderAction = actions.createOrder(order);

    await createOrderAction(store.dispatch);

    expect(serviceStub).toHaveBeenCalledWith('orders');
    expect(createStub).toHaveBeenCalledWith(order);
    expect(store.getActions()).toMatchSnapshot();
  });

  it('createOrder dispatches error', async () => {
    const order = { id: 1 };
    const createStub = stubs.create;
    createStub.mockRejectedValueOnce({});
    const createOrderAction = actions.createOrder(order);

    await createOrderAction(store.dispatch);

    expect(serviceStub).toHaveBeenCalledWith('orders');
    expect(store.getActions()).toMatchSnapshot();
  });

  it('clearOrderStatus dispatches', async () => {
    const clearOrderStatusAction = actions.clearOrderStatus();

    await clearOrderStatusAction(store.dispatch);

    expect(store.getActions()).toMatchSnapshot();
  });

  it('reducer updates state on createOrder', () => {
    const action = {
      type: types.createOrder,
      order: { id: 1 },
    };

    expect(reducer(initialState, action)).toMatchSnapshot();
  });

  it('reducer updates state on clearOrderStatus', () => {
    const action = {
      type: types.clearOrderStatus,
    };
    const state = {
      ...initialState,
      createdOrder: true,
    };

    expect(reducer(state, action)).toMatchSnapshot();
  });
});
