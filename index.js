// import 'node-libs-react-native/globals';
// import { AppRegistry } from 'react-native';
import { AppRegistry } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { IconsLoaded, registerComponents } from 'components';
import { registerScreens } from 'screens';
import { setDefaultOptions, registerGlobalNavEvents } from 'lib/navUtils';
import AppLaunchScreen from './app/screens/appLaunch';

/* React Native Navigation Components */
Navigation.events().registerAppLaunchedListener(() => {
  setDefaultOptions(); // set default nav options
  registerComponents();
  registerScreens();
  registerGlobalNavEvents();

  IconsLoaded.then(() => {
    AppRegistry.registerComponent('Tavour', () => AppLaunchScreen);
    Navigation.setRoot({
      root: {
        stack: {
          id: 'APP_STACK',
          children: [
            {
              component: {
                name: 'AppLaunchScreen',
              },
            }
          ]
        }
      }
    });
  });
});
