#!/bin/bash

if [ "$BUGSNAG_API_KEY" == "" ]; then
  echo ERROR: Need Bugsnag api key in environment variable before running
  exit
fi

echo "generating ios sourcemap"
react-native bundle --platform ios --dev false --entry-file index.js --bundle-output ios-release.bundle --sourcemap-output ios-release.bundle.map

echo "generating android sourcemap"
react-native bundle --platform android --dev false --entry-file index.js --bundle-output android-release.bundle --sourcemap-output android-release.bundle.map

echo "uploading ios sourcemap"
bugsnag-sourcemaps upload --api-key $BUGSNAG_API_KEY --minified-file ios-release.bundle --source-map ios-release.bundle.map --minified-url main.jsbundle --upload-sources


echo "uploading android sourcemap"
bugsnag-sourcemaps upload --api-key $BUGSNAG_API_KEY --minified-file android-release.bundle --source-map android-release.bundle.map --minified-url index.android.bundle --upload-sources

