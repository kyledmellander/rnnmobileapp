curDir="$PWD"
apkDir='./app/build/outputs/apk/release'
cd ./android

if [ -f $curDir/release.apk ]; then
  rm -f $curDir/release.apk
fi

if [ -f  $apkDir/app-release-unsigned.apk ]; then
  rm -f $apkDir/app-release-unsigned.apk
fi

./gradlew app:assembleRelease

pw=$(security find-generic-password -s android_keystore -w)
if [ -z "$pw" ]; then
    echo "ERROR: no keychain named 'android_keystore' found!" >&2
    exit 1
fi
ujar=$(ls $apkDir | grep 'app-release-unsigned')
if [ -z "$ujar" ]; then
    echo 'ERROR: no .apk file found!' >&2
    exit 1
fi
uapk="$apkDir/$ujar"
echo 'signing apk...'
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore ./app/my-release-key.keystore -storepass $pw $uapk alias_name > /dev/null

echo 'zip aligning apk...'
$ANDROID_HOME/build-tools/27.0.3/zipalign -v 4 $uapk $curDir/release.apk > /dev/null
cd $curDir