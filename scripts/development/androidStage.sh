curDir="$PWD"
apkDir='./app/build/outputs/apk/releaseStaging'
cd ./android

if [ -f $curDir/staging.apk ]; then
  rm -f $curDir/staging.apk
fi

if [ -f  $apkDir/app-releaseStaging-unsigned.apk ]; then
  rm -f $apkDir/app-releaseStaging-unsigned.apk
fi

./gradlew app:assembleReleaseStaging

ujar=$(ls $apkDir | grep 'app-releaseStaging-unsigned')

if [ -z "$ujar" ]; then
    echo 'ERROR: no .apk file found!' >&2
    exit 1
fi

pw=$(security find-generic-password -s android_keystore -w)
if [ -z "$pw" ]; then
    echo "ERROR: no keychain named 'android_keystore' found!" >&2
    exit 1
fi

uapk="$apkDir/$ujar"

echo 'signing apk...'
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore ./app/my-release-key.keystore -storepass $pw $uapk alias_name > /dev/null

echo 'zip aligning apk...'
$ANDROID_HOME/build-tools/27.0.3/zipalign -v 4 $uapk $curDir/staging.apk > /dev/null

cd $curDir

deviceCount=$(adb devices | grep -o 'device$' | wc -l)

if [ $deviceCount -gt 0 ]; then
  echo 'installing new staging build...'
  adb install -r staging.apk
fi
