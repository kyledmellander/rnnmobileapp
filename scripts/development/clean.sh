watchman watch-del-all
rm -rf ~/.rncache
rm -rf $TMPDIR/react-*
rm -rf node_modules/
yarn cache clean
yarn install
yarn start -- --reset-cache