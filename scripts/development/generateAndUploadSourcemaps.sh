#!/bin/bash

if [ "$BUGSNAG_API_KEY" == "" ]; then
  echo ERROR: Need Bugsnag api key in environment variable before running
  exit
fi

echo "generating ios sourcemap"
react-native bundle --platform ios --dev true --entry-file index.ios.js --bundle-output ios-debug.bundle --sourcemap-output ios-debug.bundle.map

echo "uploading ios sourcemap"
bugsnag-sourcemaps upload --api-key $BUGSNAG_API_KEY --minified-file ios-debug.bundle --source-map ios-debug.bundle.map --minified-url "http://localhost:8081/index.ios.bundle?platform=ios&dev=true&minify=false"

