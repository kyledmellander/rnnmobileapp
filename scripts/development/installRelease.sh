deviceCount=$(adb devices | grep -o 'device$' | wc -l)

if [ $deviceCount -gt 0 ]; then
  echo 'installing new release build...'
  adb install -r release.apk
fi
