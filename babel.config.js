/* eslint-disable */
module.exports = function (api) {
  api.cache(true);
  return {
    "presets": ["module:metro-react-native-babel-preset"],
    "plugins": [
      [
        "module-resolver",
        {
          "root": ["./"],
          "alias": {
            "src": "./app",
            "lib": "./app/lib",
            "store": "./app/config/store",
            "images": "./app/config/images",
            "appRedux": "./app/redux",
            "assets": "./app/assets",
            "config": "./app/config",
            "components": "./app/components",
            "svgs": "./app/svgs",
            "screens": "./app/screens",
            "styles": "./app/styles",
            "experiments": "./app/experiments",
            "e2eLib": "./e2e/lib"
          }
        }
      ]
    ]
  };
};

