module.exports = {
  extends: 'airbnb',
  env: {
    node: true,
    jest: true,
    browser: true,
    es6: true
  },
  globals: {
    rootRequire: true,
    device: true,
    element: true,
    waitFor: true,
    by: true,
    graphite: true,
    __DEV__: true
  },
  parser: 'babel-eslint',
  rules: {
    'comma-dangle': ['error', 'only-multiline'],
    'no-param-reassign': ['error', { props: false }],
    camelcase: ['error', { properties: 'always' }],
    'class-methods-use-this': ['error', { exceptMethods: ['componentWillReceiveProps', 'render', 'componentWillUnmount'] }],
    'no-console': ['error', { allow: ['log', 'warn', 'error'] }],
    'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
    'react/prop-types': [2, { skipUndeclared: true }],

    // Stuff that probably shouldn't do but it's everywhere
    'react/destructuring-assignment': false,
    'react/jsx-one-expression-per-line': false,
    'object-curly-newline': 0,
    'operator-linebreak': 0,
    'implicit-arrow-linebreak': 0,
    'lines-between-class-members': 0,
    'indent': 0,
    'jsx-wrap-multilines': 0,
    'no-multiple-empty-lines': 0,
  },
  settings: {
    'import/resolver': {
      'babel-module': {}
    }
  }
};
