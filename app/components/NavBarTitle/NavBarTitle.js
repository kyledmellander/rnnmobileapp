import React from 'react';
import { Text, View } from 'react-native';
import { navigationBar } from 'styles';

export default ({ title }) => (<Text style={navigationBar.title}>{title.toUpperCase()}</Text>);
