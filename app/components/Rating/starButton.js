import React from 'react';
import Button from 'react-native-button';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome';

const StarButton = props => (
  <Button
    activeOpacity={0.2}
    onPress={() => {
        props.selectedStar(props.rating);
      }}
    containerStyle={props.buttonStyle}
    style={{
        height: props.starSize,
        width: props.starSize,
      }}
  >
    <FontAwesomeIcons
      name={props.starIconName}
      size={props.starSize}
      color={props.starColor}
      style={props.starStyle}
    />
  </Button>
);

export default StarButton;
