import { colors } from 'styles';

const styles = {
  starRatingContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  starColor: colors.tavourOrange,
};

export default styles;
