import { View } from 'react-native';
import React from 'react';
import StarButton from './starButton';
import styles from './styles';

export default class StarRating extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      rating: props.rating,
    };
    this.starSelected = this.starSelected.bind(this);
  }

  getStars() {
    const starButtons = [];
    const emptyStar = 'star-o';
    const fullStar = 'star';

    for (let i = 0; i < 5; i += 1) {
      let starIconName = emptyStar;
      if (i + 1 <= this.state.rating) {
        starIconName = fullStar;
      }

      starButtons.push(<StarButton
        key={i}
        rating={i + 1}
        selectedStar={this.starSelected}
        starSize={this.props.starSize || 30}
        starIconName={starIconName}
        starColor={this.props.starColor || styles.starColor}
        buttonStyle={this.props.buttonStyle}
      />);
    }
    return starButtons;
  }

  starSelected(val) {
    this.setState({
      rating: val,
    });
    this.props.selectedStar(val);
  }

  render() {
    return (
      <View
        style={[styles.starRatingContainer, this.props.style]}
      >
        {this.getStars()}
      </View>
    );
  }
}
