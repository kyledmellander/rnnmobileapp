import { Navigation } from 'react-native-navigation';
import ABSplit from './ABSplit';
import Accordion from './Accordion';
import CustomKeyboardAvoidingView from './CustomKeyboardAvoidingView';
import CustomButton from './CustomButton';
import CustomIcon from './CustomIcon';
import Modal, { modalTypes } from './Modals';
import NavBarTitle from './NavBarTitle';
import OrderProductView from './OrderProductView';
import StoreRating from './Modals/StoreRatingModal';
import KeyboardAvoidingButton from './KeyboardAvoidingButton';
import Swiper from './Swiper';

export * from './NavBarIcons';

/*    Navigation Components need to be registered with RNN
      This includes things like Custom Icon Components, Custom
      Title Components, Custom Nav Buttons, etc
 */
const registerComponents = () => {
  Navigation.registerComponent('NavBarTitle', () => NavBarTitle);
};

export {
  ABSplit,
  Accordion,
  CustomIcon,
  CustomButton,
  CustomKeyboardAvoidingView,
  KeyboardAvoidingButton,
  modalTypes,
  Modal,
  OrderProductView,
  registerComponents,
  StoreRating,
  Swiper,
};
