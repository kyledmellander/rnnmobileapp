import { colors, appStyles } from '../../styles';

const styles = {
  container: {
    flexDirection: 'row',
    width: '100%',
    padding: 10,
    justifyContent: 'space-between',
  },
  labelContainer: {
    flex: 0.8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  label: {
    fontSize: 17,
    fontFamily: appStyles.fontFamily,
  },
  buttonsContainer: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  button: {
    paddingVertical: 13,
    paddingHorizontal: 11,
    marginHorizontal: 5,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#CECECE',
  },
  buttonEnabled: {
    borderColor: colors.tavourOrange,
  },
  buttonText: {
    fontSize: 12,
    color: colors.disabledButtonGrey,
    fontFamily: appStyles.fontFamily,
  },
  buttonTextEnabled: {
    color: colors.tavourOrange,
  },
};

export default styles;
