import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './styles';

class CustomABButtons extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: props.selected,
    };
    const functionsToBind = [
      'aPushed',
      'bPushed',
      'aButtonStyle',
      'aButtonTextStyle',
      'bButtonStyle',
      'bButtonTextStyle',
    ];
    functionsToBind.forEach((functionToBind) => {
      this[functionToBind] = this[functionToBind].bind(this);
    });
  }

  aPushed() {
    this.setState({ selected: this.props.aValue });
    this.props.onSelected(this.props.aValue);
  }

  bPushed() {
    this.setState({ selected: this.props.bValue });
    this.props.onSelected(this.props.bValue);
  }

  aButtonStyle() {
    if (this.state.selected === this.props.aValue) {
      return [styles.button, styles.buttonEnabled];
    }
    return [styles.button];
  }

  aButtonTextStyle() {
    if (this.state.selected === this.props.aValue) {
      return [styles.buttonText, styles.buttonTextEnabled];
    }
    return [styles.buttonText];
  }

  bButtonStyle() {
    if (this.state.selected === this.props.bValue) {
      return [styles.button, styles.buttonEnabled];
    }
    return [styles.button];
  }

  bButtonTextStyle() {
    if (this.state.selected === this.props.bValue) {
      return [styles.buttonText, styles.buttonTextEnabled];
    }
    return [styles.buttonText];
  }

  render() {
    return (
      <View style={styles.container} testID="ABButtonsContainer">
        <View style={styles.labelContainer}>
          <Text style={styles.label}>{this.props.labelText}</Text>
        </View>
        <View style={styles.buttonsContainer}>
          <TouchableOpacity style={this.aButtonStyle()} onPress={this.aPushed}>
            <Text style={this.aButtonTextStyle()}>{this.props.aText}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={this.bButtonStyle()} onPress={this.bPushed}>
            <Text style={this.bButtonTextStyle()}>{this.props.bText}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default CustomABButtons;
