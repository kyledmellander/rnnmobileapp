import React from 'react';
import { Platform } from 'react-native';
import KeyboardAvoidingView from './CustomKeyboardAvoidingView';

const KeyboardAvoidingViewWrapper = ({ children, behavior, ...props }) => {
  if (Platform.OS === 'ios') {
    return (
      <KeyboardAvoidingView behavior={behavior} {...props}>
        {children}
      </KeyboardAvoidingView>
    );
  }

  return (
    <KeyboardAvoidingView {...props}>
      {children}
    </KeyboardAvoidingView>
  );
};

export default KeyboardAvoidingViewWrapper;
