import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import { KeyboardAvoidingView } from 'react-native';

export default class CustomKeyboardAvoidingView extends Component {
  componentWillMount() {
    this.state = {
      offset: 64,
    };

    Navigation.constants().then((constants) => {
      const { statusBarHeight, topBarHeight } = constants;

      this.setState({ offset: statusBarHeight + topBarHeight });
    });
  }

  render() {
    const { children, ...extraProps } = this.props;
    const { offset } = this.state;
    return (
      <KeyboardAvoidingView keyboardVerticalOffset={offset} {...extraProps}>
        {children}
      </KeyboardAvoidingView>
    );
  }
}
