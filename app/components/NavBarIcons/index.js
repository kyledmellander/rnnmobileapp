/* eslint-disable */
import Icon from 'react-native-vector-icons/EvilIcons';

const IconImages = {};

// Used to initialize the icon images
// Format: [source, name, size, color]
const icons = {
  hamburger: [Icon, 'navicon', 32, 'black'],
};

// Move all the loaded icons into the icons map
const IconsLoaded = new Promise((resolve) => {
  new Promise.all(
    Object.keys(icons).map(iconName =>
      icons[iconName][0].getImageSource(
        icons[iconName][1],
        icons[iconName][2],
        icons[iconName][3]
      ))
  ).then(sources => {
    Object.keys(icons)
      .forEach((iconName, idx) => (IconImages[iconName] = sources[idx]));
    resolve(true);
  });
});

export {
  IconImages as Icons,
  IconsLoaded,
};

