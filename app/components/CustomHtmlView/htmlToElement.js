import React from 'react';
import { Text, View } from 'react-native';
import htmlparser from 'htmlparser2-without-node-native';
import entities from 'entities';
import { appStyles } from '../../styles';

const LINE_BREAK = '\n';
const BULLET = '\u2022 ';
const fontStyle = { fontFamily: appStyles.fontFamily };

function getLineBreak(nodeName, index, list, opts) {
  let linebreakAfter = null;
  if (opts.addLineBreaks) {
    switch (nodeName) {
      case 'li': {
        const lastIndex = list.length - 1;
        linebreakAfter = index === lastIndex ? null : LINE_BREAK;
        break;
      }
      case 'br':
      case 'h1':
      case 'h2':
      case 'h3':
      case 'h4':
      case 'h5':
        linebreakAfter = LINE_BREAK;
        break;
      default:
        linebreakAfter = null;
        break;
    }
  }
  return linebreakAfter;
}

function getListItemPrefix(nodeName, index, parent) {
  let listItemPrefix = null;
  if (nodeName === 'li') {
    if (parent.name === 'ol') {
      listItemPrefix = `${index + 1}. `;
    } else if (parent.name === 'ul') {
      listItemPrefix = BULLET;
    }
    listItemPrefix = `   ${listItemPrefix}`; // mock indent
  }
  return listItemPrefix;
}

export default function htmlToElement(rawHtml, opts, done) {
  function domToElement(dom, parent) {
    if (!dom) return null;
    const elements = dom.map((node, index, list) => {
      if (node.type === 'text') {
        if (node.data.trim() === '') { return null; }
        const parentStyle = parent ? opts.styles[parent.name] : null;
        const grandParentStyle =
          parent && parent.parent
            ? opts.styles[parent.parent.name]
            : null || null;
        const extraLineBreak =
          node.next && node.next.name === 'br' ? LINE_BREAK : null;
        return (
          <Text style={[fontStyle, parentStyle, grandParentStyle]}>
            {entities.decodeHTML(node.data)}
            {extraLineBreak}
          </Text>
        );
      }

      if (node.type === 'tag') {
        let linkPressHandler = null;
        if (node.name === 'a' && node.attribs && node.attribs.href) {
          linkPressHandler = () =>
            opts.linkHandler(entities.decodeHTML(node.attribs.href));
        }

        const linebreakAfter = getLineBreak(node.name, index, list, opts);
        const listItemPrefix = getListItemPrefix(node.name, index, parent);
        const props = {
          key: index,
          onPress: linkPressHandler,
          fontSize: 14,
          fontFamily: appStyles.fontFamily,
        };
        if (opts.preview) {
          props.numberOfLines = 2;
        }
        const child = domToElement(node.children, node);
        if (child.length === 0) {
          return null;
        }
        const children = [];
        if (listItemPrefix) children.push(listItemPrefix);

        children.push(child);

        if (linebreakAfter) children.push(linebreakAfter);
        // no parent means we're wrapping a top level element
        if (!parent) {
          const paddingTop = index === 0 ? 0 : 10;
          return (
            <View style={{ paddingTop }}>
              <Text style={fontStyle} {...props}>{children}</Text>
            </View>
          );
        }

        return <Text style={fontStyle} {...props}>{children}</Text>;
      }
      return null;
    });
    return elements.filter(e => e !== null);
  }

  const handler = new htmlparser.DomHandler(((err, dom) => {
    if (err) done(err);
    done(null, domToElement(dom));
  }));
  const parser = new htmlparser.Parser(handler);
  parser.write(rawHtml);
  parser.done();
}
