import { StyleSheet } from 'react-native';

const baseStyles = StyleSheet.create({
  b: { fontWeight: '500' },
  u: { textDecorationLine: 'underline' },
  i: { fontStyle: 'italic' },
  a: {
    fontWeight: '500',
    color: '#007AFF',
  },
  h1: { fontWeight: '500', fontSize: 36 },
  h2: { fontWeight: '500', fontSize: 30 },
  h3: { fontWeight: '500', fontSize: 24 },
  h4: { fontWeight: '500', fontSize: 18 },
  h5: { fontWeight: '500', fontSize: 14 },
  h6: { fontWeight: '500', fontSize: 12 },
});

export default baseStyles;
