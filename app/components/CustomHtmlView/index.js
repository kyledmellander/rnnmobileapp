import React, { Component } from 'react';
import { Linking, View } from 'react-native';
import htmlToElement from './htmlToElement';
import baseStyles from './styles';

// https://github.com/jsdf/react-native-htmlview
class HtmlView extends Component {
  constructor() {
    super();
    this.state = {
      element: null,
    };
  }

  componentDidMount() {
    this.mounted = true;
    this.startHtmlRender(this.props.value);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.value !== nextProps.value) {
      this.startHtmlRender(nextProps.value);
    }
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  startHtmlRender(value) {
    if (!value) {
      this.setState({ element: null });
    }

    const opts = {
      addLineBreaks: this.props.addLineBreaks,
      linkHandler: this.props.onLinkPress,
      styles: Object.assign({}, baseStyles, this.props.stylesheet),
      preview: this.props.preview
    };

    htmlToElement(value, opts, (err, element) => {
      if (err) {
        this.props.onError(err);
      }

      if (this.mounted) {
        if (this.props.preview) {
          // eslint-disable-next-line
          element = element[0];
        }
        this.setState({ element });
      }
    });
  }

  render() {
    if (this.state.element) {
      return React.createElement(View, { style: this.props.style }, ...this.state.element);
    }
    return <View style={this.props.style} />;
  }
}

HtmlView.defaultProps = {
  addLineBreaks: true,
  onLinkPress: url => Linking.openURL(url),
  onError: console.error.bind(console),
};

export default HtmlView;
