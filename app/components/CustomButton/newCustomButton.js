import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import CustomIcon from '../CustomIcon';
import styles from './styles';

const customButton = ({
  customIcon,
  iconName,
  enabled,
  ...props
}) => {
  let style = styles.enabledButton;
  let textStyle = styles.enabledButtonText;
  let { onPress } = props;
  let activeOpacity = 0.6;

  if (!enabled) {
    style = styles.disabledButton;
    textStyle = styles.disabledButtonbuttonText;
    onPress = undefined;
    activeOpacity = 1;
  }

  let icon = null;
  const iconStyle = [styles.iconStyle, props.buttonTextStyle, textStyle];
  if (iconName && customIcon) {
    icon = <CustomIcon name={iconName} style={iconStyle} />;
  } else if (iconName) {
    icon = <Icon name={iconName} style={iconStyle} />;
  }
  return (
    <View style={[styles.buttonContainer, props.buttonContainerStyle]} testID={props.testID}>
      <TouchableOpacity
        style={[styles.button, props.buttonStyle, style]}
        onPress={onPress}
        activeOpacity={activeOpacity}
      >
        <View style={styles.buttonTextContainer}>
          <Text
            style={[styles.newButtonText, props.buttonTextStyle, textStyle]}
          >
            {props.buttonText}
          </Text>
          {icon}
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default customButton;
