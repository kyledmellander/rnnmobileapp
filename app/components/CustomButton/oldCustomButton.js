import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import styles from './styles';

const customButton = ({
  enabled,
  onPress: onPressProp,
  buttonText,
  buttonTextStyle,
  buttonContainerStyle,
  buttonStyle,
  testID,
}) => {
  let style = styles.disabledButton;
  let textStyle = styles.disabledButtonbuttonText;
  let onPress;
  let activeOpacity = 1;

  if (enabled) {
    style = styles.enabledButton;
    textStyle = styles.enabledButtonText;
    // eslint-disable-next-line prefer-destructuring
    onPress = onPressProp;
    activeOpacity = 0.6;
  }

  return (
    <View style={[styles.buttonContainer, buttonContainerStyle]} testID={testID}>
      <TouchableOpacity
        style={[styles.button, buttonStyle, style]}
        onPress={onPress}
        activeOpacity={activeOpacity}
      >
        <Text
          style={[styles.buttonText, buttonTextStyle, textStyle]}
        >
          {buttonText}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default customButton;
