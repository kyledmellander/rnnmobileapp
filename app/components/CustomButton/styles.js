import { colors, appStyles } from '../../styles';

const styles = {
  buttonTextContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1
  },
  buttonContainer: {
    flex: 1,
    maxHeight: 60,
    minHeight: 50,
  },
  button: {
    flex: 1,
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  disabledButton: {
    backgroundColor: colors.tavourLightGray,
  },
  enabledButton: {
    backgroundColor: colors.tavourOrange,
  },
  iconStyle: {
    textAlign: 'center',
    fontSize: appStyles.buttonFontSize,
    fontFamily: appStyles.fontFamily,
    paddingLeft: 10,
  },
  // Should be regular button text if we accept AB split
  newButtonText: {
    textAlign: 'center',
    fontSize: appStyles.buttonFontSize,
    fontFamily: appStyles.fontFamily,
  },
  buttonText: {
    textAlign: 'center',
    flex: 1,
    fontSize: appStyles.buttonFontSize,
    fontFamily: appStyles.fontFamily,
  },
  enabledButtonText: {
    color: 'white',
  },
  disabledButtonbuttonText: {
    color: colors.disabledTextGray,
  },
};

export default styles;
