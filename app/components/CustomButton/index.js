import React from 'react';
import { selectors } from 'appRedux/experiments';
import ABSplit from '../ABSplit';
import NewCustomButton from './newCustomButton';
import OldCustomButton from './oldCustomButton';

const ABButton = props => (
  <ABSplit
    {...props}
    splitFunction={selectors.newPurchaseFlowSelector}
    NewComponent={NewCustomButton}
    OldComponent={OldCustomButton}
  />
);


export default (ABButton);
