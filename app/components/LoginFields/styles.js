import { colors, appStyles } from 'styles';

const styles = {
  formContainer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderTopWidth: 1,
    borderColor: colors.borderGray,
  },
  formField: {
    width: '100%',
    height: 60,
    borderBottomColor: colors.borderGray,
    borderBottomWidth: 1,
    flexDirection: 'row',
  },
  formText: {
    flex: 1,
    textAlign: 'center',
    fontFamily: appStyles.fontFamily,
  },
  passwordButtonText: {
    fontSize: 14,
    color: '#858585',
    fontFamily: appStyles.fontFamilyBold,
  },
  errorText: {
    color: colors.failRed,
    fontSize: 15,
    paddingTop: 15,
    fontFamily: appStyles.fontFamily,
  },
};

export default styles;
