import React, { Component } from 'react';
import empty from 'is-empty';
import { View, TextInput, TouchableOpacity, Text } from 'react-native';
import styles from './styles';

export default class LoginFields extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      text: '',
      hidePassword: false,
    };
    this.passwordButtonTouched = this.passwordButtonTouched.bind(this);
    this.getErrorText = this.getErrorText.bind(this);
  }

  getEmailField() {
    const changeText = (text) => {
      this.props.onEmailChange(text);
    };
    const submitEditing = () => {
      this.password.focus();
    };

    return (
      <TextInput
        underlineColorAndroid="transparent"
        ref={(c) => { this.email = c; }}
        style={styles.formText}
        placeholder={this.props.emailPlaceHolder}
        autoCorrect={false}
        autoFocus={this.props.autoFocusEmail ? this.props.autoFocusEmail : true}
        autoCapitalize="none"
        keyboardType="email-address"
        onChangeText={changeText}
        onSubmitEditing={submitEditing}
        testID="EmailInput"
      />
    );
  }

  getPasswordField() {
    const changeText = (text) => {
      this.props.onPasswordChange(text);
      const newState = {
        ...this.state,
      };
      newState.password = text;
      if (empty(text)) {
        this.state.hidePassword = false;
      }
      this.setState(newState);
    };
    const submitEditing = () => {
      this.props.submit();
    };

    return (
      <TextInput
        ref={(c) => { this.password = c; }}
        underlineColorAndroid="transparent"
        style={[styles.formText, styles.passwordField]}
        placeholder={this.props.passwordPlaceHolder}
        autoCorrect={false}
        autoCapitalize="none"
        keyboardType="email-address"
        secureTextEntry
        onChangeText={changeText}
        onSubmitEditing={submitEditing}
        testID="PasswordInput"
      />
    );
  }

  getPasswordButton() {
    return (
      <TouchableOpacity
        onPress={this.passwordButtonTouched}
        style={styles.passwordButton}
      >
        <Text style={styles.passwordButtonText} />
      </TouchableOpacity>
    );
  }

  getErrorText() {
    return this.props.messages ? React.createElement(Text, {
      style: [styles.errorText, this.props.errorTextStyle],
      testID: 'LoginErrorText'
    }, this.props.messages) : null;
  }

  passwordButtonTouched() {
    const newState = {
      ...this.state,
      hidePassword: !this.state.hidePassword,
    };
    this.setState(newState);
  }

  passwordButtonText() {
    return this.state.hidePassword ? 'SHOW' : 'HIDE';
  }

  render() {
    return (
      <View style={[styles.formContainer, this.props.formContainerStyle]}>
        <View style={[styles.formField, this.props.fieldStyle]}>
          {this.getEmailField()}
        </View>
        <View style={[styles.formField, this.props.fieldStyle]}>
          {this.getPasswordField()}
          {this.getPasswordButton()}
        </View>
        {this.getErrorText()}
      </View>
    );
  }
}
