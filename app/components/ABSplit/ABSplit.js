import React from 'react';
import { connect } from 'react-redux';

// Show a given component for an A or B user
// Pass in a custom split function or by default return the old component
const ABSplit = ({
  splitFunction = () => false,
  reduxAppState,
  NewComponent,
  OldComponent,
  ...props,
}) => (
  splitFunction(reduxAppState) ? <NewComponent {...props} /> : <OldComponent {...props} />
);

const mapStateToProps = state => ({ reduxAppState: state });

export default connect(mapStateToProps)(ABSplit);
