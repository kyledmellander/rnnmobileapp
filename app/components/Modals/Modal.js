import React from 'react';
import { connect } from 'react-redux';
import StoreRatingModal from './StoreRatingModal';
import modalTypes from './types';

// Show a modal based on the redx store visible Modal property
const Modal = ({ visibleModal, ...props }) => {
  switch (visibleModal) {
    case modalTypes.storeRatingModal:
      return (<StoreRatingModal {...props} />);
    default:
      return null;
  }
};

const mapStateToProps = ({ modals }) => ({ visibleModal: modals.visibleModal });

export default connect(mapStateToProps)(Modal);
