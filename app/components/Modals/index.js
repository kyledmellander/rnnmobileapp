import Modal from './Modal';
import modalTypes from './types';

export { modalTypes };
export default Modal;
