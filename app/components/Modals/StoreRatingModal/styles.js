import { Dimensions } from 'react-native';
import { colors, appStyles } from 'styles';

const { height: deviceHeight, width: deviceWidth } = Dimensions.get('window');

export default {
  modalBackground: {
    flexDirection: 'row',
    position: 'absolute',
    left: 0,
    top: 0,
    width: deviceWidth,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: '5%',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  modalContentContainer: {
    backgroundColor: 'white',
    flex: 1,
    paddingHorizontal: 20,
    paddingTop: 20,
    shadowRadius: 5,
    shadowColor: 'black',
    shadowOpacity: 0.5,
    shadowOffset: {
      width: 1,
      height: 3,
    }
  },
  modalBottom: {
    flexDirection: 'row',
    paddingVertical: 20,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  title: {
    textAlign: 'center',
    fontSize: 22,
    marginBottom: 5,
    fontFamily: appStyles.fontFamily,
    fontWeight: '500',
  },
  subTitle: {
    textAlign: 'center',
    fontSize: 16,
    fontFamily: appStyles.fontFamily,
    color: colors.textGray,
    fontWeight: '400',
  },
  feedbackContainer: {
    marginTop: 20,
    padding: 10,
    backgroundColor: '#f2f2f2'
  },
  starContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  titleContainer: {
    marginBottom: 30,
  },
  feedBack: {
    fontSize: 20,
    minHeight: 100,
    maxHeight: deviceHeight / 3
  },
  iconStyle: {
    fontSize: 40,
    color: colors.tavourOrange,
  },
  buttonText: {
    fontWeight: '300',
    color: colors.tavourOrange,
    fontSize: 22,
    marginLeft: 20,
  }
};

