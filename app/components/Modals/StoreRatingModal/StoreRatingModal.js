import React, { Component } from 'react';
import {
  Animated,
  BackHandler,
  Dimensions,
  Easing,
  Keyboard,
  Linking,
  Platform,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import * as StoreReview from 'react-native-store-review';

import Icon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';
import { colors } from 'styles';
import { actions as appRatingActions } from 'appRedux/appStoreRating';
import { actions as modalActions } from 'appRedux/modals';
import { getStoreURL } from 'lib/utils';
import styles from './styles';

const { height: deviceHeight } = Dimensions.get('window');

const star = (index, onPress) => (
  <TouchableOpacity key={`star_${index}`} onPress={onPress}>
    <Icon style={styles.iconStyle} name="star-o" />
  </TouchableOpacity>
);

class StoreRatingModal extends Component {
  constructor(props) {
    super(props);

    this.modalHeight = new Animated.Value(deviceHeight);
  }

  componentWillMount() {
    this.state = {
      rating: undefined,
      feedback: '',
      fadeAnim: new Animated.Value(0),
      slideAnim: new Animated.Value(25),
    };

    const keyboardShow = Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow';
    const keyboardHide = Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide';

    this.keyboardWillShowSub = Keyboard.addListener(keyboardShow, this.keyboardWillShow);
    this.keyboardWillHideSub = Keyboard.addListener(keyboardHide, this.keyboardWillHide);
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      this.onPressClose();
      return true;
    });

    Animated.parallel([
      Animated.timing(this.state.fadeAnim, {
        toValue: 1,
        duration: 200,
      }),
      Animated.timing(this.state.slideAnim, {
        toValue: 0,
        duration: 200,
        easing: Easing.ease,
      }),
    ]).start();
  }

  componentWillUnmount() {
    this.keyboardWillShowSub.remove();
    this.keyboardWillHideSub.remove();
    this.backHandler.remove();
  }

  onPressClose() {
    const { rating, feedback } = this.state;
    if (rating || feedback) {
      this.props.patchAppStoreRating({ rating, feedback });
    }

    Animated.parallel([
      Animated.timing(this.state.fadeAnim, {
        toValue: 0.01,
        duration: 200,
      }),
      Animated.timing(this.state.slideAnim, {
        toValue: 20,
        duration: 200,
        easing: Easing.ease,
      }),
    ]).start(this.props.hideModal);
  }

  onPressStar(rating) {
    this.props.patchAppStoreRating({ rating });

    if (rating > 3) {
      Linking.openURL(getStoreURL());
      this.onPressClose();
    } else {
      this.setState({ rating });
    }
  }

  onPressSave() {
    const { rating, feedback } = this.state;
    this.props.patchAppStoreRating({ rating, feedback });
    this.onPressClose();
  }

  keyboardWillShow = (event) => {
    const duration = (event && event.duration) ? event.duration : 0;

    Animated.parallel([
      Animated.timing(this.modalHeight, {
        duration,
        toValue: deviceHeight - event.endCoordinates.height,
      }),
    ]).start();
  };

  keyboardWillHide = (event) => {
    const duration = (event && event.duration) ? event.duration : 0;

    Animated.parallel([
      Animated.timing(this.modalHeight, {
        duration,
        toValue: deviceHeight,
      }),
    ]).start();
  };

  renderStars() {
    const ratingToStar = rating => star(rating, () => this.onPressStar(rating));

    return (
      <View style={styles.starContainer}>
        {[1, 2, 3, 4, 5].map(ratingToStar)}
      </View>
    );
  }

  renderFormOrFeedback() {
    const { rating } = this.state;

    return rating ? this.renderFeedback() : this.renderRatingForm();
  }

  renderFeedback() {
    const { slideAnim: marginTop, fadeAnim: opacity } = this.state;
    return (
      <Animated.View style={[styles.modalBackground, { opacity, height: this.modalHeight }]}>
        <Animated.View style={[styles.modalContentContainer, { marginTop }]}>
          <Text style={styles.title}>
            What can we improve?
          </Text>
          <View style={styles.feedbackContainer}>
            <TextInput
              onChangeText={feedback => this.setState({ feedback })}
              autoFocus
              style={styles.feedBack}
              multiline
              underlineColorAndroid={colors.tavourOrange}
            />
          </View>
          <View style={styles.modalBottom}>
            <TouchableOpacity onPress={() => this.onPressClose()}>
              <Text style={[styles.buttonText]}>
                CANCEL
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.onPressSave()}>
              <Text style={styles.buttonText}>
                SUBMIT
              </Text>
            </TouchableOpacity>
          </View>
        </Animated.View>
      </Animated.View>
    );
  }

  renderRatingForm() {
    const { slideAnim: marginTop, fadeAnim: opacity } = this.state;
    const storeName = Platform.OS === 'ios' ? 'App Store' : 'Play Store';
    return (
      <Animated.View style={[styles.modalBackground, { opacity, height: this.modalHeight }]}>
        <Animated.View style={[styles.modalContentContainer, { marginTop }]}>
          <View style={styles.titleContainer}>
            <Text style={styles.title}>
              Enjoying Tavour?
            </Text>
            <Text style={styles.subTitle}>
              Tap a star to rate us in the {storeName}.
            </Text>
          </View>
          {this.renderStars()}
          <View style={styles.modalBottom}>
            <TouchableOpacity onPress={() => this.onPressClose()}>
              <Text style={styles.buttonText}>
                CLOSE
              </Text>
            </TouchableOpacity>
          </View>
        </Animated.View>
      </Animated.View>
    );
  }

  render() {
    if (Platform.OS === 'ios' && StoreReview.isAvailable) {
      StoreReview.requestReview();
      return null;
    }

    return this.renderFormOrFeedback();
  }
}

function mapDispatchToProps(dispatch) {
  return {
    hideModal: () => dispatch(modalActions.hideModal()),
    patchAppStoreRating: rating => dispatch(appRatingActions.patchAppStoreRating(rating))
  };
}
export default connect(null, mapDispatchToProps)(StoreRatingModal);
