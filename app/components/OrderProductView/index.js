import React from 'react';
import { View, Text } from 'react-native';
import styles from './styles';

const orderProductView = (props) => {
  const style = props.first
    ? [styles.orderProduct, styles.orderProductBordered]
    : styles.orderProduct;
  const { orderProduct } = props;
  return (
    <View style={[style, props.style]} key={orderProduct.id}>
      <Text style={[styles.orderTitle, styles.orange]} onPress={props.onPress}>
        {' '}
        {orderProduct.name}{' '}
      </Text>
      <View style={styles.details}>
        <Text style={styles.text}>{`${orderProduct.offerProduct.product
          .producerLocation || ''}`}
        </Text>
        <Text style={[styles.text, styles.textCenter]}>
          {' '}
          {`$${orderProduct.unitPrice} ea`}{' '}
        </Text>
        <Text style={[styles.text, styles.textRight]}>
          {' '}
          {`Qty: ${orderProduct.unitQuantity}`}
        </Text>
      </View>
    </View>
  );
};

export default orderProductView;

export const orderProductBundleView = (props) => {
  const { orderProduct } = props;
  return (
    <View style={[styles.orderProduct, props.style]} key={orderProduct.id}>
      <Text style={styles.orderTitle}>{orderProduct.name} </Text>
      <View style={styles.details}>
        <Text style={[styles.text, styles.textCenter]}>
          {' '}
          {`$${orderProduct.unitPrice} ea`}{' '}
        </Text>
        <Text style={[styles.text, styles.textRight]}>
          {' '}
          {`Qty: ${orderProduct.unitQuantity}`}
        </Text>
      </View>
    </View>
  );
};
