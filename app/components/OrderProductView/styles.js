import { colors, appStyles } from '../../styles';

const styles = {
  orderTitle: {
    fontFamily: appStyles.appFamilySemibold,
    fontWeight: '500',
    fontSize: 12,
    paddingBottom: 5,
  },
  orderProduct: {
    paddingBottom: 11,
    paddingTop: 15,
    paddingHorizontal: 20,
    height: 65,
    flex: 1,
  },
  orderProductBordered: {
    borderTopWidth: 1,
    borderColor: colors.borderGray,
  },
  details: {
    flex: 0,
    paddingLeft: 4,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  text: {
    flex: 1,
    color: colors.textGray,
    fontSize: 12,
    fontFamily: appStyles.fontFamily,
  },
  textRight: {
    textAlign: 'right',
  },
  textCenter: {
    textAlign: 'center',
  },
  textLeft: {
    textAlign: 'left',
  },
  orange: {
    color: colors.tavourOrange,
  },
};

export default styles;
