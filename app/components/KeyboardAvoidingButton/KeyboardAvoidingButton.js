import React, { Component } from 'react';
import {
  KeyboardAvoidingView,
  TouchableOpacity,
  Text,
  View,
} from 'react-native';
import { getBottomSpace } from 'react-native-iphone-x-helper';
import { Navigation } from 'react-native-navigation';
import { colors } from 'styles';

const styles = {
  buttonText: {
    color: 'white',
    textColor: 'white',
    paddingLeft: 10,
    paddingRight: 10,
    placeholderTextColor: 'white',
  },
  buttonBackground: {
    position: 'relative',
    backgroundColor: 'white',
    width: '100%',
    height: '100%',
  },
  button: {
    position: 'relative',
    backgroundColor: colors.tavourOrange,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonContainer: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: getBottomSpace(),
    height: 50,
  }
};

export default class KeyboardAvoidingButton extends Component {
  componentWillMount() {
    this.state = {
      offset: 64,
    };

    Navigation.constants().then((constants) => {
      const { statusBarHeight, topBarHeight } = constants;

      this.setState({ offset: statusBarHeight + topBarHeight });
    });
  }

  getEnabledStyle() {
    const { enabled = true } = this.props;

    const backgroundColor = enabled ? colors.tavourOrange : colors.tavourLightGray;
    return { backgroundColor };
  }

  render() {
    const {
      testID,
      buttonText,
      onPress,
      enabled,
    } = this.props;
    const { offset } = this.state;

    return (
      <KeyboardAvoidingView
        behavior="position"
        style={styles.buttonContainer}
        keyboardVerticalOffset={offset}
      >
        <View style={styles.buttonBackground}>
          <TouchableOpacity
            testID={testID}
            activeOpacity={enabled ? 0.6 : 1}
            onPress={onPress}
            style={[styles.button, this.getEnabledStyle()]}
          >
            <Text style={styles.buttonText}>
              {buttonText.toUpperCase()}
            </Text>
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    );
  }
}
