import React, { Component } from 'react';
import { View, TouchableOpacity } from 'react-native';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome';

import styles from './styles';

export default class Accordion extends Component {
  constructor() {
    super();
    this.state = {
      expanded: false,
    };
    this.iconClicked = this.iconClicked.bind(this);
  }

  iconClicked() {
    this.setState(prevState => ({
      expanded: !prevState.expanded,
    }));
    if (this.props.iconClicked) {
      this.props.iconClicked();
    }
  }

  render() {
    const containerStyle = this.state.expanded
      ? [styles.accordion, styles.expanded, this.props.expandedStyle]
      : [styles.accordion, styles.collapsed, this.props.collapsedStyle];
    const contents = this.state.expanded ? this.props.content : null;
    const icon = this.state.expanded ? 'angle-up' : 'angle-down';
    return (
      <View style={containerStyle}>
        <TouchableOpacity
          style={[styles.titleContainer, this.props.titleContainerStyle]}
          onPress={this.iconClicked}
          activeOpacity={1}
        >
          {this.props.titleContent}
          <FontAwesomeIcons style={{ ...styles.icon, ...this.props.angleStyle }} color="black" name={icon} />
        </TouchableOpacity>
        {contents}
      </View>
    );
  }
}
