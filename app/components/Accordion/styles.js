import { colors, appStyles } from '../../styles';

const styles = {
  accordion: {
    flex: 1,
    borderBottomWidth: 1,
    borderColor: colors.borderGray,
  },
  collapsed: {
    height: 72,
    flex: 0,
    justifyContent: 'center',
  },
  expanded: {
    marginTop: 23,
    justifyContent: 'flex-start',
    paddingBottom: 30,
  },
  titleContainer: {
    height: 22,
    alignItems: 'center',
    flexDirection: 'row',
  },
  icon: {
    width: 21,
    height: 25,
    position: 'absolute',
    right: 35,
    fontSize: 30,
    fontWeight: '100',
  },
  title: {
    color: 'black',
    fontFamily: appStyles.fontFamilySemibold,
    fontSize: 16,
  },
  orderProduct: {
    paddingHorizontal: 20,
  },
};

export default styles;
