
const styles = {
  container: {
    height: 60,
    width: 60,
    borderRadius: 30,
  },
  photo: {
    height: 60,
    width: 60,
    borderRadius: 30,
  },
};

export default styles;
