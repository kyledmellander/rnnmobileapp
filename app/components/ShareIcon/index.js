import React from 'react';
import {
  Image,
  TouchableOpacity,
  Platform,
  Share,
} from 'react-native';
import { connect } from 'react-redux';
import { icons } from 'images';
import { actions as metricActions } from 'appRedux/metrics';
import styles from './styles';

class shareIcon extends React.Component {
  constructor(props) {
    super(props);
    this.shareTextWithTitle = this.shareTextWithTitle.bind(this);
    this.showResult = this.showResult.bind(this);
  }

  /* The following has been tested on Hollie's android phone,
     the share feature opens a drawer with the option to share
     with any installed app that opt-ed in. We may want to install
     facebook's more in-depth sharing api to support photos.

      https://developers.facebook.com/docs/react-native


   Top of share drawer menu:
     dialog title

    ----------

   Text Message:
    line1: title
    line2: message

   Hangouts:
    line1: message

   Slack:
    line1: message

   Email:
    Title: title
    body: message

  */
  shareTextWithTitle() {
    Share.share(
      {
        message: this.props.message,
        title: this.props.title,
      },
      {
        dialogTitle: this.props.dialogTitle,
      }
    )
      .then(this.showResult)
      .catch(err => console.log(err));
  }

  showResult() {
    this.props.addMetric(`appv4.${Platform.OS}.share.count`, 1);
  }

  render() {
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={this.shareTextWithTitle}
      >
        <Image style={styles.photo} source={icons.shareIcon} />
      </TouchableOpacity>
    );
  }
}

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {
    addMetric(...args) {
      dispatch(metricActions.addMetric(...args));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(shareIcon);
