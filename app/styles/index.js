const colors = {
  tavourOrange: '#DC8128',
  tavourLightGray: '#D3D3D3',
  tavourDarkGray: '#5E5E5E',
  borderGray: '#C8C7CB',
  disabledTextGray: '#818181',
  backgroundGray: '#EEEEEE',
  titleGray: '#606060',
  buttonGray: '#f2f2f2',
  successGreen: '#61A658',
  failRed: '#a62921',
  okGreen: '#61A658',
  textGray: '#545454',
  disabledButtonGrey: '#898989',
  transparent: 'rgba(0,0,0,0)',
  ccBlue: 'blue',
};

const appStyles = {
  marginTop: 20,
  paddingBottom: '20%',
  fontFamily: 'OpenSans',
  fontFamilySemibold: 'OpenSans-Semibold',
  fontFamilyBold: 'OpenSans-bold',
  fontFamilyLight: 'OpenSans-Light',
  buttonMinHeight: 50,
  buttonFontSize: 18,
  errorFontSize: 12,
  minorSpacing: 5,
  borderWidth: 1,
  paddedItemHorizontalPadding: 22,
  paddedItemVerticalPadding: 9,
};

const navigationBar = {
  navigationBar: {
    borderBottomWidth: 0,
    backgroundColor: colors.backgroundGray,
    opacity: 97,
    height: 70,
    paddingLeft: 10,
    paddingRight: 10,
  },
  transparent: {
    backgroundColor: 'transparent',
  },
  backButtonText: {
    marginTop: 10,
    textAlignVertical: 'center',
    alignSelf: 'center',
    color: colors.tavourOrange,
  },
  leftButtonText: {
    marginTop: 10,
    textAlignVertical: 'center',
    alignSelf: 'center',
    color: colors.tavourOrange,
  },
  leftButton: {
    marginTop: 5,
    tintColor: colors.tavourOrange,
  },
  hamburger: {
    marginTop: 10,
    height: 22,
    width: 22,
  },
  leftButtonWhite: {
    tintColor: 'white',
  },
  title: {
    fontSize: 16,
    color: colors.titleGray,
    fontFamily: appStyles.fontFamilySemibold,
    letterSpacing: 2,
  },
  titleDark: {
    fontSize: 18,
    color: 'black',
    width: 'auto',
    fontFamily: appStyles.fontFamily,
  },
  about: {
    backgroundColor: colors.tavourOrange,
    borderRadius: 50,
    marginTop: 5,
    height: 24,
    width: 24,
  },
  aboutText: {
    color: 'white',
    marginTop: -2,
    fontSize: 20,
    fontFamily: appStyles.fontFamilySemibold,
    backgroundColor: 'transparent',
    fontWeight: 'bold',
    textAlign: 'center',
    paddingLeft: 0,
  },
};

const borderedItem = {
  paddingLeft: appStyles.paddedItemHorizontalPadding,
  paddingRight: appStyles.paddedItemHorizontalPadding,
  paddingTop: appStyles.paddedItemVerticalPadding,
  paddingBottom: appStyles.paddedItemVerticalPadding,
  borderColor: colors.borderGray,
  width: '100%',
};

const { minorSpacing, borderWidth } = appStyles;

const commonStyles = {
  borderedItemTop: {
    ...borderedItem,
    borderTopWidth: borderWidth,
  },
  borderedItemBottom: {
    ...borderedItem,
    borderBottomWidth: borderWidth,
  },
  sectionTitle: {
    fontFamily: appStyles.fontFamily,
    fontSize: 16,
    margin: minorSpacing,
  },
  bigButtonContainer: {
    flex: 0,
    marginTop: 15,
    marginBottom: 5,
    width: '90%',
    marginLeft: '5%'
  },
  bigButton: {
    padding: 25
  },
};

export { colors, navigationBar, appStyles, commonStyles };
