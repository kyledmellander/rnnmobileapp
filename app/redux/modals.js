import modalTypes from 'components/Modals/types';

export const types = {
  showStoreRating: 'SHOW_STORE_RATING',
  hideModal: 'HIDE_MODAL',
};

export const initialState = {
  visibleModal: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.showStoreRating: {
      return {
        ...state,
        visibleModal: modalTypes.storeRatingModal,
      };
    }
    case types.hideModal: {
      return {
        ...state,
        visibleModal: '',
      };
    }
    default: {
      return state;
    }
  }
};

export const actions = {
  hideModal: () => ({ type: types.hideModal }),
  showStoreRating: () => ({ type: types.showStoreRating })
};

