import { AsyncStorage } from 'react-native';
import app from '../config/feathers';

export const types = {
  getOpenBasket: 'BASKETS__GET_OPEN_BASKET',
  updateBasket: 'BASKETS__UPDATE_BASKET',
  getClosedBaskets: 'BASKETS__GET_CLOSED_BASKETS',
  populateBasket: 'BASKETS__POPULATE_BASKET',
  loadMoreBaskets: 'BASKETS__LOAD_MORE_BASKETS',
  setHasSeen: 'BASKETS__SET_HAS_SEEN',
  getHasSeen: 'BASKETS__GET_HAS_SEEN',
  basketsError: 'BASKETS__BASKETS_ERROR',
  localStorageError: 'BASKETS__LOCAL_STORAGE_ERROR',
};

const pageSize = 5;

export const initialState = {
  openBasket: null,
  openShippingCycles: [],
  closedBaskets: [],
  basketPage: 0,
  canLoadMoreBaskets: true,
};

const convertShippedAt = (basket) => {
  basket.shippedAt = new Date(basket.shippedAt);
};

const dateifyShippingCycle = (shippingCycle) => {
  shippingCycle.shippingDate = new Date(shippingCycle.shippingDate);
  shippingCycle.closingDate = new Date(shippingCycle.closingDate);
  shippingCycle.lastDayToAddBeer = new Date(shippingCycle.lastDayToAddBeer);
  shippingCycle.pickupsReady = new Date(shippingCycle.pickupsReady);
  shippingCycle.closingDateForPickups = new Date(shippingCycle.closingDateForPickups);
  shippingCycle.lastDayToAddBeerPickups = new Date(shippingCycle.lastDayToAddBeerPickups);
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.getOpenBasket:
      if (action.basket) {
        dateifyShippingCycle(action.basket.shippingCycle);
      }
      if (action.openShippingCycles) {
        action.openShippingCycles.forEach(dateifyShippingCycle);
      }
      return {
        ...state,
        openBasket: action.basket,
        openShippingCycles: action.openShippingCycles,
      };

    case types.updateBasket:
      dateifyShippingCycle(action.basket.shippingCycle);
      return {
        ...state,
        openBasket: action.basket,
      };

    case types.getClosedBaskets:
      action.closedBaskets.map(convertShippedAt);
      return {
        ...state,
        closedBaskets: action.closedBaskets,
        canLoadMoreBaskets: action.closedBaskets.length >= pageSize,
      };

    case types.loadMoreBaskets: {
      const newBaskets = state.closedBaskets.concat(action.additionalBaskets);
      newBaskets.map(convertShippedAt);
      const basketPage = state.basketPage + 1;
      const canLoadMoreBaskets = action.additionalBaskets.length === pageSize;
      return {
        ...state,
        closedBaskets: newBaskets,
        canLoadMoreBaskets,
        basketPage,
      };
    }

    case types.populateBasket: {
      const closedBaskets = state.closedBaskets.slice();
      const match = closedBaskets.findIndex(cb => cb.id === action.populatedBasket.id);
      if (match !== -1) {
        action.populatedBasket.shippedAt = new Date(action.populatedBasket.shippedAt);
        closedBaskets[match] = action.populatedBasket;
      }
      return {
        ...state,
        closedBaskets,
      };
    }
    case types.getHasSeen:
      return {
        ...state,
        hasSeen: action.hasSeen,
      };

    case types.setHasSeen:
      return {
        ...state,
      };

    case types.basketsError:
      return {
        ...state,
      };

    case types.localStorageError:
      return {
        ...state,
      };

    default:
      return state;
  }
};

export const actions = {
  getOpenBasket: () => async (dispatch) => {
    try {
      const basketService = app.service('baskets');
      const baskets = await basketService.find({ query: { open: true } });
      const openBasket =
          baskets.length !== 0 ?
            await basketService.get(baskets[0].id) : null;
      const openShippingCycles = openBasket
        ? await app
          .service('shippingCycles')
          .find({ query: { basketId: openBasket.id } })
        : [];
      dispatch({
        type: types.getOpenBasket,
        basket: openBasket,
        openShippingCycles,
      });
    } catch (e) {
      dispatch({
        type: types.basketsError,
        value: e.message,
      });
      throw e;
    }
  },

  updateBasket: (basket, markedFor, shippingCycle) => async (dispatch) => {
    try {
      const basketService = app.service('baskets');
      const data = { shippingCycle, markedFor };
      const updatedBasket = await basketService.update(basket.id, data);
      dispatch({
        type: types.updateBasket,
        basket: updatedBasket,
      });
    } catch (e) {
      dispatch({
        type: types.basketsError,
        value: e.message,
      });
      throw e;
    }
  },

  getClosedBaskets: () => async (dispatch) => {
    try {
      const basketService = app.service('baskets');
      const baskets = await basketService.find({ query: { open: false } });
      dispatch({
        type: types.getClosedBaskets,
        closedBaskets: baskets,
      });
    } catch (e) {
      dispatch({
        type: types.basketsError,
        value: e.message,
      });
      throw e;
    }
  },

  populateBasket: closedBasket => async (dispatch) => {
    try {
      const basketService = app.service('baskets');
      const basket = await basketService.get(closedBasket.id);
      dispatch({
        type: types.populateBasket,
        populatedBasket: basket,
      });
    } catch (e) {
      dispatch({
        type: types.basketsError,
        value: e.message,
      });
      throw e;
    }
  },

  loadMoreBaskets: page => async (dispatch) => {
    try {
      const basketService = app.service('baskets');
      const baskets = await basketService.find({
        query: { open: false, page: { number: page, size: pageSize } },
      });
      dispatch({
        type: types.loadMoreBaskets,
        additionalBaskets: baskets,
      });
    } catch (e) {
      dispatch({
        type: types.basketsError,
        value: e.message,
      });
      throw e;
    }
  },

  getHasSeen: () => async (dispatch) => {
    try {
      let hasSeen = await AsyncStorage.getItem('hasSeenNewUserDetails');
      hasSeen = hasSeen === 'yes';
      dispatch({
        type: types.getHasSeen,
        hasSeen,
      });
    } catch (e) {
      dispatch({
        type: types.localStorageError,
        value: e.message,
      });
    }
  },

  setHasSeen: () => async (dispatch) => {
    try {
      AsyncStorage.setItem('hasSeenNewUserDetails', 'yes');
      dispatch({
        type: types.setHasSeen,
      });
    } catch (e) {
      dispatch({
        type: types.localStorageError,
        value: e.message,
      });
    }
  },
};
