import app from '../config/feathers';

export const types = {
  getProducts: 'PRODUCTS__GET_PRODUCTS',
  getUnratedProducts: 'PRODUCTS__GET_UNRATED_PRODUCTS',
  searchProducts: 'PRODUCTS__SEARCH_PRODUCTS',
  updateUnratedProducts: 'PRODUCTS__UPDATE_UNRATED_PRODUCTS',
  updateProductRating: 'PRODUCTS__UPDATE_PRODUCT_RATING',
  productsError: 'PRODUCTS__PRODUCTS_ERROR',
};

export const keys = {
  name: 'name',
  style: 'style',
  brewery: 'producer',
};

const pageSize = 10;
const defaultMeta = { totalProducts: 0, totalRatings: 0, totalResults: 0 };
/*
  Match Object Schema:
  {
    beers: Array<Product>, set of Products which match the search term
    totalMatches: int, total # of matching Products in the db
    page: int, current page of results
    canLoadMore: bool, indicative of more results available in the db,
    term: search term used
  }
*/

export const initialState = {
  products: {},
  unratedProducts: {},
  nameMatches: {},
  breweryMatches: {},
  styleMatches: {},
};

function mapMatches(matches, state, key, page, term) {
  const matchObj = {
    totalMatches: matches[0] ? matches[0].meta.totalResults : 0,
    page,
    canLoadMore: matches.length >= pageSize,
  };

  switch (key) {
    case keys.name:
      return {
        ...state,
        nameMatches: {
          beers:
            state.nameMatches.beers && term === state.nameMatches.term
              ? state.nameMatches.beers.concat(matches)
              : matches,
          ...matchObj,
          term,
        },
      };
    case keys.brewery:
      return {
        ...state,
        breweryMatches: {
          beers:
            state.breweryMatches.beers && term === state.nameMatches.term
              ? state.breweryMatches.beers.concat(matches)
              : matches,
          ...matchObj,
          term,
        },
      };
    case keys.style:
      return {
        ...state,
        styleMatches: {
          beers:
            state.styleMatches.beers && term === state.nameMatches.term
              ? state.styleMatches.beers.concat(matches)
              : matches,
          ...matchObj,
          term,
        },
      };
    default:
      return {
        ...state,
      };
  }
}

function mapProducts(state, property, total, action) {
  const beers = (state[property].beers || []).concat(action.products);
  const { page } = action;
  const canLoadMore = action.products.length >= pageSize;
  const newState = {
    ...state,
  };
  newState[property] = {
    beers,
    total,
    page,
    canLoadMore,
  };
  return newState;
}

function updateUnratedProducts(state, action) {
  const beersFromUnrated = state.unratedProducts.beers
    ? state.unratedProducts.beers
    : [];
  const match = beersFromUnrated.find(p => p.id === action.product.id);
  const newUnratedProducts = {
    ...state.unratedProducts,
  };
  if (match) {
    newUnratedProducts.beers = beersFromUnrated.filter(p => p.id !== match.id);
    newUnratedProducts.total -= 1;
  }
  return newUnratedProducts;
}

function updateProductRating(state, action) {
  const beers = state.products.beers ? state.products.beers : [];
  const match = beers.find(p => p.id === action.product.id);
  if (match) {
    match.rating = action.product.rating;
  }
}

export default (state = initialState, action) => {
  let total;
  let meta;
  switch (action.type) {
    case types.getProducts:
      meta = action.products[0] ? action.products[0].meta : defaultMeta;
      total = meta.totalProducts;
      return mapProducts(state, 'products', total, action);
    case types.getUnratedProducts:
      meta = action.products[0] ? action.products[0].meta : defaultMeta;
      total = meta.totalProducts - meta.totalRatings;
      return mapProducts(state, 'unratedProducts', total, action);
    case types.searchProducts:
      return mapMatches(
        action.matches,
        state,
        action.key,
        action.page,
        action.term
      );
    case types.updateUnratedProducts:
      return {
        ...state,
        unratedProducts: updateUnratedProducts(state, action),
      };
    case types.updateProductRating:
      updateProductRating(state, action);
      return {
        ...state,
      };
    case types.searchProductsError:
      return {
        ...state,
      };

    default:
      return state;
  }
};

function getQueryParam(number, filter) {
  const query = {
    page: {
      size: pageSize,
      number,
    },
    filter,
  };
  return { query };
}

export const actions = {
  getProducts: page => async (dispatch) => {
    try {
      const products = await app
        .service('products')
        .find(getQueryParam(page));
      dispatch({
        type: types.getProducts,
        page,
        products,
      });
    } catch (e) {
      dispatch({
        type: types.productsError,
      });
      throw e;
    }
  },
  getUnratedProducts: page => async (dispatch) => {
    try {
      const products = await app
        .service('products')
        .find(getQueryParam(page, { rated: false }));
      dispatch({
        type: types.getUnratedProducts,
        page,
        products,
      });
    } catch (e) {
      dispatch({
        type: types.productsError,
      });
      throw e;
    }
  },
  updateUnratedProducts: product => (dispatch) => {
    dispatch({
      type: types.updateUnratedProducts,
      product,
    });
  },
  updateProductRating: product => (dispatch) => {
    dispatch({
      type: types.updateProductRating,
      product,
    });
  },
  searchProducts: (page, term, key) => async (dispatch) => {
    try {
      const filter = {};
      filter[key] = encodeURI(term);
      const matches = await app
        .service('products')
        .find(getQueryParam(page, filter));
      dispatch({
        type: types.searchProducts,
        matches,
        key,
        page,
        term,
      });
    } catch (e) {
      dispatch({
        type: types.productsError,
      });
      throw e;
    }
  },
};
