import app from '../config/feathers';

export const parsePhone = (text) => {
  let newText = text;
  const numbers = text.replace(/\D/g, '');
  if (numbers.length <= 3 || numbers.length > 10) {
    newText = `${numbers}`;
  } else if (numbers.length > 3 && numbers.length < 7) {
    newText = `(${numbers.slice(0, 3)}) ${numbers.slice(3, numbers.length)}`;
  } else if (numbers.length > 6 && numbers.length < 11) {
    newText = `(${numbers.slice(0, 3)}) ${numbers.slice(3, 6)}-${numbers.slice(6, 10)}`;
  }
  return newText;
};

export const types = {
  resetStore: 'ADDRESS_FORM__RESET_STORE',
  setIsLoadingCity: 'ADDRESS_FORM__SET_IS_LOADING_CITY',
  setupFromAddress: 'ADDRESS_FORM__SETUP_FROM_ADDRESS',
  setAddressType: 'ADDRESS_FORM__SET_ADDRESS_TYPE',
  setName: 'ADDRESS_FORM__SET_NAME',
  setPhone: 'ADDRESS_FORM__SET_PHONE',
  setLine1: 'ADDRESS_FORM__SET_LINE_1',
  setLine2: 'ADDRESS_FORM__SET_LINE_2',
  setCity: 'ADDRESS_FORM__SET_CITY',
  setZip: 'ADDRESS_FORM__SET_ZIP',
  setState: 'ADDRESS_FORM__SET_STATE',
};

export const initialState = {
  isLoadingCity: false,
  addressType: 'Commercial',
  name: '',
  phone: '',
  line1: '',
  line2: '',
  city: '',
  zip: '',
  state: '',
  id: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.resetStore:
      return { ...initialState };
    case types.setIsLoadingCity:
      return { ...state, isLoadingCity: action.isLoadingCity };
    case types.setupFromAddress: {
      const { address } = action;
      return {
        ...state,
        name: address.name,
        phone: parsePhone(address.phone),
        line1: address.line1,
        line2: address.line2,
        city: address.city,
        zip: address.zip,
        state: address.state,
        id: address.id,
      };
    }
    case types.setAddressType:
      return { ...state, addressType: action.addressType };
    case types.setName:
      return { ...state, name: action.name };
    case types.setPhone:
      return { ...state, phone: parsePhone(action.phone) };
    case types.setLine1:
      return { ...state, line1: action.line1 };
    case types.setLine2:
      return { ...state, line2: action.line2 };
    case types.setCity:
      return { ...state, city: action.city };
    case types.setZip:
      return { ...state, zip: action.zip };
    case types.setState:
      return { ...state, state: action.state };
    default:
      return state;
  }
};

export const actions = {
  resetStore: () => async (dispatch) => { dispatch({ type: types.resetStore }); },
  setupFromAddress: address => async (dispatch) => {
    dispatch({ type: types.setupFromAddress, address });
  },
  getCityFromZip: zip => async (dispatch) => {
    dispatch({ type: types.setIsLoadingCity, isLoadingCity: true });
    try {
      dispatch({ type: types.setIsLoadingCity, isLoadingCity: true });
      const zipInfo = await app.service('zipCodes').get(zip);
      dispatch({ type: types.setCity, city: zipInfo.city });
      dispatch({ type: types.setState, state: zipInfo.stateCode });
    } catch (e) { /* catch error so doesn't crash, just let the user write in the city */ }
    dispatch({ type: types.setIsLoadingCity, isLoadingCity: false });
  },
  setAddressType: addressType => async (dispatch) => {
    dispatch({ type: types.setAddressType, addressType });
  },
  setName: name => async (dispatch) => { dispatch({ type: types.setName, name }); },
  setPhone: phone => async (dispatch) => { dispatch({ type: types.setPhone, phone }); },
  setLine1: line1 => async (dispatch) => { dispatch({ type: types.setLine1, line1 }); },
  setLine2: line2 => async (dispatch) => { dispatch({ type: types.setLine2, line2 }); },
  setCity: city => async (dispatch) => { dispatch({ type: types.setCity, city }); },
  setZip: zip => async (dispatch) => {
    dispatch({ type: types.setZip, zip });
    if (zip.length === 5) {
      await actions.getCityFromZip(zip)(dispatch);
    }
  },
};
