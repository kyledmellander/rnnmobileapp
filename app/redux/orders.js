import analytics from 'lib/analytics';
import app from '../config/feathers';

export const types = {
  createOrder: 'ORDERS__CREATE_ORDER',
  getOrders: 'ORDERS__GET_ORDERS',
  clearOrdersForOffer: 'ORDERS__CLEAR_ORDERS_FOR_OFFER',
  clearOrderStatus: 'ORDERS__CLEAR_ORDER_STATUS',
  ordersError: 'ORDERS__ORDERS_ERROR',
};

export const initialState = {
  createdOrder: false,
  order: null,
  ordersForOffer: null,
  ordersPage: 0,
  canLoadMoreOrders: true,
  totalProducts: 0,
  totalUnratedProducts: 0,
  errors: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.createOrder:
      return {
        ...state,
        createdOrder: true,
        order: action.order,
        errors: null,
      };
    case types.getOrders:
      return {
        ...state,
        ordersForOffer: action.orders,
      };
    case types.ordersError:
      return {
        ...state,
        createdOrder: false,
        errors: action.errors,
      };
    case types.clearOrdersForOffer:
      return {
        ...state,
        ordersForOffer: null,
      };
    case types.clearOrderStatus:
      return {
        ...state,
        createdOrder: false,
        order: null,
        errors: null,
      };
    default:
      return state;
  }
};

export const actions = {
  createOrder: data => async (dispatch) => {
    try {
      const order = await app.service('orders').create(data);
      dispatch({
        type: types.createOrder,
        order,
      });
      const event = order.state === 'waitlisted' ? 'Waitlist Creation' : 'Order Creation';
      analytics.track({
        event,
        properties: {
          orderId: order.id,
          offerId: order.offerId,
        }
      });
    } catch (e) {
      dispatch({
        type: types.ordersError,
        errors: e.errors,
      });
      analytics.track({ event: 'Order Creation Error' });
    }
  },
  clearOrderStatus: () => async (dispatch) => {
    dispatch({
      type: types.clearOrderStatus,
    });
  },
  getOrders: offer => async (dispatch) => {
    dispatch({
      type: types.clearOrdersForOffer,
    });
    try {
      const orders = await app
        .service('orders')
        .find({ query: { offerId: offer.id } });
      offer.orders = orders;
      dispatch({
        type: types.getOrders,
        orders,
      });
    } catch (e) {
      if (e.name === 'NotFound') {
        dispatch({
          type: types.getOrders,
          orders: [],
        });
      }
    }
  },
};
