import store from 'app/config/store';
import * as navUtils from 'lib/navUtils';

import app from '../config/feathers';

const DEFAULT_ERROR_MESSAGE = 'Error encountered, please try again later.';

export const types = {
  redeemCode: 'USER_PROMO__REDEEM_CODE',
  redeemedCode: 'USER_PROMO__REDEEMED_CODE',
  resetStore: 'USER_PROMO__RESET_STORE',
  redeemError: 'USER_PROMO__ERROR',
  setCode: 'USER_PROMO__SET_CODE',
};

const initialState = {
  code: '',
  errors: [],
  loading: false,
  newUserPromoId: 0
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.redeemCode: {
      return { ...state, loading: true, errors: [] };
    }
    case types.redeemedCode: {
      return {
        ...state,
        loading: false,
        errors: [],
        newUserPromoId: action.userPromoId
      };
    }
    case types.setCode: {
      return { ...state, code: action.code };
    }
    case types.resetStore:
      return { ...initialState };
    case types.redeemError: {
      return {
        ...state,
        errors: action.errors,
        loading: false,
      };
    }
    default:
      return state;
  }
};

export const actions = {
  redeem: () => async (dispatch) => {
    const state = store.getState();
    const { code } = state.userPromos;
    try {
      dispatch({ type: types.redeemCode });
      const userPromo = await app.service('userPromos').create({ code });
      dispatch({ type: types.redeemedCode, userPromoId: userPromo.id });
      navUtils.pop();
    } catch (e) {
      const errors = e.errors && e.errors.length ? e.errors : [DEFAULT_ERROR_MESSAGE];
      dispatch({ type: types.redeemError, errors });
    }
  },
  resetStore: () => async (dispatch) => {
    dispatch({
      type: types.resetStore
    });
  },
  setCode: code => async (dispatch) => {
    dispatch({ type: types.setCode, code });
  }
};
