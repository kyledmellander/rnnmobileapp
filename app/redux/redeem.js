import store from 'app/config/store';
import * as navUtils from 'lib/navUtils';
import app from '../config/feathers';

const DEFAULT_ERROR_MESSAGE = 'Unrecognized Code.';

export const types = {
  redeemCode: 'REDEEM__REDEEM_CODE',
  redeemedCode: 'REDEEM__REDEEMED_CODE',
  resetStore: 'REDEEM__RESET_STORE',
  redeemError: 'REDEEM__ERROR',
  setCode: 'REDEEM__SET_CODE',
  openRedeemScreen: 'REDEEM__OPEN_REDEEM_SCREEN',
};

const initialState = {
  code: '',
  errors: [],
  loading: false,
  onSuccess: () => { /* noop */ }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.openRedeemScreen: {
      return {
        ...initialState,
        onSuccess: action.onSuccess
      };
    }
    case types.redeemCode: {
      return { ...state, loading: true, errors: [] };
    }
    case types.redeemedCode: {
      return {
        ...state,
        loading: false,
        errors: []
      };
    }
    case types.setCode: {
      return { ...state, code: action.code };
    }
    case types.redeemError: {
      return {
        ...state,
        errors: action.errors,
        loading: false,
      };
    }
    default:
      return state;
  }
};

export const actions = {
  redeem: () => async (dispatch) => {
    const state = store.getState();
    const { code, onSuccess } = state.redeem;
    try {
      dispatch({ type: types.redeemCode });
      try {
        await app.service('userPromos').create({ code });
      } catch (e) {
        if (e.data && e.data.importantError) { throw e; }
        // if not a promo-cde lets try as a gift card
        await app.service('creditEvents').create({ code });
      }
      dispatch({ type: types.redeemedCode });
      onSuccess();
    } catch (e) {
      const errors = e.errors && e.errors.length ? e.errors : [DEFAULT_ERROR_MESSAGE];
      dispatch({ type: types.redeemError, errors });
    }
  },
  setCode: code => async (dispatch) => {
    dispatch({ type: types.setCode, code });
  },
  // onSuccess should navigate user to the desired screen
  openRedeemScreen: onSuccess => async (dispatch) => {
    dispatch({ type: types.openRedeemScreen, onSuccess });
    navUtils.pushScreen('Redeem');
  }
};

