import app from '../config/feathers';

export const types = {
  getInitData: 'PROMOS__GET_INIT_DATA',
  getInitDataSuccess: 'PROMOS__GET_INIT_DATA_SUCCESS',
  clearStatus: 'PROMOS__CLEAR_STATUS',
  error: 'PROMOS__PROMOS_ERROR',
};

const initialState = {
  userPromos: [],
  loading: false,
  error: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.getInitDataSuccess: {
      return {
        ...state,
        errors: false,
        loading: false,
        userPromos: action.userPromos,
      };
    }
    case types.getInitData:
      return { ...state, error: false, loading: true };
    case types.error:
      return { ...state, error: true, loading: false };
    case types.clearStatus:
      return {
        ...state,
        success: false,
        errors: null,
      };
    default:
      return state;
  }
};

export const actions = {
  getInitData: () => async (dispatch) => {
    try {
      dispatch({ type: types.getInitData });
      const userPromos = await app.service('userPromos').find();
      dispatch({ type: types.getInitDataSuccess, userPromos });
    } catch (e) {
      dispatch({ type: types.error });
    }
  },
  clearStatus: () => async (dispatch) => {
    dispatch({
      type: types.clearStatus
    });
  },
};
