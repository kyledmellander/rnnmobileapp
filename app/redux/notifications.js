import pushNotifications from '../config/pushNotifications';
import app from '../config/feathers';

export const types = {
  toggleNotifications: 'NOTIFICATIONS__TOGGLE_NOTIFICATIONS',
  toggleAppNotifications: 'NOTIFICATIONS__TOGGLE_APP_NOTIFICATIONS',
  toggleEmail: 'NOTIFICATIONS__TOGGLE_EMAIL',
  toggleNotificationsError: 'NOTIFICATIONS__TOGGLE_NOTIFICATIONS_ERROR',
  toggleAppNotificationsError: 'NOTIFICATIONS__TOGGLE_APP_NOTIFICATIONS_ERROR',
  toggleEmailError: 'NOTIFICATIONS__TOGGLE_EMAIL_ERROR',
  getNotificationSettings: 'NOTIFICATIONS__GET_NOTIFICATION_SETTINGS',
  setEmailLoading: 'NOTIFICATIONS__SET_EMAIL_LOADING',
  setNotificationsLoading: 'NOTIFICATIONS__SET_NOTIFICATIONS_LOADING',
  setAppNotificationsLoading: 'NOTIFICATIONS__SET_APP_NOTIFICATIONS_LOADING',
};

const initialState = {
  notificationsEnabled: false,
  emailEnabled: false,
  appNotificationsEnabled: false,
  notificationsLoading: true,
  emailLoading: true,
  appNotificationsLoading: true
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.getNotificationSettings:
      return {
        ...state,
        notificationsEnabled: action.notificationsEnabled,
        emailEnabled: action.emailEnabled,
        appNotificationsEnabled: action.appNotificationsEnabled,
        notificationsLoading: false,
        emailLoading: false,
        appNotificationsLoading: false
      };
    case types.toggleNotifications:
      return {
        ...state,
        notificationsEnabled: action.notificationsEnabled,
        notificationsLoading: false,
      };
    case types.toggleAppNotifications:
      return {
        ...state,
        appNotificationsEnabled: action.appNotificationsEnabled,
        appNotificationsLoading: false,
      };
    case types.toggleEmail:
      return {
        ...state,
        emailEnabled: action.emailEnabled,
        emailLoading: false,
      };
    case types.setNotificationsLoading:
      return {
        ...state,
        notificationsLoading: true,
      };
    case types.setAppNotificationsLoading:
      return {
        ...state,
        appNotificationsLoading: true,
      };
    case types.setEmailLoading:
      return {
        ...state,
        emailLoading: true,
      };
    case types.toggleNotificationsError:
      return {
        ...state,
        notificationsLoading: false,
      };
    case types.toggleAppNotificationsError:
      return {
        ...state,
        appNotificationsLoading: false,
      };
    case types.toggleEmailError:
      return {
        ...state,
        emailLoading: false,
      };
    default:
      return state;
  }
};

export const actions = {
  toggleNotifications: () => async (dispatch) => {
    try {
      dispatch({
        type: types.setNotificationsLoading,
      });
      const action = {
        type: types.toggleNotifications,
      };
      if (await pushNotifications.notificationsEnabled()) {
        await pushNotifications.setNotificationsEnabled(false);
        action.notificationsEnabled = false;
      } else {
        await pushNotifications.setNotificationsEnabled(true);
        action.notificationsEnabled = true;
      }
      dispatch(action);
    } catch (e) {
      dispatch({
        type: types.toggleNotificationsError,
      });
    }
  },

  toggleEmail: () => async (dispatch) => {
    try {
      dispatch({
        type: types.setEmailLoading,
      });
      const value = await app.service('subscriptions').update(1, {});
      dispatch({
        type: types.toggleEmail,
        emailEnabled: value,
      });
    } catch (e) {
      dispatch({
        type: types.toggleEmailError,
      });
    }
  },

  toggleAppNotifications: () => async (dispatch) => {
    try {
      dispatch({
        type: types.setAppNotificationsLoading
      });
      const preferences = await app.service('preferences').update(1, {
        appNotifications: true
      });
      dispatch({
        type: types.toggleAppNotifications,
        appNotificationsEnabled: preferences.appNotifications
      });
    } catch (e) {
      dispatch({
        type: types.toggleAppNotificationsError
      });
    }
  },

  getNotificationSettings: () => async (dispatch) => {
    const emailEnabled = (await app.service('subscriptions').find()) || false;
    const notificationsEnabled = await pushNotifications.notificationsEnabled();
    const preferences = (await app.service('preferences').find()) || false;
    dispatch({
      type: types.getNotificationSettings,
      notificationsEnabled,
      emailEnabled,
      appNotificationsEnabled: preferences.appNotifications
    });
  },
};
