import app from '../config/feathers';

export const types = {
  addMetric: 'METRICS__ADD_METRIC',
  metricsError: 'METRICS__METRICS_ERROR',
};

const getTimestamp = () => Math.round(new Date().getTime() / 1000);

const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.metricsError:
      return {
        ...state,
        errors: action.errors,
      };
    case types.addMetric:
      return {
        ...state,
      };
    default:
      return state;
  }
};

export const actions = {
  addMetric: (path, value, timestamp) => async (dispatch) => {
    try {
      const service = app.service('metrics');
      if (!timestamp) {
        await service.create({ path, value, timestamp: getTimestamp() });
      } else {
        await service.create({ path, value, timestamp });
      }
      dispatch({
        type: types.addMetric,
      });
    } catch (e) {
      dispatch({
        type: types.metricsError,
      });
    }
  },
};
