import app from '../config/feathers';

export const types = {
  setMessages: 'RESET_PASSWORD__SET_MESSAGES',
  resetStore: 'RESET_PASSWORD__RESET_STORE'
};

const initialState = {
  messages: { errorMessage: '', successMessage: '' },
  isWaiting: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.setMessages: {
      const messages = {
        errorMessage: '',
        successMessage: '',
        ...action.messages
      };
      return {
        ...state,
        messages,
        isWaiting: action.isWaiting
      };
    }
    case types.resetStore:
      return { ...initialState };
    default:
      return state;
  }
};

export const actions = {
  resetPassword: email => async (dispatch) => {
    dispatch({
      type: types.setMessages,
      messages: {}, // set back to default
      isWaiting: true
    });
    let messages = {};
    try {
      const response = await app.service('resetPassword').create({ email });
      messages = {
        successMessage: `Password reset instructions have been sent to ${response.email}.`,
        errorMessage: response.errorMsg
      };
    } catch (e) {
      messages.errorMessage = 'Unable to reset password. Please try again later.';
    }
    dispatch({
      type: types.setMessages,
      messages,
      isWaiting: false
    });
  },
};
