import { createSelector } from 'reselect';
import app from 'config/feathers';

export const types = {
  fetchingExperiments: 'FETCHING_EXPERIMENTS',
  fetchingExperimentsFailure: 'FETCHING_EXPERIMENTS_FAILURE',
  fetchingExperimentsSuccess: 'FETCHING_EXPERIMENTS_SUCCESS',
};

export const initialState = {
  fetchingExperiments: false,
  userExperiments: [],
  errorMessage: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.fetchingExperiments:
      return {
        ...state,
        loading: true,
        errorMessage: '',
        fetchingExperiments: true
      };
    case types.fetchingExperimentsFailure:
      return {
        ...initialState,
        errorMessage: 'Error fetching user information',
      };
    case types.fetchingExperimentsSuccess: {
      const { experiments } = action.payload;

      return {
        ...state,
        userExperiments: experiments,
        errorMessage: '',
        fetchingExperiments: false,
      };
    }
    default:
      return state;
  }
};

export const actions = {
  fetchExperiments: userId => async (dispatch) => {
    try {
      dispatch({ type: types.fetchingExperiments });

      const experiments = await app.service('userExperiments')
        .find({ query: { userId } });

      dispatch({
        type: types.fetchingExperimentsSuccess,
        payload: { experiments },
      });
    } catch (e) {
      dispatch({ type: types.fetchingExperimentsFailure });
      throw e;
    }
  },
};

function showNewPurchaseFlow(userExperiment) {
  const { experiment: { name }, variant } = userExperiment;
  return name === 'new_purchase_flow' && variant === 'new';
}

const userExperimentSelector = state => state.experiments.userExperiments;
const newPurchaseFlowSelector = createSelector(
  userExperimentSelector,
  userExperiments =>
    userExperiments.reduce((showNew, userExp) => showNew || showNewPurchaseFlow(userExp), false)
);

export const selectors = {
  userExperimentSelector,
  newPurchaseFlowSelector,
};
