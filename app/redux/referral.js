import app from '../config/feathers';

export const types = {
  getInitData: 'REFERRALS__GET_INIT_DATA',
  getInitDataSuccess: 'REFERRALS__GET_INIT_DATA_SUCCESS',
  clearStatus: 'REFERRALS__CLEAR_STATUS',
  error: 'REFERRALS__REFERRALS_ERROR',
};

const initialState = {
  userReferralPromo: {},
  loading: false,
  error: false,
  descriptions: [],
  code: '',
  giftCardLink: '',
  shareText: {
    title: '',
    message: '',
    dialogTitle: '',
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.getInitDataSuccess: {
      return {
        ...state,
        errors: false,
        loading: false,
        code: action.code,
        descriptions: action.descriptions,
        shareText: action.shareText,
        giftCardLink: action.giftCardLink
      };
    }
    case types.getInitData:
      return { ...state, error: false, loading: true };
    case types.error:
      return { ...state, error: true, loading: false };
    default:
      return state;
  }
};

export const actions = {
  getInitData: () => async (dispatch) => {
    try {
      dispatch({ type: types.getInitData });
      const userReferralPromo = await app.service('userReferralPromo').find();
      const {
        descriptions,
        code,
        shareText, // object in the form of { title, message, dialogTitle }
        giftCardLink
      } = userReferralPromo;
      dispatch({
        type: types.getInitDataSuccess,
        descriptions,
        code,
        shareText,
        giftCardLink
      });
    } catch (e) {
      dispatch({ type: types.error });
    }
  },
  clearStatus: () => async (dispatch) => {
    dispatch({
      type: types.clearStatus
    });
  },
};

