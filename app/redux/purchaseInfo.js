import app from 'config/feathers';

import { types as addressTypes } from './addresses';
import { types as orderTypes } from './orders';
import { types as paymentMethodTypes } from './paymentMethods';

export const types = {
  fetchingPurchaseInfo: 'FETCHING_PURCHASE_INFO',
  fetchingPurchaseInfoFailure: 'FETCHING_PURCHASE_INFO_FAILURE',
  fetchingPurchaseInfoSuccess: 'FETCHING_PURCHASE_INFO_SUCCESS',
};

export const initialState = {
  loading: false,

  needsRefresh: true,

  hasValidAddress: false,
  hasValidCreditCard: false,

  fetchingPurchaseInfo: false,
  creditBalance: undefined,
  taxRate: undefined,
  message: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case addressTypes.removeAddress: {
      return initialState;
    }
    case orderTypes.createOrder: {
      const { creditBalance } = state;

      // Refresh if user had a credit and placed an order
      if (creditBalance !== 0 && creditBalance !== undefined) return initialState;
      return state;
    }
    case addressTypes.addAddress: {
      return initialState;
    }
    case paymentMethodTypes.removePaymentMethod: {
      return initialState;
    }
    case paymentMethodTypes.addPaymentMethod: {
      return initialState;
    }
    case types.fetchingPurchaseInfo:
      return {
        ...state,
        loading: true,
        message: '',
        fetchingPurchaseInfo: true
      };
    case types.fetchingPurchaseInfoFailure:
      return {
        ...initialState,
        message: 'Unable to fetch your tax information',
      };
    case types.fetchingPurchaseInfoSuccess: {
      const {
        hasValidCreditCard,
        hasValidAddress,
        taxRate,
        creditBalance,
      } = action.payload;

      return {
        ...state,
        taxRate,
        creditBalance,
        hasValidAddress,
        hasValidCreditCard,
        needsRefresh: false,
        fetchingPurchaseInfo: false,
        loading: false,
      };
    }
    default:
      return state;
  }
};

export const actions = {
  fetchPurchaseInfo: () => async (dispatch) => {
    try {
      dispatch({ type: types.fetchingPurchaseInfo });
      const {
        hasValidCreditCard,
        hasValidAddress,
        creditBalance,
        taxRate,
      } = await app.service('purchaseInfo').find();
      dispatch({
        type: types.fetchingPurchaseInfoSuccess,
        payload: {
          hasValidCreditCard,
          hasValidAddress,
          creditBalance,
          taxRate,
        },
      });
    } catch (e) {
      dispatch({ type: types.fetchingPurchaseInfoFailure });
      throw e;
    }
  },
};
