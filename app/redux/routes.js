export const types = {
  setCurrentScreen: 'ROUTES_SET_CURRENT_SCREEN',
};

const initialState = {
  scene: {},
  screenName: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.setCurrentScreen: {
      const { screenName } = action;

      // Only register actual screens to the state
      if (screenName === 'Drawer') return state;

      return {
        ...state,
        screenName,
      };
    }
    default:
      return state;
  }
};
