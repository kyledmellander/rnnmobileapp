import { AsyncStorage } from 'react-native';
import moment from 'moment';
import adjust from 'lib/adjust';
import app from 'src/config/feathers';
import store from 'store';
import { userStateKey, userStateCodeKey, userKey } from 'src/config/storageConsts';
import types from './types';


const nDaysFromNow = numDays => moment().add(numDays, 'days').toISOString();

const login = (email, password) => async (dispatch) => {
  dispatch({ type: types.fetchingUser });
  try {
    const data = {
      email,
      password,
      strategy: 'local',
    };

    const token = await app.authenticate(data);
    if (token === null) {
      throw new Error('Incorrect email or password');
    }
    const user = await app.service('users').get(0);
    dispatch({
      type: types.fetchingUserSuccess,
      email,
      token,
      user,
      userStateCode: user.stateCode,
      userState: user.state,
      loggedIn: true,
    });
  } catch (e) {
    dispatch({
      type: types.fetchingUserError,
      name: 'logInError',
      value: e.errors ? e.errors[0] : e.message,
    });
  }
};

const signOut = () => async (dispatch) => {
  await AsyncStorage.removeItem(userStateKey);
  await AsyncStorage.removeItem(userStateCodeKey);
  await AsyncStorage.removeItem(userKey);
  await app.clearSession();
  dispatch({
    type: types.logout,
    token: null,
  });
};

const createUser = (email, password, fname, lname) => async (dispatch) => {
  try {
    const data = {
      email, password, fname, lname
    };
    const user = await app.service('users').create(data);
    await app.setSessionToken(user, user.refreshToken);
    adjust.fireRegistrationEvent();
    adjust.setUser(user);
    adjust.getAttribution();
    dispatch({
      type: types.createUser,
      user,
      email: user.email,
      userStateCode: user.stateCode,
      sessionToken: user.userToken,
      userState: user.state,
    });
  } catch (e) {
    dispatch({
      type: types.createUserError,
      name: 'createUserError',
      value: e.errors ? e.errors[0] : e.message,
    });
  }
};

function shouldSeeOnboarding(user) {
  return user.showOnboarding && !user.sawOnboarding;
}

const getUser = () => async (dispatch) => {
  dispatch({
    type: types.fetchingUser,
  });

  try {
    const userString = await AsyncStorage.getItem(userKey);
    let user = userString ? JSON.parse(userString) : {};

    if (!userString || shouldSeeOnboarding(user)
      || !user.expireAt || moment(user.expireAt).isBefore(moment())) {
      user = await app.service('users').get(0);
      user.expireAt = nDaysFromNow(14);
      AsyncStorage.setItem(userKey, JSON.stringify(user));
    }
    if (!user.preferences) {
      const preferences = await app.service('preferences').find() || {};
      user.preferences = preferences;
      AsyncStorage.setItem(userKey, JSON.stringify(user));
    }

    dispatch({
      type: types.fetchingUserSuccess,
      user,
    });
  } catch (e) {
    console.log(e);
    dispatch({
      type: types.fetchingUserError,
      name: 'getUserError',
      value: e.errors ? e.errors[0] : e.message,
    });
  }
};

const restoreSession = () => async (dispatch) => {
  // Get a session token
  const token = await app.getSessionToken();
  dispatch({
    type: types.restoringSessionSuccess,
    token,
  });

  const state = store.getState();
  if (!state.user.user) dispatch(getUser());
};

const getUserState = () => async (dispatch) => {
  try {
    let userState = await AsyncStorage.getItem(userStateKey);
    let userStateCode = await AsyncStorage.getItem(userStateCodeKey);
    if (userState == null || userStateCode == null) {
      const user = await app.service('users').get(0);
      userState = user.state;
      userStateCode = user.stateCode;
      AsyncStorage.setItem(userStateKey, userState);
      AsyncStorage.setItem(userStateCodeKey, userStateCode);
    }
    dispatch({
      type: types.getUserState,
      userState,
      userStateCode,
    });
  } catch (e) {
    dispatch({
      type: types.localStorageError,
      value: e.errors ? e.errors[0] : e.message,
    });
  }
};

const updateUser = user => async (dispatch) => {
  console.log(user);
  try {
    const updatedUser = await app.service('users').update(user.id, user);
    AsyncStorage.setItem(userKey, JSON.stringify(updatedUser));
    dispatch({
      type: types.updateUser,
      user: updatedUser,
    });
  } catch (e) {
    console.log('error', e);
    dispatch({
      type: types.getUserError,
      name: 'getUserError',
      value: e.errors ? e.errors[0] : e.message,
    });
  }
};

const updateUserPreferences = (user, preferences) => async (dispatch) => {
  try {
    const newPreferences = await app.service('preferences').update(user.id, preferences);
    const updatedUser = {
      ...user,
      preferences: newPreferences
    };
    AsyncStorage.setItem(userKey, JSON.stringify(updatedUser));
    dispatch({
      type: types.updateUser,
      user: updatedUser,
    });
  } catch (e) {
    dispatch({
      type: types.getUserError,
      name: 'getUserError',
      value: e.errors ? e.errors[0] : e.message,
    });
  }
};

const setUserToken = token => async (dispatch) => {
  try {
    await app.setSessionToken(token);
  } catch (e) {
    dispatch({
      type: types.localStorageError,
      name: 'localStorageError',
      value: e.errors ? e.errors[0] : e.message,
    });
  }
};

export default {
  createUser,
  setUserToken,
  updateUserPreferences,
  updateUser,
  getUserState,
  getUser,
  login,
  restoreSession,
  signOut,
};

