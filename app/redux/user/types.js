export default {
  login: 'USER__LOGIN',
  logout: 'USER__LOGOUT',
  setSessionToken: 'USER__SET_SESSION_TOKEN',

  restoringSession: 'USER__RESTORING_SESSION',
  restoringSessionSuccess: 'USER__RESTORING_SESSION_SUCCESS',
  restoringSessionError: 'USER__RESTORING_SESSION_ERROR',

  fetchingUser: 'USER__FETCHING_USER',
  fetchingUserSuccess: 'USER__FETCHING_USER_SUCCESS',
  fetchingUserError: 'USER__FETCHING_USER_ERROR',

  getUserState: 'USER__GET_USER_STATE',
  createUser: 'USER__CREATE_USER',
  updateUser: 'USER__UPDATE_USER',
  logInError: 'USER__LOGIN_ERROR',
  createUserError: 'USER__CREATE_USER_ERROR',
  getUserError: 'USER__GET_USER_ERROR',
  localStorageError: 'USER__LOCAL_STORAGE_ERROR',
};

