import { createSelector } from 'reselect';

const userSelector = state => state.user.user;
const crateWordSelector = createSelector(userSelector, (user) => {
  if (!user) return 'Crate';
  switch (user.userStateCode) {
    case 'NY':
    case 'CT':
      return 'Cellar';
    default:
      return 'Crate';
  }
});

const crateWordLowerSelector = createSelector(crateWordSelector, word => word.toLowerCase());
const userHasSessionToken = state => state.user.sessionToken;
const userNeedsState = state => state.user.user && !state.user.user.stateCode;
const userNeedsOnboardingFlow = createSelector(userSelector, user =>
  user && user.showOnboarding && !user.sawOnboarding);

export default {
  crateWordSelector,
  crateWordLowerSelector,
  userNeedsState,
  userNeedsOnboardingFlow,
  userHasSessionToken,
};

