import types from './types';

export const initialState = {
  sessionToken: null,

  // loading must default to true so the app knows it's initializing
  restoringSession: true,
  fetchingUser: true,

  loggedOut: false,
  email: '',
  user: null,
  loggedIn: null,
  errors: [],
  userStateCode: '',
  userState: null,
  firstLogin: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.restoringSessionSuccess:
      return {
        ...state,
        restoringSession: false,
        sessionToken: action.token,
        errors: [],
      };
    case types.fetchingUserSuccess:
      return {
        ...state,
        loggedIn: true,
        loggedOut: false,
        email: action.email,
        fetchingUser: false,
        errors: [],
        userState: action.userState,
        userStateCode: action.userStateCode,
        user: action.user,
      };
    case types.logout:
      return {
        ...state,
        firstLogin: false,
        sessionToken: action.token,
        loading: false,
        loggedOut: true,
        loggedIn: false,
        errors: [],
      };
    case types.createUser:
      return {
        ...state,
        user: action.user,
        loggedIn: true,
        loading: false,
        sessionToken: action.sessionToken,
        email: action.email,
        errors: [],
        firstLogin: true,
      };
    case types.getUser: {
      const { user } = action;
      user.showOnboarding = true;
      user.sawOnboarding = false;

      return {
        ...state,
        user,
        email: user.email,
        userState: user.state,
        userStateCode: user.stateCode,
      };
    }
    case types.getUserState:
      return {
        ...state,
        userState: action.userState,
        userStateCode: action.userStateCode,
      };
    case types.fetchingUserError:
    case types.restoringSessionError:
    case types.createUserError:
    case types.logInError:
      return {
        ...state,
        loggedIn: false,
        fetchingUser: false,
        restoringSession: false,
        errors: [action.value],
        actionCount: state.actionCount + 1,
      };
    case types.updateUser:
      return {
        ...state,
        user: action.user,
        userState: action.user.state,
        userStateCode: action.user.stateCode,
        errors: [],
      };

    case types.localStorageError:
      return state;

    default:
      return state;
  }
};

