import app from 'config/feathers';
import stripeClient from 'config/stripe';
import userTypes from './user/types';

export const types = {
  getInitData: 'PAYMENT_METHODS__GET_PAYMENT_METHODS',
  gotInitData: 'PAYMENT_METHODS__GOT_PAYMENT_METHODS',
  addPaymentMethod: 'PAYMENT_METHODS__ADD_PAYMENT_METHOD',
  removePaymentMethod: 'PAYMENT_METHODS__REMOVE_PAYMENT_METHOD',
  updatePaymentMethod: 'PAYMENT_METHODS__UPDATE_PAYMENT_METHOD',
  clearCardMessage: 'PAYMENT_METHODS__CLEAR_CARD_MESSAGE',
  paymentMethodsError: 'PAYMENT_METHODS__PAYMENT_METHODS_ERROR',
};

export const initialState = {
  errors: null,
  paymentMethods: [],
  creditTotal: '',
  refundsAndCredits: [],
  loading: false,
  cardMessage: '',
  creditTotalDisplay: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case userTypes.logOut: {
      return initialState;
    }
    case types.getInitData:
      return {
        ...state,
        errors: null,
        loading: true,
      };
    case types.gotInitData:
      return {
        ...state,
        paymentMethods: action.paymentMethods,
        creditTotalDisplay: action.creditTotalDisplay,
        refundsAndCredits: action.refundsAndCredits,
        loading: false,
      };
    case types.paymentMethodsError:
      return {
        ...state,
        errors: action.errors,
      };
    case types.addPaymentMethod:
      return {
        ...state,
        addedCard: true,
        errors: null,
        cardMessage: action.cardMessage
      };
    case types.removePaymentMethod:
      return {
        ...state,
        removedCard: true,
        paymentMethods: action.paymentMethods,
        errors: null,
        cardMessage: action.cardMessage,
      };
    case types.updatePaymentMethod:
      return {
        ...state,
        updatedCard: true,
        paymentMethods: action.paymentMethods,
        errors: null,
        cardMessage: action.cardMessage,
      };
    case types.clearCardMessage:
      return {
        ...state,
        cardMessage: ''
      };
    default:
      return state;
  }
};

export const actions = {
  getInitData: () => async (dispatch) => {
    try {
      dispatch({ type: types.getInitData });
      const [cards, refundsAndCredits] = await Promise.all([
        app.service('paymentMethods').find(),
        app.service('refundsAndCredits').find(),
      ]);
      const credits = refundsAndCredits.filter(refundOrCredit => refundOrCredit.type === 'creditEvents');
      const creditTotalDisplay = credits.length > 0 ? credits[0].meta.total : '$0.00';
      dispatch({
        type: types.gotInitData,
        paymentMethods: cards,
        creditTotalDisplay,
        refundsAndCredits,
      });
    } catch (e) {
      dispatch({
        type: types.paymentMethodsError,
        errors: e.errors,
      });
    }
  },
  addPaymentMethod: paymentMethod => async (dispatch) => {
    try {
      /* eslint-disable */
      const token = await stripeClient.createToken({
        number: paymentMethod.number,
        exp_month: paymentMethod.month,
        exp_year: paymentMethod.year,
        cvc: paymentMethod.cvv
      });
      /* eslint-enable */
      await app.service('paymentMethods').create({ cardToken: token.id });
      await actions.getInitData()(dispatch);
      dispatch({
        type: types.addPaymentMethod,
        cardMessage: 'Nice, your card has been added!',
      });
      actions.delayClearCardMessage()(dispatch);
    } catch (e) {
      dispatch({
        type: types.paymentMethodsError,
        errors: e.errors,
      });
    }
  },
  removePaymentMethod: paymentMethod => async (dispatch) => {
    try {
      const service = app.service('paymentMethods');
      await service.remove(paymentMethod.id);
      const paymentMethods = await service.find();
      dispatch({
        type: types.removePaymentMethod,
        paymentMethods,
        cardMessage: 'Nice, your card has been deleted!',
      });
      actions.delayClearCardMessage()(dispatch);
    } catch (e) {
      dispatch({
        type: types.paymentMethodsError,
        errors: e.errors,
      });
    }
  },
  updatePaymentMethod: paymentMethod => async (dispatch) => {
    try {
      const service = app.service('paymentMethods');
      await service.update(paymentMethod.id, {});
      const paymentMethods = await service.find();
      dispatch({
        type: types.updatePaymentMethod,
        paymentMethods,
        cardMessage: 'Nice, saved new default card!',
      });
      actions.delayClearCardMessage()(dispatch);
    } catch (e) {
      dispatch({
        type: types.paymentMethodsError,
        errors: e.errors,
      });
    }
  },
  delayClearCardMessage: () => (dispatch) => {
    setTimeout(() => {
      dispatch({ type: types.clearCardMessage });
    }, 3000);
  },
};
