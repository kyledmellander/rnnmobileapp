import app from '../config/feathers';

export const types = {
  gotZipCode: 'ZIP_CODE__GET_ZIP_CODE',
  zipCodeError: 'ZIP_CODE__ZIP_CODE_ERROR',
  preFetchZip: 'ZIP_CODE__PRE_FETCH_ZIP',
};

const initialState = {
  zipCodeCovered: false,
  loading: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.gotZipCode:
      return {
        ...state,
        stateCode: action.stateCode,
        city: action.city,
        zipCodeCovered: action.zipCodeCovered,
        loading: false,
      };
    case types.zipCodeError:
      return {
        ...state,
        zipCodeError: action.value,
        loading: false,
      };
    case types.preFetchZip:
      return {
        zipCodeCovered: false,
        loading: true,
      };
    default:
      return state;
  }
};

export const actions = {
  getZipCode: zipCode => async (dispatch) => {
    try {
      dispatch({ type: types.preFetchZip });
      const zip = await app.service('zipCodes').get(zipCode);
      dispatch({
        type: types.gotZipCode,
        stateCode: zip.stateCode,
        city: zip.city,
        zipCodeCovered: zip.zipCodeCovered,
      });
    } catch (e) {
      dispatch({
        type: types.zipCodeError,
        name: 'getZipCodeError',
        value: e.message,
      });
    }
  },
};
