import user, { actions as userActions, selectors as userSelectors } from './user';
import appStoreRating from './appStoreRating/appStoreRatingReducer';
import routes from './routes';
import zipCode from './zipCode';
import offers from './offers';
import addresses from './addresses';
import orders from './orders';
import paymentMethods from './paymentMethods';
import baskets from './baskets';
import ratings from './ratings';
import notifications from './notifications';
import products from './products';
import appNotifications from './appNotifications';
import resetPassword from './resetPassword';
import contactUs from './contactUs';
import promos from './promos';
import addressForm from './addressForm';
import purchaseInfo from './purchaseInfo';
import experiments from './experiments';
import userPromos from './userPromos';
import redeem from './redeem';
import referral from './referral';
import modals from './modals';

export const actions = {
  ...userActions,
};

export const selectors = {
  ...userSelectors,
};

const reducers = {
  appStoreRating,
  experiments,
  user,
  routes,
  zipCode,
  offers,
  orders,
  paymentMethods,
  addresses,
  baskets,
  ratings,
  notifications,
  products,
  appNotifications,
  resetPassword,
  promos,
  addressForm,
  contactUs,
  purchaseInfo,
  modals,
  userPromos,
  redeem,
  referral
};

export default reducers;
