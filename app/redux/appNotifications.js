import { createSelector } from 'reselect';
import app from '../config/feathers';

export const types = {
  setDeepLink: 'APP_NOTIFICATIONS__SET_DEEP_LINK',
  clearDeepLink: 'APP_NOTIFICATIONS__CLEAR_DEEP_LINK',
  getNotifications: 'APP_NOTIFICATIONS__GET_APP_NOTIFICATIONS',
  updateNotification: 'APP_NOTIFICATIONS__UPDATE_APP_NOTIFICATION'
};

const initialState = {
  deepLink: null,
  notifications: []
};

const updateNotification = (state, notification) => {
  const match = state.notifications.find(n => n.id === notification.id);
  if (match) {
    match.isActive = false;
  }
  return [...state.notifications];
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.setDeepLink: {
      const { deepLink } = action;
      return {
        ...state,
        deepLink,
      };
    }
    case types.clearDeepLink:
      return {
        ...state,
        deepLink: null,
      };
    case types.getNotifications:
      return {
        ...state,
        notifications: action.notifications
      };
    case types.updateNotification:
      return {
        ...state,
        notifications: updateNotification(state, action.notification)
      };
    default:
      return state;
  }
};

export const actions = {
  getNotifications: () => async (dispatch) => {
    try {
      const notifications = await app.service('appNotifications').find();
      notifications.forEach((notification) => {
        notification.body = notification.body.replace(/\\n/g, '');
      });
      dispatch({
        type: types.getNotifications,
        notifications
      });
    } catch (e) {
      dispatch({
        type: types.getNotifications,
        notifications: []
      });
    }
  },
  updateNotification: appNotification => async (dispatch) => {
    try {
      await app.service('appNotifications').update(appNotification.id, appNotification);
      dispatch({
        type: types.updateNotification,
        notification: appNotification
      });
    } catch (e) {
      console.log(e);
    }
  },
  setDeepLink: url => async (dispatch) => {
    const action = {
      type: types.setDeepLink,
      deepLink: url,
    };

    dispatch(action);
  },
  clearDeepLink: () => async (dispatch) => {
    const action = {
      type: types.clearDeepLink,
    };
    dispatch(action);
  },
};

const notificationsSelector = state => state.appNotifications.notifications;
const activeNotificationCountSelector = createSelector(
  notificationsSelector,
  notifications => notifications.filter(n => n.isActive).length
);

export const selectors = {
  notificationsSelector,
  activeNotificationCountSelector
};
