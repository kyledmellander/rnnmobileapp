import app from 'config/feathers';
import store from 'app/config/store';
import { actions as modalActions } from '../modals';
import types from './types';

export default {
  patchAppStoreRating: rating => async (dispatch) => {
    try {
      const { appStoreRatingId } = store.getState().appStoreRating;
      dispatch({ type: types.patchingAppStoreRating });

      const body = { id: appStoreRatingId, ...rating };
      const appStoreRating = await app.service('appStoreRatings').update(appStoreRatingId, body);

      dispatch({
        type: types.patchingAppStoreRatingSuccess,
        payload: { appStoreRating },
      });
    } catch (e) {
      dispatch({ type: types.patchingAppStoreRatingFailure });
      throw e;
    }
  },
  fetchAppStoreRating: () => async (dispatch) => {
    try {
      // Only search if the user is in the show rating prompt state
      const { user } = store.getState().user;
      if (!(user && user.showRatingPrompt)) return;

      dispatch({ type: types.fetchingAppStoreRating });

      const appStoreRating = await app.service('appStoreRatings').find();
      if (appStoreRating.showRatingModal) dispatch(modalActions.showStoreRating());
      dispatch({
        type: types.fetchingAppStoreRatingSuccess,
        payload: { appStoreRating },
      });
    } catch (e) {
      dispatch({ type: types.fetchingAppStoreRatingFailure });
      throw e;
    }
  },
};

