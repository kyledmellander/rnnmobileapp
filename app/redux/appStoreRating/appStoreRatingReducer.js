import types from './types';

export const initialState = {
  fetchingAppStoreRating: false,
  patchingAppStoreRating: false,
  appStoreRatingId: '',
  showRatingModal: false,
  errorMessage: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.fetchingAppStoreRating:
      return {
        ...state,
        errorMessage: '',
        fetchingAppStoreRating: true
      };
    case types.fetchingAppStoreRatingFailure:
      return {
        ...initialState,
        errorMessage: 'Error fetching user information',
      };
    case types.patchingAppStoreRating: {
      return {
        ...state,
        patchingAppStoreRating: true,
      };
    }
    case types.patchingAppStoreRatingFailure: {
      return {
        ...state,
        showRatingModal: false,
        patchingAppStoreRating: false,
      };
    }
    case types.patchingAppStoreRatingSuccess: {
      return {
        ...state,
        showRatingModal: false,
        patchingAppStoreRating: false,
      };
    }
    case types.fetchingAppStoreRatingSuccess: {
      const { appStoreRating } = action.payload;

      return {
        ...state,
        appStoreRatingId: appStoreRating.id,
        showRatingModal: appStoreRating.showRatingModal,
        errorMessage: '',
        fetchingAppStoreRating: false,
      };
    }
    default:
      return state;
  }
};
