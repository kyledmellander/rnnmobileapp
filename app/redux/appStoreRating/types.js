export default {
  fetchingAppStoreRating: 'FETCHING_APP_STORE_RATING',
  fetchingAppStoreRatingFailure: 'FETCHING_APP_STORE_RATING_FAILURE',
  fetchingAppStoreRatingSuccess: 'FETCHING_APP_STORE_RATING_SUCCESS',
  patchingAppStoreRating: 'PATCHING_APP_STORE_RATING',
  patchingAppStoreRatingFailure: 'PATCHING_APP_STORE_RATING_FAILURE',
  patchingAppStoreRatingSuccess: 'PATCHING_APP_STORE_RATING_SUCCESS',
};

