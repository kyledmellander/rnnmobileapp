import { Linking } from 'react-native';
import app from '../config/feathers';

export const types = {
  setContactUs: 'CONTACT_US__SET_CONTACT_US',
  setCanOpenButtons: 'CONTACT_US__SET_CAN_OPEN_BUTTONS',
  setErrorMessage: 'CONTACT_US__SET_ERROR_MESSAGE',
  setIsLoading: 'CONTACT_US__SET_IS_LOADING',
  fetchingContactUs: 'CONTACT_US__FETCHING_CONTACT_US',
};

const initialState = {
  errorMessage: '',
  isLoading: false,
  contactUsItems: [],
  email: '',
  phone: '',
  writeUsLink: '',
  questionsForUsText: '',
  questionsForUsTitle: '',
  canOpenTel: false,
  canOpenEmail: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.fetchingContactUs: {
      return {
        ...state,
        errorMessage: '',
        isLoading: true,
      };
    }
    case types.setContactUs: {
      return {
        ...state,
        contactUsItems: action.contactUs.contactUsItems,
        email: action.contactUs.email,
        phone: action.contactUs.phone,
        writeUsLink: action.contactUs.writeUsLink,
        questionsForUsText: action.contactUs.questionsForUsText,
        questionsForUsTitle: action.contactUs.questionsForUsTitle,
      };
    }
    case types.setErrorMessage: {
      return {
        ...state,
        errorMessage: action.errorMessage
      };
    }
    case types.setIsLoading: {
      return {
        ...state,
        isLoading: action.isLoading,
      };
    }
    case types.setCanOpenButtons: {
      return {
        ...state,
        canOpenTel: action.canOpenTel,
        canOpenEmail: action.canOpenEmail,
      };
    }
    default:
      return state;
  }
};

export const actions = {
  loadContactUs: () => async (dispatch) => {
    dispatch({ type: types.fetchingContactUs });
    try {
      const [contactUs, canOpenTel, canOpenEmail] = await Promise.all([
        app.service('contactUs').find(),
        Linking.canOpenURL('tel:test'),
        Linking.canOpenURL('mailto:test'),
      ]);
      dispatch({ type: types.setContactUs, contactUs });
      dispatch({ type: types.setCanOpenButtons, canOpenTel, canOpenEmail });
    } catch (e) {
      dispatch({
        type: types.setErrorMessage,
        errorMessage: 'Email us at support@tavour.com for help.'
      });
    }
    dispatch({ type: types.setIsLoading, isLoading: false });
  },
};
