import store from 'app/config/store';
import app from '../config/feathers';

export const types = {
  getOffers: 'OFFERS__GET_OFFERS',
  getOffersError: 'OFFERS__GET_OFFERS_ERROR',
};

const initialState = {
  offers: [],
  errors: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.getOffers:
      return {
        ...state,
        offers: action.offers,
        errors: [],
      };
    case types.getOffersError:
      return {
        ...state,
        errors: action.value,
      };
    default:
      return state;
  }
};

export const actions = {
  getOffers: () => async (dispatch) => {
    try {
      const state = store.getState();
      const userId = state.user.user.id;
      const offers = await app
        .service('offers')
        .find({ query: { includeOrders: false, userId } });
      dispatch({
        type: types.getOffers,
        offers,
      });
    } catch (e) {
      if (e.errors && Array.isArray(e.errors) && e.errors[0].title) {
        dispatch({
          type: types.getOffersError,
          name: 'getOffersError',
          value: e.errors,
        });
      } else {
        throw e;
      }
    }
  },
};
