import app from '../config/feathers';
import { types as productActionTypes } from './products';

export const types = {
  rating: 'RATINGS__RATING',
  ratingError: 'RATINGS__RATING_ERROR',
};

const initialState = {
  lastRating: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.rating:
      return {
        ...state,
        lastRating: { beer: action.beer, score: action.score },
      };
    case types.ratingError:
      return {
        ...state,
      };
    default:
      return state;
  }
};

export const actions = {
  rateBeer: (beer, score) => async (dispatch) => {
    try {
      const service = app.service('ratings');
      let rating;
      if (beer.rating.id) {
        rating = await service.update(beer.rating.id, { score });
      } else {
        rating = await service.create({ productId: beer.id, score });
      }
      beer.rating = rating;
      dispatch({
        type: productActionTypes.updateProductRating,
        product: beer,
      });
      dispatch({
        type: productActionTypes.updateUnratedProducts,
        product: beer,
      });
      dispatch({
        type: types.rating,
        score,
        beer,
      });
    } catch (e) {
      dispatch({
        type: types.ratingError,
      });
      throw e;
    }
  },
};
