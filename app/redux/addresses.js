import app from '../config/feathers';
import userTypes from './user/types';

export const types = {
  getAddresses: 'ADDRESSES__GET_ADDRESSES',
  addAddress: 'ADDRESSES__ADD_ADDRESS',
  removeAddress: 'ADDRESSES__REMOVE_ADDRESS',
  updateAddress: 'ADDRESSES__UPDATE_ADDRESS',
  setDefaultAddress: 'ADDRESSES__SET_DEFAULT_ADDRESS',
  clearAddressStatus: 'ADDRESSES__CLEAR_ADDRESS_STATUS',
  addressesError: 'ADDRESSES__ADDRESSES_ERROR',
};

export const initialState = {
  addresses: null,
  addedAddress: false,
  updatedAddress: false,
  updatedDefault: false,
  removedAddress: false,
  errors: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case userTypes.logOut: {
      return initialState;
    }
    case types.getAddresses:
      return {
        ...state,
        addresses: action.addresses,
        errors: null,
      };
    case types.addressesError:
      return {
        ...state,
        errors: action.errors,
      };
    case types.addAddress:
      return {
        ...state,
        addresses: action.addresses,
        addedAddress: true,
        errors: null,
      };
    case types.updateAddress:
      return {
        ...state,
        addresses: action.addresses,
        updatedAddress: true,
        errors: null,
      };
    case types.setDefaultAddress:
      return {
        ...state,
        updatedDefault: true,
        addresses: action.addresses,
        errors: null,
      };
    case types.removeAddress:
      return {
        ...state,
        removedAddress: true,
        addresses: action.addresses,
        errors: null,
      };
    case types.clearAddressStatus:
      return {
        ...state,
        addedAddress: false,
        updatedAddress: false,
        updatedDefault: false,
        removedAddress: false,
      };
    default:
      return state;
  }
};

export const actions = {
  getAddresses: () => async (dispatch) => {
    try {
      const addresses = await app.service('addresses').find();
      dispatch({
        type: types.getAddresses,
        addresses,
      });
    } catch (e) {
      dispatch({
        type: types.addressesError,
        name: 'getAddressesError',
        value: e.message,
      });
    }
  },
  addAddress: address => async (dispatch) => {
    try {
      const service = app.service('addresses');
      await service.create(address);
      const addresses = await service.find();
      dispatch({
        type: types.addAddress,
        addresses,
      });
    } catch (e) {
      dispatch({
        type: types.addressesError,
        errors: e.errors,
      });
    }
  },
  removeAddress: address => async (dispatch) => {
    try {
      const service = app.service('addresses');
      await service.remove(address.id);
      const addresses = await service.find();
      dispatch({
        type: types.removeAddress,
        addresses,
      });
    } catch (e) {
      dispatch({
        type: types.addressesError,
        errors: e.errors,
      });
    }
  },
  updateAddress: address => async (dispatch) => {
    try {
      const service = app.service('addresses');
      await service.update(address.id, address);
      const addresses = await service.find();
      dispatch({
        type: types.updateAddress,
        addresses,
      });
    } catch (e) {
      dispatch({
        type: types.addressesError,
        errors: e.errors,
      });
    }
  },
  setDefaultAddress: address => async (dispatch) => {
    try {
      const service = app.service('addresses');
      await service.update(address.id, address);
      const addresses = await service.find();
      dispatch({
        type: types.setDefaultAddress,
        addresses,
      });
    } catch (e) {
      dispatch({
        type: types.addressesError,
      });
    }
  },
  clearAddressStatus: () => async (dispatch) => {
    dispatch({
      type: types.clearAddressStatus,
    });
  },
};
