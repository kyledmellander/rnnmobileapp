/* eslint-disable no-console */

// Framework
import React from 'react';
import codePush from 'react-native-code-push';
import { AppLaunch } from 'screens';

const App = () => <AppLaunch />;

const toExport = __DEV__ ? App : codePush(App);
export default toExport;
