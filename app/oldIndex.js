/* eslint react/no-did-update-set-state: 0 */
/* eslint react/no-did-mount-set-state: 0 */
/*
import React from 'react';
import { Text, Image, TouchableOpacity } from 'react-native';
import { Provider, connect } from 'react-redux';
// import { Scene, Router, ActionConst, Actions } from 'react-native-router-flux';
import codePush from 'react-native-code-push';
import utils from 'lib/utils';
import adjust from 'lib/adjust';
import analytics from 'lib/analytics';

// Config
import store from './config/store';
import { actions as userActions } from './redux/user';
import images from './config/images';
import appNotifications from './config/appNotifications';
import './config/exceptions';

// Styles
import { navigationBar as navStyle } from './styles';

// Screens
import Landing from './screens/landing';
import NavigationDrawer from './screens/navigationDrawer';
import Login from './screens/login';
import Home from './screens/home';
import Addresses from './screens/addresses';
import addUpdateAddress from './screens/addresses/addUpdateAddress';
import { ConfirmAge, CreateLogin, EnterZip } from './screens/signup';
import Purchase from './screens/purchase';
import FirstTimePurchase from './screens/purchase/firstTimePurchase';
import Receipt from './screens/purchase/receipt';
import PaymentMethods from './screens/paymentMethods';
import AddCreditCard from './screens/paymentMethods/addCreditCard';
import Basket from './screens/baskets';
import AboutBasket from './screens/baskets/about';
import Contact from './screens/contact';
import MyBeers from './screens/myBeers';
import ShippedCrates from './screens/shippedCrates';
import NotificationSettings from './screens/notificationSettings';
import TastingNotes from './screens/tastingNotes';
import AccountSettings from './screens/accountSettings';
import ForgotPassword from './screens/forgotPassword';
import NotificationIcon from './notificationIcon';
import AppNotifications from './screens/appNotifications';
import Promos from './screens/promos';
import Referral from './screens/referral';
import {
  signUpConfirmation,
  dailyBeers,
  notifications,
  stockUpAndShip,
  flatRateShipping
} from './screens/onboarding';
import Redeem from './screens/redeem';

// Experimental Screens
import { crateCollage } from './experiments';

const RouterWithRedux = connect()(Router);
const hamburger = require('./assets/images/general/hamburger.png');

class galaxy extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sessionExists: false,
      loading: true,
      firstLogin: false,
      stateNeeded: false,
    };
    this.checkState = this.checkState.bind(this);
    this.state.unsubscribe = store.subscribe(this.checkState);
    adjust.init();
    store.dispatch(userActions.restoreSession());
    Text.defaultProps.allowFontScaling = false;
    console.log(props);
  }

  async componentDidMount() {
    analytics.track({ event: 'App Open' });
    appNotifications.addURLListener();
    const initialURL = await appNotifications.getInitialURL();
    if (initialURL) {
      this.setState({
        ...this.state,
        initialURL,
      });
    }
  }

  componentDidUpdate() {
    if (
      this.state.initialURL &&
      !this.state.loading &&
      !this.stateNeeded &&
      this.state.sessionExists
    ) {
      appNotifications.routeFromURL(this.state.initialURL);
      appNotifications.clearInitialURL();
      const newState = this.state;
      delete newState.initialURL;
      this.setState(newState);
    }
  }

  componentWillUnmount() {
    appNotifications.removeURLListener();
  }

  checkState() {
    const state = store.getState();
    const newState = {
      ...this.state,
      appNotifications: state.appNotifications
    };
    if (this.state.loading !== state.user.loading) {
      if (state.user.sessionToken !== null) {
        if (!state.user.user && state.user.errors.length === 0) {
          store.dispatch(userActions.getUser());
        } else {
          if (state.user.user && !state.user.user.stateCode) { newState.stateNeeded = true; }
          newState.loading = state.user.loading;
          newState.sessionExists = true;
          this.state.unsubscribe();
        }
      } else {
        newState.loading = state.user.loading;
        newState.sessionExists = false;
        this.state.unsubscribe();
      }
    }
    if (this.state.firstLogin !== state.user.firstLogin) {
      newState.firstLogin = state.user.firstLogin;
      newState.sessionExists = state.user.sessionToken !== null;
      this.state.unsubscribe();
    }
    if (state.appNotifications.deepLink) {
      newState.initialURL = state.appNotifications.deepLink;
    }
    if (state.user.user && state.user.user.showOnboarding && !state.user.user.sawOnboarding) {
      newState.showOnboarding = true;
    }
    if (JSON.stringify(this.state) !== JSON.stringify(newState)) {
      this.setState(newState);
    }
  }

  render() {
    if (this.state.loading) {
      return (
        <Image
          source={images.splash}
          style={{ width: '100%', height: '100%' }}
          resizeMode="cover"
        />
      );
    }
    return (
      <Provider store={store}>
        <RouterWithRedux>
          <Scene
            key="root"
            navigationBarStyle={navStyle.navigationBar}
            leftButtonIconStyle={navStyle.leftButton}
            backButtonTextStyle={navStyle.backButtonText}
            titleStyle={navStyle.title}
          >
            <Scene
              key="Landing"
              component={Landing}
              hideNavBar
              type={ActionConst.RESET}
            />
            <Scene
              key="SignUp"
              initial={this.state.stateNeeded}
              navigationBarStyle={[
                navStyle.navigationBar,
                navStyle.transparent,
              ]}
              leftButtonIconStyle={navStyle.leftButton}
              backButtonTextStyle={navStyle.backButtonText}
              type={ActionConst.RESET}
            >
              <Scene key="ConfirmAge" component={ConfirmAge} />
              <Scene
                key="CreateLogin"
                component={CreateLogin}
                backTitle="Back"
              />
              <Scene key="EnterZip" component={EnterZip} backTitle="Back" />
            </Scene>
            <Scene
              key="Login"
              component={Login}
              hideNavBar={false}
              backTitle="Back"
            />
            <Scene
              key="ForgotPassword"
              component={ForgotPassword}
              hideNavBar={false}
              backTitle="Back"
            />
            <Scene
              key="Onboarding"
              initial={this.state.showOnboarding}
              hideNavBar
              type={ActionConst.RESET}
            >
              <Scene
                key="SignUpConfirmation"
                component={signUpConfirmation}
              />
              <Scene
                key="DailyBeers"
                component={dailyBeers}
                title="How to Tavour"
                titleStyle={navStyle.titleOrange}
                hideNavBar={false}
                navigationBarStyle={[
                  navStyle.navigationBar,
                  navStyle.transparent]}
                leftButtonIconStyle={navStyle.leftButton}
                backButtonTextStyle={navStyle.backButtonText}
                backTitle="Back"
              />
              <Scene
                key="Notifications"
                component={notifications}
                title="How to Tavour"
                titleStyle={navStyle.titleOrange}
                hideNavBar={false}
                navigationBarStyle={[
                  navStyle.navigationBar,
                  navStyle.transparent]}
                leftButtonIconStyle={navStyle.leftButton}
                backButtonTextStyle={navStyle.backButtonText}
                backTitle="Back"
              />
              <Scene
                key="StockUpAndShip"
                component={stockUpAndShip}
                title="How to Tavour"
                titleStyle={navStyle.titleOrange}
                hideNavBar={false}
                navigationBarStyle={[
                  navStyle.navigationBar,
                  navStyle.transparent]}
                leftButtonIconStyle={navStyle.leftButton}
                backButtonTextStyle={navStyle.backButtonText}
                backTitle="Back"
              />
              <Scene
                key="FlatRateShipping"
                component={flatRateShipping}
                title="How to Tavour"
                titleStyle={navStyle.titleOrange}
                hideNavBar={false}
                navigationBarStyle={[
                  navStyle.navigationBar,
                  navStyle.transparent]}
                leftButtonIconStyle={navStyle.leftButton}
                backButtonTextStyle={navStyle.backButtonText}
                backTitle="Back"
              />
            </Scene>


            <Scene
              key="Drawer"
              component={NavigationDrawer}
              open={false}
              initial={
                this.state.sessionExists &&
                !this.state.stateNeeded &&
                !this.state.showOnboarding
              }
            >
              <Scene
                key="Main"
                navigationBarStyle={navStyle.navigationBar}
                leftButtonIconStyle={navStyle.hamburger}
                drawerImage={hamburger}
                backButtonTextStyle={navStyle.backButtonText}
                titleStyle={navStyle.title}
              >
                <Scene
                  key="Home"
                  component={Home}
                  type={ActionConst.RESET}
                  hideNavBar={false}
                  renderRightButton={() => new NotificationIcon({ state: this.state }).render()}
                  backButtonTextStyle={[navStyle.title, navStyle.backButtonText]}
                  backTitle="Back"
                  title="Featured Beers"
                />
                <Scene
                  key="Purchase"
                  title="Purchase"
                  component={Purchase}
                  navigationBarStyle={navStyle.navigationBar}
                  leftButtonIconStyle={navStyle.leftButton}
                  backButtonTextStyle={navStyle.backButtonText}
                  direction="vertical"
                  backTitle="Back"
                />
                <Scene
                  key="FirstTimePurchase"
                  component={FirstTimePurchase}
                  navigationBarStyle={navStyle.navigationBar}
                  leftButtonIconStyle={navStyle.leftButton}
                  backButtonTextStyle={navStyle.backButtonText}
                  direction="vertical"
                  backTitle="Back"
                />
                <Scene
                  key="Receipt"
                  component={Receipt}
                  direction="vertical"
                  hideNavBar
                />
                <Scene
                  key="Payment"
                  component={PaymentMethods}
                  type={ActionConst.RESET}
                  navigationBarStyle={navStyle.navigationBar}
                  title="Payment & Credits"
                />
                <Scene
                  key="AddCard"
                  component={AddCreditCard}
                  navigationBarStyle={[
                    navStyle.navigationBar,
                    navStyle.transparent,
                  ]}
                  backTitle="Back"
                  backButtonTextStyle={navStyle.leftButtonText}
                  leftButtonIconStyle={navStyle.leftButton}
                />
                <Scene
                  key="MyBasket"
                  title={`Your ${utils.getBasketWord()}`}
                  type={ActionConst.RESET}
                  titleStyle={navStyle.title}
                  component={Basket}
                  renderRightButton={() => (
                    <TouchableOpacity
                      style={navStyle.about}
                      // onPress={() => Actions.AboutBasket()}
                    >
                      <Text style={navStyle.aboutText}>?</Text>
                    </TouchableOpacity>
                  )}
                  navigationBarStyle={[navStyle.navigationBar]}
                />
                <Scene
                  key="AboutBasket"
                  title={`About Your ${utils.getBasketWord()}`}
                  titleStyle={navStyle.title}
                  component={AboutBasket}
                  navigationBarStyle={[navStyle.navigationBar]}
                  backTitle="Back"
                  backButtonTextStyle={navStyle.leftButtonText}
                  leftButtonIconStyle={navStyle.leftButton}
                />
                <Scene
                  key="Contact"
                  title="Contact Us"
                  type={ActionConst.RESET}
                  direction="vertical"
                  titleStyle={navStyle.title}
                  component={Contact}
                  navigationBarStyle={[navStyle.navigationBar]}
                />
                <Scene
                  key="MyBeer"
                  title="My Beers"
                  type={ActionConst.RESET}
                  component={MyBeers}
                  navigationBarStyle={[navStyle.navigationBar]}
                />
                <Scene
                  key="ShippedCrates"
                  title={`Shipped ${utils.getBasketWord()}`}
                  type={ActionConst.RESET}
                  component={ShippedCrates}
                  titleStyle={navStyle.title}
                  navigationBarStyle={[navStyle.navigationBar]}
                />
                <Scene
                  key="Addresses"
                  component={Addresses}
                  type={ActionConst.RESET}
                  hideNavBar={false}
                  title="Shipping Address"
                />
                <Scene
                  key="AddAddress"
                  component={addUpdateAddress}
                  hideNavBar={false}
                  navigationBarStyle={navStyle.transparent}
                  backTitle="Back"
                  backButtonTextStyle={navStyle.leftButtonText}
                  leftButtonIconStyle={navStyle.leftButton}
                />
                <Scene
                  key="UpdateAddress"
                  component={addUpdateAddress}
                  hideNavBar={false}
                  navigationBarStyle={navStyle.transparent}
                  backTitle="Back"
                  backButtonTextStyle={navStyle.leftButtonText}
                  leftButtonIconStyle={navStyle.leftButton}
                />
                <Scene
                  key="NotificationSettings"
                  type={ActionConst.RESET}
                  component={NotificationSettings}
                  hideNavBar={false}
                  titleStyle={navStyle.title}
                  navigationBarStyle={[navStyle.navigationBar]}
                  title="Notifications"
                />
                <Scene
                  key="TastingNotes"
                  component={TastingNotes}
                  hideNavBar={false}
                  titleStyle={navStyle.title}
                  navigationBarStyle={[
                    navStyle.navigationBar,
                    navStyle.transparent,
                  ]}
                  backTitle="Back"
                  backButtonTextStyle={navStyle.leftButtonText}
                  leftButtonIconStyle={navStyle.leftButton}
                />
                <Scene
                  key="AccountSettings"
                  title="Account Info"
                  component={AccountSettings}
                  type={ActionConst.RESET}
                />
                <Scene
                  key="Promos"
                  title="Promos"
                  component={Promos}
                  type={ActionConst.RESET}
                />
                <Scene
                  key="Redeem"
                  component={Redeem}
                  navigationBarStyle={[navStyle.navigationBar]}
                  titleStyle={navStyle.title}
                  title="Reedem your code"
                  direction="vertical"
                  backButtonTextStyle={navStyle.leftButtonText}
                  leftButtonIconStyle={navStyle.leftButton}
                />
                <Scene
                  key="CrateCollage"
                  title="Collage"
                  component={crateCollage}
                  navigationBarStyle={[navStyle.navigationBar]}
                  backTitle="Back"
                  backButtonTextStyle={navStyle.leftButtonText}
                  leftButtonIconStyle={navStyle.leftButton}
                />
                <Scene
                  key="AppNotifications"
                  title="Notifications"
                  titleStyle={navStyle.title}
                  component={AppNotifications}
                  navigationBarStyle={[navStyle.navigationBar]}
                  backTitle="Back"
                  backButtonTextStyle={navStyle.leftButtonText}
                  leftButtonIconStyle={navStyle.leftButton}
                />
                <Scene
                  key="Referral"
                  title="Free Beer"
                  type={ActionConst.RESET}
                  titleStyle={navStyle.title}
                  component={Referral}
                  navigationBarStyle={[navStyle.navigationBar]}
                />
              </Scene>
            </Scene>
          </Scene>
        </RouterWithRedux>
      </Provider>
    );
  }
}

const toExport = __DEV__ ? galaxy : codePush(galaxy);
export default toExport;
*/
