export const userStateKey = 'userState';
export const userStateCodeKey = 'userStateCode';
export const userKey = 'user';
export const anonymousIdKey = 'anonymousId';
