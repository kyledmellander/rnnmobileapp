import {
  setJSExceptionHandler,
  setNativeExceptionHandler,
} from 'react-native-exception-handler';
import { Platform, Alert } from 'react-native';
// import 'core-js';
import { Client } from 'bugsnag-react-native';
import app from './feathers';
import store from './store';
import { checkAppOutOfDate } from './appVersion';

// eslint-disable-next-line no-new
const bugsnag = new Client();

let e; // show only one alert a time if multiple errors occur from rejected promises

const ignoredMessages = ['Network request failed'];

const bundleDataForError = (error) => {
  const state = store.getState();
  const data = {
    name: error.name,
    message: error.message,
    stack: error.stack,
    user: state.user.email,
  };

  return data;
};

// TODO: Error bundling hangs, sends freezes detox on FTPF when error about payment type is received
// eslint-disable-next-line import/prefer-default-export
export const reportError = (error) => {
  console.log(error);
  bugsnag.notify(error);
  const bundledError = bundleDataForError(error);
  const service = app.service('errors');
  console.log(bundledError);
  const match = e && e.message ?
    ignoredMessages.find(m => error.message.includes(m)) :
    undefined;
  if (match === undefined) {
    service.create(bundledError);
  }
};

const logErrorGraphite = () => {
  const path = `appv4.${Platform.OS}.errors.count`;
  const timestamp = Math.round(new Date().getTime() / 1000);
  const value = 1;
  app.service('metrics').create({ path, value, timestamp });
};

const jsErrorHandler = (error, isFatal) => {
  console.log('js error', error);
  const needsAppUpdate = checkAppOutOfDate(error);
  logErrorGraphite();
  reportError(error);
  if (needsAppUpdate) return; // Error is caught and displayed by feathers

  if (!isFatal) {
    Alert.alert(
      'Error',
      'Oops, something went wrong... Please try again later'
    );
  } else {
    Alert.alert(
      'Error',
      'Oh no! Something went wrong... Please restart the app.'
    );
  }
};

const nativeExceptionHandler = (exceptionString) => {
  reportError({ name: 'native exception', message: exceptionString });
  logErrorGraphite();
};

window.onunhandledrejection = (promise) => {
  if (promise.reason && !e) {
    e = promise.reason;
    console.log(e);
    if (e.name !== 'NotAuthenticated') {
      jsErrorHandler(promise.reason, false);
    }
  }
};

setJSExceptionHandler(jsErrorHandler);

setNativeExceptionHandler(true, nativeExceptionHandler);
