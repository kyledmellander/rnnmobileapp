/* eslint-disable global-require */
const images = {
  landing: {
    screen1: require('../assets/images/landing/slide1.jpg'),
    screen2: require('../assets/images/landing/slide2.png'),
    screen3: require('../assets/images/landing/slide3.png'),
    screen4: require('../assets/images/landing/slide4.jpg'),
  },
  splash: require('../assets/images/general/splashScreen.png'),
  howToTavour: {
    getIt: require('../assets/images/howToTavour/getIt.png'),
    notified: require('../assets/images/howToTavour/notified.png'),
    crate: require('../assets/images/howToTavour/crate.png'),
    delivery: require('../assets/images/howToTavour/delivery.png')
  }
};

export const icons = {
  close: require('../assets/images/general/close.png'),
  crate: require('../assets/images/receipt/crate.png'),
  shareIcon: require('../assets/images/sharing/share-icon3x.png'),
  genericCard: require('../assets/images/cards/generic-card.png'),
  socialIcon: require('../assets/images/sharing/share-icon3x.png'),
  socialMediaIcons: {
    facebookIcon: require('../assets/images/socialMediaIcons/facebookIcon.png'),
    instagramIcon: require('../assets/images/socialMediaIcons/instagramIcon.png'),
    twitterIcon: require('../assets/images/socialMediaIcons/twitterIcon.png'),
    brewersAssociationSeal: require('../assets/images/socialMediaIcons/brewersAssociationSeal.png')
  },

  visaCard: require('../assets/images/cards/512/Visa.png'),
  masterCard: require('../assets/images/cards/512/MasterCard.png'),
  americanExpressCard: require('../assets/images/cards/512/AmericanExpress.png'),
  dinersClubCard: require('../assets/images/cards/512/DinersClub.png'),
  discoverCard: require('../assets/images/cards/512/Discover.png'),
  jcbCard: require('../assets/images/cards/512/JCB.png'),
  unionPayCard: require('../assets/images/cards/generic-card.png'),
  maestroCard: require('../assets/images/cards/512/Maestro.png'),
};

export default images;
