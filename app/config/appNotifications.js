import { Linking } from 'react-native';
import { actions as appNotificationActions } from 'appRedux/appNotifications';
import * as navUtils from 'lib/navUtils';
import { dispatch } from './store';

// Need to figure out how to switch this to react-native-navigation
function routeFromURL(url) {
  const path = url.replace(/.*?:\/\//g, '');
  const route = path.split('/')[0];
  try {
    console.log(route);
    navUtils.setStackRoot('Home');
    // Actions[route]();
  } catch (e) {
    console.log(e);
  }
}

let initialURL;
function handleURL(event) {
  console.log('handling event', event);
  routeFromURL(event.url);
}

function addURLListener() {
  Linking.addEventListener('url', handleURL);
}

function removeURLListener() {
  Linking.removeEventListener('url', handleURL);
}

async function getInitialURL() {
  if (initialURL) return initialURL;
  return Linking.getInitialURL();
}

function fireUpdateState() {
  if (initialURL) { dispatch(appNotificationActions.setDeepLink(initialURL)); }
}

function setInitialURL(url) {
  initialURL = url;
  fireUpdateState();
}

function clearInitialURL() {
  initialURL = null;
  dispatch(appNotificationActions.clearDeepLink());
}

export default {
  addURLListener,
  removeURLListener,
  getInitialURL,
  routeFromURL,
  setInitialURL,
  clearInitialURL,
  fireUpdateState,
};
