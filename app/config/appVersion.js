import { Linking, Platform, Alert } from 'react-native';

export function checkAppOutOfDate(error) {
  if (Platform.OS === 'ios') {
    return error.code === 426 && error.data && error.data.minIOSVersion;
  }
  return error.code === 426 && error.data && error.data.minAndroidVersion;
}


let displayingAlert = false;
export function getOutOfDateAlert() {
  if (displayingAlert) return; // Don't show multiple alerts if multiple API requests are sent
  displayingAlert = true;

  const buttonText = Platform.OS === 'ios' ? 'App Store' : 'Play Store';
  let linkTo = 'https://itunes.apple.com/us/app/tavour/id956371806?mt=8';
  if (Platform.OS === 'android') {
    linkTo = 'https://play.google.com/store/apps/details?id=com.tavour.app';
  }

  Alert.alert(
    'Update Required',
    'Our apologies for the inconvenience, but this version is no longer supported. To continue using Tavour, Please update your app here!',
    [
      { text: buttonText, onPress: () => { Linking.openURL(linkTo); displayingAlert = false; } },
    ],
    { cancelable: false }
  );
}

export function verifyAppVersion(hook) {
  if (checkAppOutOfDate(hook.error)) {
    getOutOfDateAlert();
  }
}

