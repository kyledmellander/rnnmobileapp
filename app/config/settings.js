/* global GLOBAL */
const ignoredWarnings = [
  'Warning: PropTypes',
  'Warning: React.createClass',
  'Warning: View.propTypes',
  'Warning: checkPropTypes',
  'Warning: BackAndroid',
  'Possible Unhandled Promise Rejection',
];

const dev = {
  apiEndpoint: 'http://localhost:3030/app',
  stripeKey: 'pk_0JP9fw33ykUfhta17vhMkjiV1NMVT',
  gcmSenderId: '812109116648',
  segmentIoWriteKey: 'auGalBsiS5iQhamQZ3giJxW06Rz0TRPo',
  cognito: {
    region: 'us-west-2',
    userPoolId: 'us-west-2_lAtB8z7Ud',
    userPoolWebClientId: '743s91df9k1melpgdlsli2tbgk'
  }
};

const prod = {
  apiEndpoint: 'https://api.tavour.com/app',
  stripeKey: 'pk_0JP9dSwyAeSTcMalrAlSGvxvcQGXW',
  gcmSenderId: '812109116648',
  segmentIoWriteKey: 'OCMA2pJC93fqHr0TxSWuBSTg4H3lqQvs',
  cognito: {
    region: 'us-west-2',
    userPoolId: 'us-west-2_15tpq125S',
    userPoolWebClientId: '10d31h3tuqgto56ei3kl7hu09b'
  }
};

function getEnv() {
  if (__DEV__) {
    // eslint-disable-next-line no-console
    console.ignoredYellowBox = ignoredWarnings;
    GLOBAL.XMLHttpRequest =
      GLOBAL.originalXMLHttpRequest || GLOBAL.XMLHttpRequest;
    return dev;
  }
  return prod;
}

export default getEnv();
