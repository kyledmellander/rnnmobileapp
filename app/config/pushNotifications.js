import PushNotification from 'react-native-push-notification';
import { AsyncStorage } from 'react-native';
import app from './feathers';
import appNotifications from './appNotifications';
import Settings from './settings';

const notificationsEnabledKey = 'notificationsEnabled';
const storedTokenKey = 'pnToken';

PushNotification.configure({
  // (optional) Called when Token is generated (iOS and Android)
  async onRegister(token) {
    const oldToken = await AsyncStorage.getItem(storedTokenKey);
    if (oldToken !== token && oldToken !== token.token) {
      const newToken = token.token ? token.token : token;
      AsyncStorage.setItem(storedTokenKey, newToken);
      app.service('devices').create({ token: newToken });
    }
  },

  // (required) Called when a remote or local notification is opened or received
  onNotification(notification) {
    console.log(notification);
    const { data } = notification;
    // TODO show in-app notification when app is in foreground
    if (!notification.foreground && data && data.deepLink) {
      console.log('testing deepLink', notification.data.deepLink);
      appNotifications.setInitialURL(notification.data.deepLink);
    }
    if (notification.userInteraction) {
      if (data && notification.data.deepLink) {
        console.log('testing deepLink', notification.data.deepLink);
        appNotifications.setInitialURL(notification.data.deepLink);
      }
    }
  },

  // ANDROID ONLY: GCM Sender ID (optional - not required for local notifications
  // but is need to receive remote push notifications)
  senderID: Settings.gcmSenderId,

  // IOS ONLY (optional): default: all - Permissions to register.
  permissions: {
    alert: true,
    badge: true,
    sound: true,
  },

  // Should the initial notification be popped automatically
  // default: true
  popInitialNotification: true,

  /**
   * (optional) default: true
   * - Specified if permissions (ios) and token (android and ios) will requested or not,
   * - if not, you must call PushNotificationsHandler.requestPermissions() later
   */
  requestPermissions: false,
});

async function requestPermissionsIfEnabled() {
  const enabled = await this.notificationsEnabled();
  if (enabled) {
    PushNotification.requestPermissions();
  }
  appNotifications.fireUpdateState();
}

async function setNotificationsEnabled(val) {
  const service = app.service('devices');
  const token = await AsyncStorage.getItem(storedTokenKey);
  const update = t => service.update(null, { token: t });
  const create = t => service.create({ token: t });
  const method = val ? create : update;
  try {
    await method(token);
    return AsyncStorage.setItem(notificationsEnabledKey, val.toString());
  } catch (e) {
    throw e; // todo helpful message
  }
}

async function notificationsEnabled() {
  let enabled = await AsyncStorage.getItem(notificationsEnabledKey);
  if (enabled === null) {
    enabled = true;
  }
  return JSON.parse(enabled);
}

export default {
  requestPermissionsIfEnabled,
  setNotificationsEnabled,
  notificationsEnabled,
};
