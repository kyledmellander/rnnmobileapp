import { AsyncStorage, Platform } from 'react-native';
import uuid from 'react-native-uuid';
import deviceInfo from 'react-native-device-info';
import hooks from 'feathers-hooks';
import feathers from 'feathers/client';
import rest from 'feathers-rest/client';
import config from './settings';
import { verifyAppVersion } from './appVersion';
import { signIn, currentSession, signOut, migrateUser } from './cognito';

const tokenKey = 'app-token';
const refreshTokenKey = 'refreshToken';
const options = {
  headers: {
    'X-Platform': Platform.OS,
    'X-Session-Id': uuid.v4(),
    'X-Version': deviceInfo.getVersion(),
    'X-Build': deviceInfo.getBuildNumber(),
    'X-Device': deviceInfo.getModel(),
  },
};

async function migrateUserTokens(u, token) {
  // transfer user tokens from old async storage keys to aws-amplify keys
  const storedUser = await AsyncStorage.getItem('user');
  const storedUserJson = storedUser ? JSON.parse(storedUser) : null;
  const userJson = u || storedUserJson;
  const refreshToken = token || await AsyncStorage.getItem(refreshTokenKey);
  if (userJson && userJson.email && refreshToken) {
    try {
      await migrateUser(userJson.email, refreshToken);
      await AsyncStorage.removeItem(refreshTokenKey);
      await AsyncStorage.removeItem(tokenKey);
      return currentSession();
    } catch (e) {
      console.log('error migrating tokens', e);
      return Promise.reject();
    }
  }
  return Promise.resolve();
}

async function getSessionToken() {
  // if using old storage keys, migrate to new keys
  const token = await AsyncStorage.getItem(tokenKey);
  if (token) {
    return migrateUserTokens();
  }
  return currentSession();
}

async function beforeHook(hook) {
  hook.params.headers = {};
  hook.params.headers['X-Correlation-Id'] = uuid.v4();
  const authToken = await getSessionToken();
  if (authToken && !hook.app.get('accessToken')) {
    hook.params.headers.Authorization = authToken;
  }
}

function setSessionToken(user, token) {
  if (!token) {
    return Promise.resolve();
  }
  // migrate any server-generated tokens to aws-amplify local storage
  return migrateUserTokens(user, token);
}

function clearSession() {
  return signOut();
}

function getNewTokens() {
  return migrateUserTokens();
}

async function signOutOrRefresh(hook) {
  if (hook.error && hook.error.name === 'NotAuthenticated') {
    const newToken = await hook.app.getNewTokens();
    hook.params.headers.Authorization = newToken;
  }
}

const app = feathers()
  .configure(hooks())
  .configure(rest(config.apiEndpoint).fetch(fetch, options))
  .hooks({
    before: [beforeHook],
    error: [verifyAppVersion, signOutOrRefresh]
  });

async function authenticateRails(data) {
  const session = await app.service('sessions').create(data);
  if (!session) {
    return session;
  }
  await migrateUserTokens(data.email, session.refreshToken);
  return signIn(data.email, data.password);
}

async function authenticate(data) {
  try {
    const result = await signIn(data.email, data.password);
    return result;
  } catch (e) {
    if (e.code === 'UserNotFoundException') {
      return authenticateRails(data);
    }
    throw e;
  }
}

function refreshTokens() {
  return currentSession();
}


app.getSessionToken = getSessionToken.bind(app);
app.clearSession = clearSession.bind(app);
app.setSessionToken = setSessionToken.bind(app);
app.authenticate = authenticate;
app.getNewTokens = getNewTokens.bind(app);
app.refreshTokens = refreshTokens.bind(app);


export default app;
