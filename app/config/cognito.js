import Amplify, { Auth } from 'aws-amplify';
import { CognitoUser, CognitoUserPool } from 'amazon-cognito-identity-js';
import settings from './settings';

let session;
Amplify.configure({
  Auth: settings.cognito
});

export async function signIn(username, password) {
  const result = await Auth.signIn(username, password);
  return result;
}

export async function currentSession() {
  try {
    if (session && session.isValid()) {
      return session.accessToken.jwtToken;
    }
    session = await Auth.currentSession();
    return session.accessToken.jwtToken;
  } catch (e) {
    console.log('aws-amplify error', e);
    return null;
  }
}

export async function signUp(userId, password, email) {
  await Auth.signUp({
    username: `${userId}`,
    password,
    attributes: {
      email
    }
  });
  return signIn(email, password);
}

export function signOut() {
  Auth.signOut();
}

export async function migrateUser(email, refreshToken) {
  Auth.signOut(); // clear existing token data
  const userPool = new CognitoUserPool({
    UserPoolId: settings.cognito.userPoolId,
    ClientId: settings.cognito.userPoolWebClientId
  });
  const cognitoUser = new CognitoUser({
    Username: email,
    Pool: userPool
  });
  const proxyPromise = new Promise((resolve, reject) => {
    cognitoUser.refreshSession({
      getToken: () => refreshToken
    }, (err, val) => {
      if (val) {
        console.log(`successfully refreshed session ${JSON.stringify(val)}`);
        Amplify.configure({
          Auth: settings.cognito
        }); // reconfigure to re-init the Auth class with new tokens
        resolve(val);
      } else {
        console.log(`error refreshing session ${err}`);
        reject(err);
      }
    });
  });
  return proxyPromise;
}
