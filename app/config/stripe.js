import Stripe from 'react-native-stripe-api';
import config from './settings';

const stripeClient = new Stripe(config.stripeKey);

export default stripeClient;
