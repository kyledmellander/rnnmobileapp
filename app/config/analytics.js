import Analytics from 'analytics-node';
import config from './settings';

// send each event right away
const analytics = new Analytics(config.segmentIoWriteKey, { flushAt: 1 });

export default analytics;

