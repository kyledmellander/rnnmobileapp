import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import reducers from '../redux';

const middleware = [thunk];
if (__DEV__) middleware.push(logger);

const reducer = combineReducers(reducers);
const rootReducer = (state, action) => {
  let newState = state;
  if (action.type === 'LOGOUT') {
    newState = undefined;
  }
  return reducer(newState, action);
};
const store = createStore(rootReducer, applyMiddleware(...middleware));

const { dispatch } = store;

export { dispatch };
export default store;
