import React, { Component } from 'react';
import { colors, appStyles } from 'styles';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome';
import { TouchableOpacity, View, Text } from 'react-native';
import * as navUtils from 'lib/navUtils';
import { selectors } from './redux/appNotifications';

const style = {
  paddingTop: 3,
  height: 30,
  width: 35,
};

const numberStyle = {
  backgroundColor: colors.tavourOrange,
  borderRadius: 24,
  height: 18,
  width: 18,
  marginLeft: 0,
  justifyContent: 'center',
  alignItems: 'center',
  position: 'absolute'
};

const iconStyle = {
  alignSelf: 'center'
};

const textStyle = {
  color: 'white',
  backgroundColor: 'transparent',
  fontSize: 14,
  fontFamily: appStyles.fontFamilySemibold
};

class notificationIcon extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeNotifications: props.notificationCount ||
      selectors.activeNotificationCountSelector(props.state)
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      activeNotifications: selectors.activeNotificationCountSelector(nextProps.state)
    });
  }

  getCount() {
    if (this.state.activeNotifications === 0) return null;
    return (
      <View style={numberStyle}>
        <Text style={textStyle}>{this.state.activeNotifications}</Text>
      </View>
    );
  }

  render() {
    return (
      <TouchableOpacity style={style} onPress={() => navUtils.pushScreen('AppNotifications')}>
        <FontAwesomeIcons
          name="bell-o"
          color={colors.titleGray}
          size={25}
          style={iconStyle}
        />
        {this.getCount()}
      </TouchableOpacity>
    );
  }
}

export default notificationIcon;
