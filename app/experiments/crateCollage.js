import React, { Component } from 'react';
import { ShareDialog } from 'react-native-fbsdk';
import Share, { Button } from 'react-native-share';
import {
  PermissionsAndroid,
  Platform,
  View,
  Image,
  Linking,
} from 'react-native';
import RNFetchBlob from 'react-native-fetch-blob';
import { connect } from 'react-redux';
import { actions as metricActions } from 'appRedux/metrics';
import utils from 'lib/utils';
import * as navUtils from 'lib/navUtils';

const styles = {
  container: {
    flex: 100,
    marginTop: '10%',
  },
  photo: {
    flex: 90,
  },
  buttonContainer: {
    flex: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    flex: 1,
  },
};

async function checkUrlScheme(base) {
  const installed = await Linking.canOpenURL(`${base}://`);
  return installed;
}

async function twitterInstalled() {
  return checkUrlScheme('twitter');
}

async function facebookInstalled() {
  return checkUrlScheme('fb');
}

async function requestPermissionsAndroid() {
  const externalStorage =
    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE;
  let granted = await PermissionsAndroid.check(externalStorage);
  if (granted) {
    return true;
  }
  granted = await PermissionsAndroid.request(
    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
    {
      title: 'Temporary Storage',
      message:
        "We'll just need to temporarily store the collage before you can share it",
    }
  );
  return granted;
}
function calculateOffsets(row, col, baseParams) {
  let x;
  let y;
  if (row === 0) {
    // hacky guess at correct offset, actually based on number of columns in collage
    const temp = col - (0.5 * (col - 1));
    x = baseParams.width * temp;
  } else {
    x = (col - 1) * baseParams.width;
  }

  if (row === 0) {
    y = 0;
  } else {
    // eslint-disable-next-line
    if (col === 0) {
      y = (baseParams.height * row) - (baseParams.height * 0.5 * (row - 1));
    } else {
      y = (baseParams.height * row) - (baseParams.height * 0.5 * row);
    }
  }

  return {
    x,
    y
  };
}

function getBaseParams(pic) {
  const shrinkFactor = 6;
  const width = Math.floor(pic.width / shrinkFactor);
  const height = Math.floor(pic.height / shrinkFactor);
  return {
    root: `${pic.host}w_${width},h_${height},c_fit/`,
    width,
    height,
    transform: 'c_fill',
    id: pic.publicId,
    shrinkFactor,
  };
}

function getUrl(orderProducts) {
  const pics = orderProducts.reduce((acc, o) => {
    const photo = o.offerProduct.product.photos[0];
    if (photo) acc.push(photo);
    return acc;
  }, []);
  if (!pics || pics.length === 0) return '';

  const basePic = pics[0];
  const baseParams = getBaseParams(basePic);
  const urlSegments = [];
  const numRows = 3;
  const numColumns = 3; // dont change this number as the hack above will break
  let currentPicIndex = 1;
  for (let row = 0; row < numRows; row += 1) {
    for (let col = 0; col < numColumns; col += 1) {
      if (currentPicIndex > pics.length - 1) {
        break;
      }
      if (row === 0 && col === 0) {
        // eslint-disable-next-line no-continue
        continue;
      }
      const currentPic = pics[currentPicIndex];
      const offsets = calculateOffsets(row, col, baseParams);
      const params = [
        `l_${currentPic.publicId}`,
        `w_${baseParams.width}`,
        `h_${baseParams.height}`,
        `x_${Math.floor(offsets.x)}`,
        `y_${Math.floor(offsets.y)}`,
        `${baseParams.transform}`
      ];
      const segment = `${params.join(',')}/`;
      urlSegments.push(segment);
      currentPicIndex += 1;
    }
  }
  const collageUrl = `${baseParams.root}${urlSegments.join('')}${baseParams.id}.jpg`;
  return collageUrl;
}

const TWITTER_ICON =
  'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAMAAAANIilAAAABvFBMVEUAAAAA//8AnuwAnOsAneoAm+oAm+oAm+oAm+oAm+kAnuwAmf8An+0AqtUAku0AnesAm+oAm+oAnesAqv8An+oAnuoAneoAnOkAmOoAm+oAm+oAn98AnOoAm+oAm+oAmuoAm+oAmekAnOsAm+sAmeYAnusAm+oAnOoAme0AnOoAnesAp+0Av/8Am+oAm+sAmuoAn+oAm+oAnOoAgP8Am+sAm+oAmuoAm+oAmusAmucAnOwAm+oAmusAm+oAm+oAm+kAmusAougAnOsAmukAn+wAm+sAnesAmeoAnekAmewAm+oAnOkAl+cAm+oAm+oAmukAn+sAmukAn+0Am+oAmOoAmesAm+oAm+oAm+kAme4AmesAm+oAjuMAmusAmuwAm+kAm+oAmuoAsesAm+0Am+oAneoAm+wAmusAm+oAm+oAm+gAnewAm+oAle0Am+oAm+oAmeYAmeoAmukAoOcAmuoAm+oAm+wAmuoAneoAnOkAgP8Am+oAm+oAn+8An+wAmusAnuwAs+YAmegAm+oAm+oAm+oAmuwAm+oAm+kAnesAmuoAmukAm+sAnukAnusAm+oAmuoAnOsAmukAqv9m+G5fAAAAlHRSTlMAAUSj3/v625IuNwVVBg6Z//J1Axhft5ol9ZEIrP7P8eIjZJcKdOU+RoO0HQTjtblK3VUCM/dg/a8rXesm9vSkTAtnaJ/gom5GKGNdINz4U1hRRdc+gPDm+R5L0wnQnUXzVg04uoVSW6HuIZGFHd7WFDxHK7P8eIbFsQRhrhBQtJAKN0prnKLvjBowjn8igenQfkQGdD8A7wAAAXRJREFUSMdjYBgFo2AUDCXAyMTMwsrGzsEJ5nBx41HKw4smwMfPKgAGgkLCIqJi4nj0SkhKoRotLSMAA7Jy8gIKing0KwkIKKsgC6gKIAM1dREN3Jo1gSq0tBF8HV1kvax6+moG+DULGBoZw/gmAqjA1Ay/s4HA3MISyrdC1WtthC9ebGwhquzsHRxBfCdUzc74Y9UFrtDVzd3D0wtVszd+zT6+KKr9UDX749UbEBgULIAbhODVHCoQFo5bb0QkXs1RAvhAtDFezTGx+DTHEchD8Ql4NCcSyoGJYTj1siQRzL/JKeY4NKcSzvxp6RmSWPVmZhHWnI3L1TlEFDu5edj15hcQU2gVqmHTa1pEXJFXXFKKqbmM2ALTuLC8Ak1vZRXRxa1xtS6q3ppaYrXG1NWjai1taCRCG6dJU3NLqy+ak10DGImx07LNFCOk2js6iXVyVzcLai7s6SWlbnIs6rOIbi8ViOifIDNx0uTRynoUjIIRAgALIFStaR5YjgAAAABJRU5ErkJggg==';
const FACEBOOK_ICON =
  'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAMAAAANIilAAAAAYFBMVEUAAAAAQIAAWpwAX5kAX5gAX5gAX5gAXJwAXpgAWZ8AX5gAXaIAX5gAXpkAVaoAX5gAXJsAX5gAX5gAYJkAYJkAXpoAX5gAX5gAX5kAXpcAX5kAX5gAX5gAX5YAXpoAYJijtTrqAAAAIHRSTlMABFis4vv/JL0o4QvSegbnQPx8UHWwj4OUgo7Px061qCrcMv8AAAB0SURBVEjH7dK3DoAwDEVRqum9BwL//5dIscQEEjFiCPhubziTbVkc98dsx/V8UGnbIIQjXRvFQMZJCnScAR3nxQNcIqrqRqWHW8Qd6cY94oGER8STMVioZsQLLnEXw1mMr5OqFdGGS378wxgzZvwO5jiz2wFnjxABOufdfQAAAABJRU5ErkJggg==';

class crateCollage extends Component {
  static options() {
    return navUtils.getOptions('Collage');
  }

  constructor(props) {
    super(props);
    const url = getUrl(props.orderProducts);
    const shareLink = `${utils.getReferralLink(props.state)}&invitedFrom=collage`;
    const platform = Platform.OS;
    this.state = {
      url,
      shareLink,
      platform,
    };

    this.downloadFile = this.downloadFile.bind(this);
    this.getShareParams = this.getShareParams.bind(this);
    this.shareTwitter = this.shareTwitter.bind(this);
    this.shareFacebook = this.shareFacebook.bind(this);
    this.componentWillMount = this.componentWillMount.bind(this);
  }

  async componentWillMount() {
    const { addMetric } = this.props;
    const { platform } = this.state;
    const showFacebook = await facebookInstalled();
    const showTwitter = await twitterInstalled();
    this.setState({
      showFacebook,
      showTwitter,
    });

    addMetric(`appv4.${platform}.collage.opened`, 1);
  }

  getShareParams(platform) {
    const { shareLink, base64 } = this.state;
    return {
      title: `Look, It's beer ${shareLink}`,
      type: 'image/jpg',
      message: `BEER ${shareLink}`,
      social: platform,
      url: `data:image/jpg;base64,${base64}`,
      subject: `BEER! ${shareLink}`,
    };
  }

  getFacebookButton() {
    if (this.state.showFacebook) {
      return (
        <Button iconSrc={{ uri: FACEBOOK_ICON }} onPress={this.shareFacebook} />
      );
    }
    return null;
  }

  getTwitterButton() {
    if (this.state.showTwitter && Platform.OS === 'android') {
      return (
        <Button iconSrc={{ uri: TWITTER_ICON }} onPress={this.shareTwitter} />
      );
    }
    return null;
  }

  async shareFacebook() {
    if (!this.state.path) {
      await this.downloadFile();
    }
    const { path } = this.state;
    if (!path) return;
    const sharePhotoContent = {
      contentType: 'photo',
      photos: [
        {
          imageUrl: path,
          userGenerated: true,
        },
      ],
    };
    const canShow = await ShareDialog.canShow(sharePhotoContent);
    if (canShow) {
      await ShareDialog.show(sharePhotoContent);
      this.props.addMetric(`appv4.${this.state.platform}.collage.facebook`, 1);
    }
  }

  async shareTwitter() {
    if (!this.state.base64) {
      await this.downloadFile();
    }
    const shareParams = this.getShareParams('twitter');
    await Share.open(shareParams);
    this.props.addMetric(`appv4.${this.state.platform}.collage.twitter`, 1);
  }

  async downloadFile() {
    const collageUrl = this.state.url;
    const { dirs } = RNFetchBlob.fs;
    if (this.state.platform === 'android') {
      if (!await requestPermissionsAndroid()) return;
    }
    const config = {
      fileCache: true,
      path: `${dirs.DocumentDir}/tempcollage.jpg`,
    };
    const file = await RNFetchBlob.config(config).fetch('GET', collageUrl);
    const path = `file://${file.path()}`;
    const base64 = await file.base64();
    this.setState({ path, base64 });
  }

  render() {
    const { url } = this.state;

    return (
      <View style={styles.container}>
        <Image
          style={styles.photo}
          source={{ uri: url }}
          resizeMode="contain"
        />
        <View style={styles.buttonContainer}>
          {this.getTwitterButton()}
          {this.getFacebookButton()}
        </View>
      </View>
    );
  }
}

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {
    addMetric(...args) {
      dispatch(metricActions.addMetric(...args));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(crateCollage);
