import store from 'store';
import { selectors } from 'appRedux/user';
import { Platform } from 'react-native';
import moment from 'moment';

const getBasketWord = (lowercase = false) => (lowercase
  ? selectors.crateWordLowerSelector(store.getState())
  : selectors.crateWordSelector(store.getState()));

const getReferralLink = state =>
  (state.user ? `http://www.tavour.com/?invitedby=${state.user.id}` : '');

// https://stackoverflow.com/questions/149055/how-can-i-format-numbers-as-dollars-currency-string-in-javascript
const formatMoney = n => parseFloat(n).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');

export const userIsEven = user => (!(user.id % 2));

export const emDash = '\u2014';

export function getStoreURL() {
  if (Platform.OS === 'ios') return 'https://itunes.apple.com/us/app/tavour/id956371806?mt=8';

  return 'https://play.google.com/store/apps/details?id=com.tavour.app';
}

export const nDaysFromNow = numDays => moment().add(numDays, 'days').toISOString();

const formatDate = (date) => {
  let month;
  if (Platform.OS === 'ios') {
    month = date.toLocaleString('en-us', { month: 'short' });
  } else {
    const monthNames = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sept',
      'Oct',
      'Nov',
      'Dec',
    ];
    month = monthNames[date.getMonth()];
  }
  const day = date.getDate();
  return `${month} ${day}`;
};

const formatDateMMDDYY = date =>
  `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear().toString().substr(-2)}`;

export default {
  getBasketWord,
  getReferralLink,
  formatMoney,
  formatDate,
  formatDateMMDDYY
};
