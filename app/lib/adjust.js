import { Adjust, AdjustConfig, AdjustEvent } from 'react-native-adjust';
import app from '../config/feathers';

let userData;
let initialized = false;
let attributionData;
function init() {
  const config = new AdjustConfig(
    '75nsgxj30mm8',
    __DEV__ ? AdjustConfig.EnvironmentSandbox : AdjustConfig.EnvironmentProduction
  );
  config.setLogLevel(AdjustConfig.LogLevelVerbose);
  config.setAttributionCallbackListener((data) => {
    if (userData) {
      app.service('registration').create(data);
    } else {
      attributionData = data;
    }
  });
  Adjust.create(config);
  initialized = true;
}

function getAttribution() {
  if (attributionData && userData) {
    app.service('registration').create(attributionData);
  } else {
    Adjust.getAttribution((data) => {
      attributionData = data;
    });
  }
}

const adjustEvent = new AdjustEvent('8wtmx3');
const registrationEvent = new AdjustEvent('y5qq0q');

function firePurchaseEvent() {
  if (!initialized) init();
  Adjust.trackEvent(adjustEvent);
}

function fireRegistrationEvent() {
  if (!initialized) init();
  Adjust.trackEvent(registrationEvent);
}

function setUser(user) {
  userData = user;
}

export default {
  init, getAttribution, setUser, firePurchaseEvent, fireRegistrationEvent
};
