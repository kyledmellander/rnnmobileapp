import { AsyncStorage } from 'react-native';
import uuid from 'react-native-uuid';
import analyticsClient from 'src/config/analytics';
import { anonymousIdKey, userKey } from 'src/config/storageConsts';
import { reportError } from 'src/config/exceptions';

// return getAnonymousId, creates if doesn't exist
export const getAnonymousId = (() => {
  let anonymousId;
  return async () => {
    if (anonymousId) { return anonymousId; }
    anonymousId = await AsyncStorage.getItem(anonymousIdKey);
    if (!anonymousId) {
      anonymousId = uuid.v4();
      await AsyncStorage.setItem(anonymousIdKey, anonymousId);
    }
    return anonymousId;
  };
})();

// return userId based on user in AsyncStorage or undefined if it doesn't exist
// will store first userId it encounters, so if a user relogs it will still register first user
export const getUserId = (() => {
  // perfomance might be bad for lots of events on an unlogged in user, since we check asyncStorage
  // for user every time. Possible alternative would be to save userId after identify call if we
  // can ensure identify event is called before any other events
  let userId;
  return async () => {
    if (userId) { return userId; }
    if (!userId) {
      const userString = await AsyncStorage.getItem(userKey);
      if (userString) {
        const user = JSON.parse(userString);
        userId = user.id;
      }
    }
    return userId;
  };
})();

export const addUserIdsToEvent = async (event) => {
  const userId = await exports.getUserId();
  const anonymousId = await exports.getAnonymousId();
  return {
    userId,
    anonymousId,
    ...event
  };
};

export const analyticsMethod = async (method, event) => {
  try {
    analyticsClient[method](await exports.addUserIdsToEvent(event));
  } catch (error) {
    reportError(error);
  }
};

export default {
  async identify(event) {
    await exports.analyticsMethod('identify', event);
  },
  async track(event) {
    await exports.analyticsMethod('track', event);
  },
  async page(event) {
    await exports.analyticsMethod('page', event);
  },
};
