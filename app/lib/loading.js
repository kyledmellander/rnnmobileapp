import { Dimensions } from 'react-native';

const darkVal = '#d2d2d2';
const lightVal = '#eeeeee';
const lighterVal = '#f2f2f2';

const darkToLight = (animatedValue, firstColor = darkVal, secondColor = lightVal) => {
  const color = animatedValue.interpolate({
    inputRange: [0, 1],
    outputRange: [firstColor, secondColor],
  });
  return { backgroundColor: color };
};

const lightToLighter = (animatedValue) => {
  const color = animatedValue.interpolate({
    inputRange: [0, 1],
    outputRange: [lightVal, lighterVal],
  });
  return { backgroundColor: color };
};

const increaseOpacity = (animatedValue) => {
  const opacity = animatedValue.interpolate({
    inputRange: [0, 1],
    outputRange: [0, 1],
  });
  return { opacity };
};

const increaseFlex = (animatedValue, inputRange = [0, 1], outputRange = [0, 1]) => {
  const flex = animatedValue.interpolate({
    inputRange,
    outputRange,
  });
  return { flex };
};

const leftToRight = (animatedValue) => {
  const max = Dimensions.get('window').width;
  const position = animatedValue.interpolate({
    inputRange: [0, 1],
    outputRange: [0, max],
  });
  return { position: 'absolute', left: position };
};

export default {
  darkToLight,
  lightToLighter,
  increaseOpacity,
  increaseFlex,
  leftToRight,
};
