import { Platform } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { Icons } from 'components';
import { dispatch } from 'store';
import { types } from 'appRedux/routes';



function setDefaultOptions() {
  Navigation.setDefaultOptions({
    topBar: {
      buttonColor: 'black',
      backButton: {
        color: 'black',
      },
      borderHeight: 1,
      borderColor: '#ddd',
      visible: true,
      hideOnScroll: false,
      drawBehind: false,
      elevation: 0,
      title: {
        alignment: 'fill',
      }
    }
  });
}

function signUpFlow(nextScreen) {
  let children = [
    { component: { name: 'Landing' } },
    { component: { name: 'ConfirmAge' } },
  ];

  if (nextScreen === 'CreateLogin') {
    children.push({ component: { name: 'CreateLogin' } });
  } else if (nextScreen === 'EnterZip') {
    children = [
      { component: { name: 'EnterZip' } }
    ];
  } else if (nextScreen === 'Onboarding1') {
    children = [
      { component: { name: 'Onboarding1' } }
    ];
  }

  Navigation.setRoot({
    root: {
      stack: {
        id: 'APP_STACK',
        children,
      }
    }
  });
}

// There is a bug in RNN, that makes getting top bar height inconsistent
// However, the top bar height if visible is 44 for all IOS platforms, and only the
// status bar height changes. (For the notch in iphoneX, XR, XS). We know on the screen
// level if there is a top bar visible, so this is a workaround until that is fixed.
function getTopOffset(topBarVisible = true) {
  return Navigation.constants().then((constants) => {
    let { topBarHeight } = constants;
    const { statusBarHeight } = constants;

    if (Platform.OS === 'ios' && !topBarHeight) {
      topBarHeight = 44;
    }

    return topBarVisible ? topBarHeight + statusBarHeight : statusBarHeight;
  });
}

function pushScreen(screenName, options = {}) {
  Navigation.push('APP_STACK', { component: { name: screenName, passProps: { ...options.passProps } } });
}

function setStackRoot(screenName, options = {}) {
  Navigation.setStackRoot('APP_STACK', {
    component: {
      name: screenName,
      passProps: {
        ...options.passProps
      }
    }
  });
}

function pop() {
  Navigation.pop('APP_STACK');
}

const defaultOptions = {
  showHamburger: false,
  topBar: {},
};

function getOptions(title, options = defaultOptions) {
  const { showHamburger, ...extras } = options;
  if (showHamburger) {
    return {
      topBar: {
        elevation: 0,
        buttonColor: 'black',
        title: {
          component: {
            name: 'NavBarTitle',
            passProps: {
              title,
            },
            alignment: 'center',
          },
        },
        leftButtons: [
          {
            id: 'hamburger',
            icon: Icons.hamburger,
            color: 'black',
            testID: 'navLeftButton',
          }
        ],
        ...extras.topBar,
      }
    };
  }

  return {
    topBar: {
      elevation: 0,
      buttonColor: 'black',
      backButton: {
        color: 'black',
      },
      title: {
        component: {
          name: 'NavBarTitle',
          passProps: {
            title,
          },
          alignment: 'center',
        },
      },
      ...extras.topBar,
    }
  };
}

function registerGlobalNavEvents() {
  Navigation.events().registerNavigationButtonPressedListener(({ buttonId }) => {
    if (buttonId === 'hamburger') {
      Navigation.showOverlay({
        component: {
          id: 'Drawer',
          name: 'Drawer'
        }
      });
    }
  });

  Navigation.events().registerComponentDidAppearListener(({ componentName }) => {
    dispatch({ type: types.setCurrentScreen, screenName: componentName });
  });
}

export {
  getTopOffset,
  getOptions,
  pop,
  pushScreen,
  registerGlobalNavEvents,
  setDefaultOptions,
  setStackRoot,
  signUpFlow,
};
