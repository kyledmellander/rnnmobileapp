const addressPage = {
  AddButtonText: 'Add another shipping address',
};

const messages = {
  addedAddress: 'Nice, your address has been added!',
  updatedAddress: 'Nice, your address has been updated!',
  removedAddress: 'Nice, your address has been deleted!',
  updatedDefault: 'Nice, saved new default address!',
};

export default {
  addressPage,
  messages,
};
