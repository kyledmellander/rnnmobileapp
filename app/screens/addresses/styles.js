import { colors, appStyles } from '../../styles';

const styles = {
  container: {
    flex: 1,
    width: '100%',
  },
  scrollContainer: {
    width: '100%',
  },
  contentContainerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  feedbackMessageContainer: {
    flexDirection: 'column',
    width: '88%',
    alignItems: 'center',
    paddingTop: 5,
    paddingBottom: 15,
  },
  feedbackMessage: {
    color: colors.successGreen,
    fontSize: 14,
  },
  buttonWrapper: {
    marginBottom: 30,
  },
  buttonWrapperNoAddress: {
    marginTop: '50%',
    minHeight: 70,
    marginBottom: 50,
  },
  buttonContainer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    width: '90%',
    backgroundColor: colors.tavourOrange,
  },
  buttonText: {
    color: '#ffffff',
    fontSize: appStyles.buttonFontSize,
  },
};

export default styles;
