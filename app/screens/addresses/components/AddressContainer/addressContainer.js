import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { actions as addressActions } from 'appRedux/addresses';
import { connect } from 'react-redux';
import * as navUtils from 'lib/navUtils';
import empty from 'is-empty';
import styles from './styles';
import config from './config';

function parsePhone(text) {
  let newText = text;
  const numbers = text.replace(/\D/g, '');
  if (numbers.length <= 3 || numbers.length > 10) {
    newText = `${numbers}`;
  } else if (numbers.length > 3 && numbers.length < 7) {
    newText = `(${numbers.slice(0, 3)}) ${numbers.slice(3, numbers.length)}`;
  } else if (numbers.length > 6 && numbers.length < 11) {
    newText = `(${numbers.slice(0, 3)}) ${numbers.slice(3, 6)}-${numbers.slice(
      6,
      10
    )}`;
  }
  return newText;
}

function confirmationView(title, confirmText, onPressYes, onPressNo) {
  return (
    <View style={styles.confirmContainer}>
      <Text style={styles.confirmTitle}>{title}</Text>
      <View style={styles.confirmButtonContainer}>
        <TouchableOpacity
          onPress={onPressYes}
          style={[styles.confirmButton, styles.confirmButtonOrange]}
        >
          <Text style={[styles.confirmButtonText, styles.fontWhite]}>
            Yes, {confirmText}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={onPressNo}
          style={[styles.confirmButton, styles.confirmButtonGrey]}
        >
          <Text style={[styles.confirmButtonText, styles.fontGrey]}>
            No, cancel
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const carrierWarningItem = warning => (
  <View style={styles.carrierWarningItem}>
    <Text style={styles.carrierWarningBullet}>{'\u2022'}</Text>
    <Text style={styles.carrierWarningText}>{warning}</Text>
  </View>
);

class addressContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showDeleteConfirmation: false,
      showSetDefaultConfirmation: false,
    };
    const functionsToBind = [
      'toggleShowDeleteConfirmation',
      'toggleShowSetDefaultConfirmation',
      'callDeleteAddress',
      'callSetAsDefault',
      'editAddress',
      'renderCarrierWarnings',
    ];
    functionsToBind.forEach((functionToBind) => {
      this[functionToBind] = this[functionToBind].bind(this);
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.removedAddress || nextProps.updatedDefault) {
      this.props.clearAddressStatus();
    }
  }

  callDeleteAddress() {
    this.props.removeAddress(this.props.address);
    this.toggleShowDeleteConfirmation();
  }

  callSetAsDefault() {
    this.props.setDefaultAddress(this.props.address);
    this.toggleShowSetDefaultConfirmation();
  }

  editAddress() {
    navUtils.pushScreen('AddUpdateAddress', {
      passProps: {
        address: this.props.address,
        isUpdate: true,
        forwardTo: 'addresses',
      },
    });
  }

  toggleShowDeleteConfirmation() {
    this.setState(prevState => ({ showDeleteConfirmation: !prevState.showDeleteConfirmation }));
  }

  toggleShowSetDefaultConfirmation() {
    this.setState(prevState => ({
      showSetDefaultConfirmation: !prevState.showSetDefaultConfirmation
    }));
  }

  addressInfoView() {
    let line2 = null;
    if (!empty(this.props.address.line2)) {
      line2 = React.createElement(
        Text,
        { style: styles.text },
        this.props.address.line2
      );
    }

    let addressType = null;
    if (this.props.address.addressType === 'Residential') {
      addressType = (
        <View style={styles.addressTypeContainer}>
          <Text style={styles.mdSmall}> {'RESIDENTIAL ADDRESS'} </Text>
          <Text style={[styles.text, styles.small]}>
            {' '}
            {'*we recommend shipping to a business address'}{' '}
          </Text>
        </View>
      );
    } else {
      addressType = (
        <View style={styles.addressTypeContainer}>
          <Text style={styles.mdSmall}> {'BUSINESS ADDRESS'} </Text>
        </View>
      );
    }

    let setDefaultLink = (
      <TouchableOpacity
        style={styles.linkItem}
        onPress={this.toggleShowSetDefaultConfirmation}
      >
        <Text style={styles.link}>
          {config.addressContainer.setDefaultText}
        </Text>
      </TouchableOpacity>
    );
    let title = null;
    if (this.props.address.isDefault) {
      setDefaultLink = <View style={styles.linkItem} />;
      title = (
        <View style={styles.titleContainer}>
          <Text style={styles.addressTitle}>
            {config.addressContainer.mainTitle}
          </Text>
        </View>
      );
    }
    return (
      <View style={styles.addressContainer}>
        {title}
        <View style={styles.addressContentContainer}>
          {addressType}
          <View style={styles.paddingLeft}>
            <Text style={[styles.text, styles.bold]}>
              {this.props.address.name}
            </Text>
            <Text style={styles.text}>
              {parsePhone(this.props.address.phone)}
            </Text>
            <Text style={styles.text}>{this.props.address.line1}</Text>
            {line2}
            <Text style={styles.text}>
              {this.props.address.city}, {this.props.address.state}{' '}
              {this.props.address.zip}
            </Text>
          </View>
          {this.renderCarrierWarnings()}
          <View style={styles.linkContainer}>
            <TouchableOpacity
              style={styles.linkItemHalf}
              onPress={this.editAddress}
            >
              <Text style={styles.link}>
                {' '}
                {config.addressContainer.editText}{' '}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.linkItemHalf}
              onPress={this.toggleShowDeleteConfirmation}
            >
              <Text style={styles.link}>
                {' '}
                {config.addressContainer.deleteText}{' '}
              </Text>
            </TouchableOpacity>
            {setDefaultLink}
          </View>
        </View>
      </View>
    );
  }

  renderCarrierWarnings() {
    const { carrierWarnings } = this.props.address;
    if (!carrierWarnings.length) { return null; }
    const carrierWarningsItems = carrierWarnings.map(c => carrierWarningItem(c.description));
    return (
      <View style={styles.carrierWarningContainer}>
        <Text style={styles.carrierWarningText}>
          Just a heads up! This delivery area has the following restrictions.
        </Text>
        {carrierWarningsItems}
      </View>
    );
  }

  render() {
    if (this.state.showDeleteConfirmation) {
      return confirmationView(
        config.confirmDelete.prompt,
        config.confirmDelete.confirmButtonText,
        this.callDeleteAddress,
        this.toggleShowDeleteConfirmation
      );
    }

    if (this.state.showSetDefaultConfirmation) {
      return confirmationView(
        config.confirmSetDefault.prompt,
        config.confirmSetDefault.confirmButtonText,
        this.callSetAsDefault,
        this.toggleShowSetDefaultConfirmation
      );
    }

    return this.addressInfoView();
  }
}

function mapStateToProps(state) {
  return {
    updatedDefault: state.addresses.updatedDefault,
    removedAddress: state.addresses.removedAddress,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setDefaultAddress: address =>
      dispatch(addressActions.setDefaultAddress(address)),
    removeAddress: address => dispatch(addressActions.removeAddress(address)),
    clearAddressStatus: address =>
      dispatch(addressActions.clearAddressStatus(address)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(addressContainer);
