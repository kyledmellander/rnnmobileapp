import { colors } from 'styles';

const small = {
  color: colors.tavourOrange,
  fontSize: 10,
};

const styles = {
  addressListContainer: {
    width: '90%',
    marginTop: 110,
  },
  addressContainer: {
    width: '100%',
    marginVertical: 10,
    flexDirection: 'column',
    borderWidth: 1,
    borderColor: colors.tavourLightGray,
  },
  addressContentContainer: {
    width: '100%',
    padding: 15,
  },
  titleContainer: {
    width: '100%',
    backgroundColor: colors.backgroundGray,
    paddingHorizontal: 10,
    paddingVertical: 15,
  },
  addressTitle: {
    fontSize: 16,
    color: colors.tavourDarkGray,
  },
  confirmContainer: {
    width: '100%',
    padding: 10,
    marginVertical: 10,
    height: 190,
    flexDirection: 'column',
    backgroundColor: colors.tavourLightGray,
    justifyContent: 'center',
    alignItems: 'center',
  },
  confirmTitle: {
    fontSize: 16,
    color: colors.tavourDarkGray,
    paddingVertical: 8,
  },
  linkContainer: {
    flexDirection: 'row',
    marginTop: 20,
  },
  linkItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  carrierWarningContainer: {
    paddingTop: 10,
  },
  carrierWarningText: {
    ...small
  },
  carrierWarningItem: {
    ...small,
    flexDirection: 'row',
    paddingLeft: 5
  },
  carrierWarningBullet: {
    ...small,
    marginLeft: 5,
    marginRight: 5,
  },
  linkItemHalf: {
    flex: 0.4,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  link: {
    color: colors.tavourOrange,
    fontSize: 15,
  },
  text: {
    color: colors.tavourDarkGray,
    fontSize: 15,
  },
  mdSmall: {
    color: colors.tavourDarkGray,
    fontSize: 13,
    padding: 0,
    margin: 0,
  },
  bold: {
    fontWeight: '700',
    fontSize: 15,
  },
  small: {
    ...small,
    fontSize: 16,
    fontWeight: 'bold'
  },
  paddingLeft: {
    paddingLeft: 3,
  },
  confirmButtonContainer: {
    justifyContent: 'center',
    flexDirection: 'row',
  },
  confirmButton: {
    paddingVertical: 13,
    paddingHorizontal: 8,
    borderRadius: 5,
    marginHorizontal: 5,
    marginTop: 15,
    marginBottom: 20,
  },
  confirmButtonGrey: {
    backgroundColor: '#ffffff',
  },
  confirmButtonOrange: {
    backgroundColor: colors.tavourOrange,
  },
  confirmButtonText: {
    fontSize: 12,
  },
  fontWhite: {
    color: '#ffffff',
  },
  fontGrey: {
    color: colors.tavourDarkGray,
  },
  addressTypeContainer: {
    margin: 0,
    padding: 0,
    marginBottom: 10,
  },
};

export default styles;
