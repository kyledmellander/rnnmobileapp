import React, { Component } from 'react';
import { View } from 'react-native';
import addressContainer from './addressContainer';
import styles from './styles';

export default class addressesContainers extends Component {
  buildAddresses() {
    const addressesSorted = this.props.addresses.sort(address => !address.isDefault);
    const children = addressesSorted.map(address => React.createElement(addressContainer, {
      address,
    }));
    return children;
  }

  render() {
    return (
      <View
        style={styles.addressListContainer}
      >
        {this.buildAddresses()}
      </View>
    );
  }
}
