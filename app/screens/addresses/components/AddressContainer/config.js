const confirmDelete = {
  prompt: 'Are you sure you want to delete?',
  confirmButtonText: 'delete',
};

const confirmSetDefault = {
  prompt: 'Ship to this address and save as default?',
  confirmButtonText: 'save',
};

const addressContainer = {
  mainTitle: 'Default Shipping Address',
  setDefaultText: 'Set as Default',
  editText: 'Edit',
  deleteText: 'Delete',
};

export default {
  confirmDelete,
  confirmSetDefault,
  addressContainer,
};
