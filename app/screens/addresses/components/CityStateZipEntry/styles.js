import { colors } from 'styles';

const styles = {
  formField: {
    height: 60,
    width: '100%',
    borderBottomColor: colors.borderGray,
    borderBottomWidth: 1,
    flexDirection: 'row',
  },
  formText: {
    flex: 1,
    textAlign: 'left',
    paddingLeft: 33,
    borderWidth: 0,
  },
  displayField: {
    height: 60,
    width: '100%',
    borderBottomColor: colors.borderGray,
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  displayText: {
    flex: 1,
    textAlign: 'left',
    paddingLeft: 18,
    borderWidth: 0,
    fontSize: 17,
  },
  oneHalf: {
    flex: 0.5,
  },
  oneFourth: {
    flex: 0.25,
  },
  loading: {
    position: 'absolute',
    right: 5,
    top: 0,
    bottom: 0,
  }
};

export default styles;
