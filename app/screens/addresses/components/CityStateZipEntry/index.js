import React from 'react';
import { View, TextInput, Text, ActivityIndicator } from 'react-native';
import styles from './styles';

const CityStateZipEntry = props => (
  <View style={styles.displayField}>
    <TextInput
      underlineColorAndroid="transparent"
      onChangeText={props.setZip}
      value={props.zip}
      style={[styles.formText, styles.oneFourth]}
      placeholder="Zip"
      autoCorrect
      autoCapitalize="none"
      maxLength={5}
      keyboardType="numeric"
      testID="ZipInput"
    />
    <Text style={[styles.displayText, styles.oneFourth]}>
      {props.state}
    </Text>
    <View style={styles.oneHalf}>
      <TextInput
        underlineColorAndroid="transparent"
        onChangeText={props.setCity}
        value={props.city}
        style={[styles.formText]}
        placeholder="City"
        autoCorrect
        autoCapitalize="none"
        testID="CityInput"
        editable={!props.isLoadingCity}
        selectTextOnFocus={!props.isLoadingCity}
      />
      {props.isLoadingCity ? (<ActivityIndicator size="small" style={styles.loading} />) : null}
    </View>
  </View>
);

export default CityStateZipEntry;
