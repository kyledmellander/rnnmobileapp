import { appStyles } from 'styles';

const width = '90%';
const styles = {
  container: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    marginTop: appStyles.marginTop,
  },
  title: {
    height: 40,
    width,
  },
  body: {
    height: 150,
    width,
  },
  button: {
    height: 70,
    width,
  },
};

export default styles;
