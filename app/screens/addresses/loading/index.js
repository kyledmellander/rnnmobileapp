import React from 'react';
import { View, Animated, Easing } from 'react-native';
import loadingLib from 'lib/loading';
import styles from './styles';

const maxTimes = 3;
let times = 0;
const animatedValue = new Animated.Value(0);
let opacityValue = new Animated.Value(0);

const lightToLighterBackgroundColor = loadingLib.lightToLighter(animatedValue);
const darkToLightBackgroundColor = loadingLib.darkToLight(animatedValue);
let increaseOpacity = loadingLib.increaseOpacity(opacityValue);

const animate = () => {
  times += 1;
  const callback = times >= maxTimes ? null : animate;
  opacityValue = new Animated.Value(0);
  increaseOpacity = loadingLib.increaseOpacity(opacityValue);
  // eslint-disable-next-line no-underscore-dangle
  const endValue = animatedValue._value === 1 ? 0 : 1;
  Animated.timing(animatedValue, {
    toValue: endValue,
    duration: 1500,
    easing: Easing.linear,
  }).start(callback);

  Animated.timing(opacityValue, {
    toValue: 1,
    duration: 1500,
    easing: Easing.linear,
  }).start();
};

const loading = () => {
  animate();
  return (
    <View style={styles.container}>
      <Animated.View
        style={[styles.title, darkToLightBackgroundColor, increaseOpacity]}
      />
      <Animated.View
        style={[styles.body, lightToLighterBackgroundColor, increaseOpacity]}
      />
      <Animated.View
        style={[
          styles.body,
          darkToLightBackgroundColor,
          increaseOpacity,
          { marginTop: 20 },
        ]}
      />
      <Animated.View
        style={[
          styles.button,
          lightToLighterBackgroundColor,
          increaseOpacity,
          { marginTop: 40 },
        ]}
      />
    </View>
  );
};

export default loading;
