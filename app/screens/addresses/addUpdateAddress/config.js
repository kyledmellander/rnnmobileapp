const updateAddressPage = {
  promptTitle: 'Where should we send your beer?',
  promptSubTitle1: 'We recommend a business address,',
  submitText: 'Update Address',
};

const addAddressPage = {
  promptTitle: 'Where should we send your beer?',
  promptSubTitle1: 'We recommend a business address,',
  submitText: 'Save address',
};

const formFiller = {
  namePlaceholder: 'Your name',
  phonePlaceholder: 'Phone number',
  line1Placeholder: 'Address',
  line2Placeholder: 'Apt number (optional)',
};

export default {
  updateAddressPage,
  addAddressPage,
  formFiller,
};
