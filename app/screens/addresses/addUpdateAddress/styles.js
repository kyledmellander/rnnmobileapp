import { colors, appStyles } from 'styles';

const styles = {
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  formContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  scrollContainer: {
    flex: 1,
    paddingTop: appStyles.marginTop,
  },
  titleContainer: {
    marginTop: 20,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 19,
    paddingBottom: 10,
    fontFamily: appStyles.fontFamily,
  },
  subTitle: {
    fontSize: 15,
    color: colors.tavourDarkGray,
    fontFamily: appStyles.fontFamily,
  },
  subTitleLink: {
    color: colors.tavourOrange,
    fontFamily: appStyles.fontFamilySemibold,
  },
  formField: {
    height: 50,
    width: '100%',
    borderBottomColor: colors.borderGray,
    borderBottomWidth: 1,
    flexDirection: 'row',
  },
  formText: {
    flex: 1,
    textAlign: 'left',
    paddingLeft: 33,
    borderWidth: 0,
    fontFamily: appStyles.fontFamily,
  },
  errorText: {
    color: colors.failRed,
    fontSize: appStyles.errorFontSize,
    paddingVertical: 15,
    paddingHorizontal: 10,
    fontFamily: appStyles.fontFamily,
  },
  buttonContainer: {
    flex: 1,
  },
  button: {
    flex: 1,
    width: '100%',
    backgroundColor: appStyles.tavourOrange,
  },
};

export default styles;
