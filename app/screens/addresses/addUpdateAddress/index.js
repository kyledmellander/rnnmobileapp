import React from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { actions as userActions } from 'appRedux/user';
import { actions as addressesActions } from 'appRedux/addresses';
import utils from 'lib/utils';
import * as navUtils from 'lib/navUtils';
import AddressForm from './AddressForm';
import styles from './styles';
import config from './config';

class addUpdateAddress extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      waiting: false,
      address: null,
      config: config.addAddressPage,
      message: '',
      userStateCode: '',
    };
    this.submit = this.submit.bind(this);

    if (props.isUpdate) {
      Navigation.mergeOptions(props.componentId, navUtils.getOptions('Update Address'));
      this.state.address = props.address;
      this.state.config = config.updateAddressPage;
    } else {
      Navigation.mergeOptions(props.componentId, navUtils.getOptions('Add Address'));
    }
  }

  componentWillMount() {
    this.props.getUserState();
  }

  componentWillReceiveProps(nextProps) {
    const newState = {
      ...this.state,
      waiting: false,
    };

    if (nextProps.errors) {
      newState.message = ''; // erase old messages before concatenating new ones
      Object.keys(nextProps.errors).forEach((key) => {
        nextProps.errors[key].forEach((error) => {
          // errors should be full sentences
          newState.message += `${error} `;
        });
      });
    }

    if (nextProps.userStateCode) {
      newState.userStateCode = nextProps.userStateCode;
    }

    this.setState(newState);

    if (nextProps.updatedAddress || nextProps.addedAddress) {
      if (this.props.forwardTo === 'addresses') {
        navUtils.setStackRoot('Addresses');
      }
    }
  }

  submit(address) {
    this.setState({ waiting: true });
    if (this.props.isUpdate) {
      this.props.updateAddress(address);
    } else {
      this.props.addAddress(address);
    }
  }

  render() {
    const buttonText = this.state.waiting
      ? 'Please wait...'
      : this.state.config.submitText;
    return (
      <View style={styles.container}>
        <AddressForm
          address={this.state.address}
          submit={this.submit}
          buttonText={buttonText}
          message={this.state.message}
          userStateCode={this.state.userStateCode}
          waiting={this.state.waiting}
          promptTitle={this.state.config.promptTitle}
          promptSubTitle1={this.state.config.promptSubTitle1}
          promptSubTitle2={`someone 21+ must sign for your ${utils.getBasketWord(true)}.`}
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    errors: state.addresses.errors,
    userStateCode: state.user.userStateCode,
    addedAddress: state.addresses.addedAddress,
    updatedAddress: state.addresses.updatedAddress,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    addAddress: address => dispatch(addressesActions.addAddress(address)),
    updateAddress: address => dispatch(addressesActions.updateAddress(address)),
    getUserState: () => dispatch(userActions.getUserState()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(addUpdateAddress);
