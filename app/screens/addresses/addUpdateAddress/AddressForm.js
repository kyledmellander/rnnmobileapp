import { View, TextInput, Text } from 'react-native';
import React from 'react';
import { connect } from 'react-redux';
import empty from 'is-empty';
import { actions as addressFormActions } from 'appRedux/addressForm';
import CustomABButtons from 'components/CustomABButtons';
import { KeyboardAvoidingButton } from 'components';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import CityStateZipEntry from '../components/CityStateZipEntry';
import config from './config';
import styles from './styles';

const formFillerConfig = config.formFiller;

class AddressForm extends React.Component {
  constructor(props) {
    super(props);
    ['enableButton', 'submit'].forEach((functionToBind) => {
      this[functionToBind] = this[functionToBind].bind(this);
    });
  }

  componentWillMount() {
    // should only be 1 address form at a time, so reset between uses
    this.props.resetReduxStore();
    if (this.props.address) {
      this.props.setupFromAddress(this.props.address);
    }
  }

  enableButton() {
    return (
      !this.props.waiting &&
      !this.props.isLoadingCity &&
      !empty(this.props.myName) &&
      !empty(this.props.myLine1) &&
      !empty(this.props.myPhone) &&
      !empty(this.props.myCity) &&
      !empty(this.props.myZip)
    );
  }

  submit() {
    const address = {
      id: this.props.myId,
      addressType: this.props.myAddressType,
      name: this.props.myName,
      phone: this.props.myPhone.replace(/\D/g, ''),
      line1: this.props.myLine1,
      line2: this.props.myLine2,
      city: this.props.myCity,
      state: this.props.myState,
      zip: this.props.myZip,
    };
    this.props.submit(address);
  }

  render() {
    return (
      <View style={styles.formContainer} testID="AddressFormScrollView">
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps="always"
          extraScrollHeight={100}
          style={{
            flex: 1,
            width: '100%',
          }}
          contentContainerStyle={{ paddingBottom: 50 }}
          keyboardOpeningTime={0}
        >
          <View style={styles.titleContainer}>
            <Text style={styles.title}>{this.props.promptTitle}</Text>
            <Text style={styles.subTitle}>{this.props.promptSubTitle1}</Text>
            <Text style={styles.subTitle}>{this.props.promptSubTitle2}</Text>
          </View>

          <CustomABButtons
            aText="Business"
            aValue="Commercial"
            bText="Residential"
            bValue="Residential"
            labelText="Address type"
            selected={this.props.myAddressType}
            onSelected={this.props.onAddressTypeSelected}
          />

          <View style={styles.formField}>
            <TextInput
              underlineColorAndroid="transparent"
              onChangeText={this.props.onChangeTextName}
              defaultValue={this.props.myName}
              style={styles.formText}
              placeholder={formFillerConfig.namePlaceholder}
              autoCorrect
              autoCapitalize="none"
              testID="AddressFormNameInput"
            />
          </View>

          <View style={styles.formField}>
            <TextInput
              underlineColorAndroid="transparent"
              onChangeText={this.props.onChangeTextPhone}
              defaultValue={this.props.myPhone}
              style={styles.formText}
              placeholder={formFillerConfig.phonePlaceholder}
              autoCorrect={false}
              maxLength={25}
              keyboardType="numeric"
              autoCapitalize="none"
              testID="AddressFormPhoneInput"
            />
          </View>

          <View style={styles.formField}>
            <TextInput
              underlineColorAndroid="transparent"
              onChangeText={this.props.onChangeTextLine1}
              defaultValue={this.props.myLine1}
              style={styles.formText}
              placeholder={formFillerConfig.line1Placeholder}
              autoCorrect
              autoCapitalize="none"
              testID="AddressFormLine1Input"
            />
          </View>

          <View style={styles.formField}>
            <TextInput
              underlineColorAndroid="transparent"
              onChangeText={this.props.onChangeTextLine2}
              defaultValue={this.props.myLine2}
              style={styles.formText}
              placeholder={formFillerConfig.line2Placeholder}
              autoCorrect
              autoCapitalize="none"
              testID="AddressFormLine2Input"
            />
          </View>

          <View style={styles.formField}>
            <CityStateZipEntry
              city={this.props.myCity}
              state={this.props.myState}
              zip={this.props.myZip}
              setCity={this.props.setCity}
              setZip={this.props.setZip}
              isLoadingCity={this.props.isLoadingCity}
            />
          </View>

          <Text style={styles.errorText}>{this.props.message}</Text>
        </KeyboardAwareScrollView>
        <KeyboardAvoidingButton
          buttonText={this.props.buttonText}
          enabled={this.enableButton}
          onPress={this.submit}
          testID="AddressFormSubmit"
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  isLoadingCity: state.addressForm.isLoadingCity,
  myAddressType: state.addressForm.addressType,
  myName: state.addressForm.name,
  myPhone: state.addressForm.phone,
  myLine1: state.addressForm.line1,
  myLine2: state.addressForm.line2,
  myCity: state.addressForm.city,
  myZip: state.addressForm.zip,
  myState: state.addressForm.state,
  myId: state.addressForm.id,
});

const mapDispatchToProps = dispatch => ({
  resetReduxStore: () => { dispatch(addressFormActions.resetStore()); },
  setupFromAddress: (address) => { dispatch(addressFormActions.setupFromAddress(address)); },
  onAddressTypeSelected: (addressType) => {
    dispatch(addressFormActions.setAddressType(addressType));
  },
  onChangeTextName: (name) => { dispatch(addressFormActions.setName(name)); },
  onChangeTextPhone: (phone) => { dispatch(addressFormActions.setPhone(phone)); },
  onChangeTextLine1: (line1) => { dispatch(addressFormActions.setLine1(line1)); },
  onChangeTextLine2: (line2) => { dispatch(addressFormActions.setLine2(line2)); },
  setCity: (city) => { dispatch(addressFormActions.setCity(city)); },
  setZip: (zip) => { dispatch(addressFormActions.setZip(zip)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(AddressForm);
