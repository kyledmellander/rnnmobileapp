import React from 'react';
import {
  View,
  ScrollView,
  Text,
  Animated,
  Easing,
} from 'react-native';
import { connect } from 'react-redux';
import { actions as addressActions } from 'appRedux/addresses';
import CustomButton from 'components/CustomButton';
import loadingLib from 'lib/loading';
import * as navUtils from 'lib/navUtils';
import { Icons } from 'components';
import AddressesContainers from './components/AddressContainer';
import Loading from './loading';
import styles from './styles';
import config from './config';

const FeedbackMessage = props => (
  <View style={styles.feedbackMessageContainer}>
    <Text style={styles.feedbackMessage}>{props.message}</Text>
  </View>);

const animatedValue = new Animated.Value(0);

function openNewAddressForm() {
  navUtils.pushScreen('AddUpdateAddress', { passProps: { isUpdate: false, forwardTo: 'addresses' } });
}

const AddAddressButton = () => (
  <CustomButton
    buttonContainerStyle={styles.buttonContainer}
    buttonStyle={styles.button}
    buttonText={config.addressPage.AddButtonText}
    enabled
    onPress={openNewAddressForm}
    buttonTextStyle={styles.buttonText}
    testID="AddAddressButton"
  />);

class addresses extends React.Component {
  static options() {
    return {
      topBar: {
        buttonColor: 'black',
        title: {
          component: {
            name: 'NavBarTitle',
            passProps: {
              title: 'Shipping Address',
            },
            alignment: 'center',
          },
        },
        leftButtons: [
          {
            id: 'hamburger',
            icon: Icons.hamburger,
            color: 'black',
          }
        ]
      }
    };
  }
  constructor(props) {
    super(props);
    this.state = {
      message: '',
      loading: true,
    };
    this.props.getAddresses();
  }

  componentWillReceiveProps(nextProps) {
    const newState = {
      ...this.state,
      loading: false,
    };

    const actions = [
      'addedAddress',
      'updatedAddress',
      'removedAddress',
      'updatedDefault',
    ];
    let setMessage = false;
    actions.forEach((action) => {
      if (nextProps.errors) return;
      if (nextProps[action]) {
        newState.message = config.messages[action];
        setMessage = true;
      }
    });
    if (nextProps.errors) {
      Object.keys(nextProps.errors).forEach((k) => {
        newState.message = nextProps.errors[k];
        setMessage = true;
      });
    }
    this.setState(newState);

    if (setMessage) {
      setTimeout(() => {
        this.props.clearAddressStatus();
        this.setState({ message: '' });
      }, 3000);
    }
  }

  addressList() {
    if (this.props.addresses && this.props.addresses.length > 0) {
      return <AddressesContainers addresses={this.props.addresses} />;
    }
    return null;
  }

  showFeedbackMessage() {
    return <FeedbackMessage message={this.state.message} />;
  }

  render() {
    if (this.state.loading) {
      return Loading();
    }
    const increaseOpacity = loadingLib.increaseOpacity(animatedValue);
    Animated.timing(animatedValue, {
      toValue: 1,
      duration: 500,
      easing: Easing.linear,
    }).start();

    return (
      <Animated.View style={[styles.container, increaseOpacity]}>
        <ScrollView
          style={styles.scrollContainer}
          contentContainerStyle={styles.contentContainerStyle}
        >
          {this.addressList()}
          {this.showFeedbackMessage()}
          <View
            style={
              this.props.addresses.length > 0
                ? styles.buttonWrapper
                : styles.buttonWrapperNoAddress
            }
          >
            <AddAddressButton />
          </View>
        </ScrollView>
      </Animated.View>
    );
  }
}

function mapStateToProps(state) {
  if (!state.addresses) {
    return {};
  }
  return {
    addresses: state.addresses.addresses,
    addedAddress: state.addresses.addedAddress,
    updatedAddress: state.addresses.updatedAddress,
    removedAddress: state.addresses.removedAddress,
    updatedDefault: state.addresses.updatedDefault,
    errors: state.addresses.errors,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getAddresses: () => {
      dispatch(addressActions.getAddresses());
    },
    clearAddressStatus: () => {
      dispatch(addressActions.clearAddressStatus());
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(addresses);
