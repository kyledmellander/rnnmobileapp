import React, { Component } from 'react';
import { View, Text, Keyboard, Platform, Button, SafeAreaView } from 'react-native';
import { connect } from 'react-redux';
import LoginFields from 'components/LoginFields';
import { CustomButton } from 'components';
import CustomKeyboardAvoidingView from 'components/CustomKeyboardAvoidingView';
import { actions as userActions } from 'appRedux/user';
import { actions as metricActions } from 'appRedux/metrics';
import { navUtils } from 'lib';
import styles from './styles';

class login extends Component {
  static options() {
    return navUtils.getOptions('Log In');
  }

  constructor(props) {
    super(props);
    this.state = {
      emailPlaceHolder: 'Email Address',
      email: '',
      passwordPlaceHolder: 'Password',
      password: '',
      loginAttempt: false,
      waiting: false,
      messages: '',
    };
    this.emailChanged = this.emailChanged.bind(this);
    this.passwordChanged = this.passwordChanged.bind(this);
    this.submit = this.submit.bind(this);
    this.buttonText = this.buttonText.bind(this);
    this.enableButton = this.enableButton.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const newState = { ...this.state, waiting: false };
    if (nextProps.error) {
      newState.messages = nextProps.error;
    }

    if (nextProps.loggedIn) {
      this.props.addMetric(`appv4.${Platform.OS}.login.count`, 1);
      if (nextProps.user.showOnboarding && !nextProps.user.sawOnboarding) {
        navUtils.setStackRoot('Onboarding1');
      } else {
        navUtils.setStackRoot('Home');
      }
    }

    this.setState(newState);
  }

  submit() {
    this.setState({
      waiting: true,
      loginAttempt: true,
      messages: '',
    });
    Keyboard.dismiss();
    this.props.callLogin(this.state.email, this.state.password);
  }

  emailChanged(email) {
    this.setState({ email });
  }

  passwordChanged(password) {
    this.setState({ password });
  }

  buttonText() {
    return this.state.waiting ? 'Please wait...' : 'Log In';
  }

  enableButton() {
    const { waiting, email, password } = this.state;

    return (!waiting && email && password);
  }

  render() {
    const { style } = this.state;
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <CustomKeyboardAvoidingView
          style={[styles.container, style]}
          behavior="padding"
        >
          <View style={styles.titleContainer}>
            <Text style={styles.titleText}>Sign in to your Tavour Account</Text>
          </View>
          <View style={{ flex: 0.35 }}>
            <LoginFields
              formContainerStyle={{}}
              passwordPlaceHolder={this.state.passwordPlaceHolder}
              emailPlaceHolder={this.state.emailPlaceHolder}
              onEmailChange={this.emailChanged}
              onPasswordChange={this.passwordChanged}
              submit={this.submit}
              messages={this.state.messages}
            />
            <View>
              <Button
                title="Forgot password?"
                onPress={() => navUtils.pushScreen('ForgotPassword')}
                testID="ForgotPassword"
              />
            </View>
          </View>
          <CustomButton
            buttonContainerStyle={styles.buttonContainer}
            onPress={this.submit}
            buttonText={this.buttonText()}
            enabled={this.enableButton()}
            buttonStyle={styles.submit}
            testID="LoginSubmit"
          />
        </CustomKeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    loggedIn: state.user.loggedIn,
    actionCount: state.user.actionCount,
    error: state.user.errors[0],
    user: state.user.user
  };
}

function mapDispatchToProps(dispatch) {
  return {
    callLogin: (email, password) => {
      dispatch(userActions.login(email, password));
    },
    addMetric(...args) {
      dispatch(metricActions.addMetric(...args));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(login);
