import { colors, appStyles } from '../../styles';

const styles = {
  container: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  titleContainer: {
    paddingVertical: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  formContainer: {
    justifyContent: 'flex-start',
  },
  buttonContainer: {
  },
  titleText: {
    flex: 0,
    color: colors.tavourOrange,
    fontFamily: appStyles.fontFamily,
    fontSize: 18,
  },
  submit: {
    justifyContent: 'center',
    alignItems: 'center',
    flexShrink: 0.2,
  },
  submitEnabled: {
    backgroundColor: colors.tavourOrange,
  },
  submitDisabled: {
    backgroundColor: colors.tavourLightGray,
  },
  submitText: {
    fontSize: 18,
    fontFamily: appStyles.fontFamily,
  },
  submitTextEnabled: {
    color: 'white',
  },
  submitTextDisabled: {
    color: colors.disabledTextGray,
  },
};

export default styles;
