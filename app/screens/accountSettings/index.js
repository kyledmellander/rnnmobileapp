import React, { Component } from 'react';
import {
  View,
  Text,
  TextInput,
  Share,
  Platform,
  SafeAreaView
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import { actions as userActions } from 'appRedux/user';
import { actions as metricActions } from 'appRedux/metrics';
import { KeyboardAvoidingButton, Icons } from 'components';
import CustomABButtons from 'components/CustomABButtons';
import CustomKeyboardAvoidingView from 'components/CustomKeyboardAvoidingView';
import utils from 'lib/utils';
import styles from './styles';

const DisplaySettings = (props) => {
  const buttonText = props.buttonText ? props.buttonText : 'Edit';
  return (
    <View style={styles.settingsContainer}>
      <View style={styles.settingTitleContainer}>
        <Text style={styles.settingTitle}>{props.title}</Text>
        <Text style={styles.settingEdit} onPress={props.onPress}>
          {buttonText}
        </Text>
      </View>
      <Text style={styles.setting}>
        {props.text}
      </Text>
    </View>
  );
};

class accountSettings extends Component {
  static options() {
    return {
      topBar: {
        buttonColor: 'black',
        title: {
          component: {
            name: 'NavBarTitle',
            passProps: {
              title: 'Account Info',
            },
            alignment: 'center',
          },
        },
        leftButtons: [
          {
            id: 'hamburger',
            icon: Icons.hamburger,
            color: 'black',
          }
        ]
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      user: null,
      oldUser: null,
      editingInfo: false,
      editingPw: false,
      message: '',
      waiting: false,
    };
    this.props.getUser();
    this.enableButton = this.enableButton.bind(this);
    this.editInfo = this.editInfo.bind(this);
    this.editPw = this.editPw.bind(this);
    this.cancel = this.cancel.bind(this);
    this.updateState = this.updateState.bind(this);
    this.submit = this.submit.bind(this);
    this.submitShippingPreference = this.submitShippingPreference.bind(this);
    this.onShippingPreferenceSelected = this.onShippingPreferenceSelected.bind(this);
    this.shareCode = this.shareCode.bind(this);
    this.editPw = this.editPw.bind(this);
    this.editShippingPreference = this.editShippingPreference.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const newState = {
      ...this.state,
      user: nextProps.user,
      oldUser: Object.assign({}, this.state.user),
      waiting: false,
      message: nextProps.errors.length !== 0 ? nextProps.errors[0].title : '',
      preferences: nextProps.preferences,
      editingShippingPreference: false
    };
    if (newState.oldUser !== this.state.oldUser && newState.message === '') {
      newState.editingInfo = false;
      newState.editPw = false;
    }
    this.setState(newState);
  }

  onShippingPreferenceSelected(selection) {
    const preferences = {
      basketDelivery: selection
    };
    this.setState({
      preferences
    });
  }

  enableButton() {
    if (this.state.user !== this.state.oldUser) {
      return !this.state.waiting;
    }
    if (this.state.editingInfo) {
      return !this.state.waiting;
    }
    if (this.state.editingShippingPreference) {
      return this.state.preferences.basketDelivery !== this.props.preferences.basketDelivery;
    }
    const checkVal = val => val && val.length >= 6;
    return (
      !this.state.waiting &&
      checkVal(this.state.user.password) &&
      checkVal(this.state.user.newPassword) &&
      checkVal(this.state.user.newPasswordConfirm)
    );
  }

  editInfo() {
    this.setState({
      editingInfo: true,
    });
  }

  editPw() {
    this.setState({
      editingPw: true,
    });
  }

  editShippingPreference() {
    this.setState({
      editingShippingPreference: true
    });
  }

  cancel() {
    this.setState(prevState => ({
      user: Object.assign({}, prevState.oldUser),
      editingInfo: false,
      editingPw: false,
      editingShippingPreference: false,
      message: '',
    }));
  }

  updateState(key, val) {
    const newState = { ...this.state };
    newState.user[key] = val;
    this.setState(newState);
  }

  submit() {
    if (
      this.state.user.newPassword &&
      this.state.user.newPassword !== this.state.user.newPasswordConfirm
    ) {
      this.setState({
        message: 'New password mismatch',
        waiting: false,
      });
      return;
    }
    this.setState({ message: '', waiting: true });
    this.props.updateUser(this.state.user);
  }

  submitShippingPreference() {
    this.setState({
      message: '',
      waiting: true
    });
    this.props.updateUserPreferences(this.state.user, this.state.preferences);
  }

  editInfoPage() {
    const { updateState } = this;
    return (
      <View style={{ flex: 1 }}>
        <KeyboardAwareScrollView keyboardOpeningTime={0} keyboardShouldPersistTaps="always" extraScrollHeight={100} style={{ flex: 1, width: '100%' }}>
          <View style={[styles.settingTitleContainer, styles.editTitleContainer]}>
            <Text style={styles.settingTitle}>EDIT PERSONAL INFO</Text>
            <Text style={styles.settingEdit} onPress={this.cancel}>
              Cancel
            </Text>
          </View>
          <View style={styles.editFormContainer}>
            <TextInput
              onChangeText={val => updateState('fname', val)}
              underlineColorAndroid="transparent"
              style={styles.editInfo}
              autoCorrect={false}
              autoCapitalize="none"
              defaultValue={this.state.user.fname}
            />
            <View style={styles.line} />
            <TextInput
              onChangeText={val => updateState('lname', val)}
              underlineColorAndroid="transparent"
              style={styles.editInfo}
              autoCorrect={false}
              defaultValue={this.state.user.lname}
              autoCapitalize="none"
            />
            <View style={styles.line} />
            <TextInput
              onChangeText={val => updateState('email', val)}
              underlineColorAndroid="transparent"
              style={styles.editInfo}
              defaultValue={this.state.user.email}
              autoCorrect={false}
              autoCapitalize="none"
            />
            <View style={styles.line} />
            <Text style={styles.errorText}>{this.state.message}</Text>
          </View>
        </KeyboardAwareScrollView>
        <KeyboardAvoidingButton
          buttonContainerStyle={styles.buttonContainer}
          onPress={this.submit}
          buttonText="Save"
          enabled={this.enableButton()}
        />
      </View>
    );
  }

  editPwPage() {
    const { updateState } = this;
    return (
      <View
        style={styles.editSettingsContainer}
      >
        <KeyboardAwareScrollView style={styles.editFormContainer}>
          <View style={[styles.settingTitleContainer, styles.editTitleContainer]}>
            <Text style={styles.settingTitle}>EDIT PASSWORD</Text>
            <Text style={styles.settingEdit} onPress={this.cancel}>
              Cancel
            </Text>
          </View>
          <Text style={styles.settingLabel}>Current Password</Text>
          <TextInput
            onChangeText={val => updateState('password', val)}
            secureTextEntry
            underlineColorAndroid="transparent"
            style={styles.editInfo}
            autoCorrect={false}
            autoCapitalize="none"
          />
          <View style={styles.line} />
          <Text style={styles.settingLabel}>New Password</Text>
          <TextInput
            onChangeText={val => updateState('newPassword', val)}
            secureTextEntry
            underlineColorAndroid="transparent"
            style={styles.editInfo}
            autoCorrect={false}
            autoCapitalize="none"
          />
          <View style={styles.line} />
          <Text style={styles.settingLabel}>Confirm New Password</Text>
          <TextInput
            onChangeText={val => updateState('newPasswordConfirm', val)}
            secureTextEntry
            underlineColorAndroid="transparent"
            style={styles.editInfo}
            autoCorrect={false}
            autoCapitalize="none"
          />
          <View style={styles.line} />
          <Text style={styles.errorText}>{this.state.message}</Text>
        </KeyboardAwareScrollView>
        <KeyboardAvoidingButton
          buttonContainerStyle={styles.buttonContainer}
          onPress={this.submit}
          buttonText="Save"
          enabled={this.enableButton()}
        />
      </View>
    );
  }

  editShippingPreferencePage() {
    return (
      <CustomKeyboardAvoidingView
        style={styles.editSettingsContainer}
        behavior="height"
      >
        <View style={[styles.settingTitleContainer, styles.editTitleContainer]}>
          <Text style={styles.settingTitle}>EDIT SHIPPING PREFERENCE</Text>
          <Text style={styles.settingEdit} onPress={this.cancel}>
            Cancel
          </Text>
        </View>
        <View style={styles.editFormContainer}>
          <CustomABButtons
            aText="Pickup"
            aValue="Pickup"
            bText="Shipping"
            bValue="Shipping"
            labelText="Shipping Preference"
            selected={this.state.preferences.basketDelivery}
            onSelected={this.onShippingPreferenceSelected}
          />
        </View>
        <KeyboardAvoidingButton
          buttonContainerStyle={styles.buttonContainer}
          onPress={this.submitShippingPreference}
          buttonText="Save"
          enabled={this.enableButton()}
        />
      </CustomKeyboardAvoidingView>
    );
  }

  shareCode() {
    const { addMetric } = this.props;
    const url = utils.getReferralLink(this.state);
    const message = `Great tasting beers from Tavour

Signup with this link ${url} and we both get $10 after you purchase a beer.`;
    const title = 'Join Tavour';
    Share.share(
      {
        message,
        title,
      },
      {
        dialogTitle: 'Share Referral Link',
      }
    )
      .then(() => {
        addMetric(`appv4.${Platform.OS}.referralshare.count`, 1);
      })
      .catch(err => console.log(err));
  }

  render() {
    const { user } = this.state;
    if (!user) {
      return null;
    }

    if (this.state.editingInfo) {
      return this.editInfoPage();
    }

    if (this.state.editingPw) {
      return this.editPwPage();
    }

    if (this.state.editingShippingPreference) {
      return this.editShippingPreferencePage();
    }
    const shippingPreference = user.stateCode === 'WA' ?
      React.createElement(DisplaySettings, {
        title: 'SHIPPING PREFERENCE',
        onPress: this.editShippingPreference,
        buttonText: 'Edit',
        text: `${this.state.preferences.basketDelivery}`
      }) :
      null;
    return (
      <SafeAreaView style={styles.container}>
        <DisplaySettings
          title="PERSONAL INFO"
          onPress={this.editInfo}
          text={`${user.fname} ${user.lname}\n${user.email}`}
        />
        <DisplaySettings
          title="PASSWORD"
          onPress={this.editPw}
          text="**********"
        />
        <DisplaySettings
          title="REFERRAL CODE"
          onPress={this.shareCode}
          buttonText="Share"
          text={user.id}
        />
        {shippingPreference}
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  if (!state.user) {
    return {};
  }
  return {
    user: state.user.user,
    preferences: state.user.user.preferences,
    errors: state.user.errors,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getUser: () => {
      dispatch(userActions.getUser());
    },
    updateUser: (user) => {
      dispatch(userActions.updateUser(user));
    },
    updateUserPreferences: (user, preferences) => {
      dispatch(userActions.updateUserPreferences(user, preferences));
    },
    addMetric: (...args) => dispatch(metricActions.addMetric(...args)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(accountSettings);
