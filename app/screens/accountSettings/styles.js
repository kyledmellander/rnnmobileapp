import { colors, appStyles } from '../../styles';

const styles = {
  container: {
    flex: 1,
    width: '100%',
    paddingTop: appStyles.marginTop,
  },
  settingsContainer: {
    flex: 0,
    borderBottomWidth: 1,
    borderColor: colors.borderGray,
    paddingVertical: 15,
  },
  setting: {
    fontFamily: appStyles.fontFamily,
    fontSize: 17,
    paddingVertical: 0,
    lineHeight: 30,
    paddingLeft: 20,
  },
  settingTitleContainer: {
    flexDirection: 'row',
    paddingLeft: 10,
    width: '90%',
    justifyContent: 'space-between',
  },
  settingTitle: {
    fontFamily: appStyles.fontFamily,
    color: colors.titleGray,
    paddingBottom: 10,
    fontSize: 17,
  },
  settingEdit: {
    fontFamily: appStyles.fontFamily,
    fontSize: 17,
    color: colors.tavourOrange,
  },
  editSettingsContainer: {
    flex: 1,
    justifyContent: 'space-between',
    paddingTop: 20,
  },
  editTitleContainer: {
    width: '100%',
    paddingTop: 5,
    minHeight: '12%',
    paddingHorizontal: '5%',
    flex: 0.1,
  },
  editInfo: {
    paddingHorizontal: '5%',
    height: 50,
    width: '90%',
  },
  editFormContainer: {
    flex: 0.7,
  },
  line: {
    width: '100%',
    marginVertical: 0,
    paddingVertical: 0,
    height: 1,
    backgroundColor: colors.borderGray,
  },
  buttonContainer: {
    minHeight: 50,
    width: '100%',
  },
  submit: {
    height: 400,
  },
  settingLabel: {
    paddingHorizontal: '5%',
    paddingTop: '2%',
    fontFamily: appStyles.fontFamily,
  },
  errorText: {
    color: colors.failRed,
    fontSize: 15,
    paddingTop: 15,
    paddingBottom: 50,
    width: '100%',
    textAlign: 'center',
    fontFamily: appStyles.fontFamily,
  },
};

export default styles;
