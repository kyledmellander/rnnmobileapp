import { colors, appStyles, commonStyles } from 'styles';

export const { minorSpacing } = appStyles;

const styles = {
  mainContainer: {
    flex: 1,
    width: '100%',
    paddingTop: appStyles.marginTop,
    justifyContent: 'flex-start'
  },
  contentWrapper: { flex: 1 }, // todo see if I can remove
  minorSpacingTop: { marginTop: minorSpacing },
  statusText: {
    fontSize: 14,
    fontFamily: appStyles.fontFamily,
    color: colors.okGreen,
    textAlign: 'center',
  },
  buttonContainer: { ...commonStyles.bigButtonContainer },
  button: { ...commonStyles.bigButton },
  sectionTitle: { ...commonStyles.sectionTitle, marginTop: 10 },
  sectionContainer: { },
};

export default styles;
