import React from 'react';
import { View, Text } from 'react-native';
import CustomButton from 'components/CustomButton';
import CardItem from './CardItem';
import paymentStyles from '../styles';

const defaultCardToFront = card => (card.isDefault ? -1 : 0);

export default ({
  onAddCard,
  onDeleteCard,
  onSetCardDefault,
  cards,
}) => (
  <View style={paymentStyles.sectionContainer}>
    <View>
      <Text style={paymentStyles.sectionTitle}>Your Cards</Text>
    </View>
    {[...cards].sort(defaultCardToFront).map((card, index) => (
      <CardItem
        card={card}
        onDeleteCard={onDeleteCard}
        onSetCardDefault={onSetCardDefault}
        isLast={index === cards.length - 1}
      />
    ))}
    <View>
      <CustomButton
        buttonContainerStyle={paymentStyles.buttonContainer}
        buttonText="Add credit card"
        enabled
        onPress={onAddCard}
        testID="AddCreditCardButton"
      />
    </View>
  </View>
);
