import { colors, commonStyles } from 'styles';

const styles = {
  creditCardImage: {
    height: 22,
    width: 32,
    marginRight: 7,
    backgroundColor: colors.ccBlue,
  },
  cardItem: {
    ...commonStyles.borderedItemTop,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  bottomBorder: {
    borderBottomWidth: 1,
    borderColor: colors.borderGray,
  },
  cardItemButtonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  cardItemButton: {
    color: colors.tavourOrange,
    marginLeft: 14,
  },
  cardItemDetailsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
};

export default styles;
