import React from 'react';
import { View, Text } from 'react-native';
import CardImage from '../CardImage';
import styles from './styles';

const CardItemButton = props => (
  <Text
    style={styles.cardItemButton}
    onPress={props.onPress}
  >
    {props.children}
  </Text>
);

export default props => (
  <View style={[styles.cardItem, props.isLast ? styles.bottomBorder : null]}>
    <View style={styles.cardItemDetailsContainer}>
      <CardImage ccNiceType={props.card.cardType} style={styles.creditCardImage} />
      <Text>**** {props.card.cardLast4}</Text>
    </View>
    <View style={styles.cardItemButtonContainer}>
      {
        props.card.isDefault ? null : (
          <CardItemButton onPress={() => props.onSetCardDefault(props.card)}>
            Set As Default
          </CardItemButton>
        )
      }
      <CardItemButton onPress={() => props.onDeleteCard(props.card)}>Delete</CardItemButton>
    </View>
  </View>
);
