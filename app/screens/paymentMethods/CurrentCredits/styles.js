import { colors } from 'styles';

const styles = {
  currentCreditsItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  creditDescription: {
    fontSize: 12,
    color: colors.tavourDarkGray,
    paddingLeft: 5,
  },
  creditBalancePositive: { color: colors.successGreen },
  creditBalanceZero: { color: colors.tavourDarkGray }
};

export default styles;
