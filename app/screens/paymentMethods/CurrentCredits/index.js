import React from 'react';
import { View, Text } from 'react-native';
import paymentStyles from '../styles';
import currentCreditStyles from './styles';

export default (props) => {
  const creditTotalNum = parseFloat(props.creditTotalDisplay.substring(1));
  const creditBalanceModifier = creditTotalNum > 0 ?
    currentCreditStyles.creditBalancePositive : currentCreditStyles.creditBalanceZero;
  return (
    <View style={paymentStyles.sectionContainer}>
      <View style={currentCreditStyles.currentCreditsItem}>
        <Text style={paymentStyles.sectionTitle}>Current Credits</Text>
        <Text style={[paymentStyles.sectionTitle, creditBalanceModifier]}>
          {props.creditTotalDisplay}
        </Text>
      </View>
      <View>
        <Text style={currentCreditStyles.creditDescription}>
          Credits automatically applied to next order
        </Text>
      </View>
    </View>);
};
