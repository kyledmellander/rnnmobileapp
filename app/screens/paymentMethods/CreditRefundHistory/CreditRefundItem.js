import React from 'react';
import { View, Text } from 'react-native';
import utils from 'lib/utils';
import styles from './styles';

export default (props) => {
  let bigText = 'Credit Issued';
  let amountStyleModifer = styles.creditAmount;
  if (props.creditRefund.type === 'refunds') {
    bigText = 'Refund';
    amountStyleModifer = null;
  }
  return (
    <View style={[styles.creditRefundItem, props.isLast ? styles.bottomBorder : null]}>
      <View style={styles.topContainer}>
        <Text>{bigText}</Text>
        <Text style={amountStyleModifer}>{props.creditRefund.amount}</Text>
      </View>
      <View style={styles.bottomContainer}>
        <View style={{ flex: 1 }}>
          <Text style={styles.bottomContainerText} numberOfLines={1}>
            {props.creditRefund.description}
          </Text>
        </View>
        <View>
          <Text style={styles.bottomContainerText}>
            {utils.formatDateMMDDYY(new Date(props.creditRefund.createdAt))}
          </Text>
        </View>
      </View>
    </View>
  );
};
