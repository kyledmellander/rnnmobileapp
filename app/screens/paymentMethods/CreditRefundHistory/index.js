import React from 'react';
import { View, Text, ScrollView } from 'react-native';
import CreditRefundItem from './CreditRefundItem';
import paymentStyles from '../styles';
import creditRefundStyles from './styles';

export default props => (
  <View style={creditRefundStyles.containerStyle}>
    <View>
      <Text
        style={[paymentStyles.sectionTitle, creditRefundStyles.historySectionTitle]}
      >
        Credit History
      </Text>
    </View>
    <ScrollView style={creditRefundStyles.containerStyle}>
      {props.refundsAndCredits.map((creditRefund, index) => (
        <CreditRefundItem
          isLast={index === props.refundsAndCredits.length - 1}
          creditRefund={creditRefund}
          key={`${creditRefund.type}-${creditRefund.id}`}
        />
      ))}
    </ScrollView>
  </View>
);
