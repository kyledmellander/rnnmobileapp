import { colors, appStyles, commonStyles } from 'styles';

const { fontSize: sectionTitleFontSize } = commonStyles.sectionTitle;

const styles = {
  containerStyle: {
    flex: 1,
  },
  creditRefundItem: {
    ...commonStyles.borderedItemTop,
  },
  bottomBorder: {
    borderBottomWidth: appStyles.borderWidth,
    borderColor: colors.borderGray,
  },
  topContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  bottomContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  bottomContainerText: {
    color: colors.tavourDarkGray,
    fontSize: 12,
  },
  creditAmount: {
    color: colors.successGreen,
  },
  // apparently this stands out too much, so make it smaller
  historySectionTitle: {
    fontSize: sectionTitleFontSize - 1
  },
};

export default styles;
