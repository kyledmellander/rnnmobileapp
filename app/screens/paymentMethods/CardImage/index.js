import React from 'react';
import { Image } from 'react-native';
import ccIcons from './cardIcons';

export default (props) => {
  let cardImage = ccIcons.generic;
  switch (props.ccNiceType) {
    case 'Visa':
      cardImage = ccIcons.visa;
      break;
    case 'MasterCard':
      cardImage = ccIcons.masterCard;
      break;
    case 'American Express':
      cardImage = ccIcons.americanExpress;
      break;
    case 'Diners Club':
      cardImage = ccIcons.dinersClub;
      break;
    case 'Discover':
      cardImage = ccIcons.discover;
      break;
    case 'JCB':
      cardImage = ccIcons.jcb;
      break;
    case 'UnionPay':
      cardImage = ccIcons.unionPay;
      break;
    case 'Maestro':
      cardImage = ccIcons.maestro;
      break;
    default:
      break;
  }
  return (<Image source={cardImage} {...props} />);
};
