import { icons } from 'images';

export default {
  generic: icons.genericCard,
  visa: icons.visaCard,
  masterCard: icons.masterCard,
  americanExpress: icons.americanExpressCard,
  dinersClub: icons.dinersClubCard,
  discover: icons.discoverCard,
  jcb: icons.jcbCard,
  unionPay: icons.unionPayCard,
  maestro: icons.maestroCard,
};
