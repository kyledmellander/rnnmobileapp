import React from 'react';
import {
  View,
  Text,
  Animated,
  Easing,
  SafeAreaView,
} from 'react-native';
import { connect } from 'react-redux';
import { actions as paymentActions } from 'appRedux/paymentMethods';
import { actions as redeemActions } from 'appRedux/redeem';
import loadingLib from 'lib/loading';
import * as navUtils from 'lib/navUtils';
import { CustomButton, Icons } from 'components';
import YourCards from './YourCards';
import CurrentCredits from './CurrentCredits';
import CreditRefundHistory from './CreditRefundHistory';
import styles from './styles';
import Loading from './loading';

function addPaymentMethod() {
  navUtils.pushScreen('AddCreditCard', {
    passProps: {
      forwardTo: 'payment'
    }
  });
}

const animatedValue = new Animated.Value(0);
class paymentMethods extends React.Component {
  static options() {
    return {
      topBar: {
        buttonColor: 'black',
        title: {
          component: {
            name: 'NavBarTitle',
            passProps: {
              title: 'Payment & Credits',
            },
            alignment: 'center',
          },
        },
        leftButtons: [
          {
            id: 'hamburger',
            icon: Icons.hamburger,
            color: 'black',
          }
        ]
      }
    };
  }

  componentDidMount() {
    this.props.getInitData();
  }

  render() {
    if (this.props.loading) {
      return Loading();
    }
    const increaseOpacity = loadingLib.increaseOpacity(animatedValue);
    Animated.timing(animatedValue, {
      toValue: 1,
      duration: 500,
      easing: Easing.linear,
    }).start();

    return (
      <Animated.View style={[styles.mainContainer, increaseOpacity]}>
        <SafeAreaView style={styles.contentWrapper}>
          <YourCards
            cards={this.props.paymentMethods}
            onDeleteCard={this.props.removePaymentMethod}
            onSetCardDefault={this.props.updatePaymentMethod}
            onAddCard={addPaymentMethod}
          />
          <View>
            <Text style={styles.statusText}>{this.props.cardMessage}</Text>
          </View>
          <CurrentCredits creditTotalDisplay={this.props.creditTotalDisplay} />
          <View style={[styles.minorSpacingTop, { flex: 1 }]}>
            <CreditRefundHistory refundsAndCredits={this.props.refundsAndCredits} />
          </View>
          <View style={{ marginTop: 30 }}>
            <CustomButton
              buttonContainerStyle={styles.buttonContainer}
              buttonText="Redeem gift card or referral"
              enabled
              onPress={this.props.openRedeem}
              testID="RedeemGiftCardOrReferral"
            />
          </View>
        </SafeAreaView>
      </Animated.View>
    );
  }
}

function mapStateToProps(state) {
  if (!state.paymentMethods) {
    return {};
  }
  return {
    addedCard: state.paymentMethods.addedCard,
    updatedCard: state.paymentMethods.updatedCard,
    removedCard: state.paymentMethods.removedCard,
    paymentMethods: state.paymentMethods.paymentMethods,
    creditTotalDisplay: state.paymentMethods.creditTotalDisplay,
    refundsAndCredits: state.paymentMethods.refundsAndCredits,
    loading: state.paymentMethods.loading,
    cardMessage: state.paymentMethods.cardMessage,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    openRedeem: () => dispatch(redeemActions.openRedeemScreen(() => {
      dispatch(paymentActions.getInitData()); // refresh data
      navUtils.setStackRoot('PaymentMethods');
    })),
    getInitData: () => dispatch(paymentActions.getInitData()),
    removePaymentMethod: pm => dispatch(paymentActions.removePaymentMethod(pm)),
    updatePaymentMethod: pm => dispatch(paymentActions.updatePaymentMethod(pm)),
    clearPaymentMethodStatus: pm =>
      dispatch(paymentActions.clearPaymentMethodStatus(pm)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(paymentMethods);
