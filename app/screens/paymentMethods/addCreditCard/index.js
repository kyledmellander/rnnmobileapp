import React from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';
import { actions as paymentMethodActions } from 'appRedux/paymentMethods';
import * as navUtils from 'lib/navUtils';
import styles from './styles';
import PaymentForm from './PaymentForm';

class addCreditCard extends React.Component {
  static options() {
    return navUtils.getOptions('Add Card');
  }

  constructor(props) {
    super(props);
    this.state = {
      waiting: false,
      messages: '',
    };
    this.submit = this.submit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const newState = { ...this.state, waiting: false };

    if (nextProps.errors) {
      newState.messages = '';
      nextProps.errors.forEach((error) => {
        newState.messages += ` ${error}`;
      });
    }

    this.setState(newState);

    if (nextProps.addedCard) {
      navUtils.setStackRoot('PaymentMethods');
    }
  }

  submit(paymentMethod) {
    this.setState({ waiting: true, messages: '' });
    this.props.addPaymentMethod(paymentMethod);
  }

  render() {
    const buttonText = this.state.waiting ? 'Please wait...' : 'Add credit card';
    return (
      <View style={styles.container}>
        <PaymentForm
          title="Add a credit card"
          messages={this.state.messages}
          buttonText={buttonText}
          submit={this.submit}
          waiting={this.state.waiting}
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    addedCard: state.paymentMethods.addedCard,
    errors: state.paymentMethods.errors,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    addPaymentMethod: pm => dispatch(paymentMethodActions.addPaymentMethod(pm)),
    clearPaymentMethodStatus: () =>
      dispatch(paymentMethodActions.clearPaymentMethodStatus()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(addCreditCard);
