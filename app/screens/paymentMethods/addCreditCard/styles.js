import { colors, appStyles } from 'styles';

const styles = {
  container: {
    flex: 1,
  },
  titleContainer: {
    flexDirection: 'column',
    paddingTop: 40,
    paddingBottom: 20,
    flex: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 19,
    paddingBottom: 10,
    fontFamily: appStyles.fontFamily,
  },
  subTitle: {
    textAlign: 'center',
    fontSize: 15,
    marginHorizontal: 20,
    color: colors.tavourDarkGray,
    fontFamily: appStyles.fontFamily,
  },
  formContainer: {
    flexDirection: 'row',
    maxHeight: 50,
    width: '100%',
    borderBottomColor: colors.borderGray,
    borderBottomWidth: 1,
  },
  body: {
    flex: 1,
    justifyContent: 'flex-start',
  },
  creditCardImage: {
    height: 22,
    width: 32,
    marginLeft: 28,
    marginTop: 9,
    marginBottom: 9,
    marginRight: 7,
    backgroundColor: colors.ccBlue,
  },
  form: {
    flex: 1,
    fontFamily: appStyles.fontFamily,
  },
  buttonContainer: {
    flex: 1,
  },
  button: {
    flex: 1,
    width: '100%',
    backgroundColor: appStyles.tavourOrange,
  },
  closeButton: {
    flex: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  linkText: {
    color: colors.tavourOrange,
    fontFamily: appStyles.fontFamily,
  },
  errorText: {
    color: colors.failRed,
    fontSize: appStyles.errorFontSize,
    fontFamily: appStyles.fontFamily,
  },
  errorContainer: {
    paddingTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
};

export default styles;
