import React from 'react';
import { View, TextInput, Text, TouchableOpacity } from 'react-native';
import creditCardType from 'credit-card-type';
import { KeyboardAvoidingButton } from 'components';
import styles from './styles';
import CardImage from '../CardImage';

const isAmex = ccType =>
  ccType && ccType.niceType === 'American Express';
const isAmexMaxLen = ccNumberString => ccNumberString.length === 15;
const isDefaultCCMaxLen = ccNumberString => ccNumberString.length === 16;
const isCCMaxLen = (ccNumStr, ccType) =>
  (isAmex(ccType) ? isAmexMaxLen(ccNumStr) : isDefaultCCMaxLen(ccNumStr));

const formatAmex = (ccNumberString) => {
  const ccNumberParts = [];
  const END_FIRST_PART = 4;
  const END_MIDDLE_PART = 10;
  const END_LAST_PART = 15;
  const addCCPartIfPart = (part) => {
    if (part) {
      ccNumberParts.push(part);
    }
  };
  addCCPartIfPart(ccNumberString.substring(0, END_FIRST_PART));
  addCCPartIfPart(ccNumberString.substring(END_FIRST_PART, END_MIDDLE_PART));
  addCCPartIfPart(ccNumberString.substring(END_MIDDLE_PART, END_LAST_PART));
  return ccNumberParts.join(' ');
};

const formatDefaultCC = text => text.match(/.{1,4}/g).join(' ');

export default class PaymentForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      creditCardEntered: false,
      creditCardNumber: '',
      creditCardDate: '',
      creditCardMonth: '',
      creditCardYear: '',
      creditCardCvv: '',
      creditCardCodeName: 'CVV',
      creditCardCvvLength: 3,
      creditCardType: null,
      cardNumberForm: null,
      cardDateForm: null,
      cardCvvForm: null,
      feedback: '',
    };

    this.onEditCardNumber = this.onEditCardNumber.bind(this);
    this.onChangeTextCardNumber = this.onChangeTextCardNumber.bind(this);
    this.onChangeTextCardDate = this.onChangeTextCardDate.bind(this);
    this.onChangeTextCardCvv = this.onChangeTextCardCvv.bind(this);
    this.enableButton = this.enableButton.bind(this);
    this.getFeedbackMessages = this.getFeedbackMessages.bind(this);
    this.submit = this.submit.bind(this);
  }

  onChangeTextCardNumber(userText) {
    const text = userText.replace(/ /g, '');
    const newState = {
      ...this.state,
      creditCardNumber: text,
    };
    // apply spacing
    if (text.length > 4) {
      const ccIsAmex = isAmex(this.state.creditCardType);
      newState.creditCardNumber = ccIsAmex
        ? formatAmex(text)
        : formatDefaultCC(text);
    }
    // card type management
    if (text.length < 4) {
      newState.creditCardType = null;
      newState.creditCardCodeName = '';
    }
    if (text.length === 4) {
      const cardTypes = creditCardType(text);
      if (cardTypes.length !== 0) {
        const cardType = cardTypes[0];
        newState.creditCardType = cardType;
        newState.creditCardCvvLength = cardType.code.size;
        newState.creditCardCodeName = cardType.code.name;
      } else {
        newState.creditCardType = null;
      }
    }
    // bad number message feedback
    if (text.length < 4 || this.state.creditCardType) {
      newState.feedback = '';
    }
    if (text.length > 4 && !this.state.creditCardType) {
      newState.feedback = 'Invalid Card Number';
    }
    // good to go
    if (
      this.state.creditCardType &&
      isCCMaxLen(text, this.state.creditCardType)
    ) {
      newState.creditCardEntered = true;
    }
    this.setState(newState);
  }

  onChangeTextCardDate(inputText) {
    const text = inputText.replace(/\//g, '');
    const newState = {
      ...this.state,
      creditCardDate: text,
    };
    if (text.length === 4) {
      console.log('Focus on the CVV!');
      newState.creditCardMonth = text.substring(0, 2);
      newState.creditCardYear = text.substring(2, 4);
      this.cvv.focus();
    }
    if (text.length > 2) {
      newState.creditCardDate = text.match(/.{1,2}/g).join('/');
    }
    this.setState(newState);
  }

  onChangeTextCardCvv(text) {
    const newState = {
      ...this.state,
      creditCardCvv: text,
    };
    if (this.state.creditCardCvv === 0 && text.length === 0) {
      this.date.focus();
    }
    this.setState(newState);
  }

  onEditCardNumber() {
    const newState = {
      ...this.state,
      creditCardEntered: !this.state.creditCardEntered,
    };

    this.setState(newState);
  }

  getForm() {
    const ccNiceType = this.state.creditCardType ? this.state.creditCardType.niceType : '';
    const children = [<CardImage ccNiceType={ccNiceType} style={styles.creditCardImage} />];
    const baseFormProps = {
      underlineColorAndroid: 'transparent',
      autoCapitalize: 'none',
      autoCorrect: false,
      keyboardType: 'numeric',
      style: styles.form,
    };
    const cardNumberFormProps = {
      ...baseFormProps,
      ref: (c) => { this.cardNumber = c; },
      placeholder: '2222 2222 2222 2222',
      defaultValue: this.state.creditCardNumber,
      maxLength: 19,
      onChangeText: this.onChangeTextCardNumber,
      testID: 'PaymentFormNumberInput',
      autoFocus: true,
    };

    if (this.state.creditCardEntered) {
      cardNumberFormProps.maxLength = 4;
      cardNumberFormProps.defaultValue = this.state.creditCardNumber
        .replace(/ /g, '')
        .substring(12);
      cardNumberFormProps.onFocus = this.onEditCardNumber;
      const cardDateFormProps = {
        ...baseFormProps,
        ref: (c) => { this.date = c; },
        maxLength: 5,
        defaultValue: this.state.creditCardDate,
        placeholder: 'MM/YY',
        onChangeText: this.onChangeTextCardDate,
        testID: 'PaymentFormCardDateInput',
        autoFocus: true,
      };

      const cardCVvFormProps = {
        ...baseFormProps,
        ref: (c) => { this.cvv = c; },
        defaultValue: this.state.creditCardCvv,
        maxLength: this.state.creditCardCvvLength,
        placeholder: this.state.creditCardCodeName,
        onChangeText: this.onChangeTextCardCvv,
        testID: 'PaymentFormCvvInput',
      };
      const cardNumberForm = React.createElement(
        TextInput,
        cardNumberFormProps
      );
      const cardDateForm = React.createElement(TextInput, cardDateFormProps);
      const cardCvvForm = React.createElement(TextInput, cardCVvFormProps);
      children.push(cardNumberForm, cardDateForm, cardCvvForm);
    } else {
      const cardNumberForm = React.createElement(
        TextInput,
        cardNumberFormProps
      );

      if (
        this.state.creditCardNumber.length === 19 &&
        this.state.creditCardType
      ) {
        const closeFormProps = {
          ref: (c) => { this.closeForm = c; },
          onPress: this.onEditCardNumber,
          style: styles.closeButton,
        };
        const text = React.createElement(
          Text,
          { style: styles.linkText },
          'ok'
        );
        const closeForm = React.createElement(
          TouchableOpacity,
          closeFormProps,
          text
        );
        children.push(cardNumberForm, closeForm);
      } else {
        children.push(cardNumberForm);
      }
    }
    return children;
  }

  getFeedbackMessages() {
    return `${this.state.feedback} ${this.props.messages}`;
  }

  enableButton() {
    return (
      !this.props.waiting &&
      this.state.creditCardEntered &&
      this.state.creditCardMonth.length === 2 &&
      this.state.creditCardYear.length === 2 &&
      this.state.creditCardCvv.length === this.state.creditCardCvvLength
    );
  }

  submit() {
    this.props.submit({
      number: this.state.creditCardNumber.replace(/ /g, ''),
      month: this.state.creditCardMonth,
      year: this.state.creditCardYear,
      cvv: this.state.creditCardCvv,
    });
  }

  render() {
    const subTitle = this.props.subTitle ? (
      <Text style={styles.subTitle}>{this.props.subTitle}</Text>
    ) : null;
    return (
      <View style={[styles.container]}>
        <View style={styles.body}>
          <View style={styles.titleContainer}>
            <Text style={styles.title}>{this.props.title}</Text>
            {subTitle}
          </View>
          <View style={styles.formContainer}>
            {this.getForm()}
          </View>
          <View style={styles.errorContainer}>
            <Text style={styles.errorText}>{this.getFeedbackMessages()}</Text>
          </View>
        </View>
        <KeyboardAvoidingButton
          buttonText={this.props.buttonText}
          enabled={this.enableButton()}
          onPress={this.submit}
          testID="PaymentFormSubmit"
        />
      </View>
    );
  }
}
