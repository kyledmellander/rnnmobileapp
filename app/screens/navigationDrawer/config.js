import { actions as userActions } from 'appRedux/user';
import { dispatch } from 'store';
import utils from 'lib/utils';
import * as navUtils from 'lib/navUtils';

// want to use function, since we're using utils.getBasketWord, which relies on the store.
// If we don't use functon, it will default to crate because store will not have user data yet.
function getConfig() {
  return {
    contents: [
      {
        title: 'Featured Beers',
        testID: 'HomeDrawerButton',
        linkTo() {
          navUtils.setStackRoot('Home');
        },
        key: 'Home',
        isActive: true,
      },
      {
        title: 'My Beers',
        testID: 'MyBeerDrawerButton',
        linkTo() {
          navUtils.setStackRoot('MyBeers');
        },
        key: 'MyBeers',
      },
      {
        isHR: true,
      },
      {
        title: `Current ${utils.getBasketWord()}`,
        testID: 'CurrentBasketDrawerButton',
        linkTo() {
          navUtils.setStackRoot('MyBasket');
        },
        key: 'MyBasket',
      },
      {
        title: `Shipped ${utils.getBasketWord()}`,
        testID: 'ShippedCratesDrawerButton',
        linkTo() {
          navUtils.setStackRoot('ShippedCrates');
        },
        key: 'ShippedCrates',
      },
      {
        isHR: true,
      },
      {
        title: 'Shipping Address',
        testID: 'AddressesDrawerButton',
        linkTo() {
          navUtils.setStackRoot('Addresses');
        },
        key: 'Addresses',
      },
      {
        title: 'Payment & Credits',
        testID: 'PaymentMethodsDrawerButton',
        linkTo() {
          navUtils.setStackRoot('PaymentMethods');
        },
        key: 'PaymentMethods',
      },
      {
        title: 'Account Info',
        testID: 'AccountInfoDrawerButton',
        linkTo() {
          navUtils.setStackRoot('AccountSettings');
        },
        key: 'AccountSettings',
      },
      {
        title: 'Promos',
        testID: 'PromosDrawerButton',
        linkTo() {
          navUtils.setStackRoot('Promos');
        },
        key: 'Promos',
      },
      {
        title: 'Notifications',
        testID: 'NotificationsDrawerButton',
        linkTo() {
          navUtils.setStackRoot('NotificationSettings');
        },
        key: 'NotificationSettings',
      },
      {
        isHR: true,
      },
      {
        title: 'Contact Us',
        testID: 'ContactUsDrawerButton',
        linkTo() {
          navUtils.setStackRoot('ContactUs');
        },
        key: 'ContactUs',
      },
      {
        title: 'Free Beer',
        testID: 'FreeBeerDrawerButton',
        async linkTo() {
          navUtils.setStackRoot('Referral');
        },
        key: 'Referral',
      },
      {
        title: 'Logout',
        testID: 'LogoutDrawerButton',
        async linkTo() {
          dispatch(userActions.signOut());
        },
        key: '',
      },
    ],
  };
}

export default getConfig;
