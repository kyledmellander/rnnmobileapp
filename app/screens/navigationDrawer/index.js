/* eslint-disable */

import React, { Component } from 'react';
import { AppState, Platform } from 'react-native';
import { connect } from 'react-redux';
import Drawer from 'react-native-drawer';
import { colors } from 'styles';
import { actions as metricActions } from 'appRedux/metrics';
import { SafeAreaView, Modal } from 'components';
import analytics from 'lib/analytics';
import DrawerContent from './drawerContent';

let homeStart;
let oldScene;
const isHome = () => oldScene && oldScene.sceneKey === 'Home';

class NavigationDrawer extends Component {
  constructor(props) {
    super(props);
    this.closeMenu = this.closeMenu.bind(this);
    this.logHomeSessionDuration = this.logHomeSessionDuration.bind(this);
  }

  componentWillMount() {
    const { logHomeSessionDuration } = this;
    AppState.addEventListener('change', (newState) => {
      // calculate duration spent on home if user on home page and sends app to background
      if (newState === 'inactive' && isHome() && homeStart) {
        logHomeSessionDuration();
      }
      // start new home screen session if users returns to app on home screen
      if (newState === 'active' && isHome()) {
        homeStart = new Date();
      }
    });
  }

  closeMenu() {
    this.drawer.close();
  }

  checkScene(childrenScenes) {
    const newScene = childrenScenes[0].children[0];

    if (!newScene) {
      return;
    }

    const newSceneName = newScene.title ? newScene.title : newScene.sceneKey;
    // if no old scene OR scene name has changed
    if (!oldScene || newSceneName !== (oldScene.title ? oldScene.title : oldScene.sceneKey)) {
      analytics.page({
        category: 'ScreenView',
        name: newSceneName,
      });
    }

    if (!oldScene) {
      oldScene = newScene;

      if (newScene.sceneKey === 'Home') {
        homeStart = new Date();
      }
      return;
    }

    if (newScene.sceneKey === 'Home' && !isHome()) {
      homeStart = new Date(); // new home screen session if user navigates home
    }

    if (newScene.sceneKey !== 'Home' && isHome()) {
      this.logHomeSessionDuration();
    }
    oldScene = newScene;
  }

  logHomeSessionDuration() {
    const homeEnd = new Date();
    const diff = (homeEnd - homeStart) / 1000;
    this.props.addMetric(`appv4.${Platform.OS}.home.duration`, diff);
    homeStart = null;
  }

  render() {
    const state = this.props.navigationState;
    const { children } = state;
    this.checkScene(children);
    return (
      <Drawer
        ref={(input) => {
          this.drawer = input;
        }}
        open={state.open}
        openDrawerOffset={100}
        panCloseMask={100}
        type="overlay"
        content={<DrawerContent closeMenu={this.closeMenu} />}
        tapToClose
      >
        <SafeAreaView style={{ backgroundColor: colors.backgroundGray, flex: 1 }}>
          <Modal />
        </SafeAreaView>
      </Drawer>
    );
  }
}

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {
    addMetric(...args) {
      dispatch(metricActions.addMetric(...args));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NavigationDrawer);
