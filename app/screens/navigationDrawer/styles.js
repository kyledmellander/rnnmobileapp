import { colors, appStyles } from 'styles';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';


const buttonsPaddingLeft = '5%';

const styles = {
  container: {
    height: '100%',
    width: '75%',
    backgroundColor: 'white',
    paddingTop: getStatusBarHeight(true),
    borderRightColor: colors.backgroundGray,
    borderRightWidth: 2,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    paddingVertical: 7,
    width: '100%',
    paddingLeft: buttonsPaddingLeft,
    flexDirection: 'row',
    borderLeftColor: 'rgba(0,0,0,0)',
    borderLeftWidth: 5,
    borderRightColor: 'rgba(0,0,0,0)',
    borderRightWidth: 5,
  },
  activeButton: {
    borderLeftColor: colors.tavourOrange,
    borderRightColor: colors.tavourOrange,
  },
  socialMediaIcon: {
    height: 40,
    width: 40
  },
  brewersAssociationIcon: {
    height: 57,
    width: 38
  },
  socialMediaIconsContainer: {
    paddingTop: 10,
    paddingHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
  },
  text: {
    fontSize: 14,
    fontFamily: appStyles.fontFamily,
    color: colors.tavourDarkGray,
  },
  activeText: {
    fontWeight: '600',
  },
  hr: {
    borderBottomColor: colors.tavourLightGray,
    borderBottomWidth: 1,
    marginVertical: 10,
    width: '80%',
    flexDirection: 'row',
  },
};

export default styles;
