import React, { Component } from 'react';
import {
  Dimensions,
  Easing,
  Animated,
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Image,
  Linking,
} from 'react-native';
import loadingLib from 'lib/loading';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';
import { icons } from 'images';
import { actions as metricActions } from 'appRedux/metrics';
import styles from './styles';
import getConfig from './config';

const translateMax = Dimensions.get('window').width * 0.75;

class drawerContent extends Component {
  constructor(props) {
    super(props);
    this.getButtons = this.getButtons.bind(this);
    this.close = this.close.bind(this);
  }

  componentWillMount() {
    if (!this.animatedValue) this.animatedValue = new Animated.Value(0);
    this.opacity = loadingLib.increaseOpacity(this.animatedValue);
    this.translateX = this.animatedValue.interpolate({
      inputRange: [0, 1], outputRange: [-translateMax, 0]
    });
    Animated.timing(this.animatedValue, {
      toValue: 1,
      duration: 250,
      easing: Easing.quad.inOut,
      useNativeDriver: true,
    }).start();
  }

  componentWillReceiveProps(props) {
    if (props.loggedOut) {
      Navigation.setStackRoot('APP_STACK', { component: { name: 'Landing' } });
    }
  }

  getButtons() {
    const { activeScene } = this.props;

    const buttons = getConfig().contents.map((button) => {
      if (button.isHR) {
        return <View style={styles.hr} />;
      }

      const buttonStyles = [styles.button];
      const textStyles = [styles.text];

      if (activeScene === button.key) {
        buttonStyles.push(styles.activeButton);
        textStyles.push(styles.activeText);
      } else {
        console.log(activeScene, button.key);
      }
      const onPress = () => {
        this.close();
        button.linkTo();
      };
      return (
        <TouchableOpacity style={buttonStyles} onPress={onPress} testID={button.testID}>
          <Text style={textStyles}>{button.title}</Text>
        </TouchableOpacity>
      );
    });

    const socialButtons = (
      <View style={styles.socialMediaIconsContainer}>
        <TouchableOpacity
          onPress={() => {
            this.props.addMetric('appv4.socialmedia.facebook', 1);
            Linking.openURL('https://www.facebook.com/tavour/');
          }}
        >
          <Image
            style={styles.socialMediaIcon}
            source={icons.socialMediaIcons.facebookIcon}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.props.addMetric('appv4.socialmedia.twitter', 1);
            Linking.openURL('https://twitter.com/tavour');
          }}
        >
          <Image
            style={styles.socialMediaIcon}
            source={icons.socialMediaIcons.twitterIcon}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.props.addMetric('appv4.socialmedia.instagram', 1);
            Linking.openURL('https://www.instagram.com/tavour/');
          }}
        >
          <Image
            style={styles.socialMediaIcon}
            source={icons.socialMediaIcons.instagramIcon}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            this.props.addMetric('appv4.socialmedia.brewersassociation', 1);
            Linking.openURL('https://www.tavour.com/blog/independence-matters/');
          }}
        >
          <Image
            style={styles.brewersAssociationIcon}
            source={icons.socialMediaIcons.brewersAssociationSeal}
          />
        </TouchableOpacity>
      </View>
    );

    buttons.push(socialButtons);

    return buttons;
  }

  close() {
    this.opacity = loadingLib.increaseOpacity(this.animatedValue);
    Animated.timing(this.animatedValue, {
      toValue: 0,
      duration: 250,
      easing: Easing.quad,
      useNativeDriver: true,
    }).start(() => Navigation.dismissOverlay(this.props.componentId));
  }

  render() {
    console.log(StyleSheet.absoluteFillObject);
    return (
      <View style={{ flex: 1, flexDirection: 'row' }}>
        <TouchableWithoutFeedback onPress={this.close}>
          <Animated.View style={[{ ...StyleSheet.absoluteFillObject, backgroundColor: 'rgba(0,0,0,0.3)' }, this.opacity]} />
        </TouchableWithoutFeedback>
        <Animated.View style={[styles.container, { transform: [{ translateX: this.translateX }] }]}>
          {this.getButtons()}
        </Animated.View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    loggedOut: state.user.loggedOut,
    activeScene: state.routes.screenName,
    userStateCode: state.user.userStateCode,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    addMetric(...args) {
      dispatch(metricActions.addMetric(...args));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(drawerContent);
