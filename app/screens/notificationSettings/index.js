import React from 'react';
import { View, Switch, Text, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { actions as notificationActions } from 'appRedux/notifications';
import { Icons } from 'components';
import styles from './styles';

const settingContainer = (props) => {
  const switchElement = React.createElement(Switch, {
    style: styles.switch,
    value: props.settingEnabled,
    onValueChange: props.onValueChange,
  });
  const loading = React.createElement(ActivityIndicator, {
    style: styles.switch,
  });
  const toggle = props.loading ? loading : switchElement;
  return (
    <View style={[styles.settingContainer, props.borderStyle]}>
      <Text style={styles.settingText}>{props.title}</Text>
      {toggle}
    </View>
  );
};

class notificationSettings extends React.Component {
  static options() {
    return {
      topBar: {
        buttonColor: 'black',
        title: {
          component: {
            name: 'NavBarTitle',
            passProps: {
              title: 'Notifications',
            },
            alignment: 'center',
          },
        },
        leftButtons: [
          {
            id: 'hamburger',
            icon: Icons.hamburger,
            color: 'black',
          }
        ]
      }
    };
  }

  constructor(props) {
    super(props);
    this.emailToggled = this.emailToggled.bind(this);
    this.notificationsToggled = this.notificationsToggled.bind(this);
    this.appNotificationsToggled = this.appNotificationsToggled.bind(this);
    this.state = {
      emailEnabled: props.emailEnabled,
      notificationsEnabled: props.notificationsEnabled,
      appNotificationsEnabled: props.appNotificationsEnabled,
      loading: true,
    };
    this.props.getNotificationSettings();
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      emailEnabled: nextProps.emailEnabled,
      notificationsEnabled: nextProps.notificationsEnabled,
      appNotificationsEnabled: nextProps.appNotificationsEnabled,
      loading: nextProps.emailLoading &&
        nextProps.notificationsLoading &&
        nextProps.appNotificationsLoading,
    });
  }

  emailToggled() {
    this.setState({ loading: true });
    this.props.toggleEmail(this.props.emailEnabled);
  }

  notificationsToggled() {
    this.props.toggleNotifications();
  }

  appNotificationsToggled() {
    this.props.toggleAppNotifications();
  }

  notifications() {
    const props = {
      title: 'Push Notifications',
      onValueChange: this.notificationsToggled,
      settingEnabled: this.state.notificationsEnabled,
      loading: this.props.notificationsLoading,
    };
    return settingContainer(props);
  }

  emails() {
    const props = {
      title: 'Daily Emails',
      borderStyle: styles.bottomBorder,
      onValueChange: this.emailToggled,
      settingEnabled: this.state.emailEnabled,
      loading: this.props.emailLoading,
    };
    return settingContainer(props);
  }

  appNotifications() {
    const props = {
      title: 'In-App Notifications',
      onValueChange: this.appNotificationsToggled,
      settingEnabled: this.state.appNotificationsEnabled,
      loading: this.props.appNotificationsLoading,
    };
    return settingContainer(props);
  }

  render() {
    if (this.state.loading) {
      return React.createElement(ActivityIndicator, {
        style: styles.container,
      });
    }
    return (
      <View style={styles.container}>
        <Text style={styles.description}>
          {' '}
          Every day we feature 2 new beers that tend to sell out quickly. Select
          how you’d like to be notified
        </Text>
        {this.notifications()}
        {this.emails()}
        {this.appNotifications()}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    appNotificationsEnabled: state.notifications.appNotificationsEnabled,
    appNotificationsLoading: state.notifications.appNotificationsLoading,
    emailLoading: state.notifications.emailLoading,
    emailEnabled: state.notifications.emailEnabled,
    notificationsLoading: state.notifications.notificationsLoading,
    notificationsEnabled: state.notifications.notificationsEnabled,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    toggleNotifications: () =>
      dispatch(notificationActions.toggleNotifications()),
    toggleEmail: () => dispatch(notificationActions.toggleEmail()),
    getNotificationSettings: () =>
      dispatch(notificationActions.getNotificationSettings()),
    toggleAppNotifications: () => dispatch(notificationActions.toggleAppNotifications())
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(notificationSettings);
