import { colors, appStyles } from '../../styles';

const styles = {
  container: {
    flex: 1,
    marginTop: appStyles.marginTop,
  },
  settingContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 50,
    alignSelf: 'center',
    alignItems: 'center',
    width: '90%',
  },
  bottomBorder: {
    borderBottomWidth: 1,
    borderColor: colors.borderGray,
  },
  settingText: {
    fontFamily: appStyles.fontFamily,
    fontSize: 15,
  },
  switch: {
    alignSelf: 'center',
  },
  description: {
    fontFamily: appStyles.fontFamily,
    fontSize: 14,
    color: colors.textGray,
    textAlign: 'center',
    marginTop: 23,
    marginHorizontal: 15,
    marginBottom: 30,
  },
};

export default styles;
