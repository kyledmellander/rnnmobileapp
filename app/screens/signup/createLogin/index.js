import React, { Component } from 'react';
import {
  View,
  Text,
  TextInput,
  ActivityIndicator,
  Keyboard,
  SafeAreaView,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import empty from 'is-empty';
import LoginFields from 'components/LoginFields';
import { KeyboardAvoidingButton } from 'components';
import { pushScreen, getOptions } from 'lib/navUtils';
import { actions as userActions } from 'appRedux/user';
import styles from './styles';
import config from './config';

function getTitle() {
  return (
    <View style={styles.titleContainer}>
      <Text style={styles.textStyle}>{config.title}</Text>
    </View>
  );
}

class createLogin extends Component {
  static options() {
    return getOptions('Sign Up');
  }

  constructor(props) {
    super(props);
    this.state = {
      emailPlaceHolder: 'Email Address',
      email: '',
      passwordPlaceHolder: 'Password',
      password: '',
      fname: '',
      fnamePlaceHolder: 'First Name',
      lname: '',
      lnamePlaceHolder: 'Last Name',
      passwordConfirm: '',
      passwordConfirmPlaceHolder: 'Confirm password',
      errors: [],
      loading: false,
    };
    this.emailChanged = this.emailChanged.bind(this);
    this.passwordChanged = this.passwordChanged.bind(this);
    this.passwordConfirmChanged = this.passwordConfirmChanged.bind(this);
    this.fnameChanged = this.fnameChanged.bind(this);
    this.lnameChanged = this.lnameChanged.bind(this);
    this.continue = this.continue.bind(this);
    this.enableButton = this.enableButton.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.loggedIn) {
      pushScreen('EnterZip');
    }
    if (nextProps.errors.length > 0) {
      this.setState({
        loading: false,
        errors: nextProps.errors,
      });
    }
  }

  getFields() {
    const textView = React.createElement(Text, {
      style: [
        styles.validationText,
        this.state.errors.length > 0 ? styles.errorText : {},
      ]
    }, this.passwordValidate());
    const loadingView = React.createElement(ActivityIndicator, {
      style: { marginTop: 20, flex: 0.2 },
    });

    const status = this.state.loading ? loadingView : textView;
    const submitFname = () => this.lname.focus();
    const submitLname = () => this.loginfields.refs.email.focus();
    const submitLoginFields = () => this.passwordConfirm.focus();
    return (
      <View style={styles.fieldsContainer}>
        <View style={styles.nameContainer}>
          <TextInput
            underlineColorAndroid="transparent"
            ref={(c) => { this.fname = c; }}
            style={styles.nameText}
            placeholder={this.state.fnamePlaceHolder}
            autoCorrect={false}
            autoFocus
            autoCapitalize="none"
            keyboardType="email-address"
            onChangeText={this.fnameChanged}
            onSubmitEditing={submitFname}
            testID="CreateLoginFnameInput"
          />
          <TextInput
            underlineColorAndroid="transparent"
            ref={(c) => { this.lname = c; }}
            style={styles.nameText}
            placeholder={this.state.lnamePlaceHolder}
            autoCorrect={false}
            autoCapitalize="none"
            keyboardType="email-address"
            onChangeText={this.lnameChanged}
            onSubmitEditing={submitLname}
            testID="CreateLoginLnameInput"
          />
        </View>
        <LoginFields
          ref={(c) => { this.loginfields = c; }}
          autoFocusEmail={false}
          formContainerStyle={styles.formContainer}
          errorTextStyle={styles.none}
          passwordPlaceHolder={this.state.passwordPlaceHolder}
          emailPlaceHolder={this.state.emailPlaceHolder}
          onEmailChange={this.emailChanged}
          onPasswordChange={this.passwordChanged}
          submit={submitLoginFields}
        />
        <View style={styles.passwordConfirmContainer}>
          <TextInput
            ref={(c) => { this.passwordConfirm = c; }}
            underlineColorAndroid="transparent"
            style={styles.passwordConfirmText}
            placeholder={this.state.passwordConfirmPlaceHolder}
            autoCorrect={false}
            secureTextEntry
            autoCapitalize="none"
            submit={this.continue}
            keyboardType="email-address"
            onChangeText={this.passwordConfirmChanged}
            testID="CreateLoginPasswordConfirmInput"
          />
        </View>
        {status}
      </View>
    );
  }

  fnameChanged(fname) {
    this.setState({
      fname,
      errors: [],
    });
  }

  lnameChanged(lname) {
    this.setState({
      lname,
      errors: [],
    });
  }

  emailChanged(email) {
    this.setState({
      email,
      errors: [],
    });
  }

  passwordChanged(password) {
    this.setState({
      password,
      errors: [],
    });
  }

  passwordConfirmChanged(passwordConfirm) {
    this.setState({
      passwordConfirm,
      errors: [],
    });
  }

  passwordValidate() {
    const { errors, password, passwordConfirm } = this.state;

    if (!empty(password) && password.length < 6) {
      return config.passwordShort;
    }
    if (errors.length > 0) {
      return errors.reduce(
        (sum, val) => `${sum} ${val.title}`,
        'Error: '
      );
    }
    if (
      !empty(password)
      && !empty(passwordConfirm)
      && password !== passwordConfirm
    ) {
      return 'Passwords do not match!';
    }
    return '';
  }

  continue() {
    Keyboard.dismiss();
    this.setState({
      loading: true,
    });
    this.props.createUser(
      this.state.email,
      this.state.password,
      this.state.fname,
      this.state.lname
    );
  }

  enableButton() {
    const { email, password, passwordConfirm } = this.state;

    return (
      !empty(email)
      && !empty(password)
      && password.length >= 6
      && password === passwordConfirm
    );
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <KeyboardAwareScrollView keyboardOpeningTime={0} keyboardShouldPersistTaps="always" extraScrollHeight={100} style={{ flex: 1, width: '100%' }}>
          {getTitle()}
          {this.getFields()}
        </KeyboardAwareScrollView>
        <KeyboardAvoidingButton
          onPress={this.continue}
          buttonText={config.buttonText}
          enabled={this.enableButton()}
          testID="CreateLoginSubmit"
        />
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    loggedIn: state.user.loggedIn,
    errors: state.user.errors,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    createUser: (email, password, fname, lname) => {
      dispatch(userActions.createUser(email, password, fname, lname));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(createLogin);
