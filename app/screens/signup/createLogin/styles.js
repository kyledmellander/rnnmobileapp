import { colors, appStyles } from 'styles';
import flowStyles from '../styles';

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  titleContainer: {
    marginTop: 40,
    justifyContent: 'center',
  },
  fieldsContainer: {
    borderBottomColor: colors.borderGray,
  },
  buttonContainer: {
    width: '100%',
  },
  nameContainer: {
    height: 60,
    marginTop: 20,
    flexDirection: 'row',
    borderTopWidth: 1,
    borderColor: colors.borderGray,
  },
  none: {
    height: 0,
    paddingTop: 0,
  },
  passwordConfirmContainer: {
    height: 60,
    borderBottomColor: colors.borderGray,
    borderBottomWidth: 1,
  },
  validationText: {
    fontFamily: appStyles.fontFamily,
    textAlign: 'center',
    marginTop: 10,
    color: '#8c8c8c',
  },
  nameText: {
    flex: 1,
    fontFamily: appStyles.fontFamily,
    textAlign: 'center',
  },
  passwordConfirmText: {
    flex: 1,
    fontFamily: appStyles.fontFamily,
    textAlign: 'center',
  },
  errorText: {
    color: 'red',
  },
};

const mergedStyles = Object.assign(styles, flowStyles);

export default mergedStyles;
