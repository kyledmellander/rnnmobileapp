const config = {
  title: 'Create your Login',
  buttonText: 'Continue',
  passwordShort: 'Password is too short, need 6+ characters',
};

export default config;
