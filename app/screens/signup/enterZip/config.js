const config = {
  title: "What's your zip code?",
  subTitle: "We'll get you the beers you can't find in your area.",
  buttonText: 'Continue',
  buttonTextNotCovered: 'Email me when Tavour is in my area',
  zipCodeNotCoveredText: "We can't deliver to this zip code yet :(",
};

export default config;
