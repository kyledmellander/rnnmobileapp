import React from 'react';
import {
  KeyboardAvoidingView,
  View,
  Text,
  TextInput,
  Platform,
  ActivityIndicator,
} from 'react-native';
import { connect } from 'react-redux';
import { actions as metricActions } from 'appRedux/metrics';
import { actions as zipCodeActions } from 'appRedux/zipCode';
import { actions as userActions } from 'appRedux/user';
import { CustomButton } from 'components';
import * as navUtils from 'lib/navUtils';
import styles from './styles';
import config from './config';

function getTitle() {
  return (
    <View style={styles.titleContainer}>
      <Text style={styles.textStyle}>{config.title}</Text>
      <Text style={styles.subTextStyle}>{config.subTitle}</Text>
    </View>
  );
}

class enterZip extends React.Component {
  static options() {
    return {
      topBar: {
        visible: false,
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      zipCodePlaceHolder: 'Zip Code',
      zipCode: '',
      buttonText: config.buttonText,
      enableButton: false,
      loading: false,
      statusText: '',
      keyboardOffset: 0,
    };
    if (!props.user) {
      this.props.getUser();
    }
    this.zipCodeChanged = this.zipCodeChanged.bind(this);
    this.continue = this.continue.bind(this);
    this.enableButton = this.enableButton.bind(this);

    this.navigating = false;
  }

  componentWillReceiveProps(nextProps) {
    const newState = {
      loading: nextProps.loading,
      enableButton: true,
      zipCodeCovered: nextProps.zipCodeCovered,
    };
    if (nextProps.user && nextProps.user.stateCode && !this.navigating) {
      this.navigating = true;
      if (nextProps.user.showOnboarding && !nextProps.user.sawOnboarding) {
        console.log(this.props, nextProps);
        navUtils.setStackRoot('Onboarding1');
      } else {
        navUtils.setStackRoot('Home');
      }
    }
    if (!nextProps.zipCodeCovered) {
      newState.buttonText = config.buttonTextNotCovered;
      newState.statusText = config.zipCodeNotCoveredText;
    } else {
      newState.buttonText = config.buttonText;
      newState.statusText = `${nextProps.city}, ${nextProps.stateCode}. Nice!`;
    }
    newState.buttonText = newState.loading ? config.buttonText : newState.buttonText;
    this.setState(newState);
  }

  getStatus() {
    const { statusText } = this.state;
    const statusStyle = this.props.zipCodeCovered
      ? styles.zipCodeCoveredText
      : styles.zipCodeUncoveredText;

    const textView = React.createElement(Text, {
      style: [styles.coverageStatusText, statusStyle]
    }, statusText);
    const loadingView = React.createElement(ActivityIndicator, {
      style: { flex: 1 },
    });
    return this.state.loading ? loadingView : textView;
  }

  getFields() {
    return (
      <View style={styles.fieldsContainer}>
        <View style={styles.field}>
          <TextInput
            autoFocus
            underlineColorAndroid="transparent"
            style={styles.input}
            onChangeText={this.zipCodeChanged}
            placeholder={this.state.zipCodePlaceHolder}
            keyboardType="numeric"
            maxLength={5}
            testID="EnterZipInput"
          />
        </View>
        {this.getStatus()}
      </View>
    );
  }

  continue() {
    this.props.updateUser({
      ...this.props.user,
      stateCode: this.props.stateCode,
      zipCode: this.state.zipCode
    });
    if (!this.state.zipCodeCovered) {
      this.props.addMetric(`appv4.${Platform.OS}.unopenstate.count`, 1);
      this.props.signOut();
      navUtils.setStackRoot('Landing');
    } else {
      this.props.addMetric(`appv4.${Platform.OS}.signup.count`, 1);
    }
    this.setState({ loading: true });
  }

  zipCodeChanged(zipCode) {
    let newState = { ...this.state, zipCode };
    if (zipCode.length < 5) {
      newState = { ...newState, enableButton: false, buttonText: config.buttonText };
    }

    if (zipCode.length === 5) {
      this.props.getZipCode(zipCode);
    }

    this.setState(newState);
  }

  enableButton() {
    return this.state.enableButton && !this.state.loading;
  }

  render() {
    return (
      <KeyboardAvoidingView behavior="height" style={styles.container}>
        {getTitle()}
        {this.getFields()}
        <CustomButton
          buttonContainerStyle={styles.buttonContainer}
          onPress={this.continue}
          buttonText={this.state.buttonText}
          enabled={this.enableButton()}
          testID="EnterZipSubmit"
        />
      </KeyboardAvoidingView>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.user.user,
    zipCodeCovered: state.zipCode.zipCodeCovered,
    city: state.zipCode.city,
    stateCode: state.zipCode.stateCode,
    loading: state.zipCode.loading,
    token: state.user.sessionToken,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setUserToken: (token) => {
      dispatch(userActions.setUserToken(token));
    },
    getZipCode: (zipCode) => {
      dispatch(zipCodeActions.getZipCode(zipCode));
    },
    addMetric: (...args) => {
      dispatch(metricActions.addMetric(...args));
    },
    updateUser: (user) => {
      dispatch(userActions.updateUser(user));
    },
    getUser: () => {
      dispatch(userActions.getUser());
    },
    signOut: () => {
      dispatch(userActions.signOut());
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(enterZip);
