import { colors, appStyles } from 'styles';
import flowStyles from '../styles';

const styles = {
  titleContainer: {
    width: '50%',
    flex: 1,
    marginTop: '20%',
    justifyContent: 'center',
  },
  buttonContainer: {
    flex: 1,
    width: '100%',
  },
  fieldsContainer: {
    width: '100%',
    flex: 1,
    justifyContent: 'flex-start',
  },
  input: {
    flex: 1,
    fontFamily: appStyles.fontFamily,
    textAlign: 'center',
  },
  field: {
    borderBottomColor: colors.borderGray,
    borderBottomWidth: 1,
    height: 50,
  },
  statusTextContainer: {
    flex: 1,
  },
  coverageStatusText: {
    fontFamily: appStyles.fontFamily,
    paddingTop: 10,
    fontSize: 10,
    textAlign: 'center',
  },
  zipCodeCoveredText: {
    color: 'green',
  },
  zipCodeUncoveredText: {
    color: colors.tavourOrange,
  },
};

const mergedStyles = Object.assign(styles, flowStyles);

export default mergedStyles;
