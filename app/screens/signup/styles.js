import { colors } from 'styles';

const styles = {
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  subContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textStyle: {
    color: colors.tavourOrange,
    fontSize: 18,
    textAlign: 'center',
  },
  subTextStyle: {
    fontSize: 14,
    textAlign: 'center',
  },
};

export default styles;
