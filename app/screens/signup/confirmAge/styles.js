import { appStyles } from 'styles';
import flowStyles from '../styles';

const styles = {
  confirmationContainer: {
    flex: 0.7,
  },
  titleContainer: {
    flex: 0.3,
    width: '80%',
    marginTop: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  dialogText: {
    textAlign: 'center',
    fontFamily: appStyles.fontFamily,
    fontSize: 18,
    marginBottom: 10,
  },
  buttonContainer: {
    height: '12%',
    width: '85%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  button: {
    height: '100%',
    width: '48%',
    justifyContent: 'center',
  },
  noButton: {
    backgroundColor: '#d9d9d9',
  },
  yesButton: {
    backgroundColor: '#d9d9d9',
  },
  yesText: {
    fontFamily: appStyles.fontFamily,
    color: '#5c5c5c',
  },
  noText: {
    fontFamily: appStyles.fontFamily,
    color: '#5c5c5c',
  },
  buttonText: {
    fontFamily: appStyles.fontFamily,
    textAlign: 'center',
    fontSize: 18,
  },
};

const mergedStyles = Object.assign(styles, flowStyles);

export default mergedStyles;
