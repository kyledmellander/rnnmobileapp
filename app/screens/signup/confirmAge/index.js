import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux';
import images from 'images';
import { pushScreen, getOptions } from 'lib/navUtils';
import styles from './styles';
import config from './config';

function noPressed() {
  pushScreen('Landing');
}

function yesPressed() {
  pushScreen('CreateLogin');
}

class confirmAge extends React.Component {
  static options() {
    return getOptions('Confirm Age');
  }

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
  }

  componentWillMount() {
    // if (this.props.stateNeeded) {
    //   Actions.EnterZip({ type: ActionConst.RESET });
    // }
    this.setState({
      loading: false,
    });
  }

  render() {
    if (this.state.loading) {
      return (
        <Image
          source={images.splash}
          style={{ width: '100%', height: '100%' }}
          resizeMode="cover"
        />
      );
    }
    return (
      <View style={styles.container}>
        <View style={styles.titleContainer}>
          <Text style={styles.textStyle}>{config.title}</Text>
        </View>
        <View style={styles.confirmationContainer}>
          <Text style={styles.dialogText}>{config.dialog}</Text>
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              style={[styles.button, styles.noButton]}
              onPress={noPressed}
              testID="Under21Button"
            >
              <Text style={[styles.buttonText, styles.noText]}>
                {config.no}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.button, styles.yesButton]}>
              <Text
                style={[styles.buttonText, styles.yesText]}
                onPress={yesPressed}
                testID="Over21Button"
              >
                {config.yes}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    stateNeeded: state.user.user ? state.user.user.stateCode === null : false,
  };
}

function mapDispatchToProps() {
  return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(confirmAge);
