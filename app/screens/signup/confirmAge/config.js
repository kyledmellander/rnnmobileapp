const config = {
  title: "Let's get started.\nThis should only take a minute.",
  dialog: 'Are you 21 or older?',
  yes: 'Yes',
  no: 'No',
};
export default config;
