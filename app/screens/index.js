import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux';
import store from 'config/store';
import { crateCollage } from 'experiments';
import AccountSettings from './accountSettings';
import AboutBasket from './baskets/about';
import AddCreditCard from './paymentMethods/addCreditCard';
import AppLaunch from './appLaunch';
import FirstTimePurchase from './purchase/firstTimePurchase';
import ForgotPassword from './forgotPassword';
import Home from './home';
import DrawerContent from './navigationDrawer/drawerContent';
import Landing from './landing';
import MyBeers from './myBeers';
import MyBasket from './baskets';
import Purchase from './purchase';
import Receipt from './purchase/receipt';
import ShippedCrates from './shippedCrates';
import Addresses from './addresses';
import AddUpdateAddress from './addresses/addUpdateAddress';
import PaymentMethods from './paymentMethods';
import Promos from './promos';
import NotificationSettings from './notificationSettings';
import ContactUs from './contact';
import Referral from './referral';
import TastingNotes from './tastingNotes';
import Login from './login';
import AppNotifications from './appNotifications';
import Redeem from './redeem';
import { ConfirmAge, CreateLogin, EnterZip } from './signup';
import {
  dailyBeers,
  notifications,
  stockUpAndShip,
  flatRateShipping,
  signUpConfirmation
} from './onboarding';

function registerScreens() {
  Navigation.registerComponentWithRedux('AboutBasket', () => AboutBasket, Provider, store);
  Navigation.registerComponentWithRedux('AccountSettings', () => AccountSettings, Provider, store);
  Navigation.registerComponentWithRedux('AddCreditCard', () => AddCreditCard, Provider, store);
  Navigation.registerComponentWithRedux('Addresses', () => Addresses, Provider, store);
  Navigation.registerComponentWithRedux('AddUpdateAddress', () => AddUpdateAddress, Provider, store);
  Navigation.registerComponentWithRedux('AppLaunchScreen', () => AppLaunch, Provider, store);
  Navigation.registerComponentWithRedux('AppNotifications', () => AppNotifications, Provider, store);
  Navigation.registerComponentWithRedux('CrateCollage', () => crateCollage, Provider, store);
  Navigation.registerComponentWithRedux('ConfirmAge', () => ConfirmAge, Provider, store);
  Navigation.registerComponentWithRedux('ContactUs', () => ContactUs, Provider, store);
  Navigation.registerComponentWithRedux('CreateLogin', () => CreateLogin, Provider, store);
  Navigation.registerComponentWithRedux('Drawer', () => DrawerContent, Provider, store);
  Navigation.registerComponentWithRedux('EnterZip', () => EnterZip, Provider, store);
  Navigation.registerComponentWithRedux('FirstTimePurchase', () => FirstTimePurchase, Provider, store);
  Navigation.registerComponentWithRedux('ForgotPassword', () => ForgotPassword, Provider, store);
  Navigation.registerComponentWithRedux('Home', () => Home, Provider, store);
  Navigation.registerComponentWithRedux('Landing', () => Landing, Provider, store);
  Navigation.registerComponentWithRedux('Login', () => Login, Provider, store);
  Navigation.registerComponentWithRedux('MyBasket', () => MyBasket, Provider, store);
  Navigation.registerComponentWithRedux('MyBeers', () => MyBeers, Provider, store);
  Navigation.registerComponentWithRedux('NotificationSettings', () => NotificationSettings, Provider, store);
  Navigation.registerComponentWithRedux('PaymentMethods', () => PaymentMethods, Provider, store);
  Navigation.registerComponentWithRedux('Onboarding1', () => signUpConfirmation, Provider, store);
  Navigation.registerComponentWithRedux('Onboarding2', () => dailyBeers, Provider, store);
  Navigation.registerComponentWithRedux('Onboarding3', () => notifications, Provider, store);
  Navigation.registerComponentWithRedux('Onboarding4', () => stockUpAndShip, Provider, store);
  Navigation.registerComponentWithRedux('Onboarding5', () => flatRateShipping, Provider, store);
  Navigation.registerComponentWithRedux('Promos', () => Promos, Provider, store);
  Navigation.registerComponentWithRedux('Purchase', () => Purchase, Provider, store);
  Navigation.registerComponentWithRedux('Receipt', () => Receipt, Provider, store);
  Navigation.registerComponentWithRedux('Redeem', () => Redeem, Provider, store);
  Navigation.registerComponentWithRedux('Referral', () => Referral, Provider, store);
  Navigation.registerComponentWithRedux('ShippedCrates', () => ShippedCrates, Provider, store);
  Navigation.registerComponentWithRedux('TastingNotes', () => TastingNotes, Provider, store);
}

export { registerScreens, AppLaunch, Home };
