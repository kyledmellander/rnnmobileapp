import React, { Component } from 'react';
import { Text, TouchableOpacity, ScrollView } from 'react-native';
import { actions as appNotificationActions, selectors as appNotificationSelectors } from 'appRedux/appNotifications';
import { connect } from 'react-redux';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome';
import appNotificationsUtil from 'src/config/appNotifications';
import * as navUtils from 'lib/navUtils';
import styles, { colors } from './styles';

const iconNames = {
  informational: 'check-circle',
  nonCriticalAction: 'exclamation-triangle',
  criticalAction: 'exclamation-triangle'
};

const iconBackgroundColors = {
  informational: colors.alertGreen,
  nonCriticalAction: colors.alertOrange,
  criticalAction: colors.alertRed
};

const getIcon = (notificationType) => {
  const iconName = iconNames[notificationType] || 'check-circle';
  const iconBackgroundColor = iconBackgroundColors[notificationType] || 'green';
  return (
    <FontAwesomeIcons
      name={iconName}
      backgroundColor={iconBackgroundColor}
      color={iconBackgroundColor}
      size={30}
      style={styles.icon}
    />
  );
};

const getTimeDiff = (notification) => {
  if (!notification.createdAt) return null;
  const now = new Date();
  const alertTime = new Date(notification.createdAt);
  const dateDiffMs = now - alertTime;
  const dateDiffS = dateDiffMs / 1000;
  const dateDiffM = dateDiffS / 60;
  const dateDiffH = dateDiffM / 60;
  const dateDiffD = dateDiffH / 24;
  let timeDiffString;
  if (dateDiffS < 60) {
    timeDiffString = `${Math.ceil(dateDiffS)}s`;
  } else if (dateDiffM < 60) {
    timeDiffString = `${Math.ceil(dateDiffM)}m`;
  } else if (dateDiffH < 24) {
    timeDiffString = `${Math.ceil(dateDiffH)}h`;
  } else {
    timeDiffString = `${Math.ceil(dateDiffD)}d`;
  }
  return (
    <Text style={[styles.time, styles[notification.alertType]]}>
      {timeDiffString}
    </Text>
  );
};

const notificationView = (props) => {
  const { appNotification } = props;
  const statusTextStyle = appNotification.isActive ? styles.activeText : styles.inactiveText;
  const statusStyle = appNotification.isActive ?
    styles.activeNotification :
    styles.inactiveNotification;
  return (
    <TouchableOpacity style={[styles.appNotificationView, statusStyle]} onPress={props.onPress}>
      {getIcon(appNotification.alertType)}
      <Text style={[styles.appNotificationText,
        styles[appNotification.alertType],
        statusTextStyle]}
      >
        {appNotification.body}
      </Text>
      {getTimeDiff(appNotification)}
    </TouchableOpacity>
  );
};

class appNotifications extends Component {
  static options() {
    return navUtils.getOptions('Notifications');
  }

  constructor(props) {
    super(props);
    this.state = {
      appNotifications: props.appNotifications
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.appNotifications !== this.state.appNotifications) {
      this.setState({
        appNotifications: nextProps.appNotifications
      });
    }
  }

  mounted() {
    this.props.getAppNotifications();
  }

  render() {
    const onPress = (n) => {
      this.props.updateNotification(n);
      n.isActive = false;
      if (n.actionUrl) {
        appNotificationsUtil.routeFromURL(n.actionUrl);
      }
    };
    const views = this.state.appNotifications.map(n =>
      notificationView({ appNotification: n, onPress: () => onPress(n) }));
    return React.createElement(
      ScrollView,
      { contentContainerStyle: styles.appNotificationContainer, bounces: false },
      views
    );
  }
}

function mapStateToProps(state) {
  return {
    appNotifications: appNotificationSelectors.notificationsSelector(state)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getAppNotifications() {
      dispatch(appNotificationActions.getNotifications());
    },
    updateNotification(appNotification) {
      dispatch(appNotificationActions.updateNotification(appNotification));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(appNotifications);
