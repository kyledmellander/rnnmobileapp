import { colors as appColors } from 'styles';

export const colors = {
  inactive: '#666666',
  alertRed: 'rgb(214, 42, 44)',
  alertOrange: 'rgb(204, 95, 2)',
  alertGreen: 'rgb(77,145,69)'
};

const styles = {
  appNotificationContainer: {
    borderColor: appColors.borderGray,
    borderTopWidth: 1,
    marginTop: 70,
    justifyContent: 'flex-start',
    flex: 1
  },
  appNotificationView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingLeft: 20,
    paddingTop: 10,
    paddingBottom: 10,
    flex: 0,
    minHeight: 75,
    borderColor: appColors.borderGray,
    borderBottomWidth: 1,
  },
  appNotificationText: {
    textAlign: 'left',
    fontSize: 16,
    flex: 1,
  },
  activeNotification: {
    backgroundColor: 'white'
  },
  inactiveNotification: {
    backgroundColor: appColors.backgroundGray
  },
  activeText: {
    fontFamily: 'OpenSans'
  },
  inActiveText: {
    color: colors.inactive,
    fontFamily: 'OpenSans'
  },
  informational: {
    color: colors.inactive,
  },
  criticalAction: {
    color: colors.alertRed
  },
  nonCriticalAction: {
  },
  icon: {
    paddingRight: 25
  },
  time: {
    fontFamily: 'OpenSans',
    fontSize: 14,
    alignSelf: 'flex-start',
    paddingTop: 10,
    paddingRight: 25,
    paddingLeft: 25,
    flex: 0
  }
};

export default styles;

