import { appStyles } from 'styles';

const width = '90%';
const styles = {
  container: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    marginTop: appStyles.marginTop,
  },
  body: {
    height: 50,
    marginTop: 15,
    width,
  },
  button: {
    height: 70,
    position: 'absolute',
    bottom: 0,
    width: '100%',
  },
};

export default styles;
