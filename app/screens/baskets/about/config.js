export default {
  about: 'This is where we’ll stash your beers until you’re ready to ship.',
  aboutPickup: 'This is where we’ll stash your beers until you’re ready to ship or pickup.',

  changeShipDateTitle: 'Changing your ship date',
  changeShipDateBody: 'Click the orange \'edit\' button to see the option to change your ship date to be sooner or later.',

  changeShipPickupDateTitle: 'Changing your shipping or pickup date',
  changeShipPickupDateBody: 'Click the orange \'edit\' button to see the option to change your shipping or pickup date to be sooner or later.',

  shippingFeeTitle: 'Shipping Fee',
  shippingFeeBody: 'Before your beer ships out we will charge you a flat $14.90 shipping fee. You can ship as many beers as you like for a flat fee of $14.90.',

  signatureRequiredTitle: 'Signature Required',
  signatureRequiredBody: 'Due to the nature of shipping beer, there will need to be an adult (over 21) present to sign for your Tavour Package.',

  trackingInfoTitle: 'Tracking Information',

  pickUpTitle: 'Pick up your beer from Tavour',
  pickUpBody: 'Live near Tavour? You can save on shipping and pickup your crate directly from us! Just edit your crate and set it to \'Pickup\'. Once its ready to pickup it will be waiting for you at our warehouse (for up to 90 days).',

  cancelOrderTitle: 'Cancelling an Order',
  cancelOrderBody: 'Individual beer orders can be refunded up to 3 days (72 hours) after the purchase, just contact us at support@tavour.com',
};
