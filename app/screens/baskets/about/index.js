import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ScrollView, Text, Image, Linking, View } from 'react-native';
import { icons } from 'images';
import utils from 'lib/utils';
import * as navUtils from 'lib/navUtils';
import { actions as userActions } from 'appRedux/user';
import styles from './styles';
import config from './config';

class aboutBasket extends Component {
  static options() {
    return navUtils.getOptions(`About Your ${utils.getBasketWord()}`);
  }

  constructor(props) {
    super(props);
    this.state = {
      userState: props.userState,
    };
  }

  componentWillMount() {
    this.props.getUserState();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.userState !== null) {
      this.setState({ userState: nextProps.userState });
    }
  }

  render() {
    const titleStyle = [styles.text, styles.textLarge, styles.marginTop20];
    const paragraphStyle = [styles.text, styles.paragraph];
    const paragraphLinkStyle = [styles.text, styles.paragraph, styles.orange];
    const trackingInfoBody = `Once your beer ships we will provide tracking information. To find your tracking info, navigate to the 'Shipped ${utils.getBasketWord()}s' page.`;
    if (this.state.userState === 'Washington') {
      return (
        <ScrollView style={styles.container}>
          <View style={styles.photoContainer}>
            <Image source={icons.crate} style={styles.icon} />
          </View>
          <Text style={[styles.text, styles.about]}>
            {' '}
            {config.aboutPickup}{' '}
          </Text>

          <Text style={titleStyle}> {config.changeShipPickupDateTitle} </Text>
          <Text style={paragraphStyle}>
            {' '}
            {config.changeShipPickupDateBody}{' '}
          </Text>

          <Text style={titleStyle}> {config.pickUpTitle} </Text>
          <Text style={paragraphStyle}> {config.pickUpBody} </Text>

          <Text style={titleStyle}> {config.cancelOrderTitle} </Text>
          <Text style={paragraphStyle}> {config.cancelOrderBody} </Text>
          <Text
            style={paragraphLinkStyle}
            onPress={() =>
              Linking.openURL('https://www.tavour.com/cancellations-and-returns')
            }
          >
            {' '}
            See our Cancellation and Returns Policy for more details.
          </Text>

          <Text style={titleStyle}> {config.shippingFeeTitle} </Text>
          <Text style={paragraphStyle}> {config.shippingFeeBody} </Text>
          <Text
            style={paragraphLinkStyle}
            onPress={() =>
              Linking.openURL('https://www.tavour.com/shipping-policy')
            }
          >
            {' '}
            See our Shipping Policy for more details.
          </Text>

          <Text style={titleStyle}> {config.signatureRequiredTitle} </Text>
          <Text style={paragraphStyle}> {config.signatureRequiredBody} </Text>

          <Text style={titleStyle}> {config.trackingInfoTitle} </Text>
          <Text style={paragraphStyle}> {trackingInfoBody} </Text>
        </ScrollView>
      );
    }
    return (
      <ScrollView style={styles.container}>
        <View style={styles.photoContainer}>
          <Image source={icons.crate} style={styles.icon} />
        </View>
        <Text style={[styles.text, styles.about]}> {config.about} </Text>

        <Text style={titleStyle}> {config.changeShipDateTitle} </Text>
        <Text style={paragraphStyle}> {config.changeShipDateBody} </Text>

        <Text style={titleStyle}> {config.cancelOrderTitle} </Text>
        <Text style={paragraphStyle}> {config.cancelOrderBody} </Text>
        <Text
          style={paragraphLinkStyle}
          onPress={() =>
              Linking.openURL('https://www.tavour.com/cancellations-and-returns')
            }
        >
          {' '}
            See our Cancellation and Returns Policy for more details.
        </Text>

        <Text style={titleStyle}> {config.shippingFeeTitle} </Text>
        <Text style={paragraphStyle}> {config.shippingFeeBody} </Text>
        <Text
          style={paragraphLinkStyle}
          onPress={() =>
              Linking.openURL('https://www.tavour.com/shipping-policy')
            }
        >
          {' '}
            See our Shipping Policy for more details.
        </Text>

        <Text style={titleStyle}> {config.signatureRequiredTitle} </Text>
        <Text style={paragraphStyle}> {config.signatureRequiredBody} </Text>

        <Text style={titleStyle}> {config.trackingInfoTitle} </Text>
        <Text style={paragraphStyle}> {trackingInfoBody} </Text>
      </ScrollView>
    );
  }
}

function mapStateToProps(state) {
  return {
    userState: state.user.userState,
    userStateCode: state.user.userStateCode,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getUserState: () => {
      dispatch(userActions.getUserState());
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(aboutBasket);
