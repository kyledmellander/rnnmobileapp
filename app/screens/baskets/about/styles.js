import { colors, appStyles } from 'styles';

const styles = {
  container: {
    paddingTop: 80,
    flex: 1,
    backgroundColor: colors.backgroundGray,
  },
  photoContainer: {
    width: '100%',
    height: 120,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: '10%',
    marginVertical: 10,
  },
  icon: {
    flex: 1,
    resizeMode: 'contain',
  },
  about: {
    marginTop: 37,
    marginBottom: 20,
    width: '90%',
    marginLeft: 20,
  },
  text: {
    marginLeft: 20,
    fontSize: 15,
    fontFamily: appStyles.fontFamily,
    width: '90%',
  },
  textLarge: {
    fontSize: 18,
  },
  paragraph: {
    marginVertical: 10,
  },
  orange: {
    color: colors.tavourOrange,
  },
  marginTop20: {
    marginTop: 20,
  },
};

export default styles;
