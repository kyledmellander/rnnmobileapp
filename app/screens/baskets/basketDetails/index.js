import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux';
import { icons } from 'images';
import { actions as basketActions } from 'appRedux/baskets';
import { actions as userActions } from 'appRedux/user';
import utils from 'lib/utils';
import * as navUtils from 'lib/navUtils';
import styles from './styles';

class basketDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      basket: props.basket,
      selectedMarkedFor: props.basket.markedFor,
      selectedCycleId: props.basket.shippingCycle.id,
      editing: false,
      hasSeen: null,
      userState: null,
      busy: false,
    };
    this.editDetailsPressed = this.editDetailsPressed.bind(this);
    this.saveDetailsPressed = this.saveDetailsPressed.bind(this);
    this.closeNewUserDetailsPressed = this.closeNewUserDetailsPressed.bind(this);
    this.hasMadeEdit = this.hasMadeEdit.bind(this);
    this.openHelp = this.openHelp.bind(this);
    this.shipPressed = this.shipPressed.bind(this);
    this.pickupPressed = this.pickupPressed.bind(this);
  }

  componentWillMount() {
    this.props.getHasSeen();
    this.props.getUserState();
  }

  componentWillReceiveProps(nextProps) {
    const newState = {
      busy: false,
      editing: false,
    };

    if (nextProps.hasSeen !== null) {
      newState.hasSeen = nextProps.hasSeen;
    }

    if (nextProps.userState !== null) {
      newState.userState = nextProps.userState;
    }

    if (nextProps.basket) {
      newState.basket = nextProps.basket;
    }

    if (nextProps.basket.shippingCycle) {
      newState.selectedCycleId = nextProps.basket.shippingCycle.id;
    }

    if (nextProps.basket.markedFor) {
      newState.markedFor = nextProps.basket.markedFor;
    }

    this.setState(newState);
  }

  markedForShipping() {
    return this.state.selectedMarkedFor === 'Shipping';
  }

  editDetailsPressed() {
    const newState = {
      ...this.state,
      editing: true,
    };
    this.setState(newState);
  }

  saveDetailsPressed() {
    const newState = { ...this.state };

    if (this.hasMadeEdit()) {
      newState.busy = true;
      const shippingCycle = {
        id: this.state.selectedCycleId,
      };
      this.props.updateBasket(
        this.state.basket,
        this.state.selectedMarkedFor,
        shippingCycle
      );
    } else {
      newState.editing = false;
    }

    this.setState(newState);
  }

  openHelp() {
    this.closeNewUserDetailsPressed();
    navUtils.pushScreen('AboutBasket');
  }

  closeNewUserDetailsPressed() {
    this.props.setHasSeen();
    this.setState({
      hasSeen: true,
    });
  }

  hasMadeEdit() {
    return (
      this.state.selectedMarkedFor !== this.state.basket.markedFor ||
      this.state.selectedCycleId !== this.state.basket.shippingCycle.id
    );
  }

  shipPressed() {
    this.setState({ selectedMarkedFor: 'Shipping' });
  }

  pickupPressed() {
    this.setState({ selectedMarkedFor: 'Pickup' });
  }

  canPickupRow() {
    const selectedButtonStyle = [styles.editButton, styles.editButtonSelected];
    if (this.state.busy) {
      selectedButtonStyle.push(styles.disabledButton);
    }
    const selectedButtonTextStyle = [
      styles.editButtonText,
      styles.editButtonSelectedText,
    ];
    return (
      <View style={[styles.rowItem, styles.pickupShippingRowItems]}>
        <View style={[styles.flex30, styles.textItem]}>
          <Text style={[styles.editDetailText, styles.textAlignRight]}>
            I want to
          </Text>
        </View>

        <View style={[styles.buttonsRow, styles.flexOne]}>
          <TouchableOpacity
            style={
              this.markedForShipping() ? selectedButtonStyle : styles.editButton
            }
            onPress={this.shipPressed}
            disabled={this.state.busy}
          >
            <Text
              style={
                this.markedForShipping()
                  ? selectedButtonTextStyle
                  : styles.editButtonText
              }
            >
              Ship
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={
              !this.markedForShipping()
                ? selectedButtonStyle
                : styles.editButton
            }
            onPress={this.pickupPressed}
            disabled={this.state.busy}
          >
            <Text
              style={
                !this.markedForShipping()
                  ? selectedButtonTextStyle
                  : styles.editButtonText
              }
            >
              Pickup
            </Text>
          </TouchableOpacity>
          <Text
            style={[
              styles.editDetailText,
              styles.textAlignLeft,
              styles.paddingLeft10,
            ]}
          >
            my {utils.getBasketWord(true)}
          </Text>
        </View>
      </View>
    );
  }

  shippingCyclePressed(shippingCycle) {
    this.setState({ selectedCycleId: shippingCycle.id });
  }

  shippingCycleRow() {
    const cycles = this.props.shippingCycles;
    const shippingCycle = this.state.selectedCycleId;
    const shippingPressed = this.shippingCyclePressed.bind(this);
    const markedForShipping = this.markedForShipping();
    const { formatDate } = this.props;
    const cycleButton = (cycle) => {
      const date = markedForShipping ? cycle.shippingDate : cycle.pickupsReady;
      const style =
        cycle.id === shippingCycle
          ? [
            styles.editButton,
            styles.editButtonSelected,
            styles.editButtonMultiLine,
          ]
          : [styles.editButton, styles.editButtonMultiLine];
      if (this.state.busy && cycle.id === shippingCycle) {
        style.push(styles.disabledButton);
      }
      const textStyle =
        cycle.id === shippingCycle
          ? [styles.editButtonText, styles.editButtonSelectedText]
          : styles.editButtonText;
      return (
        <TouchableOpacity
          style={style}
          onPress={() => shippingPressed(cycle)}
          disabled={this.state.busy}
        >
          <Text style={textStyle}>{formatDate(date)}</Text>
        </TouchableOpacity>
      );
    };

    const children = [];
    children.push(cycles.map(cycleButton));

    return (
      <View style={[styles.buttonsRow, styles.flexOne]}>
        {children}
      </View>
    );
  }

  saveCloseButton() {
    if (this.state.busy) {
      return (
        <Text
          style={[
            styles.basketDetailText,
            styles.editText,
            styles.disabledText,
          ]}
        >
          saving...
        </Text>
      );
    }
    return (
      <Text
        style={[styles.basketDetailText, styles.editText]}
        onPress={this.saveDetailsPressed}
      >
        {this.hasMadeEdit() ? 'save' : 'close'}
      </Text>
    );
  }

  editDetails() {
    const deliveryRow =
      this.state.userState === 'Washington'
        ? this.canPickupRow()
        : React.createElement(
          Text,
          {
            style: [
              styles.quarterRowItem,
              styles.editDetailText,
              styles.textAlignCenter,
            ],
          },
          `I want to ship my ${utils.getBasketWord(true)}`
        );
    return (
      <View style={[styles.basketDetails, styles.editDetails]}>
        <View
          style={[
            styles.basketDetailRow,
            styles.editDetailTitleRow,
            styles.flex30,
          ]}
        >
          <Text
            style={[
              styles.basketDetailText,
              styles.basketDetailTitle,
              styles.editBasketDetailTitle,
            ]}
          >
            {utils.getBasketWord()} Details
          </Text>

          {this.saveCloseButton()}
        </View>

        {deliveryRow}

        <View style={[styles.rowItem, styles.shippingCycleRowItems]}>
          <View style={[styles.flex30, styles.textItem]}>
            <Text
              style={[
                styles.editDetailText,
                styles.textAlignCenter,
                styles.absPosition,
              ]}
            >
              on
            </Text>
          </View>

          {this.shippingCycleRow()}
        </View>
      </View>
    );
  }

  showDetails() {
    return (
      <View style={styles.basketDetails}>
        <View style={[styles.basketDetailRow, styles.editDetailTitleRow]}>
          <Text style={[styles.basketDetailText, styles.basketDetailTitle]}>
            {utils.getBasketWord()} Details
          </Text>
          <Text
            style={[styles.basketDetailText, styles.editText]}
            onPress={this.editDetailsPressed}
          >
            edit
          </Text>
        </View>

        <View style={styles.basketDetailRow}>
          <Text style={styles.basketDetailText}>
            {this.markedForShipping()
              ? 'Your beer ships on'
              : 'Pickup your beer on'}
          </Text>
          <Text style={[styles.basketDetailText]}>
            {this.props.formatDate(this.markedForShipping()
                ? this.state.basket.shippingCycle.shippingDate
                : this.state.basket.shippingCycle.pickupsReady)}
          </Text>
        </View>
      </View>
    );
  }

  newUserDetails() {
    return (
      <View style={styles.newUserBasketDetails}>
        <View style={styles.newUserBasketDetailRow}>
          <TouchableOpacity
            style={styles.closeNewUserBasketDetail}
            onPress={this.closeNewUserDetailsPressed}
          >
            <Image source={icons.close} style={styles.closeImage} />
          </TouchableOpacity>
          <Text style={[styles.basketDetailText, styles.basketDetailTitle]}>
            About Your {utils.getBasketWord()}
          </Text>
        </View>
        <View style={styles.newUserBasketDetailRow}>
          <View style={styles.photoContainer}>
            <Image source={icons.crate} style={styles.photo} />
          </View>
        </View>
        <View style={styles.newUserBasketDetailRow}>
          <Text style={styles.newUserBasketDetailText}>
            This is where we’ll stash the beers you add until you’re ready to
            ship. Shipping is a $14.90 flat fee each time. Right now you’re set
            to ship once a month to spread the cost over more beers.
          </Text>
        </View>
        <View
          style={[
            styles.newUserBasketDetailRow,
            styles.justifyContentFlexStart,
          ]}
        >
          <Text style={styles.newUserBasketDetailText}>Cheers!</Text>
        </View>
        <View style={styles.newUserBasketDetailRow}>
          <TouchableOpacity
            style={[styles.button, styles.orangeButton]}
            onPress={this.openHelp}
          >
            <Text style={styles.whiteText}>Tell me more</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.button, styles.whiteButton]}
            onPress={this.closeNewUserDetailsPressed}
          >
            <Text style={styles.blackText}>Got it</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  render() {
    if (this.state.hasSeen !== null && !this.state.hasSeen) {
      return this.newUserDetails();
    }

    if (this.state.editing) {
      return this.editDetails();
    }

    return this.showDetails();
  }
}

function mapStateToProps(state) {
  return {
    basket: state.baskets.openBasket,
    shippingCycles: state.baskets.openShippingCycles,
    hasSeen: state.baskets.hasSeen,
    userState: state.user.userState,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getHasSeen: () => {
      dispatch(basketActions.getHasSeen());
    },
    setHasSeen: (hasSeen) => {
      dispatch(basketActions.setHasSeen(hasSeen));
    },
    getUserState: () => {
      dispatch(userActions.getUserState());
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(basketDetails);
