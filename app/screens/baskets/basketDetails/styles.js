import { Platform } from 'react-native';
import { colors, appStyles } from 'styles';

const styles = {
  photoContainer: {
    width: '100%',
    height: 120,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: '10%',
    marginVertical: 20,
  },
  photo: {
    flex: 1,
    resizeMode: 'contain',
  },
  basketDetails: {
    flex: 0,
    width: '100%',
    paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: '#EAEAEA',
    borderTopWidth: 1,
    borderColor: '#D1D1D1',
  },
  basketDetailTitle: {
    fontSize: 16,
  },
  editText: {
    color: colors.tavourOrange,
    fontSize: 15,
    justifyContent: 'center',
  },
  disabledText: {
    color: colors.tavourDarkGray,
  },
  basketDetailText: {
    color: colors.textGray,
    fontFamily: appStyles.fontFamily,
    fontSize: 12,
  },
  basketDetailRow: {
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  basketDetailDivider: {
    borderBottomWidth: 1,
    borderColor: '#BDBDBD',
  },
  basketDetailRight: {
    position: 'absolute',
    right: 0,
    bottom: 7,
  },
  editDetails: {
    flex: 0.7,
    paddingTop: 8,
    paddingBottom: 14,
    justifyContent: 'space-around',
    minHeight: 200
  },
  editBasketDetailTitle: {
    color: '#161616',
  },
  editDetailTitleRow: {
    paddingBottom: 9,
  },
  editDetailRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexWrap: 'wrap',
  },
  buttonsRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexWrap: 'wrap',
  },
  rowItem: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  quarterRowItem: {
    flex: 0.25,
  },
  shippingCycleRowItems: {
    flex: 1,
    marginTop: 10,
  },
  pickupShippingRowItems: {
    flex: 0.5,
  },
  editDetailText: {
    color: 'black',
    fontFamily: appStyles.fontFamilySemibold,
    fontSize: 12,
  },
  editButton: {
    width: 70,
    height: 41,
    backgroundColor: 'white',
    alignItems: 'center',
    borderRadius: 3,
    marginLeft: 11,
  },
  editButtonMultiLine: {
    marginTop: 11,
  },
  editButtonSelected: {
    backgroundColor: colors.tavourOrange,
  },
  editButtonText: {
    color: '#898989',
    flex: 1,
    fontFamily: appStyles.fontFamily,
    paddingVertical: 11,
    fontSize: 12,
  },
  editButtonSelectedText: {
    color: 'white',
  },
  newUserBasketDetails: {
    width: '100%',
    paddingTop: 10,
    paddingBottom: 20,
    paddingHorizontal: 20,
    backgroundColor: '#EAEAEA',
    borderTopWidth: 1,
    borderColor: '#D1D1D1',
  },
  newUserBasketDetailRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 5,
  },
  justifyContentFlexStart: {
    justifyContent: 'flex-start',
  },
  newUserBasketDetailText: {
    color: colors.textGray,
    fontFamily: appStyles.fontFamily,
    fontSize: 14,
  },
  closeNewUserBasketDetail: {
    position: 'absolute',
    left: 0,
    bottom: 0,
  },
  closeImage: {
    height: 21,
    width: 21,
  },
  crateImage: {
    height: 100,
    width: 130,
    marginVertical: 10,
  },
  button: {
    height: Platform.OS === 'ios' ? 30 : 50,
    width: '45%',
    marginHorizontal: 10,
    marginVertical: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  orangeButton: {
    backgroundColor: colors.tavourOrange,
  },
  disabledButton: {
    backgroundColor: colors.tavourDarkGray,
  },
  whiteText: {
    color: 'white',
  },
  whiteButton: {
    backgroundColor: 'white',
  },
  blackText: {
    color: colors.tavourDarkGray,
  },
  textAlignLeft: {
    textAlign: 'left',
  },
  textAlignRight: {
    textAlign: 'right',
  },
  textAlignCenter: {
    textAlign: 'center',
  },
  flexOneHalf: {
    flex: 0.5,
  },
  flex30: {
    flex: 0.3,
  },
  flexOne: {
    flex: 1,
  },
  alignItemsFlexEnd: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  padding15: {
    padding: 15,
  },
  paddingLeft10: {
    paddingLeft: 10,
  },
  textItem: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  absPosition: {
    position: 'absolute',
    top: 15,
    right: 13,
  },
};

export default styles;
