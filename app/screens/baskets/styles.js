import { colors, appStyles } from 'styles';

const styles = {
  marginTop: {
    marginTop: appStyles.marginTop,
  },
  container: {
    flex: 1,
  },
  titleContainer: {
    flex: 0,
    marginVertical: 10,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    textAlign: 'center',
    flex: 0,
    color: colors.textGray,
    fontSize: 14,
    fontFamily: appStyles.fontFamilyLight,
  },
  subTitleLink: {
    flex: 0,
    textAlign: 'center',
    fontSize: 14,
    color: colors.tavourOrange,
    fontFamily: appStyles.fontFamilySemibold,
  },
};

export default styles;
