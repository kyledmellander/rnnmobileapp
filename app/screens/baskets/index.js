import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  Animated,
  Easing,
} from 'react-native';
import { connect } from 'react-redux';
import { actions as basketActions } from 'appRedux/baskets';
import { OrderProductView, Icons } from 'components';
import utils from 'lib/utils';
import loadingLib from 'lib/loading';
import * as navUtils from 'lib/navUtils';
import BasketDetails from './basketDetails';
import Loading from './loading';
import styles from './styles';

const animatedValue = new Animated.Value(0);

class basket extends Component {
  static options() {
    return {
      topBar: {
        buttonColor: 'black',
        title: {
          component: {
            name: 'NavBarTitle',
            passProps: {
              title: `Current ${utils.getBasketWord()}`,
            },
            alignment: 'center',
          },
        },
        leftButtons: [
          {
            id: 'hamburger',
            icon: Icons.hamburger,
            color: 'black',
          }
        ]
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      userStateCode: null,
    };
    this.lastDayToAddBeer = this.lastDayToAddBeer.bind(this);
    this.getOrderProducts = this.getOrderProducts.bind(this);
    this.transitionToCollage = this.transitionToCollage.bind(this);
  }

  componentWillMount() {
    this.props.fetchBasket();
  }

  componentWillReceiveProps(nextProps) {
    const newState = {
      ...this.state,
      selectedMarkedFor: nextProps.basket ? nextProps.basket.markedFor : 'Shipping'
    };
    newState.loading = false;
    this.setState(newState);
  }

  getOrderProducts() {
    if (this.props.basket == null) return [];
    const orderProducts = this.props.basket.orders.reduce((acc, o) =>
      acc.concat(o.orderProducts), []);

    return orderProducts;
  }

  getOrders() {
    if (this.props.basket == null) {
      return null;
    }

    const orderProducts = this.getOrderProducts();

    const orderProductViews = orderProducts.map((op) => {
      if (orderProducts.indexOf(op) !== 0) {
        return OrderProductView({ orderProduct: op, first: true });
      }
      return OrderProductView({ orderProduct: op, first: false });
    });
    return (
      <ScrollView style={styles.orderContainer}>
        {orderProductViews}
      </ScrollView>
    );
  }

  getSubTitle() {
    if (this.props.basket && this.props.basket.orders.length > 0) {
      return null;
    }
    const onPress = () => navUtils.setStackRoot('Home');
    return (
      <Text>
        <Text style={styles.title}> Add</Text>
        <Text style={[styles.subTitleLink]} onPress={onPress}>
          {' '}
          a few beers{' '}
        </Text>
      </Text>
    );
  }

  getTitle() {
    const title =
      !this.props.basket || this.props.basket.orders.length === 0
        ? `Your ${utils.getBasketWord(true)} is empty!`
        : `Add as much beer as you'd like until ${this.lastDayToAddBeer()}`;
    const subTitle = this.getSubTitle();
    return (
      <View style={styles.titleContainer}>
        <Text style={styles.title}> {title} </Text>
        {subTitle}
      </View>
    );
  }

  getDetails() {
    if (!this.props.basket) {
      return null; // todo show 'about' info
    }
    const basketDetailProps = {
      basket: this.props.basket,
      shippingCycles: this.props.shippingCycles.slice(0, 6),
      updateBasket: this.props.updateBasket,
      formatDate: utils.formatDate
    };
    return React.createElement(BasketDetails, basketDetailProps);
  }

  getCollageButton() {
    const orderProducts = this.getOrderProducts();
    if (orderProducts.length <= 0) {
      return null;
    }
    return (
      <Text style={[styles.subTitleLink]} onPress={this.transitionToCollage}>
        View Collage
      </Text>
    );
  }

  transitionToCollage() {
    navUtils.pushScreen('CrateCollage', {
      passProps: {
        orderProducts: this.getOrderProducts(),
        state: this.props,
      }
    });
  }

  lastDayToAddBeer() {
    const date =
      this.state.selectedMarkedFor === 'Shipping'
        ? this.props.basket.shippingCycle.lastDayToAddBeer
        : this.props.basket.shippingCycle.lastDayToAddBeerPickups;
    return utils.formatDate(date);
  }

  render() {
    if (this.state.loading) {
      return Loading();
    }
    const increaseOpacity = loadingLib.increaseOpacity(animatedValue);
    Animated.timing(animatedValue, {
      toValue: 1,
      duration: 500,
      easing: Easing.linear,
    }).start();
    return (
      <Animated.View
        style={[styles.container, styles.marginTop, increaseOpacity]}
      >
        {this.getTitle()}
        <ScrollView style={styles.container}>{this.getOrders()}</ScrollView>
        {this.getCollageButton()}
        {this.getDetails()}
      </Animated.View>
    );
  }
}

function mapStateToProps(state) {
  return {
    basket: state.baskets.openBasket,
    shippingCycles: state.baskets.openShippingCycles,
    userStateCode: state.user.userStateCode,
    user: state.user.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchBasket: () => {
      dispatch(basketActions.getOpenBasket());
    },
    updateBasket: (bask, markedFor, shippingCycle) => {
      dispatch(basketActions.updateBasket(bask, markedFor, shippingCycle));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(basket);
