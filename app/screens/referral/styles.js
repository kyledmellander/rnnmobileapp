import { colors, appStyles, commonStyles } from 'styles';

const minorSpacing = 5;

const styles = {
  titleContainer: {
    paddingTop: 10,
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderColor: colors.borderGray,
  },
  title: { ...commonStyles.sectionTitle },
  mainContainer: {
    flex: 1,
    width: '100%',
    paddingTop: appStyles.marginTop,
    justifyContent: 'flex-start'
  },
  buttonContainer: { ...commonStyles.bigButtonContainer, marginBottom: 30 },
  button: { ...commonStyles.bigButton },
  emptyContainer: {
    flex: 1,
    justifyContent: 'center'
  },
  emptyTextContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginLeft: minorSpacing,
    marginRight: minorSpacing
  },
  emptyText: {
    fontSize: 15,
    marginBottom: 10,
    justifyContent: 'center',
    fontFamily: appStyles.fontFamily,
    textAlign: 'center'
  },
  preCodeText: {
    fontSize: 11
  },
  CodeText: {
    fontSize: 49,
    marginBottom: 20
  },
  errorText: {
    textAlign: 'center',
    color: colors.failRed
  },
  giftCardText: {
    color: colors.tavourOrange
  }
};

export default styles;
