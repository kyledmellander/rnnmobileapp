import React, { Component } from 'react';
import {
  View,
  Text,
  Animated,
  Easing,
  Share,
  Linking,
} from 'react-native';
import { actions as referralActions } from 'appRedux/referral';
import { connect } from 'react-redux';
import loadingLib from 'lib/loading';
import { Icons, CustomButton } from 'components';
import analytics from 'lib/analytics';
import Loading from './loading';
import styles from './styles';

const animatedValue = new Animated.Value(0);

const Error = () => (
  <View style={styles.emptyContainer}>
    <Text style={styles.errorText}>Sorry it looks like we ran into an error. Try again later.</Text>
  </View>
);

const Description = props => (
  <View style={styles.emptyTextContainer}>
    <Text style={styles.emptyText}>
      {props.text}
    </Text>
  </View>
);

const shareReferral = ({ title, message, dialogTitle }) => {
  Share
    .share({ message, title }, { dialogTitle })
    .then(() => analytics.track({ event: 'Share Referral Button' }))
    .catch(err => console.log(err));
};

class credits extends Component {
  static options() {
    return {
      topBar: {
        buttonColor: 'black',
        title: {
          component: {
            name: 'NavBarTitle',
            passProps: {
              title: 'Free Beer',
            },
            alignment: 'center',
          },
        },
        leftButtons: [
          {
            id: 'hamburger',
            icon: Icons.hamburger,
            color: 'black',
          }
        ]
      }
    };
  }
  componentWillMount() {
    this.props.getInitData();
  }

  render() {
    if (this.props.loading) {
      return Loading();
    }
    if (this.props.error) {
      return (<Error />);
    }
    const increaseOpacity = loadingLib.increaseOpacity(animatedValue);

    Animated.timing(animatedValue, {
      toValue: 1,
      duration: 1500,
      easing: Easing.in,
    }).start();

    return (
      <Animated.View style={[styles.emptyContainer, increaseOpacity]}>
        <View style={styles.emptyTextContainer}>
          <Text style={styles.preCodeText}>YOUR CODE</Text>
        </View>
        <View style={styles.emptyTextContainer}>
          <Text style={styles.CodeText}>{this.props.code}</Text>
        </View>
        {this.props.descriptions.map(desc => (<Description text={desc} />))}
        <CustomButton
          buttonContainerStyle={styles.buttonContainer}
          buttonText="Share code"
          enabled
          onPress={() => shareReferral(this.props.shareText)}
          testID="ShareCodeButton"
        />
        <View style={styles.emptyTextContainer}>
          <Text style={styles.emptyText}>
            You can also give the gift of free beer by
            <Text
              style={styles.giftCardText}
              onPress={async () => Linking.openURL(this.props.giftCardLink)}
            >
              {' '}purchasing a gift card.
            </Text>
          </Text>
        </View>
      </Animated.View>
    );
  }
}

function mapStateToProps(state) {
  if (!state.promos) {
    return {};
  }
  return {
    code: state.referral.code,
    descriptions: state.referral.descriptions,
    error: state.referral.error,
    loading: state.referral.loading,
    shareText: state.referral.shareText,
    giftCardLink: state.referral.giftCardLink
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getInitData: () => {
      dispatch(referralActions.getInitData());
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(credits);
