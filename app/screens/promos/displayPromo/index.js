import React from 'react';
import {
  View,
  Text,
  Animated,
} from 'react-native';
import styles from './styles';


const displayPromo = props => (
  <Animated.View style={styles.container} key={props.id}>
    <View style={styles.leftInfo}>
      <Text style={styles.leftInfoBig}>{props.name}</Text>
      <Text style={styles.leftInfoSmall}>{props.description}</Text>
    </View>
    <View style={styles.rightInfo}>
      <Text style={styles.rightInfoBig}>{props.progressText}</Text>
      <Text style={styles.rightInfoSmall}>{props.statusText}</Text>
    </View>
  </Animated.View>
);

export default displayPromo;
