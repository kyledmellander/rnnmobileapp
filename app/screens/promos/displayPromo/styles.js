import { colors, appStyles, commonStyles } from 'styles';
import { titeFontSize } from '../styles';

const smallText = {
  fontFamily: appStyles.fontFamilyLight,
  fontSize: 12,
  color: colors.textGray
};

export default {
  container: {
    ...commonStyles.borderedItemBottom,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  leftInfo: {
    flexDirection: 'column',
    justifyContent: 'center',
    flex: 2,
  },
  rightInfo: {
    flexDirection: 'column',
    flex: 1,
  },
  leftInfoSmall: {
    ...smallText,
    flex: 1,
  },
  leftInfoBig: {
    fontFamily: appStyles.fontFamily,
    fontSize: titeFontSize,
    flex: 1
  },
  rightInfoBig: {
    fontFamily: appStyles.fontFamily,
    alignSelf: 'flex-end',
    fontSize: titeFontSize,
    flex: 0
  },
  rightInfoSmall: {
    ...smallText,
    alignSelf: 'flex-end',
    flex: 0
  },
  fulfilledRequirements: {
    color: colors.successGreen,
  },
  redeemedColor: {
    color: colors.successGreen,
  },
};
