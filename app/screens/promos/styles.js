import { colors, appStyles, commonStyles } from 'styles';

export const titeFontSize = 16;

const styles = {
  titleContainer: {
    paddingTop: 10,
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderColor: colors.borderGray,
  },
  title: { ...commonStyles.sectionTitle },
  mainContainer: {
    flex: 1,
    width: '100%',
    paddingTop: appStyles.marginTop,
    justifyContent: 'flex-start'
  },
  buttonContainer: {
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: { ...commonStyles.bigButton },
  emptyContainer: {
    flex: 1,
    justifyContent: 'center'
  },
  emptyTextContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginLeft: appStyles.minorSpacing,
    marginRight: appStyles.minorSpacing
  },
  emptyText: {
    fontSize: 15,
    marginBottom: 10,
    justifyContent: 'center',
    fontFamily: appStyles.fontFamily,
    textAlign: 'center'
  },
};

export default styles;
