import { appStyles } from 'styles';

const styles = {
  container: {
    flex: 1,
    width: '100%',
    marginTop: appStyles.marginTop,
  }
};

export default styles;
