import React from 'react';
import { ActivityIndicator } from 'react-native';
import styles from './styles';

const loading = () => (
  <ActivityIndicator style={styles.container} />
);

export default loading;
