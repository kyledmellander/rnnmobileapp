import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Text,
  Animated,
  Easing,
} from 'react-native';
import { actions as promoActions } from 'appRedux/promos';
import { actions as redeemActions } from 'appRedux/redeem';
import { connect } from 'react-redux';
import loadingLib from 'lib/loading';
import * as navUtils from 'lib/navUtils';
import { CustomButton, Icons } from 'components';
import Loading from './loading';
import displayPromo from './displayPromo';
import styles from './styles';

const increaseOpacity = () => {
  const animatedValue = new Animated.Value(0);

  Animated.timing(animatedValue, {
    toValue: 1,
    duration: 1500,
    easing: Easing.in,
  }).start();
  return loadingLib.increaseOpacity(animatedValue);
};

const AddPromoButton = ({ onPress }) => (
  <CustomButton
    buttonContainerStyle={styles.buttonContainer}
    buttonText="Enter promo code"
    enabled
    onPress={onPress}
    testID="AddPromoButton"
  />
);

class credits extends Component {
  static options() {
    return {
      topBar: {
        buttonColor: 'black',
        title: {
          component: {
            name: 'NavBarTitle',
            passProps: {
              title: 'Promos',
            },
            alignment: 'center',
          },
        },
        leftButtons: [
          {
            id: 'hamburger',
            icon: Icons.hamburger,
            color: 'black',
          }
        ]
      }
    };
  }

  componentWillMount() {
    this.props.getInitData();
  }

  getPromos() {
    const contents = this.props.userPromos.map(ce => displayPromo(ce));
    return (
      <ScrollView contentContainerStyle={styles.activitiesContainer}>
        {contents}
      </ScrollView>
    );
  }

  renderNoPromos() {
    const { openRedeem } = this.props;

    return (
      <Animated.View style={[styles.emptyContainer, increaseOpacity()]}>
        <View style={styles.emptyTextContainer}>
          <Text style={styles.emptyText}>
            {'Oh no! You Haven\'t added any promo codes yet.'}
          </Text>
        </View>
        <AddPromoButton onPress={openRedeem} />
      </Animated.View>
    );
  }

  render() {
    if (this.props.loading) {
      return Loading();
    }
    if (!this.props.userPromos.length) {
      return this.renderNoPromos();
    }

    return (
      <Animated.View style={[styles.mainContainer, increaseOpacity()]}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>Your Promos</Text>
        </View>
        <View style={{ flex: 1 }}>
          {this.getPromos()}
        </View>
        <View>
          <AddPromoButton onPress={this.props.openRedeem} />
        </View>
      </Animated.View>
    );
  }
}

function mapStateToProps(state) {
  if (!state.promos) {
    return {};
  }
  return {
    userPromos: state.promos.userPromos,
    loading: state.promos.loading,
    error: state.promos.error,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    openRedeem: () => dispatch(redeemActions.openRedeemScreen(() => {
      dispatch(promoActions.getInitData()); // refresh data
      navUtils.setStackRoot('Promos');
    })),
    getInitData: () => {
      dispatch(promoActions.getInitData());
    },
    clearStatus: () => {
      dispatch(promoActions.clearStatus());
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(credits);
