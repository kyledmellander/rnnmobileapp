import { colors, appStyles } from '../../styles';

const styles = {
  container: {
    flex: 1,
    marginTop: '20%',
    width: '100%',
    paddingLeft: 20,
    paddingRight: 20,
  },
  text: {
    fontFamily: appStyles.fontFamily,
    fontSize: 14,
    color: colors.textGray,
  },
  contactButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 14,
    alignSelf: 'flex-start',
    flexWrap: 'wrap'
  },
  contactButton: {
    marginTop: 14,
    alignSelf: 'flex-start',
    width: 100,
    height: 41,
    marginRight: 20,
    justifyContent: 'center',
    borderColor: colors.tavourOrange,
    borderWidth: 1,
    borderRadius: 3,
  },
  contactButtonText: {
    color: colors.tavourOrange,
    fontSize: 12,
    fontFamily: appStyles.fontFamilySemibold,
    textAlign: 'center',
  },
  contactTextNoButton: {
    width: 130,
    height: 41,
    marginRight: 20,
    justifyContent: 'center',
  },
  bold: {
    fontFamily: appStyles.fontFamilySemibold,
  },
  orange: {
    color: colors.tavourOrange,
  },
  title: {
    color: 'black',
    fontFamily: appStyles.fontFamilySemibold,
    fontSize: 16,
  },
  errorText: {
    color: colors.failRed,
    fontSize: 16,
    paddingVertical: 15,
  },
};

export default styles;
