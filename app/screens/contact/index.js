import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Linking,
  ActivityIndicator,
} from 'react-native';
import HTMLView from 'components/CustomHtmlView';
import { connect } from 'react-redux';
import { Icons, Accordion } from 'components';
import { actions as contactUsActions } from 'appRedux/contactUs';
import styles from './styles';

function makeContactUsItemAccordian(titleString, content, key) {
  const title = (<Text style={styles.title}>{titleString}</Text>);
  return (
    <Accordion
      titleContent={title}
      content={content}
      angleStyle={{ right: 10 }}
      key={key}
    />
  );
}

function makeContactUsItem(contactUsItem) {
  const stylesheet = {
    p: styles.text,
    a: { ...styles.text, ...styles.orange },
    strong: { ...styles.text, ...styles.bold }
  };
  const content = (
    <HTMLView
      style={styles.text}
      stylesheet={stylesheet}
      value={contactUsItem.body}
      preview={false}
      onLinkPress={e => Linking.openURL(e)}
    />
  );
  return makeContactUsItemAccordian(contactUsItem.title, content, contactUsItem.id);
}

class contact extends Component {
  static options() {
    return {
      topBar: {
        buttonColor: 'black',
        title: {
          component: {
            name: 'NavBarTitle',
            passProps: {
              title: 'Contact Us',
            },
            alignment: 'center',
          },
        },
        leftButtons: [
          {
            id: 'hamburger',
            icon: Icons.hamburger,
            color: 'black',
          }
        ]
      }
    };
  }

  componentDidMount() {
    this.props.loadContactUs();
  }

  getPhoneButton() {
    const { phone } = this.props;
    const justNumber = phone.replace(/[\D]+/g, '');
    const telUrl = `tel:${justNumber}`;
    if (this.props.canOpenTel) {
      return (
        <TouchableOpacity
          style={styles.contactButton}
        >
          <Text
            style={styles.contactButtonText}
            onPress={async () => {
              await Linking.openURL(telUrl);
            }}
          >
            {' '}
            Call Support{' '}
          </Text>
        </TouchableOpacity>
      );
    }
    return (
      <Text style={[styles.contactButtonText, styles.contactTextNoButton]}>
        {' '}
          Phone: {phone}
      </Text>
    );
  }

  getEmailButton() {
    const { email } = this.props;
    const mailUrl = `mailto:${email}`;
    if (this.props.canOpenEmail) {
      return (
        <TouchableOpacity
          style={styles.contactButton}
        >
          <Text
            style={styles.contactButtonText}
            onPress={async () => {
              await Linking.openURL(mailUrl);
            }}
          >
            {' '}
            Email Support
          </Text>
        </TouchableOpacity>
      );
    }
    return (
      <Text style={[styles.contactButtonText, styles.contactTextNoButton]}>
        {' '}
          Email: {email}
      </Text>
    );
  }

  getMessageButton() {
    const { writeUsLink } = this.props;
    return (
      <TouchableOpacity
        style={styles.contactButton}
      >
        <Text
          style={styles.contactButtonText}
          onPress={async () => {
            await Linking.openURL(writeUsLink);
          }}
        >
          {' '}
          Write Us
        </Text>
      </TouchableOpacity>
    );
  }

  contactInfo() {
    const children = [
      (<Text style={styles.text}>{this.props.questionsForUsText}</Text>)
    ];
    const messageButton = this.getMessageButton();
    const callButton = this.getPhoneButton();
    const emailButton = this.getEmailButton();
    const buttons = (
      <View
        style={styles.contactButtonContainer}
        key="buttons"
      >
        {[messageButton, callButton, emailButton]}
      </View>
    );
    children.push(buttons);
    return makeContactUsItemAccordian(this.props.questionsForUsTitle, children, 'contact-info');
  }

  render() {
    if (this.props.isLoading) {
      return (<ActivityIndicator style={styles.container} />);
    }
    if (this.props.errorMessage) {
      return (<Text style={[styles.container, styles.errorText]}>{this.props.errorMessage}</Text>);
    }

    return (
      <ScrollView style={styles.container}>
        {this.contactInfo()}
        {this.props.contactUsItems.map(i => makeContactUsItem(i))}
      </ScrollView>
    );
  }
}

function mapStateToProps(state) {
  return {
    isLoading: state.contactUs.isLoading,
    errorMessage: state.contactUs.errorMessage,
    contactUsItems: state.contactUs.contactUsItems,
    questionsForUsText: state.contactUs.questionsForUsText,
    questionsForUsTitle: state.contactUs.questionsForUsTitle,
    phone: state.contactUs.phone,
    email: state.contactUs.email,
    writeUsLink: state.contactUs.writeUsLink,
    canOpenTel: state.contactUs.canOpenTel,
    canOpenEmail: state.contactUs.canOpenEmail,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    loadContactUs: () => {
      dispatch(contactUsActions.loadContactUs());
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(contact);
