import React, { Component } from 'react';
import { Animated, Easing, Image, View } from 'react-native';
import { connect } from 'react-redux';
import { actions, selectors } from 'appRedux';
import images from 'config/images';
import loadingLib from 'lib/loading';
import * as navUtils from 'lib/navUtils';

const { getUser, restoreSession } = actions;
const { userHasSessionToken, userNeedsState, userNeedsOnboardingFlow } = selectors;

// First screen the users sees when they open the app. Fires an event to determine logged in status
// If logged in =>
//   if from App Notification
//     => route to the proper notification screen
//   else
//     => route to home screen
// else => route to the SignUp / login section of the app
class AppLaunch extends Component {
  static options() {
    return {
      topBar: {
        visible: false,
        animate: false,
        drawBehind: true,
      }
    };
  }

  constructor(props) {
    console.log('App has launched successfully');
    super(props);
    this.state = {
      animatedValue: new Animated.Value(0),
    };
    this.animate = this.animate.bind(this);
    navUtils.setDefaultOptions();
  }

  componentDidMount() {
    console.log('restoring session');
    navUtils.setDefaultOptions();
    if (!this.routeUser()) {
      this.props.restoreSession();
    }
  }

  componentWillReceiveProps(nextProps) {
    console.log('componentWillReceiveProps');
    this.routeUser(nextProps);
  }

  animate() {
    const { animatedValue } = this.state;
    animatedValue.setValue(0);
    Animated.timing(animatedValue, {
      toValue: 1,
      duration: 1200,
      easing: Easing.sin.inOut,
    }).start();
  }

  routeUser(props = this.props) {
    const {
      hasSessionToken,
      needsState, // WA, KS, etc
      loading,
      showOnboarding,
    } = props;

    if (loading) {
      return false;
    }

    if (!hasSessionToken) {
      navUtils.setStackRoot('Landing');
    } else if (needsState) {
      navUtils.signUpFlow('EnterZip');
    } else if (showOnboarding) {
      navUtils.setStackRoot('Onboarding1');
    } else {
      navUtils.setStackRoot('Home');
    }
    return true;
  }

  render() {
    const { animatedValue } = this.state;
    const leftToRight = loadingLib.leftToRight(animatedValue);
    return (
      <View style={{ width: '100%', height: '100%' }}>
        <Image
          source={images.splash}
          style={{ width: '100%', height: '100%' }}
          resizeMode="cover"
        />
        <Animated.View
          style={[
            {
              height: '100%',
              width: 10,
              backgroundColor: 'white',
              opacity: 0.5,
            },
            leftToRight,
          ]}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const { user, fetchingUser, restoringSession } = state.user;
  const needsState = userNeedsState(state);
  const hasSessionToken = userHasSessionToken(state);
  const showOnboarding = userNeedsOnboardingFlow(state);

  const loading = fetchingUser || restoringSession;
  return {
    user,
    loading,
    hasSessionToken,
    needsState,
    showOnboarding,
  };
};

export default connect(mapStateToProps, { getUser, restoreSession })(AppLaunch);
