import React, { Component } from 'react';
import { View, Text, Keyboard, TextInput } from 'react-native';
import { connect } from 'react-redux';
import empty from 'is-empty';
import CustomButton from 'components/CustomButton';
import CustomKeyboardAvoidingView from 'components/CustomKeyboardAvoidingView';
import { actions as passwordResetActions, types as passwordResetTypes } from 'appRedux/resetPassword';
import * as navUtils from 'lib/navUtils';
import styles from './styles';

const ResetPasswordMessage = (props) => {
  const messageStyles = [styles.messageStyle];
  const { errorMessage } = props;
  let message;
  if (props.isWaiting) {
    message = 'Please wait...';
  } else if (errorMessage) {
    message = errorMessage;
    messageStyles.push(styles.messageStyleError);
  } else {
    message = props.successMessage;
  }
  return (
    <Text style={messageStyles} testID="PasswordResetMessage">
      {message}
    </Text>
  );
};

class login extends Component {
  static options() {
    return navUtils.getOptions('Forgot Password', { topBar: { drawBehind: true } });
  }

  constructor(props) {
    super(props);
    this.state = {
      emailPlaceHolder: 'Email Address',
      email: '',
    };
    this.emailChanged = this.emailChanged.bind(this);
    this.submit = this.submit.bind(this);
    this.enableButton = this.enableButton.bind(this);
  }

  componentDidMount() {
    this.props.setupScreen();
  }

  emailChanged(email) {
    this.setState({ email });
  }

  enableButton() {
    return !this.props.isWaiting && !empty(this.state.email);
  }

  submit() {
    Keyboard.dismiss();
    this.props.callResetPassword(this.state.email);
  }

  render() {
    return (
      <CustomKeyboardAvoidingView
        style={styles.container}
        behavior="height"
      >
        <View style={styles.titleContainer}>
          <Text style={styles.titleText}>Forgot Your Password</Text>
        </View>
        <View style={styles.bodyContainer}>
          <View style={styles.formField}>
            <TextInput
              underlineColorAndroid="transparent"
              style={styles.formText}
              placeholder={this.state.emailPlaceHolder}
              autoFocus
              autoCapitalize="none"
              keyboardType="email-address"
              onChangeText={this.emailChanged}
              onSubmitEditing={this.submit}
              testID="PasswordResetInput"
            />
          </View>
          <ResetPasswordMessage {...this.props.messages} isWaiting={this.props.isWaiting} />
        </View>
        <CustomButton
          buttonContainerStyle={styles.buttonContainer}
          onPress={this.submit}
          buttonText="Reset Password"
          enabled={this.enableButton()}
          buttonStyle={styles.submit}
          testID="SubmitPasswordReset"
        />
      </CustomKeyboardAvoidingView>
    );
  }
}

function mapStateToProps(state) {
  return {
    messages: state.resetPassword.messages,
    isWaiting: state.resetPassword.isWaiting
  };
}

function mapDispatchToProps(dispatch) {
  return {
    callResetPassword: (email) => {
      dispatch(passwordResetActions.resetPassword(email));
    },
    setupScreen: () => {
      dispatch({ type: passwordResetTypes.resetStore });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(login);

