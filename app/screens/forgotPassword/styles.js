import { colors, appStyles } from '../../styles';

const styles = {
  formText: {
    flex: 1,
    textAlign: 'center',
    borderWidth: 0,
    fontFamily: appStyles.fontFamily
  },
  formField: {
    height: 30,
    width: '100%',
    borderBottomColor: colors.borderGray,
    borderBottomWidth: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    marginTop: appStyles.marginTop
  },
  titleContainer: {
    flex: 0.25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonContainer: {
    flex: 0.3,
  },
  titleText: {
    flex: 0,
    color: colors.tavourOrange,
    fontFamily: appStyles.fontFamily,
    fontSize: 18,
  },
  submit: {
    justifyContent: 'center',
    alignItems: 'center',
    flexShrink: 0.2
  },
  messageStyle: {
    paddingTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'center'
  },
  messageStyleError: {
    color: colors.failRed
  },
  bodyContainer: {
    flex: 0.35,
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
};

export default styles;
