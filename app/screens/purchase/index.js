import React, { Component } from 'react';
import { ABSplit } from 'components';
import { selectors } from 'appRedux/experiments';
import * as navUtils from 'lib/navUtils';
import NewPurchase from './newPurchase';
import OldPurchase from './oldPurchase';

class ABPurchase extends Component {
  static options() {
    return navUtils.getOptions('Purchase');
  }

  render() {
    return (
      <ABSplit
        {...this.props}
        splitFunction={selectors.newPurchaseFlowSelector}
        NewComponent={NewPurchase}
        OldComponent={OldPurchase}
      />
    );
  }
}

export default (ABPurchase);
