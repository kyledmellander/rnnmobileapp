import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { actions } from 'appRedux/purchaseInfo';
import { connect } from 'react-redux';
import { colors } from 'styles';

const styles = {
  lineStyle: {
    height: 1,
    width: '95%',
    backgroundColor: colors.borderGray,
  },
  borderTop: {
    borderTopWidth: 1,
    borderColor: colors.borderGray,
  },
  row: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: 10,
    paddingHorizontal: '5%',
    paddingTop: 10,
  },
  container: {
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textStyle: {
    color: colors.textGray,
  }
};

class PurchaseInfo extends Component {
  static renderLoading() {
    return (
      <View style={styles.container}>
        <Text style={styles.textStyle}>
          Fetching tax info...
        </Text>
      </View>
    );
  }

  static valueText(text, color = colors.textGray, parenColor = colors.transparent) {
    return (
      <Text style={[styles.textStyle, { color }]}>
        <Text style={{ color: parenColor }}>(</Text>
        $ {text}
        <Text style={{ color: parenColor }}>)</Text>
      </Text>
    );
  }

  componentDidMount() {
    const {
      loading,
      needsRefresh,
      creditBalance,
      hasValidAddress,
      hasValidCreditCard,
      taxRate,
    } = this.props;

    if (!loading &&
      (needsRefresh
        || taxRate === undefined
        || creditBalance === undefined
        || !hasValidAddress
        || !hasValidCreditCard
      )
    ) {
      this.props.fetchPurchaseInfo();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.needsRefresh && nextProps.needsRefresh) {
      this.props.fetchPurchaseInfo();
    }
  }

  calcSubTotal() {
    const { selectedProducts, offerProducts } = this.props;
    let subTotal = 0;
    Object.values(offerProducts).forEach(({ id, unitPrice }) => {
      const productQuantity = selectedProducts[id] ? selectedProducts[id].orderQuantity : 0;
      const productValue = Number(unitPrice);

      if (productQuantity) {
        subTotal += productQuantity * productValue;
      }
    });

    return subTotal;
  }

  totalWithTaxLabel() {
    const { hasValidAddress } = this.props;
    return hasValidAddress ? 'Total with tax' : 'Total';
  }

  subtotalWithTax() {
    const { hasValidAddress, taxRate = 0 } = this.props;
    const subTotal = this.calcSubTotal();

    if (!hasValidAddress) return `${subTotal.toFixed(2)} + local taxes`;

    return (subTotal + (subTotal * taxRate)).toFixed(2);
  }

  totalWithTaxAndCredit() {
    const { creditBalance, hasValidAddress, taxRate = 0 } = this.props;
    let subTotal = this.calcSubTotal();
    subTotal -= creditBalance;
    if (subTotal < 0) subTotal = 0;

    if (!hasValidAddress) return `${subTotal.toFixed(2)} + local taxes`;

    return (subTotal + (subTotal * taxRate)).toFixed(2);
  }


  render() {
    const {
      loading,
      creditBalance = 0,
    } = this.props;
    if (loading) return PurchaseInfo.renderLoading();
    if (creditBalance !== 0) {
      return (
        <View style={styles.container}>
          <View style={[styles.row, { paddingBottom: 0 }]}>
            <Text style={styles.textStyle}>
              {this.totalWithTaxLabel()}
            </Text>
            {PurchaseInfo.valueText(this.subtotalWithTax())}
          </View>
          <View style={styles.row}>
            <Text style={[styles.textStyle, { color: colors.okGreen }]}>
              Credit
            </Text>
            {PurchaseInfo.valueText(creditBalance.toFixed(2), colors.okGreen, colors.okGreen)}
          </View>
          <View style={styles.lineStyle} />
          <View style={styles.row}>
            <Text style={styles.textStyle}>
              Total charged to card
            </Text>
            {PurchaseInfo.valueText(this.totalWithTaxAndCredit())}
          </View>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <View style={styles.row}>
          <Text>
            {this.totalWithTaxLabel()}
          </Text>
          {PurchaseInfo.valueText(this.totalWithTaxAndCredit())}
        </View>
      </View>
    );
  }
}

const mapStateToProps = ({ purchaseInfo }) => {
  const {
    creditBalance,
    hasValidAddress,
    hasValidCreditCard,
    loading,
    message,
    taxRate,
    needsRefresh,
  } = purchaseInfo;

  return {
    creditBalance,
    hasValidAddress,
    hasValidCreditCard,
    needsRefresh,
    loading,
    message,
    taxRate,
  };
};

export default connect(
  mapStateToProps,
  { fetchPurchaseInfo: actions.fetchPurchaseInfo }
)(PurchaseInfo);
