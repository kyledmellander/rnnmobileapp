import React, { Component } from 'react';
import { ABSplit } from 'components';
import { selectors } from 'appRedux/experiments';
import * as navUtils from 'lib/navUtils';
import NewFirstTimePurchase from './newFirstTimePurchase';
import OldFirstTimePurchase from './oldFirstTimePurchase';

class ABPurchase extends Component {
  static options() {
    return navUtils.getOptions('', { topBar: { noBorder: true } });
  }

  render() {
    return (
      <ABSplit
        {...this.props}
        splitFunction={selectors.newPurchaseFlowSelector}
        NewComponent={NewFirstTimePurchase}
        OldComponent={OldFirstTimePurchase}
      />
    );
  }
}

export default (ABPurchase);
