const styles = {
  container: {
    flex: 1,
  },
  scrollContainer: {
    flex: 1,
  },
  photoContainer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  photo: {
    width: '100%',
  },
  summaryContainer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  summaryRow: {
    width: '100%',
  },
  hr: {
    borderBottomWidth: 1,
    borderBottomColor: 'black',
  },
};

export default styles;
