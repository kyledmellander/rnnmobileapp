const addressForm = {
  promptTitle: 'Where should we send your beer?',
  promptSubTitle1: 'We recommend a business address,',
  submitText: 'Continue',
};

const paymentForm = {
  title: 'Add a credit card',
  subTitle:
    'We’ll give you a few minutes to go grab a credit card until we put the beer back on the shelf',
};

export default {
  addressForm,
  paymentForm,
};
