import React, { Component } from 'react';
import { SafeAreaView, View } from 'react-native';
import { connect } from 'react-redux';
import { actions as addressActions } from 'appRedux/addresses';
import { actions as paymentActions } from 'appRedux/paymentMethods';
import { actions as userActions } from 'appRedux/user';
import { actions as orderActions } from 'appRedux/orders';
import { actions as metricActions } from 'appRedux/metrics';
import utils from 'lib/utils';
import * as navUtils from 'lib/navUtils';
import AddressForm from 'screens/addresses/addUpdateAddress/AddressForm';
import PaymentForm from 'screens/paymentMethods/addCreditCard/PaymentForm';

import styles from './styles';
import config from './config';

class firstTimePurchaseFlow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      waiting: false,
      userStateCode: '',
      message: '',
      address: {},
      paymentMethod: {},
      needsAddress: props.needsAddress,
      needsPayment: props.needsPayment,
      orderFailed: false,
    };
    const functionsToBind = [
      'addressSubmit',
      'addressEntryForm',
      'paymentSubmit',
      'paymentEntryForm',
    ];
    functionsToBind.forEach((functionToBind) => {
      this[functionToBind] = this[functionToBind].bind(this);
    });
  }

  componentWillMount() {
    // const { addMetric } = this.props;
    // const onBack = () => {
    //   addMetric('appv4.ftpf.bounce.count', 1);
    //   Actions.pop();
    // };
    // Actions.refresh({ onBack });
    // this.props.addMetric('appv4.ftpf.enter.count', 1);
    this.props.getUserState();
  }

  componentWillReceiveProps(nextProps) {
    const newState = { ...this.state, waiting: false };

    if (nextProps.addrErrors) {
      Object.keys(nextProps.addrErrors).forEach((key) => {
        nextProps.addrErrors[key].forEach((error) => {
          newState.message += `${key} ${error}. `;
        });
      });
    }

    if (
      !newState.needsAddress &&
      !newState.needsPayment &&
      nextProps.orderErrors &&
      !this.state.orderFailed
    ) {
      newState.orderFailed = true;
      newState.orderPlaced = false;
      navUtils.pop();
    }

    if (nextProps.addedAddress) {
      if (!this.state.loggedAddress) {
        // this.props.addMetric('appv4.ftpf.addedaddress.count', 1);
        newState.loggedAddress = true;
      }
      newState.needsAddress = false;
    }

    if (nextProps.addedCard) {
      if (!this.state.loggedPayment) {
        // this.props.addMetric('appv4.ftpf.addedpayment.count', 1);
        newState.loggedPayment = true;
      }
      newState.needsPayment = false;
    }

    if (nextProps.userStateCode) {
      newState.userStateCode = nextProps.userStateCode;
    }

    if (
      !newState.needsAddress &&
      !newState.needsPayment &&
      !this.state.orderPlaced
    ) {
      newState.waiting = true;
      newState.orderPlaced = true;
      navUtils.pop();
    }

    this.setState(newState);
  }

  addressSubmit(address) {
    this.setState({ waiting: true, message: '' });
    this.props.addAddress(address);
  }

  addressEntryForm() {
    const submitText = this.state.needsPayment
      ? config.addressForm.submitText
      : 'Save Address';
    const buttonText = this.state.waiting ? 'Please wait...' : submitText;
    const promptSubTitle2 = `someone 21+ must sign for your ${utils.getBasketWord(true)}.`;
    return (
      <View style={styles.container}>
        <AddressForm
          submit={this.addressSubmit}
          buttonText={buttonText}
          message={this.state.message}
          userStateCode={this.state.userStateCode}
          waiting={this.state.waiting}
          promptTitle={config.addressForm.promptTitle}
          promptSubTitle1={config.addressForm.promptSubTitle1}
          promptSubTitle2={promptSubTitle2}
        />
      </View>
    );
  }

  paymentSubmit(paymentMethod) {
    this.setState({ waiting: true, message: '' });
    this.props.addPaymentMethod(paymentMethod);
  }

  paymentEntryForm() {
    const { message, waiting } = this.state;
    const { purchaseButtonText } = this.props;

    const buttonText = waiting ? 'Please wait...' : purchaseButtonText;
    return (
      <SafeAreaView style={styles.container}>
        <PaymentForm
          title={config.paymentForm.title}
          subTitle={config.paymentForm.subTitle}
          messages={message}
          buttonText={buttonText}
          submit={this.paymentSubmit}
          waiting={waiting}
        />
      </SafeAreaView>
    );
  }

  render() {
    if (this.state.needsAddress) {
      return this.addressEntryForm();
    }
    if (this.state.needsPayment) {
      return this.paymentEntryForm();
    }
    if (this.props.needsPayment) {
      return this.paymentEntryForm();
    }
    if (this.props.needsAddress) {
      return this.addressEntryForm();
    }
    return null;
  }
}

function mapStateToProps(state) {
  if (!state.orders) {
    return {};
  }
  return {
    addrErrors: state.addresses.errors,
    payErrors: state.paymentMethods.errors,
    orderErrors: state.orders.errors,
    order: state.orders.order,
    chargedOrder: state.orders.chargedOrder,
    userStateCode: state.user.userStateCode,
    addedAddress: state.addresses.addedAddress,
    updatedAddress: state.addresses.updatedAddress,
    addedCard: state.paymentMethods.addedCard,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    addAddress: address => dispatch(addressActions.addAddress(address)),
    updateAddress: address => dispatch(addressActions.updateAddress(address)),
    addPaymentMethod: paymentMethod =>
      dispatch(paymentActions.addPaymentMethod(paymentMethod)),
    createOrder: order => dispatch(orderActions.createOrder(order)),
    getUserState: () => dispatch(userActions.getUserState()),
    addMetric: (...args) => dispatch(metricActions.addMetric(...args)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(firstTimePurchaseFlow);
