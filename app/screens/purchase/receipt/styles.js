import { colors, appStyles } from 'styles';

const styles = {
  container: {
    flex: 1,
    marginTop: '10%',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  bold: {
    fontWeight: '500',
  },
  titleContainer: {
    marginTop: 20,
    width: '90%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 22,
    color: '#000000',
    fontFamily: appStyles.fontFamily,
    paddingBottom: 8,
  },
  subTitle: {
    color: colors.tavourDarkGray,
    fontFamily: appStyles.fontFamily,
    textAlign: 'center',
    fontSize: 18,
  },
  subTitleLink: {
    color: colors.tavourOrange,
    fontFamily: appStyles.fontFamily,
    fontWeight: '500',
  },
  photoContainer: {
    width: '100%',
    height: 120,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: '10%',
    marginVertical: 20,
  },
  photo: {
    flex: 1,
    resizeMode: 'contain',
  },
  summaryContainer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  summaryRow: {
    width: '90%',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingVertical: 10,
  },
  summaryRowJustWidth: {
    width: '90%',
  },
  summaryRowAllOtherThenWidth: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingVertical: 10,
  },
  hr: {
    borderTopWidth: 1,
    borderTopColor: colors.tavourLightGray,
  },
  summary: {
    color: colors.tavourDarkGray,
    textAlign: 'center',
  },
  summaryLeft: {
    color: colors.tavourDarkGray,
    width: '65%',
  },
  summaryRight: {
    color: colors.tavourDarkGray,
    width: '20%',
  },
  confirmButton: {
    width: '100%',
    height: 60,
    bottom: 0,
  },
  confirmButtonText: {
    fontSize: 20,
    fontFamily: appStyles.fontFamily,
  },
};

export default styles;
