import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  SafeAreaView,
} from 'react-native';
import { icons } from 'images';
import { connect } from 'react-redux';
import CustomButton from 'components/CustomButton';
import { actions as appRatingActions } from 'appRedux/appStoreRating';
import utils from 'lib/utils';
import * as navUtils from 'lib/navUtils';
import styles from './styles';

function getCrateLink() {
  const onPress = () => navUtils.setStackRoot('MyBasket');
  return (
    <Text style={styles.subTitleLink} onPress={onPress}>
      {' '}
      {`your ${utils.getBasketWord(true)}`}{' '}
    </Text>
  );
}

function getCratePageLink() {
  const onPress = () => navUtils.setStackRoot('ShippedCrates');
  return (
    <Text style={styles.subTitleLink} onPress={onPress}>
      {`shipped ${utils.getBasketWord(true)}s page`}
    </Text>
  );
}

function getButton() {
  return (
    <CustomButton
      buttonContainerStyle={styles.confirmButton}
      buttonTextStyle={styles.confirmButtonText}
      buttonText="Back to Featured Beers"
      enabled
      onPress={() => navUtils.setStackRoot('Home')}
      testID="ReceiptContinue"
    />);
}


class receipt extends Component {
  constructor(props) {
    super(props);
    this.state = {
      units: 0,
      packageContainer: '',
    };
    this.getBeerSummaries = this.getBeerSummaries.bind(this);
    this.getWaitlistSummaries = this.getWaitlistSummaries.bind(this);
    this.getSummaryDiscount = this.getSummaryDiscount.bind(this);
    this.getTaxSummary = this.getTaxSummary.bind(this);
    this.getTotalSummary = this.getTotalSummary.bind(this);
    this.getTotalUnits = this.getTotalUnits.bind(this);
    this.getPackageContainer = this.getPackageContainer.bind(this);
    this.areIs = this.areIs.bind(this);
    this.thisThese = this.thisThese.bind(this);
    this.getShipDate = this.getShipDate.bind(this);
    this.isOrderProductBundle = this.isOrderProductBundle.bind(this);
    this.subtitle = this.subtitle.bind(this);
    this.titleContainer = this.titleContainer.bind(this);
    this.containerString = this.containerString.bind(this);
  }

  componentWillMount() {
    const units = this.getTotalUnits();
    const packageContainer = this.getPackageContainer();
    this.setState({
      units,
      packageContainer,
    });
    this.props.fetchAppStoreRating();
  }


  getBasketVerb() {
    return this.props.order.basket.markedFor === 'Pickup'
      ? 'for pickup'
      : 'to ship';
  }

  getShipDate() {
    const onPress = () => navUtils.setStackRoot('MyBasket');
    return (
      <Text style={styles.subTitleLink} onPress={onPress}>
        {' '}
        {this.props.order.basket.shippedAt}{' '}
      </Text>
    );
  }

  getTotalUnits() {
    if (this.isOrderProductBundle()) {
      return 1;
    }
    let units = 0;
    this.props.order.orderProducts.forEach((orderProduct) => {
      units += orderProduct.unitQuantity;
    });
    return units;
  }

  getPackageContainer() {
    if (this.isOrderProductBundle()) {
      return 'curated box';
    }
    const { packageContainer } = this.props.order.orderProducts.pop();
    return this.getTotalUnits() > 1 ? `${packageContainer}s` : packageContainer;
  }

  getBeerSummaries() {
    let isFirst = true;
    const summaries = this.props.order.orderProducts.map((orderProduct) => {
      const units = orderProduct.unitQuantity;
      const { name, price } = orderProduct;
      let style = [styles.summaryRowAllOtherThenWidth, styles.hr];
      if (isFirst) {
        isFirst = false;
        style = [styles.summaryRowAllOtherThenWidth];
      }
      return (
        <View style={style}>
          <Text style={styles.summaryLeft}>
            {' '}
            {units} {name}{' '}
          </Text>
          <Text style={styles.summaryRight}> {`$${price}`} </Text>
        </View>
      );
    });
    return React.createElement(
      View,
      { style: styles.summaryRowJustWidth },
      ...summaries
    );
  }

  getWaitlistSummaries() {
    let isFirst = true;
    const summaries = this.props.order.orderProducts.map((orderProduct) => {
      const { unitQuantity: units, name } = orderProduct;
      let style = [styles.summaryRowAllOtherThenWidth, styles.hr];
      if (isFirst) {
        isFirst = false;
        style = [styles.summaryRowAllOtherThenWidth];
      }
      return (
        <View style={style}>
          <Text style={styles.summaryLeft}>
            {' '}
            {units} {name}{' '}
          </Text>
        </View>
      );
    });
    return React.createElement(
      View,
      { style: styles.summaryRowJustWidth },
      ...summaries
    );
  }

  // TODO after discount is working, use util.formatMoney on this.props.order.discount
  getSummaryDiscount() {
    if (!this.props.order.discount || this.props.order.discount !== '0.0') {
      return (
        <View style={[styles.summaryRow, styles.hr]}>
          <Text style={styles.summaryLeft}>{'discount'} </Text>
          <Text style={styles.summaryRight}>
            {' '}
            {`$${this.props.order.discount}`}{' '}
          </Text>
        </View>
      );
    }
    return null;
  }

  getTaxSummary() {
    if (this.props.order.tax && this.props.order.tax !== '0.0') {
      const formattedTax = utils.formatMoney(this.props.order.tax);
      return (
        <View style={[styles.summaryRow, styles.hr]}>
          <Text style={styles.summaryLeft}>{'Tax'} </Text>
          <Text style={styles.summaryRight}>
            {' '}
            {`$${formattedTax}`}{' '}
          </Text>
        </View>
      );
    }
    return null;
  }

  getTotalSummary() {
    if (this.props.order.tax && this.props.order.tax !== '0.0') {
      const formattedTotal = this.props.order.price;
      return (
        <View style={[styles.summaryRow, styles.hr]}>
          <Text style={styles.summaryLeft}>{'Total Charged'} </Text>
          <Text style={styles.summaryRight}>
            {' '}
            {`$${formattedTotal}`}{' '}
          </Text>
        </View>
      );
    }
    return null;
  }

  isOrderProductBundle() {
    return this.props.order.orderProductBundle;
  }

  areIs() {
    return this.state.units > 1 ? 'are' : 'is';
  }

  thisThese() {
    return this.state.units > 1 ? 'these' : 'this';
  }

  subtitle() {
    const container = this.containerString();
    if (this.isOrderProductBundle()) {
      return (
        <Text style={styles.subTitle}>
          This box can be found on the {getCratePageLink()} and will ship as soon as possible
        </Text>
      );
    }
    return (
      <Text style={styles.subTitle}>
        {`Your ${container} ${this.areIs()} now in `}
        {getCrateLink()}
        {`which is scheduled ${this.getBasketVerb()} on `}{this.getShipDate()}
      </Text>
    );
  }

  containerString() {
    // this solution will not work if we introduce containers that can't be
    // pluralized by adding just an 's'
    const plural = this.state.units === 1 ? '' : 's';
    return `${this.state.packageContainer}${plural}`;
  }

  titleContainer() {
    if (this.isOrderProductBundle()) {
      const { name } = this.props.order.orderProductBundle;
      return (
        <View style={styles.titleContainer}>
          <Text style={styles.title}>
            {`We Just Set Aside a ${name}`}
          </Text>
          {this.subtitle()}
        </View>
      );
    }
    const container = this.containerString();
    return (
      <View style={styles.titleContainer}>
        <Text style={styles.title}>
          {`We Just Stashed ${this.state.units} ${container} for you`}
        </Text>
        {this.subtitle()}
      </View>
    );
  }

  render() {
    const { order } = this.props;
    if (order) {
      if (order.state === 'charged') {
        return (
          <SafeAreaView style={{ flex: 1 }}>
            <View style={[styles.container]}>
              {this.titleContainer()}
              <View style={styles.photoContainer}>
                <Image style={styles.photo} source={icons.crate} />
              </View>
              <View style={styles.summaryContainer}>
                {this.getBeerSummaries()}
                {this.getSummaryDiscount()}
                {this.getTaxSummary()}
                {this.getTotalSummary()}
              </View>
            </View>
            {getButton()}
          </SafeAreaView>
        );
      }

      if (order.state === 'waitlisted') {
        return (
          <SafeAreaView style={{ flex: 1 }}>
            <View style={[styles.container]}>
              <View style={styles.titleContainer}>
                <Text style={styles.title}> {'You\'re on the Waitlist!'} </Text>
                <Text style={styles.subTitle}>
                  {`If we get more ${this.thisThese()} ${
                    this.state.packageContainer
                  } ${this.areIs()} yours!`}
                </Text>
              </View>
              <View style={styles.photoContainer}>
                <Image style={styles.photo} source={icons.crate} />
              </View>
              <View style={styles.summaryContainer}>
                {this.getWaitlistSummaries()}
              </View>
            </View>
            {getButton()}
          </SafeAreaView>
        );
      }
    } else {
      return <Text>Order is not defined</Text>; // todo: generic err? Loading?
    }
    return null;
  }
}

function mapDispatchToProps(dispatch) {
  return {
    fetchAppStoreRating() {
      dispatch(appRatingActions.fetchAppStoreRating());
    },
  };
}
export default connect(null, mapDispatchToProps)(receipt);
