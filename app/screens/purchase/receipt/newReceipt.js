import React, { Component } from 'react';
import { View, Text, Image, SafeAreaView } from 'react-native';
import { icons } from 'images';
import { connect } from 'react-redux';
import moment from 'moment';
import CustomButton from 'components/CustomButton';
import utils, { emDash } from 'lib/utils';
import * as navUtils from 'lib/navUtils';
import { actions as appRatingActions } from 'appRedux/appStoreRating';
import styles from './styles';

const isInCurrentCycle = daysRemaining => (daysRemaining < 13);

function getCratePageLink(text) {
  const onPress = () => navUtils.setStackRoot('MyBasket');
  return (
    <Text style={styles.subTitleLink} onPress={onPress}>
      {text}
    </Text>
  );
}

function getButton() {
  return (
    <CustomButton
      buttonContainerStyle={styles.confirmButton}
      buttonText="Back to Featured Beers"
      enabled
      onPress={() => navUtils.setStackRoot('Home')}
      testID="ReceiptContinue"
    />);
}


class receipt extends Component {
  constructor(props) {
    super(props);
    this.state = {
      units: 0,
      packageContainer: '',
    };
    this.getBeerSummaries = this.getBeerSummaries.bind(this);
    this.getWaitlistSummaries = this.getWaitlistSummaries.bind(this);
    this.getSummaryDiscount = this.getSummaryDiscount.bind(this);
    this.getTaxSummary = this.getTaxSummary.bind(this);
    this.getTotalSummary = this.getTotalSummary.bind(this);
    this.getTotalUnits = this.getTotalUnits.bind(this);
    this.getPackageContainer = this.getPackageContainer.bind(this);
    this.areIs = this.areIs.bind(this);
    this.thisThese = this.thisThese.bind(this);
    this.isOrderProductBundle = this.isOrderProductBundle.bind(this);
    this.subtitle = this.subtitle.bind(this);
    this.titleContainer = this.titleContainer.bind(this);
    this.containerString = this.containerString.bind(this);
  }

  componentWillMount() {
    const units = this.getTotalUnits();
    const packageContainer = this.getPackageContainer();

    this.setState({
      units, // Number of beers purchased
      packageContainer, // 'Curated box' or 'bottles' or 'cans'
    });
    this.props.fetchAppStoreRating();
  }

  getIsPickup() {
    return this.props.order.basket.markedFor === 'Pickup';
  }

  getBasketAdjustDate(capitalize) {
    const adjust = capitalize ? 'Adjust' : 'adjust';
    if (this.getIsPickup()) {
      return getCratePageLink(`${adjust} your pickup date.`);
    }
    return getCratePageLink(`${adjust} your shipment date.`);
  }

  getTotalUnits() {
    if (this.isOrderProductBundle()) {
      return 1;
    }
    let units = 0;
    this.props.order.orderProducts.forEach((orderProduct) => {
      units += orderProduct.unitQuantity;
    });
    return units;
  }

  getPackageContainer() {
    if (this.isOrderProductBundle()) {
      return 'curated box';
    }
    const { packageContainer } = this.props.order.orderProducts.pop();
    return this.getTotalUnits() > 1 ? `${packageContainer}s` : packageContainer;
  }

  getShippingExplanation() {
    if (this.getIsPickup()) return null;
    if (this.isOrderProductBundle()) {
      return (
        <Text style={[styles.summary, { marginTop: 15 }]}>
          We would recommend getting your bundle ASAP,
          but you can still {this.getBasketAdjustDate()}
          (beer freshness permitting of course)
        </Text>
      );
    }

    return (
      <Text style={[styles.summary, { marginTop: 15 }]}>
        Remember, you can ship <Text style={{ fontStyle: 'italic' }}>any</Text> amount of beer for a flat rate {emDash} add
        more to your {utils.getBasketWord(true)}!
      </Text>
    );
  }

  getWeeksUntilShip() {
    const basketDate = moment(this.props.order.basket.shippedAt);
    const todaysDate = moment().hour(0).minute(0).second(0);
    const duration = moment.duration(basketDate.diff(todaysDate));

    return Math.round(duration.asWeeks());
  }

  getDaysUntilShip() {
    const basketDate = moment(this.props.order.basket.shippedAt);
    const todaysDate = moment().hour(0).minute(0).second(0);
    const duration = moment.duration(basketDate.diff(todaysDate));

    return Math.round(duration.asDays());
  }

  getHumanDuration() {
    const days = this.getDaysUntilShip();
    if (days === 0) return 'today';
    if (days === 1) return 'tomorrow';

    // If > 12 days prefer to describe using weeks
    if (days <= 12) return `in ${days} days`;

    const weeks = this.getWeeksUntilShip();

    return `in ${weeks} weeks`;
  }

  getBasketSummary() {
    const basketDate = moment(this.props.order.basket.shippedAt);

    if (this.isOrderProductBundle()) {
      return (
        <View style={{ width: '100%', paddingHorizontal: '10%' }}>
          <Text style={[styles.summary, { fontSize: 18 }]}>
            Your entire {utils.getBasketWord(true)} will now ship {this.getHumanDuration()}, on {getCratePageLink(basketDate.format('MMM DD'))}.
          </Text>
          {this.getShippingExplanation()}
        </View>
      );
    }
    return (
      <View style={{ width: '100%', paddingHorizontal: '10%' }}>
        <Text style={[styles.summary, { fontSize: 18 }]}>
          Your {utils.getBasketWord(true)} {this.willShipWillPickup()} {this.getHumanDuration()}, on {getCratePageLink(basketDate.format('MMM DD'))}.
        </Text>
        {this.soonerTooSoon()}
        {this.getShippingExplanation()}
      </View>
    );
  }

  getBeerSummaries() {
    let isFirst = true;
    const summaries = this.props.order.orderProducts.map((orderProduct) => {
      const units = orderProduct.unitQuantity;
      const { name, price } = orderProduct;
      let style = [styles.summaryRowAllOtherThenWidth, styles.hr];
      if (isFirst) {
        isFirst = false;
        style = [styles.summaryRowAllOtherThenWidth];
      }
      return (
        <View style={style}>
          <Text style={styles.summaryLeft}>
            {' '}
            {units} {name}{' '}
          </Text>
          <Text style={styles.summaryRight}> {`$${price}`} </Text>
        </View>
      );
    });
    return React.createElement(
      View,
      { style: styles.summaryRowJustWidth },
      ...summaries
    );
  }

  getWaitlistSummaries() {
    let isFirst = true;
    const summaries = this.props.order.orderProducts.map((orderProduct) => {
      const { unitQuantity: units, name } = orderProduct;
      let style = [styles.summaryRowAllOtherThenWidth, styles.hr];
      if (isFirst) {
        isFirst = false;
        style = [styles.summaryRowAllOtherThenWidth];
      }
      return (
        <View style={style}>
          <Text style={styles.summaryLeft}>
            {' '}
            {units} {name}{' '}
          </Text>
        </View>
      );
    });
    return React.createElement(
      View,
      { style: styles.summaryRowJustWidth },
      ...summaries
    );
  }

  // TODO after discount is working, use util.formatMoney on this.props.order.discount
  getSummaryDiscount() {
    if (!this.props.order.discount || this.props.order.discount !== '0.0') {
      return (
        <View style={[styles.summaryRow, styles.hr]}>
          <Text style={styles.summaryLeft}>{'discount'} </Text>
          <Text style={styles.summaryRight}>
            {' '}
            {`$${this.props.order.discount}`}{' '}
          </Text>
        </View>
      );
    }
    return null;
  }

  getTaxSummary() {
    if (this.props.order.tax && this.props.order.tax !== '0.0') {
      const formattedTax = utils.formatMoney(this.props.order.tax);
      return (
        <View style={[styles.summaryRow, styles.hr]}>
          <Text style={styles.summaryLeft}>{'Tax'} </Text>
          <Text style={styles.summaryRight}>
            {' '}
            {`$${formattedTax}`}{' '}
          </Text>
        </View>
      );
    }
    return null;
  }

  getTotalSummary() {
    if (this.props.order.tax && this.props.order.tax !== '0.0') {
      const formattedTotal = this.props.order.price;
      return (
        <View style={[styles.summaryRow, styles.hr]}>
          <Text style={styles.summaryLeft}>{'Total Charged'} </Text>
          <Text style={styles.summaryRight}>
            {' '}
            {`$${formattedTotal}`}{' '}
          </Text>
        </View>
      );
    }
    return null;
  }

  soonerTooSoon() {
    const days = this.getDaysUntilShip();
    let firstSentence = 'Want it sooner? You can always';
    let capitalize = false;

    if (isInCurrentCycle(days)) {
      firstSentence = 'Too soon?';
      capitalize = true;
    }

    return (
      <Text style={[styles.summary, { marginTop: 15 }]}>
        {firstSentence} {this.getBasketAdjustDate(capitalize)}
      </Text>
    );
  }


  willShipWillPickup() {
    return this.props.order.basket.markedFor === 'Pickup'
      ? 'will be available for pickup'
      : 'will ship';
  }

  isOrderProductBundle() {
    return this.props.order.orderProductBundle;
  }

  areIs() {
    return this.state.units > 1 ? 'are' : 'is';
  }

  haveHas() {
    return this.state.units > 1 ? 'have' : 'has';
  }

  thisThese() {
    return this.state.units > 1 ? 'these' : 'this';
  }

  subtitle() {
    if (this.isOrderProductBundle()) {
      const { name } = this.props.order.orderProductBundle;
      return (
        <Text style={styles.subTitle}>
          Thanks for purchasing the {name} {emDash} shipping is now
          <Text style={styles.bold}> on the house!</Text>
        </Text>
      );
    }

    return (
      <Text style={styles.subTitle}>
        {`${this.state.units} ${this.containerString()} ${this.haveHas()} `}
        {`been added to your ${utils.getBasketWord(true)}`}
      </Text>
    );
  }

  containerString() {
    // this solution will not work if we introduce containers that can't be
    // pluralized by adding just an 's'
    const plural = this.state.units === 1 ? '' : 's';
    return `${this.state.packageContainer}${plural}`;
  }

  titleContainer() {
    return (
      <View style={styles.titleContainer}>
        <Text style={styles.title}>
          Success!
        </Text>
        {this.subtitle()}
      </View>
    );
  }

  render() {
    const { order } = this.props;
    if (order) {
      if (order.state === 'charged') {
        return (
          <SafeAreaView style={{ flex: 1 }}>
            <View style={[styles.container, { flex: 1 }]}>
              {this.titleContainer()}
              <View style={styles.photoContainer}>
                <Image source={icons.crate} style={styles.photo} />
              </View>
              <View style={{ paddingHorizontal: 10 }}>
                {this.getBasketSummary()}
              </View>
            </View>
            {getButton()}
          </SafeAreaView>
        );
      }

      if (order.state === 'waitlisted') {
        return (
          <SafeAreaView style={{ flex: 1 }}>
            <View style={styles.container}>
              <View style={styles.titleContainer}>
                <Text style={styles.title}> {'You\'re on the Waitlist!'} </Text>
                <Text style={styles.subTitle}>
                  {`If we get more ${this.thisThese()} ${
                    this.state.packageContainer
                  } ${this.areIs()} yours!`}
                </Text>
              </View>
              <View style={styles.photoContainer}>
                <Image style={styles.photo} source={icons.crate} />
              </View>
              <View style={styles.summaryContainer}>
                {this.getWaitlistSummaries()}
              </View>
            </View>
            {getButton()}
          </SafeAreaView>
        );
      }
    } else {
      return <Text>Something went wrong. Try again later.</Text>;
    }
    return null;
  }
}

function mapStateToProps(state) {
  return {
    basket: state.baskets.openBasket,
    shippingCycles: state.baskets.openShippingCycles,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchAppStoreRating() {
      dispatch(appRatingActions.fetchAppStoreRating());
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(receipt);
