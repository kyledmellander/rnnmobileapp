import React, { Component } from 'react';
import { ABSplit } from 'components';
import { selectors } from 'appRedux/experiments';
import NewReceipt from './newReceipt';
import OldReceipt from './oldReceipt';


class ABPurchase extends Component {
  static options() {
    return {
      topBar: {
        visible: false,
      }
    };
  }

  render() {
    return (
      <ABSplit
        {...this.props}
        splitFunction={selectors.newPurchaseFlowSelector}
        NewComponent={NewReceipt}
        OldComponent={OldReceipt}
      />
    );
  }
}

export default (ABPurchase);
