import { colors, appStyles } from '../../styles';

const styles = {
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  title: {
    alignSelf: 'center',
    marginTop: 30,
    marginBottom: 20,
    fontSize: 19,
    fontFamily: appStyles.fontFamilySemibold,
    textAlign: 'center',
    paddingLeft: 10,
    paddingRight: 10,
  },
  offerProductsScrollView: {
    flex: 0.76,
    width: '100%',
  },
  offerProductsContainer: {
    paddingHorizontal: '5%',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  newErrorText: {
    textAlign: 'center',
    color: colors.failRed,
    marginBottom: 10,
    fontSize: 13,
    fontFamily: appStyles.fontFamily,
  },
  errorText: {
    position: 'absolute',
    bottom: 80,
    color: colors.failRed,
    fontSize: 13,
    fontFamily: appStyles.fontFamily,
  },
  confirmButton: {
    width: '100%',
  },
  confirmButtonText: {
    fontSize: 18,
    fontFamily: appStyles.fontFamily,
  },
  loading: {
    flex: 1,
    justifyContent: 'center',
    marginTop: appStyles.marginTop,
  },
};

export default styles;
