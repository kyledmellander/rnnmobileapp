import { colors, appStyles } from 'styles';

const styles = {
  productNameText: {
    fontSize: 14,
    paddingBottom: 2,
    fontFamily: appStyles.fontFamily,
  },
  packageText: {
    fontSize: 12,
    color: '#777777',
    fontFamily: appStyles.fontFamily,
  },
  productContainer: {
    maxHeight: 120,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    marginBottom: 30,
  },
  productDataContainer: {
    paddingBottom: 10,
  },
  productPackageContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '95%',
  },
  productButtonContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  productButton: {
    backgroundColor: colors.buttonGray,
    width: 37,
    height: 41,
    borderRadius: 3,
    justifyContent: 'center',
    marginRight: 14,
    marginBottom: 14,
  },
  productButtonSelected: {
    backgroundColor: colors.tavourOrange,
  },
  productButtonText: {
    fontSize: 12,
    textAlign: 'center',
    fontFamily: appStyles.fontFamily,
  },
  productButtonSelectedText: {
    color: 'white',
    fontFamily: appStyles.fontFamilyBold,
  },
};

export default styles;
