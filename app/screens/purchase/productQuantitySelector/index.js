import React from 'react';
import { View, Text, TouchableOpacity, ActivityIndicator } from 'react-native';
import get from 'lodash/get';
import utils from 'lib/utils';
import styles from './styles';

function getPreviousOrderQuantity(hasContainer, previousOrder, offerProduct) {
  if (!previousOrder) return 0;
  return hasContainer
    ? previousOrder.quantity
    : previousOrder.quantity * offerProduct.package.units;
}

function getMaxQuantity(
  hasContainer,
  hasPreviousOrder,
  previousOrderQuantity,
  offerProduct
) {
  const maxOrder = hasContainer
    ? offerProduct.maxOrder
    : offerProduct.maxOrder * offerProduct.package.units;
  return hasPreviousOrder ? Math.max(maxOrder - previousOrderQuantity, 0) : maxOrder;
}

function getUnitStep(offerProduct) {
  return offerProduct.unitStep
    ? offerProduct.unitStep
    : offerProduct.package.units;
}

export default class ProductQuantitySelector extends React.Component {
  constructor(props) {
    super(props);
    this.selectButton = this.selectButton.bind(this);
    const state = {
      index: props.showZero ? 0 : 1,
    };
    if (props.offerProduct) {
      const { offerProduct } = props;
      const hasContainer = get(offerProduct, ['product', 'container']);
      const hasPreviousOrder = typeof props.previousOrder !== 'undefined';
      const previousOrderQuantity = getPreviousOrderQuantity(
        hasContainer,
        props.previousOrder,
        offerProduct
      );
      const maxQuantity = getMaxQuantity(
        hasContainer,
        hasPreviousOrder,
        previousOrderQuantity,
        offerProduct
      );

      state.hasPreviousOrder = hasPreviousOrder;
      state.previousOrderQuantity = previousOrderQuantity;
      state.maxQuantity = maxQuantity;
      state.hasContainer = hasContainer;
    }
    this.state = state;
  }

  componentWillMount() {
    if (!this.props.showZero && this.state.maxQuantity >= 1) {
      const unitStep = getUnitStep(this.props.offerProduct);
      this.selectButton(1, unitStep);
    }
  }

  getCurrentQuantityText() {
    if (this.props.loading) {
      return <ActivityIndicator style={{ width: 10, height: 10 }} />;
    }
    if (this.state.hasPreviousOrder) {
      return `You have ${
        this.state.previousOrderQuantity
      } in your ${utils.getBasketWord(true)}`;
    }
    return '';
  }

  getLimitText() {
    if (this.props.loading) {
      return <ActivityIndicator style={{ paddingBottom: '5%' }} />;
    }

    let text = '';
    if (this.props.previousOrder) {
      text =
        this.state.maxQuantity > 0
          ? `Limit ${this.state.maxQuantity} more`
          : "Woah, hey now! You've bought the maximum amount, we gotta save the rest for the other Tavour peeps!";
    }
    return React.createElement(
      Text,
      { style: [styles.packageText, { paddingBottom: '5%' }] },
      text
    );
  }

  getQuantitySelector(offerProduct) {
    const { maxQuantity: max } = this.state;
    const price = this.state.hasContainer
      ? offerProduct.unitPrice * this.state.unitCount
      : offerProduct.unitPrice *
        (this.state.unitCount / offerProduct.package.units);
    const unitStep = getUnitStep(offerProduct);
    const productName = offerProduct.name || offerProduct.product.name;
    const parsePackageName = name => name.replace(/^\d x /, '');
    const packageName = this.state.hasContainer
      ? offerProduct.product.container.name
      : parsePackageName(offerProduct.package.name);
    const buttons = this.getButtons(max, unitStep);
    const limitText = this.getLimitText(offerProduct);
    const buttonContainerChildren = [...buttons, limitText];
    // eslint-disable-next-line no-unused-vars
    const buttonContainer = React.createElement(
      View,
      { style: styles.productButtonContainer },
      ...buttonContainerChildren
    );
    return (
      <View style={[styles.productContainer, this.props.style]}>
        <View style={styles.productDataContainer}>
          <Text style={styles.productNameText}>{productName}</Text>
          <View style={styles.productPackageContainer}>
            <Text style={styles.packageText}>{packageName}</Text>
            <Text style={styles.packageText}>
              {price ? ` for $${price}` : ''}
            </Text>
            <Text style={styles.packageText}>
              {this.getCurrentQuantityText()}
            </Text>
          </View>
        </View>
        {buttonContainer}
      </View>
    );
  }

  getButtons(maxUnits, step = 1) {
    const num = Math.floor(maxUnits / step);
    const buttons = Array(num + 1)
      .fill('')
      .map((curVal, n) => {
        if (!this.props.showZero && n === 0) {
          return null;
        }
        const buttonStyle = [styles.productButton];
        const textStyle = [styles.productButtonText];
        const unitCount = n * step;
        if (this.state.index === n) {
          buttonStyle.push(styles.productButtonSelected);
          textStyle.push(styles.productButtonSelectedText);
        }
        return (
          <TouchableOpacity
            ref={n}
            style={buttonStyle}
            testID={`ProductQuantity${n}`}
            onPress={() => this.selectButton(n, unitCount)}
          >
            <Text style={textStyle}>{unitCount}</Text>
          </TouchableOpacity>
        );
      });

    return buttons;
  }

  selectButton(index, unitCount) {
    const newState = {
      ...this.state,
      index,
      unitCount,
    };
    if (this.props.onSelected) {
      this.props.onSelected(unitCount);
    }

    this.setState(newState);
  }

  render() {
    if (!this.props.offerProduct) return null;
    return this.getQuantitySelector(this.props.offerProduct);
  }
}
