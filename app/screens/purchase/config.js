const errors = {
  generalError: 'Unable to create order, try again!',
  paymentFailed: 'Payment failed to charge, check credentials and try again.',
  needsPaymentMethod: 'Missing payment method.',
  needsShippingAddress: 'Missing shipping address.',
};

export default { errors };
