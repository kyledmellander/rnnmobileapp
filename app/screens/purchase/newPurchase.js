import React from 'react';
import {
  Text,
  ScrollView,
  Platform,
  ActivityIndicator,
  SafeAreaView,
} from 'react-native';
import { connect } from 'react-redux';
import { CustomButton } from 'components';
import { actions as metricActions } from 'appRedux/metrics';
import { actions as orderActions } from 'appRedux/orders';
import adjust from 'lib/adjust';
import * as navUtils from 'lib/navUtils';
import get from 'lodash/get';
import ProductQuantitySelector from './productQuantitySelector';
import { PurchaseInfo } from './components';
import styles from './styles';

class Purchase extends React.Component {
  constructor(props) {
    super(props);
    this.productSelected = this.productSelected.bind(this);
    this.getProductQuantitySelector = this.getProductQuantitySelector.bind(this);
    this.enableButton = this.enableButton.bind(this);
    this.createOrder = this.createOrder.bind(this);
    this.state = {
      selectedProducts: {},
      triedToCharge: false,
      messages: '',
      waiting: false,
      loading: true,
    };
  }

  // componentWillMount() {
  // const { addMetric } = this.props;
  // const onBack = () => {
  //   addMetric(`appv4.${Platform.OS}.purchase.bounce`, 1);
  //   Actions.pop();
  // };
  // Actions.refresh({ onBack });
  // this.props.addMetric(`appv4.${Platform.OS}.purchase.start`, 1);
  // }

  componentWillReceiveProps(nextProps) {
    const newState = { ...this.state, waiting: false };
    if (nextProps.ordersForOffer) {
      newState.loading = false;
    }
    if (nextProps.errors) {
      let message = '';
      let needsAddr = false;
      let needsPay = false;
      nextProps.errors.forEach((error) => {
        if (error.cardError) {
          message += error.cardError.message;
        } else {
          switch (error) {
            case 'payment_failed':
              message +=
                'We were unable to charge the card on file, please double check your payment information.';
              break;
            case 'duplicate_order':
              message +=
                'This looks like a duplicate order.\nPlease wait 5 minutes to try again.';
              break;
            case 'needs_shipping_address':
              needsAddr = true;
              break;
            case 'needs_payment_method':
              needsPay = true;
              break;
            default:
              message +=
                'We were unable to process your order at this time, please try again later.';
          }
        }
      });
      newState.messages = message;
      if (needsAddr || needsPay) {
        const orderDetails = this.props.offer.offerProductBundle
          ? this.createOrderProductBundle()
          : this.createOrderProducts();

        navUtils.pushScreen('FirstTimePurchase', {
          passProps: {
            orderDetails,
            needsAddress: needsAddr,
            needsPayment: needsPay,
            purchaseButtonText: 'Save Card',
          }
        });
        this.props.clearOrderStatus();
      }
    }

    if (
      nextProps.order &&
      (nextProps.order.state === 'charged' ||
        nextProps.order.state === 'waitlisted')
    ) {
      newState.messages = '';
      const { order } = nextProps;
      this.props.addMetric(`appv4.${Platform.OS}.orders.count`, 1);
      adjust.firePurchaseEvent();
      navUtils.setStackRoot('Receipt', { passProps: { order } });
      this.props.clearOrderStatus();
    }

    this.setState(newState);
  }

  getProductQuantitySelector(offerProduct, showZero, order) {
    const selectedQuantity = num => this.productSelected(offerProduct, num);
    return (
      <ProductQuantitySelector
        style={{ flex: 1, marginTop: 15 }}
        showZero={showZero}
        offerProduct={offerProduct}
        onSelected={selectedQuantity}
        previousOrder={order}
        loading={this.state.loading}
        key={offerProduct.id}
      />
    );
  }

  getTotalSelected() {
    return Object.keys(this.state.selectedProducts).reduce((val, k) => {
      const prod = this.state.selectedProducts[k];
      return prod.units + val;
    }, 0);
  }

  getPackageContainer() {
    if (this.props.offer.offerProductBundle) {
      return this.props.offer.offerProductBundle.package.name;
    }

    let packageContainer = '';
    this.props.offer.offerProducts.forEach((op) => {
      packageContainer = op.product.container.name;
    });
    return this.getTotalSelected() > 1
      ? `${packageContainer}s`
      : packageContainer;
  }

  getBuyAddWording() {
    const totalSelected = this.getTotalSelected();
    const { hasValidAddress, hasValidCreditCard } = this.props;

    let inStockText = 'Charge My Card';
    if (!(hasValidAddress && hasValidCreditCard)) {
      inStockText = 'Get it';
    }

    if (totalSelected === 0) {
      return this.props.offer.inStock
        ? inStockText
        : 'Waitlist me';
    }

    const container = this.getPackageContainer();
    return this.props.offer.inStock
      ? inStockText
      : `Waitlist for ${totalSelected} ${container}`;
  }

  getButtonIcon() {
    return this.canCharge() ? 'credit-card' : undefined;
  }

  getButtonText() {
    const loadingText = this.canCharge() ? 'Charging...' : 'Loading...';

    return this.state.waiting
      ? loadingText
      : this.getBuyAddWording();
  }

  getOfferProducts() {
    const offerProducts = this.props.offer.offerProductBundle
      ? [this.props.offer.offerProductBundle]
      : this.props.offer.offerProducts;

    return offerProducts;
  }

  canCharge() {
    const { inStock } = this.props.offer;
    const { hasValidAddress, hasValidCreditCard } = this.props;

    return inStock && hasValidAddress && hasValidCreditCard;
  }


  isLoading() {
    return this.state.loading;
  }

  enableButton() {
    return !this.state.waiting && this.getTotalSelected() > 0;
  }

  productSelected(offerProduct, quantity) {
    const newState = {
      ...this.state,
    };
    const orderQuantity = get(offerProduct, ['product', 'container'])
      ? quantity
      : quantity / offerProduct.package.units;
    newState.selectedProducts[offerProduct.id] = {
      orderQuantity,
      id: offerProduct.id,
      units: quantity,
    };

    // Get a new object to trigger rerender
    newState.selectedProducts = { ...newState.selectedProducts };
    this.setState(newState);
  }

  createOrder() {
    if (this.state.waiting) return; // to prevent double ordering before component re-renders

    const data = this.props.offer.offerProductBundle
      ? this.createOrderProductBundle()
      : this.createOrderProducts();

    this.setState({ waiting: true, triedToCharge: true });
    this.props.createOrder(data);
  }

  createOrderProductBundle() {
    const offerProductBundleId = Object.keys(this.state.selectedProducts)[0];
    return {
      offerId: this.props.offer.id,
      orderProductBundle: {
        offerProductBundleId,
      },
    };
  }

  createOrderProducts() {
    const orderProducts = Object.keys(this.state.selectedProducts).map(k => ({
      offerProductId: k,
      quantity: this.state.selectedProducts[k].orderQuantity,
    }));
    return {
      offerId: this.props.offer.id,
      orderProducts,
    };
  }

  renderOfferProducts() {
    const offerProducts = this.getOfferProducts();
    const showZero = offerProducts.length > 1;
    const products = offerProducts.map(op =>
      this.getProductQuantitySelector(
        op,
        showZero,
        this.props.offer.orders[op.id]
      ));
    return products;
  }


  render() {
    const { selectedProducts } = this.state;
    const offerProducts = this.getOfferProducts();

    if (this.isLoading()) {
      return <ActivityIndicator style={styles.loading} />;
    }
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView
          style={styles.offerProductsScrollView}
          contentContainerStyle={styles.offerProductsContainer}
          horizontal={false}
        >
          <Text style={styles.title}>How many would you like?</Text>
          {this.renderOfferProducts()}
          <Text style={styles.newErrorText}>{this.state.messages}</Text>
        </ScrollView>
        <PurchaseInfo selectedProducts={selectedProducts} offerProducts={offerProducts} />
        <CustomButton
          buttonContainerStyle={styles.confirmButton}
          buttonText={this.getButtonText()}
          enabled={this.enableButton()}
          onPress={this.createOrder}
          testID="PurchaseBeer"
          iconName={this.getButtonIcon()}
          customIcon
        />
      </SafeAreaView>
    );
  }
}

function mapStateToProps({ orders, purchaseInfo }) {
  const { order, errors, ordersForOffer } = orders;
  const {
    fetchingPurchaseInfo,
    hasValidCreditCard,
    hasValidAddress,
  } = purchaseInfo;

  return {
    fetchingPurchaseInfo,
    hasValidAddress,
    hasValidCreditCard,
    order,
    errors,
    ordersForOffer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    createOrder: data => dispatch(orderActions.createOrder(data)),
    clearOrderStatus: data => dispatch(orderActions.clearOrderStatus(data)),
    addMetric: (...args) => dispatch(metricActions.addMetric(...args)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Purchase);
