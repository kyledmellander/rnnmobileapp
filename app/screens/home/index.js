import React, { Component } from 'react';
import {
  View,
  Image,
  Platform,
  Animated,
  Easing,
  AppState,
  Alert,
  Dimensions,
  SafeAreaView,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';
import images from 'images';
import { selectors } from 'appRedux';
import { actions as experimentActions } from 'appRedux/experiments';
import { actions as offerActions } from 'appRedux/offers';
import { actions as orderActions } from 'appRedux/orders';
import { actions as metricActions } from 'appRedux/metrics';
import { actions as userActions } from 'appRedux/user';
import { actions as appNotificationActions, selectors as appNotificationSelectors } from 'appRedux/appNotifications';
import loadingLib from 'lib/loading';
import analytics from 'lib/analytics';
import { Swiper } from 'components';
import { navUtils, adjust } from 'lib';
import OfferPreview from './offerPreview';
import styles from './styles';
import PushNotification from '../../config/pushNotifications';

class home extends Component {
  static options() {
    return navUtils.getOptions('Featured Beers', { showHamburger: true });
  }

  constructor(props) {
    super(props);
    this.state = {
      errors: null,
      animatedValue: new Animated.Value(0),
    };
    this.componentDidAppear.bind(this);
    this.componentDidDisappear.bind(this);
    this.resetOffers = this.resetOffers.bind(this);
    this.animate = this.animate.bind(this);
    this.opacity = new Animated.Value(0);
    Animated.timing(this.opacity, {
      toValue: 1,
      duration: 600,
      easing: Easing.quad,
      useNativeDriver: true,
    }).start();
    Navigation.events().bindComponent(this);
  }

  componentDidMount() {
    PushNotification.requestPermissionsIfEnabled();
    const { resetOffers } = this;
    AppState.addEventListener('change', (newState) => {
      if (newState === 'active') {
        resetOffers();
      }
    });
    this.props.fetchExperiments();
    this.props.fetchOffers();
    this.props.getAppNotifications();
    adjust.getAttribution();
  }

  componentWillReceiveProps(nextProps) {
    // first time getting offers should render with first offer displayed
    const hadOffers = this.props.offers && this.props.offers.length;
    const nextPropsHasOffers = nextProps.offers && nextProps.offers.length;
    if (this.isLoading(this.props) && !this.isLoading(nextProps)) {
      console.log('huzzah');
      this.opacity = new Animated.Value(0);
      Animated.timing(this.opacity, {
        toValue: 1,
        duration: 600,
        easing: Easing.quad,
        useNativeDriver: true,
      }).start();
    }

    if (!hadOffers && nextPropsHasOffers) {
      // TODO: Figure out why Analytics/this function is freezing detox tests
      // this.props.onSeeOffer(nextProps.offers[0]);
    }
    if (nextProps.userNeedsState) {
      navUtils.signUpFlow('EnterZip');
      return;
    }
    if (nextProps.appNotifications > 0 && !this.state.appNotifications) {
      // TODO: Figure out an alternative to Actions.refresh
      // Actions.refresh({ renderRightButton: () => icon.render() });
      this.setState({
        appNotifications: true
      });
    }
    if (nextProps.errors.length > 0 && nextProps.errors !== this.state.errors) {
      const { signOut } = this.props;
      const message = nextProps.errors.reduce(
        (sum, val) => `${sum}${val.title} `,
        ''
      );
      if (message.indexOf('State Not Open') !== -1) {
        Alert.alert(
          'Tavour Not Available',
          "Looks like Tavour isn't open in your state yet!",
          [
            {
              text: 'Ok, Sign me out',
              onPress: () => {
                signOut();
              },
            },
          ],
          {
            cancelable: false,
          }
        );
      }
    }
    this.setState({ offers: nextProps.offers });
  }

  getOffers() {
    const { offers, getOrders } = this.props;

    if (offers && offers.length !== 0) {
      const previews = offers.map((content, index) => {
        const buttonPress = () => {
          getOrders(content);
          // TODO: FIGURE OUT WHY ANALYTICS IS CAUSING DETOX TO HANG
          // analytics.track({
          //   event: 'Get It Button',
          //   properties: {
          //     offerId: content.id,
          //     title: content.title,
          //     waitlist: !content.inStock && content.canGetMore,
          //   },
          // });
          navUtils.pushScreen('Purchase', { passProps: { hide: false, offer: content } });
        };
        const width = Math.round(Dimensions.get('window').width * 2);
        const photo = content.offerProductBundle
          ? content.offerProductBundle.photo
          : content.offerProducts[0].product.photos[0];
        const url = photo
          ? `${photo.host}w_${width}/${photo.path}`
          : 'add alternative photo'; // TODO: alternative photo?
        let buttonText = 'Sold out';
        let buttonEnabled = false;
        if (content.inStock) {
          buttonText = 'Get it';
          buttonEnabled = true;
        } else if (content.canGetMore) {
          buttonText = 'Waitlist me';
          buttonEnabled = true;
        }
        const product =
          content.offerProducts.length > 0
            ? content.offerProducts[0].product
            : content.offerProductBundle;
        const title = product.name;
        const producerLocationText = product.producerLocation
          ? `, ${product.producerLocation}`
          : '';
        const abv = product.abv
          ? `\n${product.style} - ABV: ${product.abv}%`
          : '';
        const subTitle = product.producerName
          ? `${product.producerName}${producerLocationText}${abv}`
          : '';
        return OfferPreview({
          title,
          subTitle,
          offer: content,
          body: content.body,
          image: url,
          width: styles.width,
          buttonPress,
          style: styles.preview,
          buttonText,
          buttonEnabled,
          index,
        });
      });

      return (
        <View style={{ flex: 1 }}>
          <Swiper
            height="100%"
            activeDotColor="black"
            removeClippedSubviews={false}
            paginationStyle={{ bottom: 7 }}
          >
            { previews }
          </Swiper>
        </View>
      );
    }
    return null;
  }

  // Android ViewPager workaround
  componentDidAppear() {
    this.setState({ visible: true });
  }

  // Android ViewPager workaround
  componentDidDisappear() {
    this.setState({ visible: false });
  }


  resetOffers() {
    const { fetchOffers } = this.props;
    this.setState({
      offers: null,
      errors: null,
    });

    fetchOffers();
  }

  animate() {
    const { animatedValue } = this.state;
    const callback = this.props.offers ? null : this.animate;
    animatedValue.setValue(0);
    Animated.timing(animatedValue, {
      toValue: 1,
      duration: 2000,
      easing: Easing.linear,
    }).start(callback);
  }

  isLoading(props = this.props) {
    const { offers, fetchingExperiments, experimentErrorMessage } = props;

    return (!offers || fetchingExperiments || experimentErrorMessage);
  }

  render() {
    if (this.isLoading()) {
      this.animate();
      const { animatedValue } = this.state;
      const leftToRight = loadingLib.leftToRight(animatedValue);
      return (
        <View style={{ flex: 1 }}>
          <Image
            source={images.splash}
            style={{ width: '100%', height: '100%' }}
            resizeMode="cover"
          />
          <Animated.View
            style={[
              {
                height: '100%',
                width: 10,
                backgroundColor: 'white',
                opacity: 0.3,
              },
              leftToRight,
            ]}
          />
        </View>
      );
    }

    const increaseOpacity = loadingLib.increaseOpacity(this.opacity);

    const offerCount = this.props.offers ? this.props.offers.length : 0;
    this.props.addMetric(`appv4.${Platform.OS}.offers.count`, offerCount);
    this.props.addMetric(`appv4.${Platform.OS}.home.loaded`, 1);
    return (
      <SafeAreaView style={styles.safeArea}>
        <Animated.View style={[styles.container, increaseOpacity]} testID="HomeLoaded">
          {this.getOffers()}
        </Animated.View>
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  const {
    experiments: { fetchingExperiments, errorMessage: experimentErrorMessage },
    offers: { offers, errors },
  } = state;

  return {
    fetchingExperiments,
    experimentErrorMessage,
    offers,
    errors,
    userNeedsState: selectors.userNeedsState(state),
    appNotifications: appNotificationSelectors.activeNotificationCountSelector(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchExperiments() {
      dispatch(experimentActions.fetchExperiments());
    },
    fetchOffers() {
      dispatch(offerActions.getOffers());
    },
    addMetric(...args) {
      dispatch(metricActions.addMetric(...args));
    },
    getUser() {
      dispatch(userActions.getUser());
    },
    signOut() {
      dispatch(userActions.signOut());
    },
    getOrders(offer) {
      dispatch(orderActions.getOrders(offer));
    },
    getAppNotifications() {
      dispatch(appNotificationActions.getNotifications());
    },
    onSeeOffer(offer) {
      analytics.track({
        event: 'Saw Offer',
        properties: {
          offerId: offer.id,
          title: offer.title,
          waitlist: !offer.inStock && offer.canGetMore,
        }
      });
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(home);
