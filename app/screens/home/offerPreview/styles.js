import { Platform, Dimensions } from 'react-native';
import { appStyles } from 'styles';

const { height, width } = Dimensions.get('window');
const imageHeight = height * 0.7;
const imageOffset = -60;

const styles = {
  container: {
    flex: 1,
    alignItems: 'flex-start',
  },
  textContainer: {
    flex: 1,
    width: '100%',
    backgroundColor: 'white',
    paddingTop: Platform.OS === 'ios' ? 0 : 20,
  },
  button: {
    flex: 0,
    height: Platform.OS === 'ios' ? '9%' : '13%',
    width: '100%',
  },
  photoContainer: {
    marginTop: imageOffset,
    height: imageHeight,
    width: '100%',
  },
  photo: {
    height: '100%',
    width: '100%',
  },
  titleText: {
    fontSize: 19,
    paddingTop: Platform.OS === 'ios' ? 20 : 0,
    paddingLeft: '8%',
    paddingRight: '5%',
    paddingBottom: 5, // Regardless of device size, have space between paragraph
    width: '88%',
    textAlignVertical: 'center',
    fontFamily: appStyles.fontFamilySemibold,
    fontWeight: 'bold',
  },
  subTitleText: {
    fontFamily: appStyles.fontFamilyLight,
    paddingTop: 6,
    paddingLeft: '8%',
    paddingRight: '5%',

    paddingBottom: 9,
  },
  bodyText: {
    width: '88%',
    paddingLeft: '8%',
    paddingBottom: Platform.OS === 'ios' ? 5 : 8,
  },
  shareIcon: {
    position: 'absolute',
    top: imageHeight + imageOffset - 30, // shareIcon is 30 tall
    right: width * 0.08,
  }
};

export default styles;
