import React from 'react';
import {
  Text,
  View,
  Image,
  Dimensions,
  ScrollView,
} from 'react-native';
import HTMLView from 'components/CustomHtmlView';
import ShareIcon from 'components/ShareIcon';
import CustomButton from 'components/CustomButton';
import styles from './styles';
import { shareTitle, shareMessage } from './util';
import LoyaltyBanner from './loyaltyBanner';

const offerPreview = (props) => {
  const {
    body,
    buttonEnabled,
    buttonText,
    buttonPress,
    index,
    image,
    offer,
    style,
    subTitle,
    title,
    width,
    showButton = true,
    extraPadding = 30, // Give extra padding for the swiper dots
  } = props;

  const containerWidth = Dimensions.get('window').width;
  let Button = null;
  if (showButton) {
    Button = (
      <CustomButton
        buttonContainerStyle={[styles.button, width]}
        buttonText={buttonText}
        onPress={buttonPress}
        enabled={buttonEnabled}
        testID={`GetIt_${index}`}
      />
    );
  }

  return (
    <View style={[styles.container, style, { paddingBottom: extraPadding, width: containerWidth }]} testID="OfferPreview">
      <ScrollView style={{ flex: 1, width: containerWidth }}>
        <View style={[styles.photoContainer, width]} activeOpacity={1}>
          <Image
            style={styles.photo}
            source={{ uri: image }}
            resizeMode="cover"
          />
          <LoyaltyBanner offerType={offer.offerType} />
        </View>
        <View style={[styles.textContainer, width]} activeOpacity={1}>
          <Text style={styles.titleText}>{title}</Text>
          <Text style={styles.subTitleText}>{subTitle}</Text>
          <HTMLView
            style={styles.bodyText}
            value={body}
            preview={false}
          />
        </View>
        <View style={styles.shareIcon}>
          <ShareIcon
            dialogTitle="Tell your friends about this cool beer!"
            title={shareTitle(offer)}
            message={shareMessage(offer)}
          />
        </View>
      </ScrollView>
      {Button}
    </View>
  );
};

export default offerPreview;
