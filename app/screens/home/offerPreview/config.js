export const shareTitlesForProduct = [
  'Sweet beer @tavour today!',
  'Excited about the beer @tavour today!',
  'Awesome beer @tavour today!',
];

export const shareTitlesForProductBundle = [
  'Sweet beer box @tavour today!',
  'Excited about this box of beer @tavour today!',
  'Awesome beer box @tavour today!',
];

export const getProductBundleShareMessages = name => [
  `@tavour has ${name} for sale today. This will sell out quickly, so signup and grab yours now!`,
  `@tavour has ${name} today and I am getting me some. Get yours before they run out.`,
  `If any of you are into ${name}, get it on @tavour now before they run out.`,
];

export const getProductShareMessages = (name, thisThese, itThem) => [
  `@tavour has ${name} for sale today. ${thisThese} will sell out quickly, so signup and grab a few now!`,
  `@tavour has ${name} today and I am getting me some. Get yours before they run out.`,
  `If any of you are into ${name}, get ${itThem} on @tavour now before they run out.`,
];
