import React from 'react';
import { Text, View } from 'react-native';
import styles from './styles';

export const loyaltyOfferTitle = 'Secret Stash!';
export const loyaltyOfferMessage = 'Thanks for being awesome';

const loyaltyBanner = (props) => {
  if (props.offerType !== 'loyalty') return null;
  return (
    <View style={styles.container}>
      <View style={styles.loyaltyBannerPadding} />
      <View style={styles.loyaltyBanner}>
        <Text style={styles.loyaltyBannerTitle}> {loyaltyOfferTitle} </Text>
        <Text style={styles.loyaltyBannerMessage}> {loyaltyOfferMessage} </Text>
      </View>
    </View>
  );
};

export default loyaltyBanner;
