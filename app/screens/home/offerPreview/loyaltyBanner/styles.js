import { appStyles } from 'styles';

const textStyle = {
  color: 'white',
  fontFamily: appStyles.fontFamilySemibold,
  width: '100%',
  textAlign: 'center'
};

const styles = {
  container: {
    marginTop: '38%',
    position: 'absolute',
    height: '100%',
    flex: 0,
    width: '100%'
  },
  loyaltyBannerPadding: {
    height: 5,
    width: '100%',
    backgroundColor: '#225007'
  },
  loyaltyBanner: {
    height: 60,
    width: '100%',
    backgroundColor: 'rgba(88,156,66,90)',
    justifyContent: 'center',
    alignItems: 'center'
  },
  loyaltyBannerTitle: {
    ...textStyle,
    fontSize: 19
  },
  loyaltyBannerMessage: {
    ...textStyle,
    fontSize: 14
  }
};

export default styles;
