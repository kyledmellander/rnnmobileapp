import {
  shareTitlesForProduct,
  shareTitlesForProductBundle,
  getProductBundleShareMessages,
  getProductShareMessages
} from './config';

function shareTitle(offer) {
  const msgs = offer.offerProductBundle ?
    shareTitlesForProductBundle :
    shareTitlesForProduct;
  return msgs[Math.floor(Math.random() * msgs.length)];
}

function shareMessage(offer) {
  let name = '';
  let msgs = [];
  if (offer.offerProductBundle) {
    name = offer.offerProductBundle;
    msgs = getProductBundleShareMessages(name);
  } else if (offer.offerProducts) {
    let names = [];
    let producers = [];
    offer.offerProducts.forEach((offerProduct) => {
      names.push(offerProduct.product.name);
      producers.push(offerProduct.product.producerName);
    });
    names = [...new Set(names)];
    [name] = names;

    for (let i = 1; i < names.length; i += 1) {
      if (i + 1 === names.length) {
        name += ' and ';
      } else {
        name += ', ';
      }
      name += `${names[i]}`;
    }

    producers = [...new Set(producers)];
    if (producers.length === 1) {
      name += ' by ';
      name += producers[0];
    }

    const thisThese = names.length > 1 ? 'these' : 'this';
    const itThem = names.length > 1 ? 'them' : 'it';
    msgs = getProductShareMessages(name, thisThese, itThem);
  }
  return msgs[Math.floor(Math.random() * msgs.length)];
}

export { shareTitle, shareMessage };
