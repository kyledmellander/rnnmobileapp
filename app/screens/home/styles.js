import { colors } from 'styles';

const styles = {
  safeArea: {
    flex: 1,
    backgroundColor: colors.backgroundGray,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor: colors.backgroundGray,
  },
  width: {
    width: '98%',
  },
  slider: {
    justifyContent: 'center',
  },
  preview: {
    flex: 0,
    marginLeft: '1%',
    height: '100%',
    backgroundColor: colors.backgroundGray,
  },
};

export default styles;
