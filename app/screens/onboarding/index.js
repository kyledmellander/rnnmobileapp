import dailyBeers from './dailyBeers';
import notifications from './notifications';
import stockUpAndShip from './stockUpAndShip';
import flatRateShipping from './flatRateShipping';
import signUpConfirmation from './signUpConfirmation';

export {
  dailyBeers,
  notifications,
  stockUpAndShip,
  flatRateShipping,
  signUpConfirmation
};
