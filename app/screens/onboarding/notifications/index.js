import React, { Component } from 'react';
import { SafeAreaView, Text, Image, View } from 'react-native';
import images from 'images';
import PushNotification from 'src/config/pushNotifications';
import CustomButton from 'components/CustomButton';
import { navUtils } from 'lib';
import styles from './styles';

const noNotifications = async () => {
  await PushNotification.setNotificationsEnabled(false);
  navUtils.pushScreen('Onboarding4');
};

const allowNotifications = async () => {
  await PushNotification.setNotificationsEnabled(true);
  await PushNotification.requestPermissionsIfEnabled();
  navUtils.pushScreen('Onboarding4');
};

class Onboarding3 extends Component {
  static options() {
    return navUtils.getOptions('How to Tavour');
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Text style={styles.titleText}>GET NOTIFIED</Text>
        <View style={styles.imageContainer}>
          <Image
            style={styles.image}
            source={images.howToTavour.notified}
            resizeMode="contain"
          />
        </View>
        <Text style={styles.bodyText}>
          Beers sell out quickly. Be notified as
          soon as they’re released.
        </Text>
        <Text
          style={styles.noThanks}
          onPress={noNotifications}
        >No thanks
        </Text>
        <CustomButton
          buttonContainerStyle={styles.button}
          buttonText="Allow Notifications"
          onPress={allowNotifications}
          enabled
          testID="NotificationsContinue"
        />
      </SafeAreaView>
    );
  }
}

export default Onboarding3;
