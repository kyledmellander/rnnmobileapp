import { appStyles, colors } from 'styles';
import baseFontSize
  from '../styles';

const styles = {
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  button: {
    flex: 1,
    justifyContent: 'flex-end',
    width: '100%',
  },
  buttonText: {
    fontFamily: appStyles.fontFamilySemibold,
    fontSize: baseFontSize,
  },
  titleText: {
    flex: 0,
    marginTop: 30,
    fontSize: baseFontSize + 5,
    textAlignVertical: 'center',
    textAlign: 'center',
    fontFamily: appStyles.fontFamilySemibold,
    color: '#363636'
  },
  imageContainer: {
    flex: 2,
    width: '90%',
    alignItems: 'center',
  },
  image: {
    height: '100%',
    width: '100%',
  },
  bodyText: {
    flex: 1,
    fontSize: baseFontSize,
    width: '99%',
    textAlign: 'center',
    fontFamily: appStyles.fontFamily,
    color: colors.textGray
  },
  noThanks: {
    flex: 0,
    paddingBottom: 5,
    fontFamily: appStyles.fontFamily,
    color: '#9f9f9f',
    textDecorationLine: 'underline',
    fontSize: baseFontSize - 3
  }
};

export default styles;
