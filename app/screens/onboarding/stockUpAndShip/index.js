import React, { Component } from 'react';
import { SafeAreaView, Text, Image, View } from 'react-native';
import images from 'images';
import CustomButton from 'components/CustomButton';
import { navUtils } from 'lib';
import styles from './styles';

const onPress = () => {
  navUtils.pushScreen('Onboarding5');
};

const body = 'Grow your collection until it automatically ships after 4 weeks.';
class Onboarding4 extends Component {
  static options() {
    return navUtils.getOptions('How to Tavour');
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Text style={styles.titleText}>STOCK UP</Text>
        <Image
          style={styles.image}
          source={images.howToTavour.crate}
          resizeMode="contain"
        />
        <View style={styles.bodyContainer}>
          <Text style={styles.bodyText}>{body}</Text>
          <Text style={styles.bodyText} />
          <Text style={styles.bodyText}><Text style={{ fontWeight: 'bold' }}>Too long?</Text> You can always bump up your
            shipment date.
          </Text>
        </View>
        <CustomButton
          buttonContainerStyle={styles.button}
          buttonTextStyle={styles.buttonText}
          buttonText="Got it"
          onPress={onPress}
          enabled
          testID="StockUpGotIt"
        />
      </SafeAreaView>
    );
  }
}

export default Onboarding4;
