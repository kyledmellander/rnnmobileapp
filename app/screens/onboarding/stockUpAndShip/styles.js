import { appStyles, colors } from 'styles';
import baseFontSize
  from '../styles';

const styles = {
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  button: {
    flex: 1,
    width: '100%',
  },
  titleText: {
    flex: 0,
    marginTop: 30,
    fontSize: baseFontSize + 5,
    textAlignVertical: 'center',
    textAlign: 'center',
    fontFamily: appStyles.fontFamilySemibold,
    color: '#363636'
  },
  image: {
    flex: 1,
    marginTop: '-25%',
    width: '75%',
    backgroundColor: 'transparent'
  },
  bodyContainer: {
    flex: 1,
    paddingTop: '15%',
    width: '95%'
  },
  bodyText: {
    flex: 0,
    fontSize: baseFontSize,
    textAlign: 'center',
    fontFamily: appStyles.fontFamily,
    color: colors.textGray
  }
};

export default styles;
