import React from 'react';
import { SafeAreaView, Text, Image, View } from 'react-native';
import { connect } from 'react-redux';
import images from 'images';
import { actions as userActions } from 'appRedux/user';
import CustomButton from 'components/CustomButton';
import { navUtils } from 'lib';
import styles from './styles';

class flatRateShipping extends React.Component {
  static options() {
    return navUtils.getOptions('How to Tavour');
  }

  constructor(props) {
    super(props);
    this.onPress = this.onPress.bind(this);
    this.enableButton = this.enableButton.bind(this);
    this.state = {
      enableButton: true
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.user.sawOnboarding) {
      navUtils.setStackRoot('Home');
    }
    this.setState({ enableButton: true });
  }

  onPress() {
    const updatedUser = { ...this.props.user };
    updatedUser.sawOnboarding = true;

    this.props.updateUser(updatedUser);
    this.setState({ enableButton: false });
  }

  enableButton() {
    return this.state.enableButton;
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Text style={styles.titleText}>FLAT RATE SHIPPING</Text>
        <Image
          style={styles.image}
          source={images.howToTavour.delivery}
          resizeMode="contain"
        />
        <Text style={styles.bodyText}>Ship any amount of beer for $14.90</Text>
        <Text style={styles.bodyText}>Yep, <Text style={{ fontStyle: 'italic' }}>any amount.</Text></Text>
        <View style={styles.buttonContainer}>
          <CustomButton
            buttonContainerStyle={styles.button}
            buttonTextStyle={styles.buttonText}
            buttonText="Got it, show me the beer!"
            onPress={this.onPress}
            enabled={this.enableButton()}
            testID="OnboardingGoHome"
          />
        </View>
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.user.user
  };
}

function mapDispatchToProps(dispatch) {
  return {
    updateUser: user =>
      dispatch(userActions.updateUser(user))
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(flatRateShipping);
