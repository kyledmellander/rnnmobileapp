import { appStyles, colors } from 'styles';
import baseFontSize
  from '../styles';

const styles = {
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  titleText: {
    flex: 0,
    marginTop: 30,
    fontSize: baseFontSize + 5,
    textAlignVertical: 'center',
    textAlign: 'center',
    fontFamily: appStyles.fontFamilySemibold,
    color: '#363636'
  },
  image: {
    flex: 4,
    width: '52%',
  },
  bodyText: {
    marginTop: '5%',
    fontSize: baseFontSize,
    width: '88%',
    textAlign: 'center',
    fontFamily: appStyles.fontFamily,
    color: colors.textGray
  },
  buttonContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    width: '100%',
  },
  button: {
    height: '100%',
    width: '100%',
  },
};

export default styles;
