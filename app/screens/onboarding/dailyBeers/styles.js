import { appStyles, colors } from 'styles';
import baseFontSize from '../styles';

const styles = {
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  button: {
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  buttonText: {
    fontFamily: appStyles.fontFamilySemibold,
    fontSize: baseFontSize,
  },
  titleText: {
    flex: 0,
    marginTop: 30,
    paddingBottom: 15,
    fontSize: baseFontSize + 5,
    textAlignVertical: 'center',
    textAlign: 'center',
    fontFamily: appStyles.fontFamilySemibold,
    color: '#363636'
  },
  image: {
    flex: 3,
    maxWidth: '58%',
    maxHeight: '58%'
  },
  bodyText: {
    flex: 1,
    fontSize: baseFontSize,
    textAlign: 'center',
    fontFamily: appStyles.fontFamily,
    color: colors.textGray,
    paddingTop: 15,
  }
};

export default styles;
