import React, { Component } from 'react';
import { SafeAreaView, Text, Image } from 'react-native';
import images from 'images';
import CustomButton from 'components/CustomButton';
import { navUtils } from 'lib';
import styles from './styles';

const onPress = () => {
  navUtils.pushScreen('Onboarding3');
};

const body = `Swipe to see all available beers.

Buy only the ones you want.`;
class Onboarding2 extends Component {
  static options() {
    return navUtils.getOptions('How to Tavour');
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Text style={styles.titleText}>NEW BEERS DAILY</Text>
        <Image
          style={styles.image}
          source={images.howToTavour.getIt}
          resizeMode="contain"
        />
        <Text style={styles.bodyText}>{body}</Text>
        <CustomButton
          buttonContainerStyle={styles.button}
          buttonText="Got it"
          onPress={onPress}
          enabled
          testID="DailyBeersContinue"
        />
      </SafeAreaView>
    );
  }
}

export default Onboarding2;
