import { appStyles, colors } from 'styles';
import baseFontSize
  from '../styles';

const styles = {
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textContainer: {
    flex: 1,
    width: '95%',
    justifyContent: 'center'
  },
  button: {
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  titleText: {
    fontSize: baseFontSize,
    width: '100%',
    textAlignVertical: 'center',
    textAlign: 'center',
    fontFamily: appStyles.fontFamily,
    color: colors.tavourOrange
  },
  buttonText: {
    fontFamily: appStyles.fontFamilySemibold,
    fontSize: baseFontSize,
  },
  subTitleText: {
    fontSize: baseFontSize,
    fontFamily: appStyles.fontFamily,
    color: colors.textGray,
    textAlign: 'center'
  }
};

export default styles;
