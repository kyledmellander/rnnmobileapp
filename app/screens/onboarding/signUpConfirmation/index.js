import React, { Component } from 'react';
import { View, SafeAreaView, Text } from 'react-native';
import CustomButton from 'components/CustomButton';
import { navUtils } from 'lib';
import styles from './styles';

const onPress = () => {
  navUtils.pushScreen('Onboarding2');
};

const body = `Beer is just a few swipes away.

First, let's learn how Tavour works.
`;
class Onboarding1 extends Component {
  static options() {
    return navUtils.getOptions('How to Tavour');
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.textContainer}>
          <Text style={styles.titleText}>Welcome To Tavour!</Text>
          <Text style={styles.subTitleText} />
          <Text style={styles.subTitleText}> {body} </Text>
        </View>
        <CustomButton
          buttonContainerStyle={styles.button}
          buttonText="Continue"
          onPress={onPress}
          enabled
          testID="SignUpConfirmationContinue"
        />
      </SafeAreaView>
    );
  }
}

export default Onboarding1;
