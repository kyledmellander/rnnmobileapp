import { Dimensions } from 'react-native';

const { height } = Dimensions.get('window');

const baseFontSize = Math.floor(height * 0.03);

export default baseFontSize;
