import { colors, appStyles } from 'styles';

const styles = {
  container: {
    flex: 1,
    marginTop: appStyles.marginTop,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textStyle: {
    fontFamily: appStyles.fontFamily,
    fontSize: 16
  },
  buttonContainer: {
    flex: 1,
    width: '100%',
  },
  fieldsContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    flex: 1,
    fontFamily: appStyles.fontFamily,
    textAlign: 'center',
  },
  field: {
    borderBottomColor: colors.borderGray,
    borderBottomWidth: 1,
    height: 50,
  },
  statusTextContainer: {
    flex: 1,
  },
  coverageStatusText: {
    fontFamily: appStyles.fontFamily,
    paddingTop: 10,
    fontSize: 10,
    textAlign: 'center',
  },
  zipCodeCoveredText: {
    color: 'green',
  },
  errorText: {
    color: 'red',
    fontSize: 14
  },
};

export default styles;
