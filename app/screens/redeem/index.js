import React from 'react';
import {
  View,
  Text,
  TextInput,
  ActivityIndicator,
  SafeAreaView,
} from 'react-native';
import { connect } from 'react-redux';
import { actions as redeemActions } from 'appRedux/redeem';
import CustomButton from 'components/CustomButton';
import CustomKeyboardAvoidingView from 'components/CustomKeyboardAvoidingView';
import * as navUtils from 'lib/navUtils';
import styles from './styles';


class redeem extends React.Component {
  static options() {
    return navUtils.getOptions('Redeem');
  }

  constructor(props) {
    super(props);
    this.enableButton = this.enableButton.bind(this);
  }

  getErrorsOrLoading() {
    const { errors } = this.props;

    const errorTextView = errors.length ? React.createElement(Text, {
      style: [styles.coverageStatusText, styles.errorText]
    }, errors) : null;
    const loadingView = React.createElement(ActivityIndicator, {
      style: { flex: 1 },
    });
    return this.props.loading ? loadingView : errorTextView;
  }

  getFields() {
    return (
      <View style={styles.fieldsContainer}>
        <View style={styles.field}>
          <TextInput
            autoFocus
            underlineColorAndroid="transparent"
            value={this.props.code}
            onChangeText={this.props.setCode}
            style={styles.input}
            placeholder="Enter your code"
            testID="RedeemInput"
            autoCapitalize="none"
          />
        </View>
        {this.getErrorsOrLoading()}
      </View>
    );
  }

  enableButton() {
    return this.props.code.length > 3 && !this.props.loading;
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <CustomKeyboardAvoidingView behavior="height" style={styles.container}>
          {this.getFields()}
          <CustomButton
            buttonContainerStyle={styles.buttonContainer}
            onPress={this.props.redeem}
            buttonText="Redeem"
            enabled={this.enableButton()}
            testID="RedeemSubmit"
          />
        </CustomKeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    errors: state.redeem.errors,
    loading: state.redeem.loading,
    code: state.redeem.code
  };
}

function mapDispatchToProps(dispatch) {
  return {
    redeem: () => {
      dispatch(redeemActions.redeem());
    },
    codeChanged: (code) => {
      dispatch(redeemActions.setCode(code));
    },
    setCode: (code) => {
      dispatch(redeemActions.setCode(code));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(redeem);
