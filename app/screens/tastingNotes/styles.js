import { Dimensions } from 'react-native';
import { colors, appStyles } from '../../styles';

const { height, width } = Dimensions.get('window');
const imageHeight = height * 0.7;
const imageOffset = -60;

const styles = {
  container: {
    flex: 1,
    alignItems: 'flex-start',
    backgroundColor: colors.backgroundGray,
  },
  scrollContainer: {
    flex: 1,
    width: '100%',
    backgroundColor: 'white',
  },
  photoContainer: {
    marginTop: imageOffset,
    height: imageHeight,
    width: '100%',
  },
  photo: {
    marginTop: imageOffset,
    height: imageHeight,
    width: '100%',
  },
  ratingsBar: {
    height: 60,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: colors.tavourLightGray,
  },
  ratingsContainer: {
    width: '60%',
    justifyContent: 'space-between',
  },
  textContainer: {
    flex: 1,
    width: '100%',
    backgroundColor: 'red',
  },
  titleText: {
    fontSize: 19,
    paddingTop: '5%',
    paddingLeft: '5%',
    paddingBottom: 10,
    width: '88%',
    fontFamily: appStyles.fontFamilySemibold,
  },
  subTitleText: {
    fontFamily: appStyles.fontFamilyLight,
    width: '88%',
    paddingLeft: '5%',
    marginBottom: 12,
  },
  bodyText: {
    width: '88%',
    paddingLeft: '5%',
    marginBottom: '10%',
  },
  shareIcon: {
    position: 'absolute',
    top: imageHeight + imageOffset - 30, // shareIcon is 30 tall
    right: width * 0.08,
  },
};

export default styles;
