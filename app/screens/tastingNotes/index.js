import React from 'react';
import {
  View,
  Image,
  ScrollView,
  Text,
  Platform,
  SafeAreaView,
} from 'react-native';
import { connect } from 'react-redux';
import OfferPreview from 'screens/home/offerPreview';
import StarRating from 'components/Rating';
import HTMLView from 'components/CustomHtmlView';
import ShareIcon from 'components/ShareIcon';
import * as navUtils from 'lib/navUtils';
import { actions as ratingActions } from 'appRedux/ratings';
import { actions as metricActions } from 'appRedux/metrics';
import styles from './styles';

const shareDialogTitle = 'Tell your friends about this cool beer!';
function shareTitle() {
  const msgs = [
    'Sweet beer @tavour!',
    'Excited about the beer @tavour!',
    'Awesome beer @tavour!',
  ];
  return msgs[Math.floor(Math.random() * msgs.length)];
}

class tastingNotes extends React.Component {
  static options() {
    return navUtils.getOptions('Tasting Notes');
  }

  constructor(props) {
    super(props);
    this.shareMessage = this.shareMessage.bind(this);
    this.rateBeer = this.rateBeer.bind(this);
  }

  componentWillMount() {
    this.props.addMetric(`appv4.${Platform.OS}.mybeer.tastingnotes`, 1);
  }

  getPhoto() {
    // todo multi product pic
    const photo = this.props.product.photos[0];
    if (photo) {
      const url = `${photo.host}h_0.5/${photo.path}`;
      return <Image style={styles.photo} source={{ uri: url }} />;
    }
    return <View style={styles.photo} />;
  }

  getBody() {
    const { product, offer } = this.props;
    const {
      name: title,
      producerName,
      producerLocation,
      abv,
      photos,
    } = product;
    const photo = photos[0];
    const url = `${photo.host}h_0.5/${photo.path}`;
    console.log(offer);
    const subTitle = `By: ${producerName}, ${producerLocation} - ABV: ${abv}`;

    return OfferPreview({
      title,
      subTitle,
      offer,
      body: offer,
      image: url,
      width: '100%',
      buttonPress: () => null,
      style: styles.preview,
      index: 0,
      showButton: false,
      extraPadding: 0,
    });
  }

  getButton() {
    const rating = this.props.product.rating.score || 0;
    return (
      <View style={styles.ratingsBar}>
        <StarRating
          style={styles.ratingsContainer}
          starSize={35}
          rating={rating}
          selectedStar={this.rateBeer}
        />
      </View>
    );
  }

  rateBeer(rating) {
    this.props.rateBeer(this.props.product, rating);
  }

  shareMessage() {
    const name = `${this.props.product.name} by ${
      this.props.product.producerName
    }`;
    const msgs = [`I got ${name} on @tavour and thought it was awesome`];

    return msgs[0];
  }

  render() {
    // todo platform specific code
    return (
      <SafeAreaView style={styles.container}>
        <View style={{ flex: 1 }}>
          {this.getBody()}
        </View>
        {this.getButton()}
      </SafeAreaView>
    );
  }
}

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {
    rateBeer: (beer, score) => {
      dispatch(ratingActions.rateBeer(beer, score));
    },
    addMetric(...args) {
      dispatch(metricActions.addMetric(...args));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(tastingNotes);
