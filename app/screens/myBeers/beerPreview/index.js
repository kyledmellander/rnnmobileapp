import React from 'react';
import { Text, View, Image, Animated, Easing } from 'react-native';
import * as navUtils from 'lib/navUtils';
import StarRating from 'components/Rating';
import styles from './styles';

export default class beerPreview extends React.PureComponent {
  constructor(props) {
    super(props);
    this.animatedValue = new Animated.Value(0);
    this.animate = this.animate.bind(this);
    this.viewNotes = this.viewNotes.bind(this);
  }

  getImage(photos) {
    if (photos[0]) {
      const source = {
        uri: `${photos[0].host}h_800/${photos[0].path}`,
      };
      return (
        <Image
          style={styles.photo}
          source={source}
          resizeMode="cover"
          onPress={this.viewNotes}
        />
      );
    }
    return <View style={styles.photo} />;
  }

  viewNotes() {
    navUtils.pushScreen('TastingNotes', {
      passProps: {
        offer: this.props.beer.offerBody,
        product: this.props.beer,
        onRating: this.props.selectedStar,
      }
    });
  }

  animate() {
    // eslint-disable-next-line no-underscore-dangle
    const endValue = this.animatedValue._value === 1 ? 0 : 1;
    Animated.timing(this.animatedValue, {
      toValue: endValue,
      duration: 1500,
      easing: Easing.linear,
    }).start(this.animate);
  }

  renderContent() {
    const { props } = this;
    const { beer } = props;
    const rating = beer.rating.score || 0;
    const locationText = beer.producerLocation
      ? `, ${beer.producerLocation}`
      : '';
    return (
      <View style={[styles.container, props.style]} key={props.key}>
        {this.getImage(beer.photos)}
        <View style={styles.infoContainer}>
          <Text style={styles.titleText} onPress={this.viewNotes}>
            {beer.name}
          </Text>
          <Text style={styles.bodyText} onPress={this.viewNotes}>
            {' '}
            {`${beer.producerName}${locationText}`}{' '}
          </Text>
          <Text style={styles.bodyText} onPress={this.viewNotes}>
            {' '}
            {`Style: ${beer.style}`}{' '}
          </Text>
          <StarRating
            style={styles.ratingContainer}
            buttonStyle={styles.rating}
            starSize={30}
            rating={rating}
            selectedStar={props.selectedStar}
          />
        </View>
      </View>
    );
  }

  renderPlaceHolder() {
    const darkVal = '#d2d2d2';
    const lightVal = '#eeeeee';
    const lighterVal = '#f2f2f2';
    const darkToLight = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [darkVal, lightVal],
    });
    const lightToLighter = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [lightVal, lighterVal],
    });
    const lightToLighterBackgroundColor = { backgroundColor: lightToLighter };
    const darkToLightBackgroundColor = { backgroundColor: darkToLight };
    this.animate();
    return (
      <View style={[styles.container, this.props.style]} key={this.props.key}>
        <Animated.View style={[styles.photo, darkToLightBackgroundColor]} />
        <View style={styles.infoContainer}>
          <Animated.View
            style={[styles.placeHolderTitle, darkToLightBackgroundColor]}
          />
          <Animated.View
            style={[styles.placeHolderProducer, lightToLighterBackgroundColor]}
          />
          <Animated.View
            style={[styles.placeHolderStyle, lightToLighterBackgroundColor]}
          />
          <Animated.View
            style={[styles.placeHolderRating, darkToLightBackgroundColor]}
          />
        </View>
      </View>
    );
  }

  render() {
    if (this.props.isPlaceholder) {
      return this.renderPlaceHolder();
    }
    return this.renderContent();
  }
}
