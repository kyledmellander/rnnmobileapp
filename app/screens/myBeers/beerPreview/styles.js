import { appStyles } from 'styles';

const styles = {
  container: {
    flex: 0,
    minHeight: 150,
    maxHeight: 250,
    backgroundColor: 'white',
    flexDirection: 'row',
    width: '100%',
  },
  infoContainer: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingTop: 15,
  },
  rating: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  ratingContainer: {
    marginTop: 15,
    marginLeft: -2,
    width: '95%',
    paddingBottom: 12,
    flex: 0,
  },
  photo: {
    height: '100%',
    width: '43%',
  },
  titleText: {
    fontSize: 14,
    flex: 0,
    fontFamily: appStyles.fontFamilySemibold,
    fontWeight: 'bold',
  },
  bodyText: {
    flex: 0,
    fontSize: 14,
    marginTop: 5,
    fontFamily: appStyles.fontFamily,
  },
  placeHolderTitle: {
    width: 144,
    height: 25,
    marginBottom: 14,
  },
  placeHolderProducer: {
    width: 174,
    height: 20,
    marginBottom: 4,
  },
  placeHolderStyle: {
    width: 96,
    height: 20,
    marginBottom: 17,
  },
  placeHolderRating: {
    width: 161,
    height: 25,
    marginBottom: 17,
  },
};

export default styles;
