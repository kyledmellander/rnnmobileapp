import { colors, appStyles } from 'styles';

export default {
  underLine: {
    backgroundColor: colors.tavourOrange,
    height: 2,
    width: 30,
  },
  container: {
    flex: 1,
    width: '100%',
    paddingTop: 15,
    height: 70,
    maxHeight: 50,
    backgroundColor: 'transparent',
    justifyContent: 'flex-start',
    flexDirection: 'row',
  },
  tabs: {
    position: 'relative',
    justifyContent: 'flex-start',
    flex: 0.9,
  },
  selectedTab: {
    fontWeight: 'bold',
  },
  disabledTab: {
    color: colors.disabledTextGray,
  },
  tabText: {
    fontSize: 14,
    fontFamily: appStyles.fontFamily,
    paddingBottom: 3,
    flex: 0,
  },
  tab: {
    flex: 0,
    height: 50,
    minWidth: 100,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
};
