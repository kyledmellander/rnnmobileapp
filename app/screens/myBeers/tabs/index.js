import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './styles';

export default class Tabs extends Component {
  constructor(props) {
    super(props);
    this.getTab = this.getTab.bind(this);
  }

  getTab(element) {
    const tabTextStyles = [styles.tabText];
    const selected = this.props.selected === element.name;
    let underLine;
    if (this.props.disabled) {
      tabTextStyles.push(styles.disabledTab);
    } else if (selected) {
      tabTextStyles.push(styles.selectedTab);
      underLine = React.createElement(View, { style: styles.underLine });
    }
    const { onSelect } = this.props;
    const buttonPressed = () => onSelect(element.name);
    return (
      <TouchableOpacity style={styles.tab} onPress={buttonPressed}>
        <Text style={tabTextStyles}>{element.text}</Text>
        {underLine}
      </TouchableOpacity>
    );
  }

  getChildren() {
    const tabs = this.props.contents.map(this.getTab);
    if (this.props.extraContent) {
      tabs.push(this.props.extraContent);
    }
    return tabs;
  }

  render() {
    return (
      <View
        style={[styles.container, this.props.style]}
      >
        {this.getChildren()}
      </View>
    );
  }
}
