import React, { Component } from 'react';
import {
  View,
  FlatList,
  Platform,
  TextInput,
  Animated,
  Easing,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome';
import { actions as ratingActions } from 'appRedux/ratings';
import { actions as productActions } from 'appRedux/products';
import { actions as metricActions } from 'appRedux/metrics';
import { icons } from 'images';
import { Icons } from 'components';
import Tabs from './tabs';
import BeerPreview from './beerPreview';
import styles from './styles';
import SearchBeers from './searchBeers';

const listSeparator = () => <View style={styles.listSeparator} />;

class myBeers extends Component {
  static options() {
    return {
      topBar: {
        buttonColor: 'black',
        title: {
          component: {
            name: 'NavBarTitle',
            passProps: {
              title: 'My Beers',
            },
            alignment: 'center',
          },
        },
        leftButtons: [
          {
            id: 'hamburger',
            icon: Icons.hamburger,
            color: 'black',
          }
        ]
      }
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      beers: {},
      unratedBeers: {},
      loading: false,
      selectedTab: 'all',
      totalProducts: 0,
      totalUnratedProducts: 0,
      showSearchBar: false,
      lastRating: null,
      searchTerm: '',
    };
    this.renderBeer = this.renderBeer.bind(this);
    this.endReached = this.endReached.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
    this.tabPressed = this.tabPressed.bind(this);
    this.searchTermChanged = this.searchTermChanged.bind(this);
    this.maxWidth = Dimensions.get('window').width;
    this.animatedValue = new Animated.Value(0);
    this.animate = this.animate.bind(this);
  }

  componentWillMount() {
    this.props.loadBeers(0);
    this.props.loadUnratedBeers(0);
    this.props.addMetric(`appv4.${Platform.OS}.mybeer.loaded`, 1);
  }

  componentWillReceiveProps(nextProps) {
    const nextState = {
      beers: nextProps.beers,
      unratedBeers: nextProps.unratedBeers,
      loading: false,
      totalProducts: nextProps.totalProducts,
      totalUnratedProducts: nextProps.totalUnratedProducts,
    };
    if (
      nextProps.lastRating !== this.state.lastRating &&
      nextProps.activeScene !== 'TastingNotes'
    ) {
    // hack to get around flatlist not properly rerendering when rating done from tastingnotes page
      nextState.lastRating = nextProps.lastRating;
    }
    this.setState(nextState);
  }

  getCurrentTabData() {
    return this.state.selectedTab === 'all'
      ? this.state.beers
      : this.state.unratedBeers;
  }

  getTabs() {
    const movingWidth = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [this.maxWidth, 0],
    });
    const contents = [
      { name: 'all', text: `ALL (${this.state.totalProducts || 0})` },
      {
        name: 'unrated',
        text: `UNRATED (${this.state.totalUnratedProducts || 0})`,
      },
    ];
    const { animate, search: element } = this;
    const onPress = () => {
      this.props.addMetric(`appv4.${Platform.OS}.mybeer.search`, 1);
      animate(1, () => element.focus());
    };

    const disabled = !this.state.beers.beers && !this.state.unratedBeers.beers;
    const iconProps = {
      style: styles.icon,
      color: disabled ? styles.disabledGray : 'black',
      name: 'search',
      size: 20,
      onPress: disabled ? null : onPress,
    };

    const extraContent = React.createElement(FontAwesomeIcons, iconProps);
    const tabsProps = {
      disabled,
      selected: this.state.selectedTab,
      contents,
      extraContent,
      onSelect: this.tabPressed,
    };

    const tabs = this.state.showSearchBar
      ? null
      : React.createElement(Tabs, tabsProps);
    return (
      <Animated.View
        style={[{ width: movingWidth }, styles.tabs]}
      >
        {tabs}
      </Animated.View>
    );
  }

  getData() {
    return this.getCurrentTabData().beers;
  }

  getSearchBar() {
    const movingWidth = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [0, this.maxWidth],
    });

    const { animate } = this;
    const onPress = () => {
      animate(0);
    };

    return (
      <Animated.View
        style={[styles.searchBarContainer, { width: movingWidth }]}
      >
        <TextInput
          ref={(c) => { this.search = c; }}
          autoCorrect={false}
          style={styles.searchBar}
          onChangeText={this.searchTermChanged}
          placeholder="Search All Your Beer"
        />
        <TouchableOpacity style={styles.searchIconContainer} onPress={onPress}>
          <Image style={{ height: 15, width: 15 }} source={icons.close} />
        </TouchableOpacity>
      </Animated.View>
    );
  }

  searchTermChanged(term) {
    this.setState({
      searchTerm: term,
    });
  }

  animate(val, cb) {
    Animated.timing(this.animatedValue, {
      toValue: val,
      duration: 250,
      easing: Easing.linear,
    }).start(cb);

    this.setState(currentState => ({ showSearchBar: !currentState.showSearchBar }));
  }

  tabPressed(tab) {
    this.setState({
      selectedTab: tab,
    });
  }

  endReached() {
    const tabData = this.getCurrentTabData();
    const loadMore =
      this.state.selectedTab === 'all'
        ? this.props.loadBeers
        : this.props.loadUnratedBeers;
    if (
      tabData.canLoadMore &&
      !this.state.loading &&
      tabData.beers.length !== 0
    ) {
      loadMore(tabData.page + 1);
      this.setState({ loading: true });
    }
  }

  shouldRenderSearchContent() {
    return this.state.searchTerm.length >= 3 && this.state.showSearchBar;
  }

  renderFooter() {
    const tabData = this.getCurrentTabData();
    if (tabData.beers && !tabData.canLoadMore) {
      return null;
    }
    return (
      <View style={{ flex: 0 }}>
        <BeerPreview style={styles.placeHolder} isPlaceholder />
        <BeerPreview style={styles.placeHolder} isPlaceholder />
        <BeerPreview style={styles.placeHolder} isPlaceholder />
      </View>
    );
  }

  renderBeer(row) {
    const beer = row.item;
    const ratingFunc = (rating) => {
      beer.rating.score = rating;
      this.props.rateBeer(beer, rating);
    };
    return <BeerPreview beer={beer} selectedStar={ratingFunc} />;
  }

  renderContent() {
    if (this.shouldRenderSearchContent()) {
      return <SearchBeers searchTerm={this.state.searchTerm} />;
    }
    if (this.state.lastRating !== this.props.lastRating) {
      this.setState({
        lastRating: this.props.lastRating,
      });
      // hack to get around router and flatlist interaction.
      // flatlist will 'rerender' but not action change content on screen without this
      return null;
    }
    return (
      <FlatList
        data={this.getData()}
        style={styles.slider}
        ItemSeparatorComponent={listSeparator}
        renderItem={this.renderBeer}
        keyExtractor={item => item.id}
        onEndReached={this.endReached}
        onEndReachedThreshold={0.75}
        ListFooterComponent={this.renderFooter}
        extraData={this.state.loading}
      />
    );
  }

  render() {
    const color = this.state.showSearchBar
      ? styles.backgroundWhite
      : styles.backgroundGray;
    return (
      <View style={[styles.container, color]}>
        <View style={[styles.contextContainer, color]}>
          {this.getTabs()}
          {this.getSearchBar()}
        </View>
        {this.renderContent()}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    lastRating: state.ratings.lastRating,
    activeScene: state.routes.scene && state.routes.scene.sceneKey,
    beers: state.products.products,
    unratedBeers: state.products.unratedProducts,
    totalProducts: state.products.products.total,
    totalUnratedProducts: state.products.unratedProducts.total,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    rateBeer: (beer, score) => {
      dispatch(ratingActions.rateBeer(beer, score));
    },
    loadBeers: (page) => {
      dispatch(productActions.getProducts(page));
    },
    loadUnratedBeers: (page) => {
      dispatch(productActions.getUnratedProducts(page));
    },
    addMetric(...args) {
      dispatch(metricActions.addMetric(...args));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(myBeers);
