import { colors, appStyles } from 'styles';
import { Platform } from 'react-native';

const borderColor = '#d2d2d2';

const styles = {
  searchResultsContainer: {
    flex: 1,
    width: '100%',
    backgroundColor: 'white',
  },
  searchResultsTab: {
    paddingTop: Platform.OS === 'ios' ? 0 : 15,
    paddingBottom: Platform.OS === 'ios' ? 0 : 15,
    backgroundColor: 'white',
    borderBottomWidth: 1,
    borderColor,
  },
  listSeparator: {
    width: '100%',
    height: 15,
  },
  slider: {
    flex: 0.9,
    backgroundColor: colors.backgroundColor,
  },
  bigText: {
    fontFamily: appStyles.fontFamily,
    fontSize: 18,
    textAlign: 'center',
    marginTop: 40,
  },
  littleText: {
    fontFamily: appStyles.fontFamily,
    fontSize: 14,
    textAlign: 'center',
    marginTop: 20,
    width: '80%',
  },
  textContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    borderTopWidth: 1,
    borderColor,
  },
  beerPreview: {
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor,
  },
  placeHolder: {
    marginTop: 15,
  },
};

export default styles;
