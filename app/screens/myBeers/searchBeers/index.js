import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
} from 'react-native';
import { connect } from 'react-redux';
import { actions as productActions, keys } from 'appRedux/products';
import { actions as ratingActions } from 'appRedux/ratings';
import styles from './styles';
import Tabs from '../tabs';
import BeerPreview from '../beerPreview';

const listSeparator = () => <View style={styles.listSeparator} />;

class searchBeers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTab: keys.name,
      nameMatches: {},
      breweryMatches: {},
      styleMatches: {},
      loadingMore: false,
      term: props.searchTerm,
    };
    this.renderBeer = this.renderBeer.bind(this);
    this.endReached = this.endReached.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
    this.tabPressed = this.tabPressed.bind(this);
    this.getCurrentTabData = this.getCurrentTabData.bind(this);
  }

  componentWillMount() {
    this.searchAll();
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.searchTerm &&
      nextProps.searchTerm.length >= 3 &&
      nextProps.searchTerm !== this.state.term
    ) {
      this.setState({
        nameMatches: {},
        breweryMatches: {},
        styleMatches: {},
        term: nextProps.searchTerm,
      });
      this.searchAll();
    } else {
      this.setState({
        loadingMore: false,
        nameMatches: nextProps.nameMatches,
        breweryMatches: nextProps.breweryMatches,
        styleMatches: nextProps.styleMatches,
      });
    }
  }

  getTabs() {
    const contents = [
      {
        name: keys.name,
        text: `NAME (${this.state.nameMatches.totalMatches || 0})`,
      },
      {
        name: keys.brewery,
        text: `BREWERY (${this.state.breweryMatches.totalMatches || 0})`,
      },
      {
        name: keys.style,
        text: `STYLE (${this.state.styleMatches.totalMatches || 0})`,
      },
    ];
    const disabled = this.isLoading();
    return (
      <Tabs
        style={styles.searchResultsTab}
        disabled={disabled}
        selected={this.state.selectedTab}
        contents={contents}
        onSelect={this.tabPressed}
      />
    );
  }

  getContent() {
    if (this.noResults()) {
      const text = `We can't find any of your beers that match that search query. Try
            searching by the beer name, brewery or style`;
      return (
        <View style={styles.textContainer}>
          <Text style={styles.bigText}>Gah! No Results!</Text>
          <Text style={styles.littleText}>
            {text}
          </Text>
        </View>
      );
    }
    return (
      <FlatList
        data={this.getCurrentTabData().beers}
        style={styles.slider}
        ItemSeparatorComponent={listSeparator}
        renderItem={this.renderBeer}
        keyExtractor={item => item.id}
        onEndReached={this.endReached}
        onEndReachedThreshold={0.75}
        ListFooterComponent={this.renderFooter}
        extraData={this.state.loadingMore}
      />
    );
  }

  getCurrentTabData() {
    switch (this.state.selectedTab) {
      case keys.name:
        return this.state.nameMatches;
      case keys.style:
        return this.state.styleMatches;
      case keys.brewery:
        return this.state.breweryMatches;
      default:
        return this.state.nameMatches;
    }
  }

  noResults() {
    return (
      this.state.nameMatches.beers &&
      this.state.nameMatches.beers.length === 0 &&
      this.state.breweryMatches.beers &&
      this.state.breweryMatches.beers.length === 0 &&
      this.state.styleMatches.beers &&
      this.state.styleMatches.beers.length === 0
    );
  }

  searchAll() {
    this.props.searchBeers(0, this.state.term, keys.name);
    this.props.searchBeers(0, this.state.term, keys.brewery);
    this.props.searchBeers(0, this.state.term, keys.style);
  }

  isLoading() {
    return (
      !this.state.nameMatches.beers &&
      !this.state.breweryMatches.beers &&
      !this.state.styleMatches.beers
    );
  }

  tabPressed(tab) {
    this.setState({
      selectedTab: tab,
    });
  }

  endReached() {
    const data = this.getCurrentTabData();
    if (
      data.canLoadMore &&
      !this.state.loadingMore &&
      !data.beers.length === 0
    ) {
      this.props.searchBeers(
        data.page + 1,
        this.state.term,
        this.state.selectedTab
      );
      this.setState({ loadingMore: true });
    }
  }

  renderFooter() {
    const data = this.getCurrentTabData();
    if ((!this.isLoading() && !data.canLoadMore) || data.beers.length === 0) {
      return null;
    }
    return <BeerPreview style={styles.placeHolder} isPlaceholder />;
  }

  renderBeer(row) {
    const beer = row.item;
    const ratingFunc = (rating) => {
      this.props.rateBeer(beer, rating);
    };
    return (
      <BeerPreview
        style={styles.beerPreview}
        beer={beer}
        selectedStar={ratingFunc}
      />
    );
  }

  render() {
    if (this.isLoading()) {
      return null;
    }
    return (
      <View style={styles.searchResultsContainer}>
        {this.getTabs()}
        {this.getContent()}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    nameMatches: state.products.nameMatches,
    breweryMatches: state.products.breweryMatches,
    styleMatches: state.products.styleMatches,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    rateBeer: (beer, score) => {
      dispatch(ratingActions.rateBeer(beer, score));
    },
    searchBeers: (page, term, key) => {
      dispatch(productActions.searchProducts(page, term, key));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(searchBeers);
