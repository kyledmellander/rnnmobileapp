import { colors, appStyles } from 'styles';
import { Platform } from 'react-native';

const styles = {
  container: {
    flex: 1,
    paddingTop: appStyles.marginTop,
    width: '100%',
    backgroundColor: colors.backgroundGray,
  },
  listSeparator: {
    width: '100%',
    height: 15,
    backgroundColor: 'transparent',
  },
  slider: {
    flex: 0.9,
    backgroundColor: colors.backgroundGray,
  },
  contextContainer: {
    flex: 0,
    width: '100%',
    flexDirection: 'row',
  },
  searchBarContainer: {
    width: 0,
    height: 70,
    backgroundColor: 'white',
    flexDirection: 'row',
    alignSelf: 'flex-end',
    paddingRight: 25,
  },
  searchBar: {
    flex: 0.95,
    fontFamily: appStyles.fontFamily,
    backgroundColor: 'transparent',
    paddingLeft: 20,
  },
  searchIconContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 0.5,
    maxWidth: 30,
    right: 0,
  },
  searchIcon: {
    height: 30,
    width: 30,
  },
  icon: {
    position: 'absolute',
    right: 0,
    justifyContent: 'center',
    textAlign: 'center',
    paddingTop: 15,
    height: 75,
    width: 75,
  },
  backgroundGray: {
    backgroundColor: colors.backgroundGray,
  },
  backgroundWhite: {
    backgroundColor: 'white',
  },
  placeHolder: {
    marginTop: 15,
  },
  tabs: {
    paddingBottom: Platform.OS === 'ios' ? 50 : 0,
  },
  disabledGray: colors.disabledTextGray,
};

export default styles;
