import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  Platform,
  Animated,
  Easing,
  ActivityIndicator,
  Linking,
  SafeAreaView,
} from 'react-native';
import { connect } from 'react-redux';
import Accordion from 'components/Accordion';
import OrderProductView, { orderProductBundleView } from 'components/OrderProductView';
import { Icons } from 'components';
import { actions as basketActions } from 'appRedux/baskets';
import utils from 'lib/utils';
import * as navUtils from 'lib/navUtils';
import loadingLib from 'lib/loading';
import styles from './styles';

function getOrderProductsViews(basket) {
  const orderProducts = basket.orders.reduce((acc, o) => {
    o.orderProducts.forEach((op) => {
      op.offer = o.offer;
    });
    return acc.concat(o.orderProducts);
  }, []);

  let orderProductViews = orderProducts.map((op) => {
    const onPress = () => {
      navUtils.pushScreen('TastingNotes', {
        passProps: {
          offer: op.offer.body,
          product: op.offerProduct.product,
        }
      });
    };
    if (orderProducts.indexOf(op) !== 0) {
      return OrderProductView({
        orderProduct: op,
        first: true,
        style: styles.orderProduct,
        onPress,
      });
    }
    return OrderProductView({
      orderProduct: op,
      first: false,
      style: styles.orderProduct,
      onPress,
    });
  });
  if (orderProductViews.length === 0) {
    orderProductViews = [
      React.createElement(ActivityIndicator, { style: styles.orderProduct }),
    ];
  }
  return orderProductViews;
}

function getOrderProductBundleView(basket) {
  const bundle = basket.orders[0].orderProductBundle;
  const props = {
    style: styles.orderProduct,
    orderProduct: {
      name: bundle.name,
      id: bundle.id,
      unitPrice: bundle.price,
      unitQuantity: 1,
    },
  };
  const productBundleView = orderProductBundleView(props);
  return [productBundleView];
}

function getOrders(basket) {
  if (basket == null) {
    return null;
  }
  const orderProductViews =
    basket.orders.length > 0 && basket.orders[0].orderProductBundle
      ? getOrderProductBundleView(basket)
      : getOrderProductsViews(basket);
  const trackingNumbers = basket.trackingNumbers.map(t => React.createElement(Text, {
    style: styles.trackingNumberText,
    onPress: () =>
      Linking.openURL(`https://tavour.com/tracking_numbers/${t.id}`),
  }, t.number, ' '));
  let trackingText = null;
  if (trackingNumbers.length > 0) {
    trackingText = React.createElement(Text, {
      style: styles.trackingNumberStatusText,
    }, 'Track your Crate: ', trackingNumbers);
  }
  const trackingNumberView = React.createElement(View, {
    style: styles.trackingNumberContainer,
  }, trackingText);
  return React.createElement(
    View,
    { style: styles.orderContainer },
    trackingNumberView,
    orderProductViews
  );
}

function formatDate(date) {
  let month;
  const day = date.getDate();
  const year = date.getFullYear();
  if (Platform.OS === 'ios') {
    month = date.toLocaleString('en-us', { month: 'short' });
  } else {
    const monthNames = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sept',
      'Oct',
      'Nov',
      'Dec',
    ];
    month = monthNames[date.getMonth()];
  }
  return `${month} ${day}, ${year}`;
}

function getTitleContent(basket) {
  return (
    <View style={styles.textContainer}>
      <Text style={styles.text}>{formatDate(basket.shippedAt)}</Text>
      <Text style={styles.text}>{basket.state}</Text>
    </View>
  );
}

const animatedValue = new Animated.Value(0);

class ShippedCrates extends Component {
  static options() {
    return {
      topBar: {
        buttonColor: 'black',
        title: {
          component: {
            name: 'NavBarTitle',
            passProps: {
              title: `Shipped ${utils.getBasketWord()}`,
            },
            alignment: 'center',
          },
        },
        leftButtons: [
          {
            id: 'hamburger',
            icon: Icons.hamburger,
            color: 'black',
          }
        ]
      }
    };
  }
  constructor(props) {
    super(props);
    this.state = {
      closedBaskets: [],
      canLoadMoreBaskets: true,
      loading: true,
      page: 1,
    };
    this.loadMoreBaskets = this.loadMoreBaskets.bind(this);
  }

  componentWillMount() {
    this.props.getClosedBaskets();
  }

  componentWillReceiveProps(nextProps) {
    const newState = {
      closedBaskets: nextProps.closedBaskets,
      loading: false,
      page: nextProps.page,
      canLoadMoreBaskets: nextProps.canLoadMoreBaskets,
    };
    this.setState(newState);
  }

  getCrates() {
    const { populateBasket } = this.props;
    const crateSummaries = this.props.closedBaskets.map((cb) => {
      const iconClicked = () => {
        if (cb.orders.length === 0) {
          populateBasket(cb);
        }
      };
      const titleContent = getTitleContent(cb);
      const props = {
        titleContent,
        titleContainerStyle: styles.titleContainer,
        collapsedStyle: styles.collapsedStyle,
        expandedStyle: styles.expandedStyle,
        content: getOrders(cb),
        key: cb.id,
        iconClicked,
      };
      return React.createElement(Accordion, props);
    });
    return crateSummaries;
  }

  getLoadMoreText() {
    if (this.state.canLoadMoreBaskets && this.state.closedBaskets.length > 0) {
      return (
        <Text
          style={styles.loadMoreText}
          onPress={this.loadMoreBaskets}
          key="loadMore"
        >
          Previous Orders
        </Text>
      );
    }
    return null;
  }

  getContent() {
    if (this.props.closedBaskets.length === 0) {
      return React.createElement(Text, {
        style: [styles.cratesContainer, styles.noCratesText]
      }, `Once you've had some ${utils.getBasketWord(true)}s close, you'll be able to view them here`);
    }
    return React.createElement(ScrollView, {
      style: styles.cratesContainer,
      contentContainerStyle: styles.cratesContainer,
    }, this.getCrates());
  }

  loadMoreBaskets() {
    this.props.loadMoreBaskets(this.state.page + 1);
  }

  render() {
    if (this.state.loading) {
      return <ActivityIndicator />;
    }
    const increaseOpacity = loadingLib.increaseOpacity(animatedValue);
    Animated.timing(animatedValue, {
      toValue: 1,
      duration: 500,
      easing: Easing.linear,
    }).start();
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Animated.View style={[styles.container, increaseOpacity]}>
          <View style={styles.scrollContainer}>{this.getContent()}</View>
          {this.getLoadMoreText()}
        </Animated.View>
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state) {
  return {
    closedBaskets: state.baskets.closedBaskets,
    page: state.baskets.basketPage,
    canLoadMoreBaskets: state.baskets.canLoadMoreBaskets,
    userStateCode: state.user.userStateCode,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    populateBasket: (basket) => {
      dispatch(basketActions.populateBasket(basket));
    },
    getClosedBaskets: () => {
      dispatch(basketActions.getClosedBaskets());
    },
    loadMoreBaskets: (page) => {
      dispatch(basketActions.loadMoreBaskets(page));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ShippedCrates);
