import { colors, appStyles } from '../../styles';

const styles = {
  container: {
    flex: 1,
  },
  scrollContainer: {
    flex: 0,
    maxHeight: '90%',
  },
  cratesContainer: {
    flex: 0,
  },
  noCratesText: {
    fontFamily: appStyles.fontFamily,
    fontSize: 14,
    textAlign: 'center',
    color: colors.textGray,
    marginLeft: '7%',
    marginRight: '8%',
    width: '84%',
  },
  text: {
    fontFamily: appStyles.fontFamily,
    fontSize: 12,
    color: '#161616',
    flex: 1,
  },
  orderContainer: {
    width: '100%',
    minHeight: 0,
    marginTop: 20,
  },
  textContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  titleContainer: {
    height: 20,
  },
  titleText: {
    flex: 1,
  },
  collapsedStyle: {
    height: 45,
  },
  expandedStyle: {
    flex: 1,
  },
  orderProduct: {
    paddingHorizontal: 20,
    maxHeight: 70,
  },
  loadMoreText: {
    height: 20,
    color: colors.tavourOrange,
    fontFamily: appStyles.fontFamilySemiBold,
    marginTop: 30,
    marginLeft: 20,
  },
  trackingNumberStatusText: {
    fontSize: 12,
    fontFamily: appStyles.fontFamily,
  },
  trackingNumberText: {
    fontSize: 12,
    fontFamily: appStyles.fontFamily,
    color: colors.tavourOrange,
  },
  trackingNumberContainer: {
    flex: 0,
    paddingLeft: 4,
    flexDirection: 'row',
    alignSelf: 'center',
    width: '90%',
  },
};
export default styles;
