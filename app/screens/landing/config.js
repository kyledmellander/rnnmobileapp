import images from 'images';
import { Platform } from 'react-native';

const { landing } = images;

export const landingScreen1 = {
  source: landing.screen1,
  text: "Exceptional craft beers you can't get anywhere else",
  textStyle: {
    color: '#fafeff',
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 20,
    fontSize: 25,
    textAlign: 'center',
  },
  subText: 'Swipe to learn more',
  subTextStyle: {
    paddingBottom: 30,
    color: 'white',
    fontSize: 16,
    textAlign: 'center',
  },
  container: {
    flexDirection: 'column',
    flex: 1,
  },
};

const config = {
  contents: [
    {
      source: landing.screen2,
      text:
        "Every day get access to 2 rare craft beers you can't get in your area",
      testID: 'landing2',
      textStyle: {
        paddingLeft: 27,
        paddingRight: 27,
        fontSize: 20,
      },
      container: {
        justifyContent: 'flex-start',
        marginTop: Platform.select({
          ios: 30,
          android: 10,
        }),
      },
      darkenImage: false,
    },
    {
      source: landing.screen3,
      testID: 'landing3',
      text: "Claim the beers you want before they're gone",
      textStyle: {
        paddingLeft: 50,
        paddingRight: 50,
        fontSize: 20,
      },
      container: {
        justifyContent: 'flex-start',
        marginTop: Platform.select({
          ios: 15,
          android: 0,
        }),
      },
      imageStyle: {
        marginTop: Platform.select({
          ios: 0,
          android: 0,
        }),
        overflow: 'visible',
      },
      darkenImage: false,
    },
    {
      source: landing.screen4,
      testID: 'landing4',
      text: 'Tavour ships your hand-picked beer straight to your door',
      textStyle: {
        alignSelf: 'center',
        right: 15,
        color: 'white',
        paddingLeft: 50,
        paddingRight: 50,
        fontSize: 20,
      },
      container: {
        justifyContent: 'flex-start',
        marginTop: Platform.select({
          ios: 40,
          android: 0,
        }),
      },
      imageStyle: {
        height: '109%',
        width: '109%',
      },
      darkenImage: true,
    },
  ],
};

export default config;
