import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Swiper } from 'components';
import { pushScreen } from 'lib/navUtils';
import LandingImage from './landingImage';
import FirstLandingImage from './firstLandingImage';
import config from './config';
import styles from './styles';

function login() {
  pushScreen('Login');
}

function signUp() {
  pushScreen('ConfirmAge');
}

class landing extends Component {
  static options() {
    return {
      topBar: {
        visible: false,
        drawBehind: true,
        animate: false,
      }
    };
  }

  render() {
    const { contents } = config;
    const firstLandingImage = FirstLandingImage();
    const remainingLandingImages = contents.map(LandingImage);
    const landingImages = [firstLandingImage].concat(remainingLandingImages);

    return (
      <View style={styles.container} testID="landingContainer">
        <Swiper
          style={styles.slider}
          paginationStyle={{ bottom: 15 }}
          activeDotColor="#fff"
          loop={false}
        >
          {landingImages}
        </Swiper>

        <View style={styles.buttons}>
          <TouchableOpacity style={styles.signUp} onPress={signUp} testID="SignUpButton">
            <Text style={[styles.text, styles.signUpText]}>Sign Up</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.login} onPress={login} testID="LoginButton">
            <Text style={[styles.text, styles.loginText]}>Log In</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default landing;
