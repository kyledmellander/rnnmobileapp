import React from 'react';
import Logo from 'svgs/landing/TavourOrangeGraySvg';
import { View, Text, ImageBackground } from 'react-native';
import landingImageStyles from '../landingImage/styles';
import styles from './styles';
import { landingScreen1 } from '../config';
import DarkenImageOverlay from '../landingImage/darkenImageOverlay';

const LayoutContainer = props => (
  <View style={landingScreen1.container}>
    <View style={styles.layoutContainerTop} />
    <View style={styles.layoutContainerBottom}>{props.children}</View>
  </View>
);

const SliderText = () => (
  <Text style={landingScreen1.subTextStyle}>{landingScreen1.subText}</Text>
);

const LogoAndText = () => (
  <View style={{ flexDirection: 'column', alignItems: 'center' }}>
    <Logo {...styles.logoProps} />
    <Text style={[landingImageStyles.text, landingScreen1.textStyle]}>
      {landingScreen1.text}
    </Text>
  </View>
);

const FirstLandingImage = () => (
  <ImageBackground
    style={landingImageStyles.backgroundImage}
    source={landingScreen1.source}
    resizeMode="cover"
    testID="FirstLandingImage"
  >
    <DarkenImageOverlay />
    <LayoutContainer>
      <LogoAndText />
      <SliderText />
    </LayoutContainer>
  </ImageBackground>
);

export default FirstLandingImage;
