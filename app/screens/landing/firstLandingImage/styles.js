const layoutContainerTopHeight = 0.55;
const layoutContainerBottomHeight = 1 - layoutContainerTopHeight;

const styles = {
  layoutContainerTop: {
    flex: layoutContainerTopHeight,
  },
  layoutContainerBottom: {
    flex: layoutContainerBottomHeight,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  logoProps: {
    width: 270,
    height: 50,
  },
};

export default styles;
