import React from 'react';
import { View } from 'react-native';
import styles from './styles';

// to be placed as a child of ImageBackground
const DarkenImageOverlay = () => <View style={styles.darkenImageOverlayStyle} />;

export default DarkenImageOverlay;

