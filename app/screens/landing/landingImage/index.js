import React from 'react';
import { View, Text, ImageBackground } from 'react-native';
import styles from './styles';
import DarkenImageOveraly from './darkenImageOverlay';

const LandingImage = props => (
  <ImageBackground
    style={[styles.backgroundImage, props.imageStyle]}
    source={props.source}
    resizeMode="cover"
    testID={props.testID}
  >
    {props.darkenImage ? <DarkenImageOveraly /> : null}
    <View style={[styles.container, props.container]}>
      <Text style={[styles.text, props.textStyle]}>{props.text}</Text>
    </View>
  </ImageBackground>
);

export default LandingImage;
