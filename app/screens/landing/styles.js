import { colors, appStyles } from 'styles';
import { Platform } from 'react-native';

const styles = {
  container: {
    flex: 1,
  },
  slider: {
    flex: Platform.select({
      ios: 0,
      android: 1,
    }),
    paddingBottom: 0,
  },
  scrollStyle: {
    flex: 1,
    alignItems: 'center',
  },
  buttons: {
    flex: 0.1,
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  signUp: {
    flex: 1,
    backgroundColor: colors.tavourOrange,
    justifyContent: 'center',
    alignItems: 'center',
  },
  signUpText: {
    color: 'white',
  },
  login: {
    flex: 1,
    backgroundColor: colors.tavourLightGray,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loginText: {
    color: 'black',
  },
  text: {
    fontSize: 16,
    fontFamily: appStyles.fontFamily,
  },
};

export default styles;
